﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Microsoft.SharePoint.Client;

namespace UNSW.SharePointOnline.ApplyBrandingWeb
{
    public partial class Default : System.Web.UI.Page
    {
        protected void Page_PreInit(object sender, EventArgs e)
        {
            Uri redirectUrl;
            switch (SharePointContextProvider.CheckRedirectionStatus(Context, out redirectUrl))
            {
                case RedirectionStatus.Ok:
                    return;
                case RedirectionStatus.ShouldRedirect:
                    Response.Redirect(redirectUrl.AbsoluteUri, endResponse: true);
                    break;
                case RedirectionStatus.CanNotRedirect:
                    Response.Write("An error occurred while processing your request.");
                    Response.End();
                    break;
            }
        }

        protected void Page_Load(object sender, EventArgs e)
        {
            // The following code gets the client context and Title property by using TokenHelper.
            // To access other properties, the app may need to request permissions on the host web.
            if (!Page.IsPostBack)
            {
                try
                {
                    string contextTokenString = TokenHelper.GetContextTokenFromRequest(Request);
                    hidToken.Value = contextTokenString;
                    Uri siteUrl = new Uri("https://unswtest.sharepoint.com/sites/jaiminiaas");
                    Uri hostSiteUrl = new Uri(Page.Request["SPHostUrl"].ToString());

                    if (!string.IsNullOrEmpty(hidToken.Value))
                    {
                        SharePointContextToken contextToken = TokenHelper.ReadAndValidateContextToken(hidToken.Value, Request.Url.Authority);
                        string accessToken = TokenHelper.GetAppOnlyAccessToken(contextToken.TargetPrincipalName, siteUrl.Authority, contextToken.Realm).AccessToken;
                        //string accessToken = TokenHelper.GetAppOnlyAccessToken(contextToken.TargetPrincipalName, siteUrl.Authority, TokenHelper.GetRealmFromTargetUrl(siteUrl)).AccessToken;
                        using (ClientContext context = TokenHelper.GetClientContextWithAccessToken(siteUrl.OriginalString, accessToken))
                        {
                            Web web = context.Web;
                            context.Load(web, x => x.Title);
                            context.ExecuteQuery();
                            Response.Write("title is: " + web.Title);

                        }
                    }

                    /*var spContext = SharePointContextProvider.Current.GetSharePointContext(Context);

                    using (var clientContext = spContext.CreateUserClientContextForSPHost())
                    {
                        clientContext.Load(clientContext.Web, web => web.MasterUrl, web => web.CustomMasterUrl);
                        clientContext.ExecuteQuery();
                        Response.Write(clientContext.Web.MasterUrl + " " + clientContext.Web.CustomMasterUrl);
                    }*/
                }
                catch(Exception ex)
                {
                    Response.Write(ex.Message);
                }
            }
        }

        protected void btnApply_Click(object sender, EventArgs e)
        {
            try
            {
                Uri siteUrl = new Uri(txtSiteUrl.Text);
                if (!string.IsNullOrEmpty(hidToken.Value))
                {
                    SharePointContextToken contextToken = TokenHelper.ReadAndValidateContextToken(hidToken.Value, Request.Url.Authority);
                    //string accessToken = TokenHelper.GetAppOnlyAccessToken(contextToken.TargetPrincipalName, siteUrl.Authority, contextToken.Realm).AccessToken;
                    string accessToken = TokenHelper.GetAppOnlyAccessToken(contextToken.TargetPrincipalName, siteUrl.Authority, TokenHelper.GetRealmFromTargetUrl(siteUrl)).AccessToken;
                    using (ClientContext context = TokenHelper.GetClientContextWithAccessToken(siteUrl.OriginalString, accessToken))
                    {
                        Web web = context.Web;
                        context.Load(web, x => x.Title);
                        context.ExecuteQuery();
                        Response.Write("title is: " + web.Title);

                    }
                }

                
            }
            catch (Exception ex)
            {
                Response.Write(ex.Message);
            }

        }

        protected void btnRemove_Click(object sender, EventArgs e)
        {

        }
    }
}