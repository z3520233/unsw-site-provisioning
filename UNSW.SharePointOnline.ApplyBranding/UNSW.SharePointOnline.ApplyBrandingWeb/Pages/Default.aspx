﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="Default.aspx.cs" Inherits="UNSW.SharePointOnline.ApplyBrandingWeb.Default" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
    <style type="text/css" >
        .buttonSubmit
        {
            float:right;
        }
    </style>
</head>
<body>
    <form id="form1" runat="server" >
        
        <div align="center" style="padding:10px">
            <label id="lblHeading" style="font-size:25px">UNSW Apply/Remove Branding</label>
        </div>
        <div align="center" style="padding:10px">
            <label id="lblSiteUrl" style="vertical-align:top; font-size:16px">Enter Site Url </label>
            <asp:TextBox ID="txtSiteUrl" runat="server" Width="300px" />
        </div>
        <div align="center" style="width:100%; padding:10px; ">
            <asp:Button ID="btnApply" runat="server" Text="Apply Branding" OnClick="btnApply_Click" Width="110px" />
            <asp:Literal Text="&nbsp;" runat="server" />
            <asp:Button ID="btnRemove" runat="server" Text="Remove Branding" OnClick="btnRemove_Click" Width="110px"/>
        </div>
        <asp:HiddenField ID="hidToken" runat="server" />

    </form>
</body>
</html>
