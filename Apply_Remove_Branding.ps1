﻿Import-Module Microsoft.Online.SharePoint.PowerShell -DisableNameChecking

#Add references to SharePoint client assemblies and authenticate to Office 365 site
Add-Type -Path "C:\Program Files\Common Files\Microsoft Shared\Web Server Extensions\16\ISAPI\Microsoft.SharePoint.Client.dll"
Add-Type -Path "C:\Program Files\Common Files\Microsoft Shared\Web Server Extensions\16\ISAPI\Microsoft.SharePoint.Client.Runtime.dll"

#Take inputs from user
$Username = Read-Host -Prompt "Please enter your username "
$Password = Read-Host -Prompt "Please enter your password " -AsSecureString
$SiteUrl  = Read-Host -Prompt "Please enter site collection url "
$Branding = Read-Host -Prompt "Press 1 to apply branding or 2 to remove branding "

#Get the context
$Context = New-Object Microsoft.SharePoint.Client.ClientContext($SiteUrl)

#Pass the credentials 
$Creds = New-Object Microsoft.SharePoint.Client.SharePointOnlineCredentials($Username,$Password)
$Context.Credentials = $Creds


if($Branding -eq '1') 
{
write-host "Applying branding on site:" $SiteUrl -ForegroundColor Cyan
$rootWeb = $Context.Web
$Context.Load($rootWeb)
$Context.ExecuteQuery()

#Load the site property bag
$allProps=$rootWeb.AllProperties
$Context.Load($allProps)
$Context.ExecuteQuery()

#Add custom master url to property bag
$rootWeb.AllProperties['SiteMasterUrl']=$rootWeb.CustomMasterUrl
$rootWeb.Update()
$Context.ExecuteQuery()

#Change the site master page to seattle.master
$rootWeb.CustomMasterUrl = $Context.Web.ServerRelativeUrl + '/_catalogs/masterpage/seattle.master'
$rootWeb.Update()
$Context.Load($rootWeb)
$Context.ExecuteQuery()
write-host 'Master page has been changed to Seattle.master' -ForegroundColor Green


#Load User Custom Actions collection
$caColl = $Context.Site.UserCustomActions
$Context.Load($caColl)
$Context.ExecuteQuery()

write-host 'Adding references to css files ' -ForegroundColor Cyan

#Remove existing reference for UNSWIntranetCore.css
$DelCA1 = DeleteCustomAction -Context $Context -fileName 'UNSWIntranetCore.css' -caColl $caColl

#Add UNSWIntranetCore.css reference
$cssUrl = "document.write('<link rel =""stylesheet"" href =""" + $rootWeb.Url + "/Style Library/Branding/CSS/UNSWIntranetCore.css"" />');"
$AddCA1 = CreateCustomAction -Context $Context -fileRef $cssUrl -seq 100 -name 'UNSWCssLink' -script 'block'


#Remove existing reference for faculty.css
$DelCA2 = DeleteCustomAction -Context $Context -fileName 'faculty.css' -caColl $caColl

#Add faculty.css reference
$facultycssUrl = "document.write('<link rel =""stylesheet"" href =""" + $rootWeb.Url + "/Style Library/Branding/CSS/faculty.css"" />');"
$AddCA2 = CreateCustomAction -Context $Context -fileRef $facultycssUrl -seq 101 -name 'facultyCssLink' -script 'block'


#Remove existing reference for jquery-ui.css
$DelCA3 = DeleteCustomAction -Context $Context -fileName 'jquery-ui.css' -caColl $caColl

#Add jquery-ui.css reference
$jqueryCSS = "document.write('<link rel =""stylesheet"" href =""" + $rootWeb.Url + "/Style Library/Branding/CSS/jquery-ui.css"" />');"
$AddCA3 = CreateCustomAction -Context $Context -fileRef $jqueryCSS -seq 102 -name 'JqueryUICssLink' -script 'block'


#Remove existing reference for font-awesome.min.css
$DelCA4 = DeleteCustomAction -Context $Context -fileName 'font-awesome.min.css' -caColl $caColl

#Add font-awesome.min.css reference
$fontAwesomeCSS = "document.write('<link rel =""stylesheet"" href =""" + $rootWeb.Url + "/Style Library/Branding/CSS/font-awesome.min.css"" />');"
$AddCA4 = CreateCustomAction -Context $Context -fileRef $fontAwesomeCSS -seq 103 -name 'fontAwesomeCSS' -script 'block'

write-host 'Adding references to js files ' -ForegroundColor Cyan

#Remove existing reference for jquery-2.2.1.min.js
$DelCA5 = DeleteCustomAction -Context $Context -fileName 'jquery-2.2.1.min.js' -caColl $caColl

#Add jquery-2.2.1.min.js reference
$jqueryUrl = '~SiteCollection/Style Library/Branding/Scripts/jquery-2.2.1.min.js'
$AddCA5 = CreateCustomAction -Context $Context -fileRef $jqueryUrl -seq 1000 -name 'jQueryMin' -script 'src'


#Remove existing reference for jquery-ui.min.js
$DelCA6 = DeleteCustomAction -Context $Context -fileName 'jquery-ui.min.js' -caColl $caColl

#Add jquery-ui.min.js reference
$jqueryUIUrl = '~SiteCollection/Style Library/Branding/Scripts/jquery-ui.min.js'
$AddCA6 = CreateCustomAction -Context $Context -fileRef $jqueryUIUrl -seq 1001 -name 'jQueryUIMin' -script 'src'


#Remove existing reference for jquery.ui.touch-punch.min.js
$DelCA7 = DeleteCustomAction -Context $Context -fileName 'jquery.ui.touch-punch.min.js' -caColl $caColl

#Add jquery.ui.touch-punch.min.js reference
$jqueryUITouchUrl = '~SiteCollection/Style Library/Branding/Scripts/jquery.ui.touch-punch.min.js'
$AddCA7 = CreateCustomAction -Context $Context -fileRef $jqueryUITouchUrl -seq 1002 -name 'jQueryUITouchMin' -script 'src'


#Remove existing reference for UNSWIntranetCore.js
$DelCA8 = DeleteCustomAction -Context $Context -fileName 'UNSWIntranetCore.js' -caColl $caColl

#Add UNSWIntranetCore.js reference
$UNSWjsUrl = '~SiteCollection/Style Library/Branding/Scripts/UNSWIntranetCore.js'
$AddCA8 = CreateCustomAction -Context $Context -fileRef $UNSWjsUrl -seq 1200 -name 'UNSWjsUrl' -script 'src'


#Create FooterContent list
$AddList = CreateFooterList -Context $Context

#Upload Page layouts
$cTypeId = '0x01010007FF3E057FA8AB4AA42FCB67B453FFC100E214EEE741181F4E9F7ACC43278EE811'
$associatedcTypeId = '0x010100C568DB52D9D0A14D9B2FDCC96666E9F2007948130EC3DB064584E219954237AF3900242457EFB8B24247815D688C526CD44D0024CFBBCC9DD93D4483D43A8329BFD52B'
$UploadPL = UploadPageLayouts -Context $Context -contentTypeID $cTypeId -fileName 'UNSWIntranetHome.aspx' -web $rootWeb -associatedContentTypeId $associatedcTypeId

#Dispose the context object
$Context.Dispose()
Write-Host 'Branding applied to the site' -ForegroundColor Green
}
elseif($Branding -eq '2') 
{
write-host 'Removing branding from site:' $SiteUrl -ForegroundColor Cyan
$rootWeb = $Context.Web
$Context.Load($rootWeb)
$Context.ExecuteQuery()

#Load the site property bag
$allProps=$rootWeb.AllProperties
$Context.Load($allProps)
$Context.ExecuteQuery()

#Get property bag value
$customMasterUrl = $rootWeb.AllProperties['SiteMasterUrl']

#Change the site master page to previous master page
$rootWeb.CustomMasterUrl = $customMasterUrl
$rootWeb.Update()
$Context.Load($rootWeb)
$Context.ExecuteQuery()
write-host 'Master page has been changed to' $customMasterUrl.Split('/')[-1] -ForegroundColor Green

#Load User Custom Actions collection
$caColl = $Context.Site.UserCustomActions
$Context.Load($caColl)
$Context.ExecuteQuery()

Write-Host 'Removing css files reference' -ForegroundColor Cyan

#Remove existing reference for UNSWIntranetCore.css
$DelCA1 = DeleteCustomAction -Context $Context -fileName 'UNSWIntranetCore.css' -caColl $caColl
#Remove existing reference for faculty.css
$DelCA2 = DeleteCustomAction -Context $Context -fileName 'faculty.css' -caColl $caColl
#Remove existing reference for jquery-ui.css
$DelCA3 = DeleteCustomAction -Context $Context -fileName 'jquery-ui.css' -caColl $caColl
#Remove existing reference for font-awesome.min.css
$DelCA4 = DeleteCustomAction -Context $Context -fileName 'font-awesome.min.css' -caColl $caColl

Write-Host 'Removing js files reference' -ForegroundColor Cyan

#Remove existing reference for jquery-2.2.1.min.js
$DelCA5 = DeleteCustomAction -Context $Context -fileName 'jquery-2.2.1.min.js' -caColl $caColl
#Remove existing reference for jquery-ui.min.js
$DelCA6 = DeleteCustomAction -Context $Context -fileName 'jquery-ui.min.js' -caColl $caColl
#Remove existing reference for jquery.ui.touch-punch.min.js
$DelCA7 = DeleteCustomAction -Context $Context -fileName 'jquery.ui.touch-punch.min.js' -caColl $caColl
#Remove existing reference for UNSWIntranetCore.js
$DelCA8 = DeleteCustomAction -Context $Context -fileName 'UNSWIntranetCore.js' -caColl $caColl

#Dispose the context object
$Context.Dispose()
Write-Host 'Branding removed from the site' -ForegroundColor Green 
} 

Function CreateCustomAction([Microsoft.SharePoint.Client.ClientContext]$Context,[string]$fileRef,[int]$seq,[string]$name,[string]$script)
{
    $customAction = $Context.Site.UserCustomActions.Add()
    $customAction.Location = 'ScriptLink'
    if($script -eq 'src'){
        $customAction.ScriptSrc = $fileRef
    }
    if($script -eq 'block') {
        $customAction.ScriptBlock = $fileRef
    }
    $customAction.Sequence = $seq
    $customAction.Name = $name
    $customAction.Update()
    $Context.ExecuteQuery()
}

Function DeleteCustomAction([Microsoft.SharePoint.Client.ClientContext]$Context,[string]$fileName,[Microsoft.SharePoint.Client.UserCustomActionCollection]$caColl)
{
    for($i=$caColl.Count-1;$i -ge 0;$i--)
    {
        if($caColl[$i].ScriptBlock){
            if($caColl[$i].ScriptBlock.Contains($fileName))
            {
                $caColl[$i].DeleteObject()
            }
        }

        if($caColl[$i].ScriptSrc){
            if($caColl[$i].ScriptSrc.Contains($fileName))
            {
                $caColl[$i].DeleteObject()
            }
        }
    }
    $Context.ExecuteQuery()

}

Function CreateFooterList([Microsoft.SharePoint.Client.ClientContext]$Context)
{
    #Retrieve lists
    $Lists = $Context.Web.Lists
    $Context.Load($Lists)
    $Context.ExecuteQuery()

    #Check if list exists
    $FooterList = $Lists | where{$_.Title -eq 'FooterContent'}
    if($FooterList)
    {
        Write-Host 'FooterContent list already exists' -ForegroundColor Yellow
    }
    else
    {
        Write-Host 'Creating FooterContent list' -ForegroundColor Cyan
        #Create a list with custom list template
        $ListInfo = New-Object Microsoft.SharePoint.Client.ListCreationInformation
        $ListInfo.Title = 'FooterContent'
        $ListInfo.TemplateType = '100'
        $List = $Context.Web.Lists.Add($ListInfo)
        $List.Description = 'This list is used to create Footer Content'
        $List.Update()
        $Context.ExecuteQuery()

        #Create field
        $FooterContentList = $Context.Web.Lists | where{$_.Title -eq 'FooterContent'}
        $Context.Load($FooterContentList)
        $Context.ExecuteQuery()
        $FooterContentList.Fields.AddFieldAsXml("<Field Type='Note' NumLines='20' DisplayName='Content'/>",$true,[Microsoft.SharePoint.Client.AddFieldOptions]:: AddFieldToDefaultView)
        $FooterContentList.Update()
        $Context.ExecuteQuery()
    }
}

Function UploadPageLayouts([Microsoft.SharePoint.Client.ClientContext]$Context,[string]$contentTypeID,[string]$fileName,[Microsoft.SharePoint.Client.Web]$web,[string]$associatedContentTypeId)
{
    Write-Host 'Uploading page layout -' $fileName -ForegroundColor Cyan
    $associatedContentType = $web.AvailableContentTypes.GetById($associatedContentTypeId)
    $catalogList = $web.GetCatalog([Microsoft.SharePoint.Client.ListTemplateType]::MasterPageCatalog)    
    $Context.Load($catalogList.RootFolder)
    $Context.Load($associatedContentType)
    $Context.ExecuteQuery()

    $masterPageGallery = $web.Lists.GetByTitle('Master Page Gallery')
    $Context.Load($masterPageGallery)
    $Context.ExecuteQuery()
    $masterPageGallery.EnableFolderCreation = $true
    $masterPageGallery.Update()
    $Context.ExecuteQuery()
    
    $folder = $masterPageGallery.RootFolder.Folders.Add('TEST')
    $Context.Load($folder)
    $Context.ExecuteQuery()
    $folder = $folder.Folders.Add('PageLayouts')
    $Context.Load($folder)
    $Context.ExecuteQuery()
    
    $fileContent = [System.IO.File]::ReadAllBytes('D:\BrandingCode\Latest\UNSWIntranetHome.aspx')
    $fileInfo = New-Object Microsoft.SharePoint.Client.FileCreationInformation
    $fileInfo.Content = $fileContent
    $fileInfo.Url = $catalogList.RootFolder.ServerRelativeUrl + '/TEST/PageLayouts/' + $fileName
    $fileInfo.Overwrite = $true

    $file = $catalogList.RootFolder.Files.Add($fileInfo)
    $Context.Load($file)
    $Context.ExecuteQuery()

    $listItem = $file.ListItemAllFields
    $listItem['ContentTypeId'] = $contentTypeID
    $listItem['PublishingAssociatedContentType'] = [string]::Format(';#{0};#{1};#', $associatedContentType.Name, $associatedContentType.Id.StringValue)
    $listItem.Update()
    $Context.ExecuteQuery()
   
}