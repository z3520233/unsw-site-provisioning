﻿
$(document).ready(function () {
    var hostUrl = _spPageContextInfo.siteAbsoluteUrl;

    $("#s4-bodyContainer").append('<div id="footer-containter" class="ms-dialogHidden noindex" style="display: block;">' +
                    '<div id="breadcrumb-bottom"></div>' +
                    '<div id="footer">' +
                        '<div id="footer-inner">' +
						'UNSW Sydney NSW 2052, Australia, CRICOS Provider Code 00098G, ABN 57 195 873 179' +
                        '</div>' +
                        '<div id="back-to-top-container">' +
                            '<a class="back-to-top" href="#top">' +
                                '+<i class="fa fa-chevron-circle-up fa-3x"></i>' +
                            '</a></div> </div>     </div>');

    $("#startNavigation").after("<div class='title-container noindex'> " +
                                "<div id='quick-links-icon' class='title-icon'>" +
                                "<i class='fa fa-bookmark fa-lg'></i>" +
                                "</div>" +
                                "<div id='quick-links-title' class='title-name'>Quick Links</div>" +
                            "</div>");


    $("#DeltaPlaceHolderSearchArea").parent().wrap("<div id='findItBox' class='ms-tableCell ms-verticalAlignTop'></div>"); $("#findItBox").insertAfter($("#titleAreaBox"));

    $("#DeltaPlaceHolderGroupActionsArea").parent().remove();

    $(".ms-breadcrumb-dropdownBox").remove();

    var deltaTopNaviElement = $("#DeltaTopNavigation").detach();
    $(deltaTopNaviElement).insertAfter($("#s4-titlerow"));
    $("#DeltaTopNavigation").wrap('<div id="topNavBar" class="ms-dialogHidden"></div>').wrap('<div id="mainmenu" class="navigation-menu"></div>');

    var pageTtlElement = $("#pageTitle").detach();
    $(pageTtlElement).insertAfter($("#topNavBar"));
    $("#pageTitle").wrap('<div id="pageTitleWrapper" class="ms-dialogHidden"></div>').wrap('<div class="ms-core-pageTitle"></div>');
    $("h1.ms-core-pageTitle").removeAttr("id");
    $("div.ms-core-pageTitle").attr("id", "pageTitle");

    var faviconUrl = hostUrl+'/Style Library/UNSW/Images/favicon.ico';
    $('link[rel="shortcut icon"]').attr('href', faviconUrl);
   

    $.ajax({
        url: hostUrl+"/_api/web?$select=title,url",
        type: "GET",
        headers: { "accept": "application/json;odata=verbose" },
        async: false,
        success: function (data) {
            $("div.ms-breadcrumb-box").empty().html('<span><span><h1 id="SiteTitle"><a class="ms-sitemapdirectional" href="' + data.d.Url + '">' + data.d.Title + '</a></h1></span></span>');
        },
        error: function (xhr) {
            console.log(xhr.status + ': ' + xhr.statusText);

        }
    });
});