﻿using System;
using Microsoft.SharePoint.Client;
using Microsoft.SharePoint.Client.Publishing;
using Microsoft.Online.SharePoint.TenantAdministration;
using System.Configuration;
using Microsoft.ApplicationInsights.Extensibility;
using Microsoft.ApplicationInsights.DataContracts;
using Microsoft.SharePoint.Client.Utilities;
using System.Collections.Generic;

namespace UNSW.SharePointOnline.CreateSiteCollection
{
    class Program
    {
        static string realm = string.Empty;
        static string instrumentationKey;
        static Microsoft.ApplicationInsights.TelemetryClient eventClient;
        static Microsoft.ApplicationInsights.TelemetryClient exClient;
        static void Main(string[] args)
        {
            //Creating TelemetryClient object for event and exception logging
            instrumentationKey = ConfigurationManager.AppSettings["InstrumentationKey"].ToString();
            TelemetryConfiguration.Active.InstrumentationKey = instrumentationKey;
            eventClient = new Microsoft.ApplicationInsights.TelemetryClient();
            exClient = new Microsoft.ApplicationInsights.TelemetryClient();

            ExecuteSiteRequestType();

            eventClient.Flush();
            exClient.Flush();

        }
        /// <summary>
        /// Function to get config value from COnfigStore list using Config Key
        /// </summary>
        /// <param name="configKey"></param>
        /// <returns></returns>
        static string GetConfigValues(string configKey)
        {

            try
            {
                string configVal;
                Uri tenantSiteUrl = new Uri(ConfigurationManager.AppSettings["TenantURL"].ToString());
                Uri configSiteUrl = new Uri(ConfigurationManager.AppSettings["MainSiteURL"].ToString());
                string realm = TokenHelper.GetRealmFromTargetUrl(tenantSiteUrl);
                string siteAdminAccessToken = TokenHelper.GetAppOnlyAccessToken(TokenHelper.SharePointPrincipal, configSiteUrl.Authority, realm).AccessToken;

                using (ClientContext clientContext = TokenHelper.GetClientContextWithAccessToken(configSiteUrl.OriginalString, siteAdminAccessToken))
                {
                    // Get the ConfigStore list
                    Web web = clientContext.Web;
                    List configStoreList = clientContext.Web.Lists.GetByTitle(ConfigurationManager.AppSettings["ConfigListName"].ToString());
                    CamlQuery getConfigVal = new CamlQuery();
                    getConfigVal.ViewXml = "<View><Query><Where><Eq><FieldRef Name='Title'/><Value Type='Text'>" + configKey + "</Value></Eq></Where></Query></View>";
                    ListItemCollection resultvalues = configStoreList.GetItems(getConfigVal);
                    clientContext.Load(resultvalues);
                    clientContext.ExecuteQuery();
                    ListItem val = resultvalues[0];
                    configVal = val["ConfigValue"].ToString();
                    return configVal;
                }

            }
            catch (Exception ex)
            {
                CreateException(ex, exClient);
                Console.WriteLine(ex.Message);
                return null;
            }
        }
        /// <summary>
        /// Function to Execute CAAS request and create the site collection
        /// </summary>
        static void ExecuteSiteRequestType()
        {

            Uri tenantSiteUrl = null;
            Uri configSiteUrl = null;
            if (ConfigurationManager.AppSettings["TenantURL"].ToString() != null)
            {
                tenantSiteUrl = new Uri(ConfigurationManager.AppSettings["TenantURL"].ToString());
                realm = TokenHelper.GetRealmFromTargetUrl(tenantSiteUrl);
            }
            if (ConfigurationManager.AppSettings["MainSiteURL"].ToString() != null)
            {
                configSiteUrl = new Uri(ConfigurationManager.AppSettings["MainSiteURL"].ToString());
            }

            //Get the access token of the tenant admin site.
            string tenantAdminAccessToken = TokenHelper.GetAppOnlyAccessToken(TokenHelper.SharePointPrincipal, tenantSiteUrl.Authority, realm).AccessToken;

            //Get the access token of the required site collection.
            string siteAdminAccessToken = TokenHelper.GetAppOnlyAccessToken(TokenHelper.SharePointPrincipal, configSiteUrl.Authority, realm).AccessToken;

            using (ClientContext clientContext = TokenHelper.GetClientContextWithAccessToken(configSiteUrl.OriginalString, siteAdminAccessToken))
            {

                // Get the list Request For New Site Collection

                List siteRequestsList = clientContext.Web.Lists.GetByTitle(GetConfigValues("RequestForNewSiteCollectionListName"));
                clientContext.ExecuteQuery();

                //Get the itmes from list for which IsJobTriggered = No
                CamlQuery getSiteRequests = new CamlQuery();
                getSiteRequests.ViewXml = GetConfigValues("QueryToGetSiteRequests");
                ListItemCollection siteRequests = siteRequestsList.GetItems(getSiteRequests);
                clientContext.Load(siteRequests);
                clientContext.ExecuteQuery();

                //Update IsJobTriggered value to "Yes" for fetched list items
                ListItemCollection itemColl = null;
                itemColl = siteRequests;
                // UpdateListItem(clientContext, itemColl);

                var siteTitle = string.Empty;
                var Url = GetConfigValues("WebSiteAddressURL").Trim(); ;
                var OwnerEmailID = string.Empty;
                var SiteTemplateNumber = string.Empty;
                int Storage;
                int ResourcesNumber = Convert.ToInt32(GetConfigValues("ResourceQuotaVal"));
                var siteUrl = string.Empty;
                var managedPath = GetConfigValues("ManagedPath");
                var applyBranding = string.Empty;
                // Loop to get every item and store the site collection related info in local variables

                foreach (ListItem siteRequest in siteRequests)
                {
                    try
                    {
                        Console.WriteLine($"Number of site collections to provision is {siteRequests.Count}");
                        siteRequest["IsJobTriggered"] = "Yes";
                        siteRequest.Update();
                        clientContext.ExecuteQuery();
                        applyBranding = (siteRequest["ApplyBranding"]).ToString();
                        Console.WriteLine("Apply Branding is " + applyBranding);
                        siteTitle = siteRequest["Title"].ToString();
                        siteUrl = siteRequest["SiteURL"].ToString().Trim();
                        OwnerEmailID = siteRequest["AdminEmailAddress"].ToString().Trim() + GetConfigValues("AdminEmailIdAppendinPart").Trim();
                        var SiteTemplateInfo = siteRequest["SiteTemplateType_x003a_SiteTempl"] as FieldLookupValue;
                        if (SiteTemplateInfo != null)
                        {
                            SiteTemplateNumber = SiteTemplateInfo.LookupValue;
                        }

                        Storage = Convert.ToInt16(siteRequest["StorageQuota"]);
                        if (siteRequest["RequestType"].ToString() == GetConfigValues("ServiceTypeIAAS"))
                        {
                            FieldLookupValue siteTemplate = new FieldLookupValue();
                            siteTemplate.LookupId = 1;
                            siteRequest["SiteTemplateType"] = siteTemplate;
                            siteRequest.Update();
                            clientContext.ExecuteQuery();
                        }
                        var completeURL = (Url + "/" + managedPath + "/" + siteUrl).ToLower();
                        Console.WriteLine(completeURL);
                        bool sitesExists = false;


                        //check if the site with value of completeURL exists or not
                        using (ClientContext tenantContext = TokenHelper.GetClientContextWithAccessToken(tenantSiteUrl.OriginalString, tenantAdminAccessToken))
                        {
                            var tenant = new Tenant(tenantContext);
                            SPOSitePropertiesEnumerable spp = null;
                            int startIndex = 0;

                            while (spp == null || spp.Count > 0)
                            {
                                spp = tenant.GetSiteProperties(startIndex, true);
                                tenantContext.Load(spp);
                                tenantContext.ExecuteQuery();

                                foreach (SiteProperties sp in spp)
                                {
                                    //if (sp.Url.ToString() == completeURL)
                                    if(string.Equals(Convert.ToString(sp.Url),completeURL,StringComparison.InvariantCultureIgnoreCase))
                                    {
                                        sitesExists = true;
                                        Console.WriteLine("Site collection already exists with URL " + completeURL);
                                        break;
                                    }
                                }
                                startIndex += spp.Count;
                                if (sitesExists == true)
                                {
                                    Console.WriteLine("Site collection already exists with URL " + completeURL);
                                    break;
                                }
                            }

                            //If site does not exist, then only create new site else just update the fields in current item 
                            if (sitesExists == false)
                            {
                                var siteCreationProperties = new SiteCreationProperties();

                                siteCreationProperties.Url = completeURL.ToLower();
                                siteCreationProperties.Title = siteTitle;
                                siteCreationProperties.Owner = OwnerEmailID;
                                if (siteRequest["RequestType"].ToString() == GetConfigValues("ServiceTypeCAAS"))
                                {
                                    siteCreationProperties.Template = SiteTemplateNumber;
                                }
                                else
                                {
                                    siteCreationProperties.Template = GetConfigValues("TeamTemplateCode");
                                }
                                siteCreationProperties.StorageMaximumLevel = Storage;
                                siteCreationProperties.UserCodeMaximumLevel = ResourcesNumber;
                                SpoOperation spo = tenant.CreateSite(siteCreationProperties);
                                tenantContext.Load(tenant);

                                //We will need the IsComplete property to check if the provisioning of the Site Collection is complete.
                                tenantContext.Load(spo, i => i.IsComplete);

                                tenantContext.ExecuteQuery();
                                Console.WriteLine("Site is getting created ...");
                                //Check if provisioning of the SiteCollection is complete.
                                while (!spo.IsComplete)
                                {
                                    //Wait for 30 seconds and then try again
                                    System.Threading.Thread.Sleep(30000);
                                    spo.RefreshLoad();
                                    tenantContext.ExecuteQuery();
                                    Console.WriteLine("Site is getting created please wait...");
                                }
                            }

                        }

                        //Create property bags

                        CreatePropertyBags(siteRequest, completeURL.ToLower());

                        //Add Branding to the newly create site
                        if (siteRequest["RequestType"].ToString() == GetConfigValues("ServiceTypeCAAS") && sitesExists == false && applyBranding == "True")
                        {
                            ApplyCAASBranding(completeURL.ToLower(), siteRequest);
                            siteRequest["IsSiteProvisioned"] = "Yes";
                            siteRequest.Update();
                            clientContext.ExecuteQuery();
                        }

                        siteRequest["IsSiteCreated"] = "Yes";
                        siteRequest["NewSiteURL"] = completeURL.ToLower();
                        siteRequest.Update();
                        clientContext.ExecuteQuery();
                        Console.WriteLine("Site creation request has been executed");

                        if (siteRequest["IsMailSent"].ToString() == "No" && string.Equals(Convert.ToString(siteRequest["RequestType"]), GetConfigValues("ServiceTypeCAAS"),StringComparison.InvariantCultureIgnoreCase))
                        {
                            var emailp = new EmailProperties();
                            emailp.To = new List<string> { OwnerEmailID };
                            Console.WriteLine(emailp.To);
                            emailp.From = GetConfigValues("EmailFromAddress");
                            Console.WriteLine(emailp.From);
                            emailp.Body = GetConfigValues("MailBody").Replace("{siteTitle}", siteTitle).Replace("{SiteUrl}", completeURL.ToLower()).Replace("{RequestType}", siteRequest["RequestType"].ToString());
                            Console.WriteLine(emailp.Body);
                            emailp.Subject = GetConfigValues("MailSubject").Replace("{siteTitle}", siteTitle);
                            Console.WriteLine(emailp.Subject);
                            Utility.SendEmail(clientContext, emailp);
                            clientContext.ExecuteQuery();
                            siteRequest["IsMailSent"] = "Yes";
                            siteRequest.Update();
                            clientContext.ExecuteQuery();
                        }
                    }
                    catch (Exception ex)
                    {
                        CreateException(ex, exClient);
                        Console.WriteLine(ex.Message);
                    }

                }


            }
        }

        private static void SetSiteResonalSettings(ClientContext context, int timeZoneID)
        {
            try
            {
                RegionalSettings regionalSettings = context.Web.RegionalSettings;
                context.Load(regionalSettings);
                context.ExecuteQuery();

                TimeZoneCollection timeZoneCollections = regionalSettings.TimeZones;
                context.Load(timeZoneCollections);
                context.ExecuteQuery();

                Microsoft.SharePoint.Client.TimeZone newtimezone = timeZoneCollections.GetById(timeZoneID);
                regionalSettings.TimeZone = newtimezone;
                regionalSettings.LocaleId = 3081;
                regionalSettings.Update();
                regionalSettings.Context.ExecuteQuery();

                Console.WriteLine("site Regional settings are set to Australia");
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.StackTrace + ex.Message);
            }
        }

        private static void CreatePropertyBags(ListItem siteReq, string newSiteUrl)
        {
            Uri newSiteCollUrl = null;
            if (newSiteUrl != "")
            {
                newSiteCollUrl = new Uri(newSiteUrl);
            }
            string siteAdminAccessToken = TokenHelper.GetAppOnlyAccessToken(TokenHelper.SharePointPrincipal, newSiteCollUrl.Authority, realm).AccessToken;

            using (ClientContext clientContext = TokenHelper.GetClientContextWithAccessToken(newSiteCollUrl.OriginalString, siteAdminAccessToken))
            {
                Web web = clientContext.Site.RootWeb;
                var allProperties = web.AllProperties;
                if (siteReq["UNSWDivision"] != null)
                {
                    allProperties["UNSWDEPT"] = siteReq["UNSWDivision"].ToString();
                    Console.WriteLine("UNSWDEPT Property created");
                }
                if (siteReq["UNSWSchool"] != null)
                {
                    allProperties["UNSWUNIT"] = siteReq["UNSWSchool"].ToString();
                    Console.WriteLine("UNSWUNIT Property created");
                }
                if (string.Equals(Convert.ToString(siteReq["RequestType"]), GetConfigValues("ServiceTypeCAAS"), StringComparison.CurrentCultureIgnoreCase))
                {
                    allProperties["UNSWSITETYPE"] = siteReq["UNSWSiteType"].ToString();
                    Console.WriteLine("Site Type Property created as COLLAB");
                }

                if (string.Equals(Convert.ToString(siteReq["RequestType"]), GetConfigValues("ServiceTypeIAAS"), StringComparison.CurrentCultureIgnoreCase))
                {
                    allProperties["UNSWSITETYPE"] = siteReq["UNSWSiteType"].ToString();
                    Console.WriteLine("Site Type Property created as INTRANET");
                }
                web.Update();
                clientContext.ExecuteQuery();

                //Applying Regional Settings in CAAS site collection
                SetSiteResonalSettings(clientContext, 76);
            }
        }
        /// <summary>
        /// This method creates events in Azure Portal
        /// </summary>
        /// <param name="appEvent"></param>
        /// <param name="eventClient"></param>
        public static void CreateEvent(string appEvent, Microsoft.ApplicationInsights.TelemetryClient eventClient)
        {
            eventClient.Context.InstrumentationKey = instrumentationKey;
            eventClient.TrackEvent(appEvent);
        }

        /// <summary>
        /// This method creates exceptions in Azure Portal
        /// </summary>
        /// <param name="appException"></param>
        /// <param name="exceptionClient"></param>
        public static void CreateException(System.Exception appException, Microsoft.ApplicationInsights.TelemetryClient exceptionClient)
        {
            exceptionClient.Context.InstrumentationKey = instrumentationKey;
            ExceptionTelemetry exTelemetry = new ExceptionTelemetry(appException);
            exceptionClient.TrackException(exTelemetry);
        }

        #region CAAS Branding

        public static void ApplyCAASBranding(string newSiteUrl, ListItem siteReq)
        {
            try
            {
                Console.WriteLine("newSiteUrl" + newSiteUrl);
                Uri newSiteCollUrl = null;
                if (newSiteUrl != "")
                {
                    newSiteCollUrl = new Uri(newSiteUrl);
                }
                string siteAdminAccessToken = TokenHelper.GetAppOnlyAccessToken(TokenHelper.SharePointPrincipal, newSiteCollUrl.Authority, realm).AccessToken;

                using (ClientContext clientContext = TokenHelper.GetClientContextWithAccessToken(newSiteCollUrl.OriginalString, siteAdminAccessToken))
                {
                    Console.WriteLine("In side ApplyCAASBranding");
                    UploadThemes(clientContext);
                    UploadToStyleLibrary(clientContext);

                    var themeName = "UNSW Branding";
                    // Let's get instance to the composite look gallery
                    var web = clientContext.Web;
                    List themesOverviewList = web.GetCatalog(124);
                    clientContext.Load(themesOverviewList);
                    clientContext.ExecuteQuery();

                    AddThemeToComposedLookList(clientContext, themeName, themesOverviewList);
                    SetThemeBasedOnName(clientContext, themeName);
                    AddCurrentThemeItem(clientContext, themesOverviewList, themeName);
                    CreateCustomAction(clientContext);
                }
            }
            catch (Exception ex)
            {
                CreateException(ex, exClient);
                Console.WriteLine(ex.Message);
            }
        }

        private static void UploadThemes(ClientContext clientContext)
        {
            try
            {
                Console.WriteLine("UploadThemes");
                var web = clientContext.Web;
                List themesList = web.GetCatalog(123);
                // get the theme list
                clientContext.Load(themesList);
                clientContext.ExecuteQuery();
                Folder rootfolder = themesList.RootFolder;
                clientContext.Load(rootfolder);
                clientContext.Load(rootfolder.Folders);
                clientContext.ExecuteQuery();
                Folder folder15 = rootfolder;
                foreach (Folder folder in rootfolder.Folders)
                {
                    if (folder.Name == "15")
                    {
                        Console.WriteLine("Folder 15 is there");
                        folder15 = folder;
                        break;
                    }
                }

                Console.WriteLine("6");
                string[] pageLayoutColl = new string[] { "unsw.spcolor", "unsw.spfont" };
                for (Int32 i = 0; i < pageLayoutColl.Length; i++)
                {
                    Console.WriteLine("UploadThemes" + ".\\Theme\\15\\" + pageLayoutColl[i].ToString());
                    FileCreationInformation newFile = new FileCreationInformation();
                    string fileLoc = ".\\Theme\\15\\" + pageLayoutColl[i].ToString();
                    newFile.Content = System.IO.File.ReadAllBytes(fileLoc);
                    newFile.Url = pageLayoutColl[i].ToString();
                    newFile.Overwrite = true;


                    Console.WriteLine("7");
                    Microsoft.SharePoint.Client.File uploadFile = folder15.Files.Add(newFile);
                    //uploadFile.CheckIn("uploading", CheckinType.MajorCheckIn);
                    clientContext.Load(uploadFile);
                    clientContext.ExecuteQuery();
                    Console.WriteLine("8");
                }

                Console.WriteLine("9");
            }
            catch (Exception ex)
            {
                CreateException(ex, exClient);
                Console.WriteLine("UploadThemes" + ex.Message);
            }
        }

        private static void UploadToStyleLibrary(ClientContext clientContext)
        {
            try
            {
                var web = clientContext.Web;
                List styleLib = web.Lists.GetByTitle("Style Library");
                // get the theme list
                clientContext.Load(styleLib);
                clientContext.ExecuteQuery();

                Folder rootfolder = styleLib.RootFolder;
                clientContext.Load(rootfolder);
                clientContext.ExecuteQuery();

                rootfolder = rootfolder.Folders.Add("UNSW");

                #region Image
                var imageFolder = rootfolder.Folders.Add("Images");
                //var rootfolder = clientContext.Web.GetFolderByServerRelativeUrl("_catalogs/theme/15");
                clientContext.Load(imageFolder);
                clientContext.ExecuteQuery();


                string[] imageFileColl = new string[] { "arrow-menu.png", "arrow-yellow.png", "favicon.ico", "logo-unsw.png" };
                for (Int32 i = 0; i < imageFileColl.Length; i++)
                {
                    FileCreationInformation newFile = new FileCreationInformation();
                    string fileLoc = ".\\Style Library\\UNSW\\Images\\" + imageFileColl[i].ToString();
                    newFile.Content = System.IO.File.ReadAllBytes(fileLoc);
                    newFile.Url = imageFileColl[i].ToString();
                    newFile.Overwrite = true;

                    Microsoft.SharePoint.Client.File uploadFile = imageFolder.Files.Add(newFile);
                    uploadFile.CheckIn("Uploading File", CheckinType.MajorCheckIn);
                    clientContext.Load(uploadFile);
                    clientContext.ExecuteQuery();
                }
                #endregion

                #region CSS
                var cssFolder = rootfolder.Folders.Add("CSS");
                //var rootfolder = clientContext.Web.GetFolderByServerRelativeUrl("_catalogs/theme/15");
                clientContext.Load(cssFolder);
                clientContext.ExecuteQuery();

                string[] cssFileColl = new string[] { "UNSWIntranet.css", "UNSWCAAS.css" };
                for (Int32 i = 0; i < cssFileColl.Length; i++)
                {
                    FileCreationInformation newFile = new FileCreationInformation();
                    string fileLoc = ".\\Style Library\\UNSW\\CSS\\" + cssFileColl[i].ToString();
                    newFile.Content = System.IO.File.ReadAllBytes(fileLoc);
                    newFile.Url = cssFileColl[i].ToString();
                    newFile.Overwrite = true;

                    Microsoft.SharePoint.Client.File uploadFile = cssFolder.Files.Add(newFile);
                    uploadFile.CheckIn("Uploading File", CheckinType.MajorCheckIn);
                    clientContext.Load(uploadFile);
                    clientContext.ExecuteQuery();
                }
                #endregion

                #region Scripts

                var scriptFolder = rootfolder.Folders.Add("Scripts");
                //var rootfolder = clientContext.Web.GetFolderByServerRelativeUrl("_catalogs/theme/15");
                clientContext.Load(scriptFolder);
                clientContext.ExecuteQuery();

                string[] scriptFileColl = new string[] { "jquery-2.1.0.min.js", "StickyFooter.js", "UNSWCAAS.js" };
                for (Int32 i = 0; i < scriptFileColl.Length; i++)
                {
                    FileCreationInformation newFile = new FileCreationInformation();
                    string fileLoc = ".\\Style Library\\UNSW\\Scripts\\" + scriptFileColl[i].ToString();
                    newFile.Content = System.IO.File.ReadAllBytes(fileLoc);
                    newFile.Url = scriptFileColl[i].ToString();
                    newFile.Overwrite = true;

                    Microsoft.SharePoint.Client.File uploadFile = scriptFolder.Files.Add(newFile);
                    uploadFile.CheckIn("Uploading File", CheckinType.MajorCheckIn);
                    clientContext.Load(uploadFile);
                    clientContext.ExecuteQuery();
                }
                #endregion

            }
            catch (Exception ex)
            {
                CreateException(ex, exClient);
                Console.WriteLine(ex.Message);
            }
        }

        private static void AddThemeToComposedLookList(ClientContext clientContext, string themeName, List themesOverviewList)
        {
            var web = clientContext.Web;
            clientContext.Load(web);
            clientContext.ExecuteQuery();

            // Do not add duplicate, if the theme is already there
            if (!ThemeEntryExists(clientContext, themesOverviewList, themeName))
            {
                // Let's create new theme entry. Notice that theme selection is not available from 
                //  UI in personal sites, so this is just for consistency sake
                ListItemCreationInformation itemInfo = new ListItemCreationInformation();
                Microsoft.SharePoint.Client.ListItem item = themesOverviewList.AddItem(itemInfo);
                item["Name"] = themeName;
                item["Title"] = themeName;


                var masterPageName = web.ServerRelativeUrl + "/_catalogs/masterpage/seattle.master";
                Console.WriteLine("masterPageName" + masterPageName);

                // we use seattle master if anythign else is not set
                if (!string.IsNullOrEmpty(masterPageName))
                {
                    item["MasterPageUrl"] = masterPageName;
                }
                /*else
                {
                    item["MasterPageUrl"] = URLCombine(web.ServerRelativeUrl, string.Format("/_catalogs/masterpage/{0}", Path.GetFileName(masterPageName)));
                }*/

                var colorFilePath = web.ServerRelativeUrl + "/_catalogs/theme/15/unsw.spcolor";
                Console.WriteLine("colorFilePath" + colorFilePath);
                if (!string.IsNullOrEmpty(colorFilePath))

                {
                    item["ThemeUrl"] = colorFilePath;
                    //URLCombine(web.ServerRelativeUrl, string.Format("/_catalogs/theme/15/{0}", System.IO.Path.GetFileName(colorFilePath)));
                }

                var backGroundPath = "";
                if (!string.IsNullOrEmpty(backGroundPath))
                {
                    item["ImageUrl"] = backGroundPath;
                    //URLCombine(web.ServerRelativeUrl, string.Format("/_catalogs/theme/15/{0}", System.IO.Path.GetFileName(backGroundPath)));
                }

                var fontFilePath = web.ServerRelativeUrl + "/_catalogs/theme/15/unsw.spfont";
                if (!string.IsNullOrEmpty(fontFilePath))
                {
                    item["FontSchemeUrl"] = fontFilePath;
                    //URLCombine(web.ServerRelativeUrl, string.Format(" / _catalogs/theme/15/{0}", System.IO.Path.GetFileName(fontFilePath)));
                }

                Console.WriteLine("End");
                item["DisplayOrder"] = 11;
                item.Update();
                clientContext.ExecuteQuery();

            }
        }

        private static void AddCurrentThemeItem(ClientContext clientContext, List themeList, string themeName)
        {
            var web = clientContext.Web;
            CamlQuery query = new CamlQuery();
            string camlString = @"
                <View>
                    <Query>                
                        <Where>
                            <Eq>
                                <FieldRef Name='DisplayOrder' />
                                <Value Type='Number'>0</Value>
                            </Eq>
                        </Where>
                     </Query>
                </View>";

            query.ViewXml = camlString;
            var item = themeList.GetItems(query);
            clientContext.Load(item);
            clientContext.ExecuteQuery();

            //ListItemCreationInformation itemInfo = new ListItemCreationInformation();
            //Microsoft.SharePoint.Client.ListItem item = themesOverviewList.AddItem(itemInfo);


            var masterPageName = web.ServerRelativeUrl + "/_catalogs/masterpage/seattle.master";
            // we use seattle master if anythign else is not set
            if (!string.IsNullOrEmpty(masterPageName))
            {
                item[0]["MasterPageUrl"] = masterPageName;
            }
            /*else
            {
                item["MasterPageUrl"] = URLCombine(web.ServerRelativeUrl, string.Format("/_catalogs/masterpage/{0}", Path.GetFileName(masterPageName)));
            }*/

            var colorFilePath = web.ServerRelativeUrl + "/_catalogs/theme/15/unsw.spcolor";
            if (!string.IsNullOrEmpty(colorFilePath))
            {
                item[0]["ThemeUrl"] = colorFilePath;
                //URLCombine(web.ServerRelativeUrl, string.Format("/_catalogs/theme/15/{0}", System.IO.Path.GetFileName(colorFilePath)));
            }

            var backGroundPath = "";
            if (!string.IsNullOrEmpty(backGroundPath))
            {
                item[0]["ImageUrl"] = backGroundPath;
                //URLCombine(web.ServerRelativeUrl, string.Format("/_catalogs/theme/15/{0}", System.IO.Path.GetFileName(backGroundPath)));
            }

            var fontFilePath = web.ServerRelativeUrl + "/_catalogs/theme/15/unsw.spfont";
            if (!string.IsNullOrEmpty(fontFilePath))
            {
                item[0]["FontSchemeUrl"] = fontFilePath;
                //URLCombine(web.ServerRelativeUrl, string.Format(" / _catalogs/theme/15/{0}", System.IO.Path.GetFileName(fontFilePath)));
            }



            item[0].Update();
            clientContext.ExecuteQuery();
        }
        public static void SetThemeBasedOnName(ClientContext clientContext, string themeName)
        {

            Console.WriteLine("SetThemeBasedOnName");
            // Let's get instance to the composite look gallery
            Web web = clientContext.Web;

            if (!web.IsObjectPropertyInstantiated("/sites/LoadTest29"))
            {
                clientContext.Load(web);
                clientContext.ExecuteQuery();
            }

            List themeList = web.GetCatalog(124);
            clientContext.Load(themeList);
            clientContext.ExecuteQuery();

            // We are assuming that the theme exists
            CamlQuery query = new CamlQuery();
            string camlString = @"
        <View>
            <Query>                
                <Where>
                    <Eq>
                        <FieldRef Name='Name' />
                        <Value Type='Text'>{0}</Value>
                    </Eq>
                </Where>
                </Query>
        </View>";
            // Let's update the theme name accordingly
            camlString = string.Format(camlString, themeName);
            query.ViewXml = camlString;
            var found = themeList.GetItems(query);
            clientContext.Load(found);
            clientContext.ExecuteQuery();
            if (found.Count > 0)
            {
                try
                {
                    Console.WriteLine("found");
                    ListItem themeEntry = found[0];
                    //Set the properties for applying custom theme which was jus uplaoded
                    string spColorURL = null;
                    if (themeEntry["ThemeUrl"] != null && themeEntry["ThemeUrl"].ToString().Length > 0)
                    {
                        Console.WriteLine(MakeAsRelativeUrl((themeEntry["ThemeUrl"] as FieldUrlValue).Url));

                        spColorURL = MakeAsRelativeUrl((themeEntry["ThemeUrl"] as FieldUrlValue).Url);
                    }
                    string spFontURL = null;
                    if (themeEntry["FontSchemeUrl"] != null && themeEntry["FontSchemeUrl"].ToString().Length > 0)
                    {
                        Console.WriteLine(MakeAsRelativeUrl((themeEntry["FontSchemeUrl"] as FieldUrlValue).Url));
                        spFontURL = MakeAsRelativeUrl((themeEntry["FontSchemeUrl"] as FieldUrlValue).Url);
                    }
                    string backGroundImage = null;
                    if (themeEntry["ImageUrl"] != null && themeEntry["ImageUrl"].ToString().Length > 0)
                    {
                        Console.WriteLine(MakeAsRelativeUrl((themeEntry["ImageUrl"] as FieldUrlValue).Url));
                        backGroundImage = MakeAsRelativeUrl((themeEntry["ImageUrl"] as FieldUrlValue).Url);
                    }

                    // Set theme to host web
                    Console.WriteLine("Set theme to host web");

                    web.ApplyTheme(spColorURL,
                                        spFontURL,
                                        backGroundImage,
                                        false);
                    web.Update();
                    clientContext.Load(web);
                    clientContext.ExecuteQuery();

                    // Let's also update master page, if needed
                    Console.WriteLine("Let's also update master page, if needed");

                    if (themeEntry["MasterPageUrl"] != null && themeEntry["MasterPageUrl"].ToString().Length > 0)
                    {
                        web.MasterUrl = MakeAsRelativeUrl((themeEntry["MasterPageUrl"] as FieldUrlValue).Url);
                        web.CustomMasterUrl = MakeAsRelativeUrl((themeEntry["MasterPageUrl"] as FieldUrlValue).Url);
                        web.Update();
                    }
                    // Execute the needed code
                    clientContext.Load(web);
                    clientContext.ExecuteQuery();
                }
                catch (Exception ex)
                {
                    CreateException(ex, exClient);
                    Console.WriteLine(ex.Message);
                }
            }
        }

        private static string MakeAsRelativeUrl(string urlToProcess)
        {
            Uri uri = new Uri(urlToProcess);
            return uri.AbsolutePath;
        }

        private static bool ThemeEntryExists(ClientContext clientContext, List themeList, string themeName)
        {

            CamlQuery query = new CamlQuery();
            string camlString = @"
                <View>
                    <Query>                
                        <Where>
                            <Eq>
                                <FieldRef Name='Name' />
                                <Value Type='Text'>{0}</Value>
                            </Eq>
                        </Where>
                     </Query>
                </View>";
            // Let's update the theme name accordingly
            camlString = string.Format(camlString, themeName);
            query.ViewXml = camlString;
            var found = themeList.GetItems(query);
            clientContext.Load(found);
            clientContext.ExecuteQuery();
            if (found.Count > 0)
            {
                return true;
            }
            return false;
        }

        private static void CreateCustomAction(ClientContext clientContext)
        {

            #region Update site logo

            Site site = clientContext.Site;
            clientContext.Load(site, w => w.RootWeb);
            clientContext.ExecuteQuery();

            var siteUrl = site.RootWeb.Url;

            site.RootWeb.SiteLogoUrl = site.RootWeb.ServerRelativeUrl + "/Style Library/UNSW/Images/logo-unsw.png";
            site.RootWeb.Update();
            clientContext.ExecuteQuery();
            #endregion

            #region Add css references
            string cssUrl = "\"" + site.RootWeb.ServerRelativeUrl + "/Style Library/UNSW/CSS/UNSWCAAS.css" + "\"";
            string cssUrlIntranet = "\"" + site.RootWeb.ServerRelativeUrl + "/Style Library/UNSW/CSS/UNSWIntranet.css" + "\"";

            UserCustomAction customActionUNSWCss = site.UserCustomActions.Add();
            customActionUNSWCss.Location = "ScriptLink";
            customActionUNSWCss.Sequence = 100;
            customActionUNSWCss.ScriptBlock = @"document.write('<link rel =""stylesheet"" href =" + cssUrlIntranet + " />');";
            customActionUNSWCss.Name = "UNSWIntranetCssLink";
            customActionUNSWCss.Update();
            clientContext.ExecuteQuery();

            UserCustomAction customActionUNSWCssIn = site.UserCustomActions.Add();
            customActionUNSWCssIn.Location = "ScriptLink";
            customActionUNSWCssIn.Sequence = 101;
            customActionUNSWCssIn.ScriptBlock = @"document.write('<link rel =""stylesheet"" href =" + cssUrl + " />');";
            customActionUNSWCssIn.Name = "UNSWCAASCssLink";
            customActionUNSWCssIn.Update();
            clientContext.ExecuteQuery();

            UserCustomAction customActionFontCSS = site.UserCustomActions.Add();
            customActionFontCSS.Location = "ScriptLink";
            customActionFontCSS.Sequence = 102;
            customActionFontCSS.ScriptBlock = @"document.write('<link rel =""stylesheet"" href =""//netdna.bootstrapcdn.com/font-awesome/4.0.3/css/font-awesome.css"" />');";
            customActionFontCSS.Name = "FontAwesomeCSSLink";
            customActionFontCSS.Update();
            clientContext.ExecuteQuery();


            #endregion

            #region Add javascript references
            UserCustomAction customActionJquery = site.UserCustomActions.Add();
            customActionJquery.Location = "ScriptLink";
            customActionJquery.ScriptSrc = "~SiteCollection/Style Library/UNSW/Scripts/jquery-2.1.0.min.js";
            customActionJquery.Sequence = 1000;
            customActionJquery.Update();
            clientContext.ExecuteQuery();

            UserCustomAction customActionJqueryUI = site.UserCustomActions.Add();
            customActionJqueryUI.Location = "ScriptLink";
            customActionJqueryUI.ScriptSrc = "~SiteCollection/Style Library/UNSW/Scripts/StickyFooter.js";
            customActionJqueryUI.Sequence = 1001;
            customActionJqueryUI.Update();
            clientContext.ExecuteQuery();

            UserCustomAction customActionUNSWCASS = site.UserCustomActions.Add();
            customActionUNSWCASS.Location = "ScriptLink";
            customActionUNSWCASS.ScriptSrc = "~SiteCollection/Style Library/UNSW/Scripts/UNSWCAAS.js";
            customActionUNSWCASS.Sequence = 1002;
            customActionUNSWCASS.Update();
            clientContext.ExecuteQuery();

            #endregion
        }
        #endregion
    }
}
