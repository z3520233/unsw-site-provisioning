﻿using System;
using System.Configuration;
using System.Linq;
using Microsoft.SharePoint.Client;
using Microsoft.SharePoint.Client.Taxonomy;
using Microsoft.ApplicationInsights.Extensibility;
using Microsoft.ApplicationInsights.DataContracts;
using System.Xml;
using System.Xml.XPath;
using Microsoft.SharePoint.Client.Publishing;
using System.IO;
using System.Xml.Linq;
using System.Xml.Serialization;
using System.Collections.Generic;
using Microsoft.SharePoint.Client.Utilities;
using Microsoft.SharePoint.Client.WebParts;
using OfficeDevPnP.Core.Entities;

namespace UNSW.SharePointOnline.SiteProsioning
{
    class Program
    {
        static string instrumentationKey;
        static Microsoft.ApplicationInsights.TelemetryClient eventClient;
        static Microsoft.ApplicationInsights.TelemetryClient exClient;
        static bool isFeatureActivatedOnSite = false;
        static bool isFeatureActivatedOnWeb = false;
        static void Main(string[] args)
        {


            //Creating TelemetryClient object for event and exception logging
            instrumentationKey = ConfigurationManager.AppSettings[Constants.AppConfigConstants.InstrumentationKey].ToString();
            TelemetryConfiguration.Active.InstrumentationKey = ConfigurationManager.AppSettings[Constants.AppConfigConstants.InstrumentationKey].ToString();
            eventClient = new Microsoft.ApplicationInsights.TelemetryClient();
            exClient = new Microsoft.ApplicationInsights.TelemetryClient();


            CreateEvent("Code execution starts", eventClient);
            Console.WriteLine("Code execution starts");

            string realm = string.Empty;
            Uri tenantSiteUrl = new Uri(ConfigurationManager.AppSettings[Constants.AppConfigConstants.TenantURL]);
            Console.WriteLine($"{tenantSiteUrl} Code execution starts");
            Uri configSiteUrl = new Uri(ConfigurationManager.AppSettings[Constants.AppConfigConstants.MainSiteURL]);
            Console.WriteLine($"{configSiteUrl} Code execution starts");

            if (tenantSiteUrl != null)
            {
                realm = TokenHelper.GetRealmFromTargetUrl(tenantSiteUrl);
            }

            //Get the access token of the tenant admin site.
            string tenantAdminAccessToken = TokenHelper.GetAppOnlyAccessToken(TokenHelper.SharePointPrincipal, tenantSiteUrl.Authority, realm).AccessToken;

            //Get the access token of the required site collection.
            string siteAdminAccessToken = TokenHelper.GetAppOnlyAccessToken(TokenHelper.SharePointPrincipal, configSiteUrl.Authority, realm).AccessToken;

            using (ClientContext clientContext = TokenHelper.GetClientContextWithAccessToken(configSiteUrl.OriginalString, siteAdminAccessToken))
            {
                try
                {
                    Console.WriteLine("Inside the Site Context");
                    // Get the list
                    Web web = clientContext.Web;
                    List siteRequestsList = clientContext.Web.Lists.GetByTitle(GetConfigValues(Constants.ListDetails.RequestForNewSiteCollectionListName));
                    clientContext.ExecuteQuery();

                    //Get the itmes from list for which IsSiteCreate = No
                    CamlQuery getSiteRequests = new CamlQuery();
                    getSiteRequests.ViewXml = GetConfigValues(Constants.ListDetails.QueryToGetSiteProvisionReqsPart1).Trim() + GetConfigValues(Constants.ListDetails.QueryToGetSiteProvisionReqsPart2).Trim();
                    ListItemCollection siteProvisionReqs = siteRequestsList.GetItems(getSiteRequests);
                    clientContext.Load(siteProvisionReqs);
                    clientContext.ExecuteQuery();

                    List list = clientContext.Web.Lists.GetByTitle("SiteConfigurationDocuments");
                    clientContext.Load(list);
                    clientContext.ExecuteQuery();
                    Microsoft.SharePoint.Client.File file = clientContext.Web.GetFileByUrl(configSiteUrl.OriginalString + "/SiteConfigurationDocuments/SiteStructure.xml");
                    clientContext.Load(file);
                    ClientResult<System.IO.Stream> data = file.OpenBinaryStream();
                    clientContext.ExecuteQuery();
                    Stream filestrem = data.Value;
                    XmlDocument xmlDoc = new XmlDocument();
                    xmlDoc.Load(filestrem);

                    realm = string.Empty;
                    tenantSiteUrl = null;
                    configSiteUrl = null;

                    // Loop to get every item and store the site collection related info in local variables
                    foreach (ListItem siteReq in siteProvisionReqs)
                    {
                        try
                        {
                            Console.WriteLine("Total Site count" + siteProvisionReqs.Count);
                            var siteUrl = siteReq[Constants.ListDetails.NewSiteURL].ToString();

                            tenantSiteUrl = new Uri(ConfigurationManager.AppSettings[Constants.AppConfigConstants.TenantURL]);
                            configSiteUrl = new Uri(siteUrl);
                            if (tenantSiteUrl != null)
                            {
                                realm = TokenHelper.GetRealmFromTargetUrl(tenantSiteUrl);
                            }

                            //Get the access token of the tenant admin site.
                            tenantAdminAccessToken = TokenHelper.GetAppOnlyAccessToken(TokenHelper.SharePointPrincipal, tenantSiteUrl.Authority, realm).AccessToken;

                            //Get the access token of the required site collection.
                            siteAdminAccessToken = TokenHelper.GetAppOnlyAccessToken(TokenHelper.SharePointPrincipal, configSiteUrl.Authority, realm).AccessToken;

                            using (ClientContext newSiteContext = TokenHelper.GetClientContextWithAccessToken(configSiteUrl.OriginalString, siteAdminAccessToken))
                            {
                                CreateRERLists(newSiteContext, xmlDoc);
                                CreateRERSiteColumn(newSiteContext, xmlDoc);
                                while (isFeatureActivatedOnSite == false && isFeatureActivatedOnWeb == false)
                                {
                                    ActivateFeatures(newSiteContext);
                                }
                                CreateSubSiteMeeting(newSiteContext);
                                CreateSiteColumnsNContentTypes(newSiteContext, xmlDoc);
                                CreateImageRenditions(newSiteContext);
                                UploadDocuments(newSiteContext, xmlDoc);
                                Uri newSiteURI = new Uri(siteUrl);
                                ManageTermStore(newSiteURI);
                                UploadFiles(newSiteContext, xmlDoc);
                                CreateHomePageAndFooterList(siteUrl);

                                CreateUNSWEventCalendar(newSiteContext);
                                CreateStaffContactsList(newSiteContext);
                                IAASBranding(siteUrl);
                                CreateWebParts(newSiteContext, xmlDoc, newSiteURI);
                                //SetSiteResonalSettings(newSiteContext, 76);
                            }
                            siteReq[Constants.ListDetails.IsSiteProvisioned] = Constants.ListDetails.Yes;
                            siteReq["IsMailSent"] = "Yes";
                            siteReq.Update();
                            clientContext.ExecuteQuery();
                            Console.WriteLine("New Site has been provisioned");
                            CreateEvent("New Site has been provisioned", eventClient);
                            var OwnerEmailID = siteReq["AdminEmailAddress"].ToString().Trim() + GetConfigValues("AdminEmailIdAppendinPart").Trim();
                            var siteTitle = siteReq["Title"].ToString();
                            var emailp = new EmailProperties();
                            emailp.To = new List<string> { OwnerEmailID };
                            Console.WriteLine(emailp.To);
                            emailp.From = GetConfigValues("EmailFromAddress");
                            Console.WriteLine(emailp.From);
                            emailp.Body = GetConfigValues("IAASMailBody").Replace("{siteTitle}", siteTitle).Replace("{SiteUrl}", siteUrl.ToLower());
                            Console.WriteLine(emailp.Body);
                            emailp.Subject = GetConfigValues("IAASMailSubject").Replace("{siteTitle}", siteTitle);
                            Console.WriteLine(emailp.Subject);
                            Utility.SendEmail(clientContext, emailp);
                            clientContext.ExecuteQuery();
                        }
                        catch (Exception ex)
                        {
                            Console.WriteLine(ex.Message);
                        }
                    }
                }
                catch (Exception ex)
                {
                    Console.WriteLine(ex.Message);
                }
            }
            eventClient.Flush();
            exClient.Flush();
        }

        /// <summary>
        /// Function to get config value from COnfigStore list using Config Key
        /// </summary>
        /// <param name="configKey"></param>
        /// <returns></returns>
        static string GetConfigValues(string configKey)
        {

            try
            {
                string configVal;
                Uri tenantSiteUrl = new Uri(ConfigurationManager.AppSettings[Constants.AppConfigConstants.TenantURL]);
                Uri configSiteUrl = new Uri(ConfigurationManager.AppSettings[Constants.AppConfigConstants.MainSiteURL]);
                string realm = TokenHelper.GetRealmFromTargetUrl(tenantSiteUrl);
                string siteAdminAccessToken = TokenHelper.GetAppOnlyAccessToken(TokenHelper.SharePointPrincipal, configSiteUrl.Authority, realm).AccessToken;

                using (ClientContext clientContext = TokenHelper.GetClientContextWithAccessToken(configSiteUrl.OriginalString, siteAdminAccessToken))
                {
                    // Get the ConfigStore list
                    Web web = clientContext.Web;
                    List configStoreList = clientContext.Web.Lists.GetByTitle(Constants.AppConfigConstants.ConfigListName);
                    CamlQuery getConfigVal = new CamlQuery();
                    getConfigVal.ViewXml = Constants.ConfigListDetails.GetConfigValQueryPart1 + configKey + Constants.ConfigListDetails.GetConfigValQueryPart2;
                    ListItemCollection resultvalues = configStoreList.GetItems(getConfigVal);
                    clientContext.Load(resultvalues);
                    clientContext.ExecuteQuery();
                    ListItem val = resultvalues[0];
                    configVal = val[Constants.ConfigListDetails.ConfigValue].ToString();
                    return configVal;
                }

            }
            catch (Exception ex)
            {
                CreateException(ex, exClient);
                Console.WriteLine(ex.Message);
                return null;
            }
        }


        private static void SetSiteResonalSettings(ClientContext context, int timeZoneID)
        {
            try
            {
                RegionalSettings regionalSettings = context.Web.RegionalSettings;
                context.Load(regionalSettings);
                context.ExecuteQuery();
                
                TimeZoneCollection timeZoneCollections = regionalSettings.TimeZones;
                context.Load(timeZoneCollections);
                context.ExecuteQuery();

                Microsoft.SharePoint.Client.TimeZone newtimezone = timeZoneCollections.GetById(timeZoneID);
                regionalSettings.TimeZone = newtimezone;
                regionalSettings.LocaleId = 3081;
                regionalSettings.Update();
                regionalSettings.Context.ExecuteQuery();

                Console.WriteLine("site Regional settings are set to Australia");
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.StackTrace + ex.Message);
            }
        }

        static void AddColumnsToList(ClientContext context, string listName, XmlDocument xmlDoc, string columnXmlTag)
        {
            var currentList = context.Web.Lists.GetByTitle(listName);
            context.Load(currentList);
            context.ExecuteQuery();
            var nodes = xmlDoc.GetElementsByTagName(columnXmlTag);
            if (nodes.Count != 0)
            {
                for (int i = 0; i < nodes.Count; i++)
                {
                    var xmlAttributeCollection = nodes[i].Attributes;
                    if (!currentList.FieldExistsById(xmlAttributeCollection["ID"].Value))
                    {
                        var currentNewField = new FieldCreationInformation(xmlAttributeCollection["Type"].Value.ToString())
                        {
                            AddToDefaultView = true,
                            DisplayName = xmlAttributeCollection["DisplayName"].Value.ToString(),
                            Id = new Guid(xmlAttributeCollection["ID"].Value),
                            Required = false,
                            InternalName = xmlAttributeCollection["InternalName"].Value.ToString()
                        };
                        currentList.CreateField(currentNewField, true);
                        currentList.Update();
                        context.ExecuteQuery();
                    }

                }
                Console.WriteLine("RER related lists' respective columns are created");
            }
        }
        private static void CreateRERLists(ClientContext context, XmlDocument xmlDoc)
        {
            XmlNodeList nodeList = xmlDoc.GetElementsByTagName("Lists");
            var nodes = xmlDoc.GetElementsByTagName("List");
            if (nodes.Count != 0)
            {
                for (int i = 0; i < nodes.Count; i++)
                {
                    var xmlAttributeCollection = nodes[i].Attributes;
                    List currentList = null;
                    if (!context.Web.ListExists(xmlAttributeCollection["InternalName"].Value.ToString()))
                    {
                        try
                        {
                            currentList = context.Web.CreateList(ListTemplateType.GenericList, xmlAttributeCollection["InternalName"].Value.ToString(), false, false);
                            currentList.Update();
                            context.ExecuteQuery();
                            context.Load(currentList);
                            context.ExecuteQuery();

                            currentList.Title = xmlAttributeCollection["DisplayName"].Value.ToString();
                            context.ExecuteQuery();
                            context.Load(currentList);
                            context.ExecuteQuery();
                            var title = currentList.Fields.GetByInternalNameOrTitle(xmlAttributeCollection["FldToChangeDispName"].Value);
                            context.Load(title);
                            context.ExecuteQuery();
                            title.Title = xmlAttributeCollection["TitleFieldDisplayName"].Value.ToString();
                            title.Update();
                            context.ExecuteQuery();
                            AddColumnsToList(context, xmlAttributeCollection["InternalName"].Value.ToString(), xmlDoc, xmlAttributeCollection["XmlTagColumns"].Value.ToString());
                            currentList.Update();
                            context.Load(currentList);
                            context.ExecuteQueryRetry();
                        }
                        catch (Exception ex)
                        {
                            Console.WriteLine(ex.Message);
                        }
                        Console.WriteLine("RER related lists are created");
                    }
                    else
                    {
                        currentList = context.Web.Lists.GetByTitle(xmlAttributeCollection["InternalName"].Value.ToString());
                        context.Load(currentList);
                        context.ExecuteQuery();
                        Console.WriteLine("RER related lists are already present");
                    }
                }
            }
        }
        private static void CreateRERSiteColumn(ClientContext context, XmlDocument xmlDoc)
        {
            XmlNodeList nodeList = xmlDoc.GetElementsByTagName("RERSiteColumns");
            var nodes = xmlDoc.GetElementsByTagName("RERSiteColumn");
            if (nodes.Count != 0)
            {
                for (int i = 0; i < nodes.Count; i++)
                {
                    var xmlAttributeCollection = nodes[i].Attributes;
                    var currentWeb = context.Site.RootWeb;
                    FieldCollection existingFields = currentWeb.Fields;
                    context.Load(existingFields);
                    context.ExecuteQuery();
                    if (xmlAttributeCollection != null)
                    {
                        try
                        {
                            existingFields.AddFieldAsXml("<Field DisplayName='" + xmlAttributeCollection["DisplayName"].Value + "' Name='" + xmlAttributeCollection["Name"].Value + "' ID='" + xmlAttributeCollection["ID"].Value + "' Group='" + xmlAttributeCollection["Group"].Value + "' Type='" + xmlAttributeCollection["Type"].Value + "' />", false, AddFieldOptions.AddFieldInternalNameHint);
                            context.ExecuteQueryRetry();
                            Field customField = existingFields.GetByInternalNameOrTitle(xmlAttributeCollection["Name"].Value);
                            context.Load(customField);
                            context.ExecuteQuery();
                            FieldLookup iaasSiteNameslookUp = context.CastTo<FieldLookup>(customField);
                            var lookList = currentWeb.Lists.GetByTitle(xmlAttributeCollection["LookupListName"].Value);
                            context.Load(lookList.Fields);
                            context.Load(lookList);
                            context.ExecuteQuery();
                            iaasSiteNameslookUp.LookupList = lookList.Id.ToString();
                            iaasSiteNameslookUp.AllowMultipleValues = true;
                            iaasSiteNameslookUp.LookupField = xmlAttributeCollection["LookupFieldName"].Value;
                            iaasSiteNameslookUp.Update();
                            context.Load(iaasSiteNameslookUp);
                            context.ExecuteQueryRetry();
                            Field iaasSiteUrls = existingFields.AddDependentLookup(xmlAttributeCollection["DependantColumnDispName"].Value, iaasSiteNameslookUp, xmlAttributeCollection["DependantColumnName"].Value);
                            context.Load(iaasSiteUrls);
                            context.ExecuteQueryRetry();
                        }
                        catch (Exception ex)
                        {
                            CreateException(ex, exClient);
                            Console.WriteLine(ex.Message);
                        }

                    }
                    
                }
                Console.WriteLine("RER related site column is created");
            }
        }

        private static void ActivateFeatures(ClientContext clientContext)
        {
            try
            {
                var sitefeatures = clientContext.Site.Features;
                clientContext.Load(sitefeatures);

                clientContext.ExecuteQuery();
                var oldFeatureCount = sitefeatures.Count;
                var feature1Id = new Guid(Constants.FeatureActivationDetails.SitePublishingFeatureID);
                sitefeatures.Add(feature1Id, true, FeatureDefinitionScope.None);
                //clientContext.RequestTimeout = Constants.FeatureActivationDetails.SitePublishingTimeOut;
                clientContext.ExecuteQuery();
                clientContext.Load(sitefeatures);
                clientContext.ExecuteQuery();
                var newFeatureCount = sitefeatures.Count;
                if ((oldFeatureCount == newFeatureCount) && isFeatureActivatedOnSite == false)
                {
                    while (newFeatureCount - oldFeatureCount == 1)
                    {
                        feature1Id = new Guid(Constants.FeatureActivationDetails.SitePublishingFeatureID);
                        sitefeatures.Add(feature1Id, true, FeatureDefinitionScope.None);
                        //clientContext.RequestTimeout = Constants.FeatureActivationDetails.SitePublishingTimeOut;
                        clientContext.ExecuteQuery();
                        clientContext.Load(sitefeatures);
                        clientContext.ExecuteQuery();
                        newFeatureCount = sitefeatures.Count;
                    }
                    isFeatureActivatedOnSite = true;
                    Console.WriteLine("isFeatureActivatedOnSite - " + isFeatureActivatedOnSite);
                }
                Console.WriteLine("Publishing feature activated on site collection level");

                // code to activate the publishing server feature from web
                var webfeatures = clientContext.Web.Features;
                clientContext.Load(webfeatures);
                clientContext.ExecuteQuery();
                oldFeatureCount = webfeatures.Count;
                var feature2Id = new Guid(Constants.FeatureActivationDetails.WebPublishingFeatureID);
                webfeatures.Add(feature2Id, true, FeatureDefinitionScope.None);
                //clientContext.RequestTimeout = Constants.FeatureActivationDetails.WebPublishingTimeOut;

                clientContext.ExecuteQuery();
                clientContext.Load(webfeatures);
                clientContext.ExecuteQuery();
                newFeatureCount = webfeatures.Count;
                if ((oldFeatureCount == newFeatureCount) && isFeatureActivatedOnWeb == false)
                {
                    while (newFeatureCount - oldFeatureCount == 1)
                    {
                        feature2Id = new Guid(Constants.FeatureActivationDetails.WebPublishingFeatureID);
                        webfeatures.Add(feature2Id, true, FeatureDefinitionScope.None);
                        clientContext.ExecuteQuery();
                        clientContext.Load(webfeatures);
                        clientContext.ExecuteQuery();
                        newFeatureCount = webfeatures.Count;
                    }
                    isFeatureActivatedOnWeb = true;
                    Console.WriteLine("isFeatureActivatedOnWeb - " + isFeatureActivatedOnWeb);
                }
                Console.WriteLine("Publishing feature activated on web level");
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
            }
        }


        private static void CreateSubSiteMeeting(ClientContext clientContext)
        {
            try
            {
                var web = clientContext.Web;
                clientContext.Load(web);
                clientContext.ExecuteQuery();
                bool siteExists = web.WebExistsByTitle("Meeting");
                if (siteExists == true)
                {
                    Console.WriteLine("subsite already exists");
                }
                else
                {
                    WebCreationInformation wci = new WebCreationInformation();
                    wci.Url = "meeting";
                    wci.Title = "Meeting";
                    wci.Description = "Meeting Sub Site";
                    wci.UseSamePermissionsAsParentSite = true;
                    wci.WebTemplate = "CMSPUBLISHING#0";// Publishing site template
                    wci.Language = 1033;
                    Web w = clientContext.Site.RootWeb.Webs.Add(wci);
                    clientContext.ExecuteQuery();
                    Console.WriteLine("New subsite Created with name Meeting");
                }
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
            }
        }
        private static void CreateSiteColumnsNContentTypes(ClientContext clientContext, XmlDocument xmlDoc)
        {
            if (isFeatureActivatedOnSite == false || isFeatureActivatedOnWeb == false)
            {
                ActivateFeatures(clientContext);
            }
            Web rootWeb = clientContext.Site.RootWeb;
            FieldCollection existingFields = rootWeb.Fields;
            clientContext.Load(existingFields);
            clientContext.ExecuteQuery();
            XmlNodeList customFields;
            customFields = xmlDoc.GetElementsByTagName("Fields");
            var items = xmlDoc.GetElementsByTagName("Field");
            for (int i = 0; i < items.Count; i++)
            {
                var xmlAttributeCollection = items[i].Attributes;
                if (xmlAttributeCollection != null)
                    try
                    {
                        Field customField = existingFields.GetByInternalNameOrTitle(xmlAttributeCollection["Name"].Value);
                        clientContext.Load(customField);
                        clientContext.ExecuteQuery();
                    }
                    catch
                    {
                        //existingFields.AddFieldAsXml("<Field DisplayName='" + xmlAttributeCollection["DisplayName"].Value + "' Name='" + xmlAttributeCollection["Name"].Value + "' ID='" + xmlAttributeCollection["ID"].Value + "' Group='" + xmlAttributeCollection["Group"].Value + "' Type='" + xmlAttributeCollection["Type"].Value + "' />", false, AddFieldOptions.AddFieldInternalNameHint);
                        existingFields.AddFieldAsXml(items[i].OuterXml, false, AddFieldOptions.AddFieldInternalNameHint);
                        clientContext.ExecuteQuery();
                    }

            }

            customFields = xmlDoc.GetElementsByTagName("ContentTypes");
            items = xmlDoc.GetElementsByTagName("ContentType");
            for (int i = 0; i < items.Count; i++)
            {
                var xmlAttributeCollection = items[i].Attributes;

                if (xmlAttributeCollection != null)
                {
                    var customContentType = rootWeb.ContentTypes.GetById(xmlAttributeCollection["ID"].Value);
                    try
                    {
                        clientContext.Load(customContentType);
                        clientContext.ExecuteQuery();
                        var customContentTypeID = customContentType.Id.ToString();
                    }
                    catch
                    {
                        rootWeb.ContentTypes.Add(new ContentTypeCreationInformation
                        {
                            Name = xmlAttributeCollection["Name"].Value,
                            Id = xmlAttributeCollection["ID"].Value,
                            Group = xmlAttributeCollection["Group"].Value
                        });
                        clientContext.ExecuteQuery();

                        customContentType = rootWeb.ContentTypes.GetById(xmlAttributeCollection["ID"].Value);
                        clientContext.Load(customContentType);
                        clientContext.ExecuteQuery();
                        FieldLinkCollection customFieldLinks = customContentType.FieldLinks;
                        clientContext.ExecuteQuery();
                        var fieldRefs = items[i].FirstChild;
                        if (fieldRefs != null)
                        {
                            var fieldLinks = items[i].FirstChild.ChildNodes;
                            for (int j = 0; j < fieldLinks.Count; j++)
                            {
                                var fieldRefAttributes = fieldLinks[j].Attributes;
                                try
                                {
                                    Field contentTypeField = customContentType.Fields.GetByInternalNameOrTitle(fieldRefAttributes["InternalName"].Value);
                                    clientContext.Load(contentTypeField);
                                    clientContext.ExecuteQuery();
                                    FieldLink customFieldLink = customFieldLinks.GetById(contentTypeField.Id);
                                    clientContext.Load(customFieldLink);
                                    clientContext.ExecuteQuery();
                                    customFieldLink.Required = Convert.ToBoolean(fieldRefAttributes["Required"].Value);
                                    customFieldLink.Hidden = Convert.ToBoolean(fieldRefAttributes["Hidden"].Value);
                                    customContentType.Update(true);
                                    clientContext.ExecuteQuery();
                                }
                                catch
                                {
                                    Field customField = existingFields.GetByInternalNameOrTitle(fieldRefAttributes["InternalName"].Value);
                                    clientContext.Load(customField);
                                    clientContext.ExecuteQuery();
                                    FieldLinkCreationInformation customFldLink = new FieldLinkCreationInformation();
                                    customFldLink.Field = customField;
                                    customFieldLinks.Add(customFldLink);
                                    customContentType.Update(true);
                                    clientContext.ExecuteQuery();
                                    Field contentTypeField = customContentType.Fields.GetByInternalNameOrTitle(fieldRefAttributes["InternalName"].Value);
                                    clientContext.Load(contentTypeField);
                                    clientContext.ExecuteQuery();
                                    FieldLink customFieldLink = customFieldLinks.GetById(contentTypeField.Id);
                                    clientContext.Load(customFieldLink);
                                    clientContext.ExecuteQuery();
                                    customFieldLink.Required = Convert.ToBoolean(fieldRefAttributes["Required"].Value);
                                    customFieldLink.Hidden = Convert.ToBoolean(fieldRefAttributes["Hidden"].Value);
                                    customContentType.Update(true);
                                    clientContext.ExecuteQuery();
                                }
                            }
                        }
                    }
                }


            }
            //Add items to choice field
            Field newsCategory = clientContext.Site.RootWeb.Fields.GetByInternalNameOrTitle("UNSWIASSNewsCategory");
            FieldChoice fieldChoiceNews = clientContext.CastTo<FieldChoice>(newsCategory);
            clientContext.Load(fieldChoiceNews);
            clientContext.ExecuteQuery();
            List<string> optionsNewsItems = new List<string>(fieldChoiceNews.Choices);
            optionsNewsItems.Add("News");
            fieldChoiceNews.Choices = optionsNewsItems.ToArray();
            fieldChoiceNews.Update();
            clientContext.ExecuteQuery();

            Field eventCategory = clientContext.Site.RootWeb.Fields.GetByInternalNameOrTitle("UNSWIASSEventCategory");
            FieldChoice fieldChoiceEvents = clientContext.CastTo<FieldChoice>(eventCategory);
            clientContext.Load(fieldChoiceEvents);
            clientContext.ExecuteQuery();
            List<string> optionsEventsItems = new List<string>(fieldChoiceEvents.Choices);
            optionsEventsItems.Add("Public Event");
            fieldChoiceEvents.Choices = optionsEventsItems.ToArray();
            fieldChoiceEvents.Update();
            clientContext.ExecuteQuery();

            Field priorityCategory = clientContext.Site.RootWeb.Fields.GetByInternalNameOrTitle("UNSWIASSPriorityOrder");
            FieldChoice fieldChoicePriority = clientContext.CastTo<FieldChoice>(priorityCategory);
            clientContext.Load(fieldChoicePriority);
            clientContext.ExecuteQuery();
            fieldChoicePriority.Choices = "1;2;3;4;5;6;7;8;9;10;11;12;13;14;15;16;17;18;19;20".Split(";".ToCharArray());
            fieldChoicePriority.DefaultValue = "2";
            fieldChoicePriority.Update();
            clientContext.ExecuteQuery();

            Field portfolio = clientContext.Site.RootWeb.Fields.GetByInternalNameOrTitle("UNSWIASSPortfolio");
            FieldChoice fieldPortfolio = clientContext.CastTo<FieldChoice>(portfolio);
            clientContext.Load(fieldPortfolio);
            clientContext.ExecuteQuery();
            fieldPortfolio.Choices = "Administration;Dean's Office;Development;Education;Finance;Health & Safety;Human Resources;International;Information Technology;Marketing;Media;Research".Split(";".ToCharArray());
            fieldPortfolio.DefaultValue = "Administration";
            fieldPortfolio.Update();
            clientContext.ExecuteQuery();

            Field UnswUnit = clientContext.Site.RootWeb.Fields.GetByInternalNameOrTitle("UNSWIASSUnit");
            FieldChoice fieldUnit = clientContext.CastTo<FieldChoice>(UnswUnit);
            clientContext.Load(fieldUnit);
            clientContext.ExecuteQuery();
            List<string> optionsUnit = new List<string>(fieldUnit.Choices);
            optionsUnit.Add("Dean's Unit");
            fieldUnit.Choices = optionsUnit.ToArray();
            fieldUnit.Update();
            clientContext.ExecuteQuery();

            Console.WriteLine("Site column and Site content types created");
        }

        private static void UploadDocuments(ClientContext DestContext, XmlDocument xmlDoc)
        {
            try
            {
                List destMasterPageGallery = DestContext.Web.Lists.GetByTitle(Constants.UploadDocumentsDetails.Master_Page_Gallery);
                DestContext.Load(destMasterPageGallery);
                DestContext.ExecuteQuery();

                destMasterPageGallery.EnableFolderCreation = true;
                destMasterPageGallery.Update();
                DestContext.ExecuteQuery();

                var rootfolder = destMasterPageGallery.RootFolder;
                DestContext.Load(rootfolder);
                DestContext.ExecuteQuery();
                rootfolder = rootfolder.Folders.Add(Constants.UploadDocumentsDetails.UNSW);
                rootfolder.Folders.Add(Constants.UploadDocumentsDetails.PageLayouts);
                rootfolder.Folders.Add(Constants.UploadDocumentsDetails.DisplayTemplates);
                DestContext.ExecuteQuery();
                rootfolder = rootfolder.Folders.Add(Constants.UploadDocumentsDetails.PageLayouts);
                DestContext.Load(rootfolder);
                DestContext.ExecuteQuery();
                Int32 existingFilesCount = 0;
                FileCollection existingFiles;
                existingFiles = rootfolder.Files;
                DestContext.Load(existingFiles);
                DestContext.ExecuteQuery();
                existingFilesCount = rootfolder.Files.Count;

                XmlNodeList nodeList = xmlDoc.GetElementsByTagName("PageLayouts");
                var nodes = xmlDoc.GetElementsByTagName("PageLayout");
                if (nodes.Count != 0)
                {
                    for (int i = 0; i < nodes.Count; i++)
                    {
                        var xmlAttributeCollection = nodes[i].Attributes;
                        if (existingFilesCount == 0)
                        {
                            FileCreationInformation newFile = new FileCreationInformation();
                            string fileLoc = Constants.UploadDocumentsDetails.PageLayoutsPathInSolution + xmlAttributeCollection["Name"].Value.ToString();
                            newFile.Content = System.IO.File.ReadAllBytes(fileLoc);
                            newFile.Url = xmlAttributeCollection["Name"].Value.ToString();

                            Microsoft.SharePoint.Client.File uploadFile = rootfolder.Files.Add(newFile);
                            DestContext.Load(uploadFile);
                            DestContext.ExecuteQuery();
                            var currentFileTemp = rootfolder.Files.Where(file => file.Name == uploadFile.Name);
                            var currentFile = currentFileTemp.FirstOrDefault();
                            DestContext.Load(currentFile);
                            DestContext.ExecuteQuery();
                            ListItem currentDoc = currentFile.ListItemAllFields;
                            var parentCT = DestContext.Web.ContentTypes.GetById(xmlAttributeCollection["ParentContentTypeID"].Value.ToString());
                            DestContext.Load(parentCT);
                            DestContext.ExecuteQuery();
                            currentDoc["ContentTypeId"] = parentCT.StringId;
                            currentDoc["Title"] = xmlAttributeCollection["Title"].Value.ToString();
                            currentDoc["PublishingHidden"] = false;
                            currentDoc.Update();
                            DestContext.ExecuteQuery();
                            var associatedContentType = DestContext.Web.ContentTypes.GetById(xmlAttributeCollection["AssociatedContentTypeID"].Value.ToString());
                            DestContext.Load(associatedContentType);
                            DestContext.ExecuteQuery();
                            String contentTypeAssociativeID = null;
                            if (associatedContentType != null)
                            {
                                contentTypeAssociativeID = String.Format(";#{0};#{1};#", associatedContentType.Name, associatedContentType.StringId);
                            }
                            currentDoc["PublishingAssociatedContentType"] = contentTypeAssociativeID;
                            currentDoc.Update();
                            DestContext.ExecuteQuery();
                            currentFile.CheckOut();
                            currentFile.CheckIn("Updated with properties", CheckinType.MajorCheckIn);
                            currentFile.Publish("Updated with properties");
                            DestContext.ExecuteQuery();
                        }
                    }
                }
                rootfolder = destMasterPageGallery.RootFolder;
                DestContext.Load(rootfolder);
                DestContext.ExecuteQuery();
                rootfolder = rootfolder.Folders.Add(Constants.UploadDocumentsDetails.UNSW);
                rootfolder = rootfolder.Folders.Add(Constants.UploadDocumentsDetails.DisplayTemplates);
                DestContext.Load(rootfolder);
                DestContext.ExecuteQuery();
                existingFiles = rootfolder.Files;
                DestContext.Load(existingFiles);
                DestContext.ExecuteQuery();
                existingFilesCount = rootfolder.Files.Count;
                XmlNodeList DisplTemps = xmlDoc.GetElementsByTagName("DisplayTemplates");
                nodes = xmlDoc.GetElementsByTagName("DisplayTemplate");
                if (nodes.Count != 0)
                {
                    for (int i = 0; i < nodes.Count; i++)
                    {
                        var xmlAttributeCollection = nodes[i].Attributes;
                        if (existingFilesCount == 0)
                        {
                            string dispTempId = xmlAttributeCollection["ParentContentTypeID"].Value.ToString();
                            ContentType dispTempCCtyupe = DestContext.Web.AvailableContentTypes.GetById(dispTempId);
                            DestContext.Load(dispTempCCtyupe);
                            DestContext.ExecuteQuery();

                            Web webSite = DestContext.Web;
                            DestContext.Load(webSite);
                            PublishingWeb web = PublishingWeb.GetPublishingWeb(DestContext, webSite);
                            DestContext.Load(web);
                            List displayTempCT = DestContext.Site.RootWeb.Lists.GetByTitle("Master Page Gallery");
                            DestContext.Load(displayTempCT);
                            DestContext.ExecuteQuery();
                            displayTempCT.ContentTypesEnabled = true;
                            displayTempCT.EnableFolderCreation = true;
                            if (!displayTempCT.ContentTypeExistsById(dispTempId))
                            {
                                displayTempCT.ContentTypes.AddExistingContentType(dispTempCCtyupe);
                                displayTempCT.Update();
                            }
                            DestContext.ExecuteQuery();
                            FileCreationInformation newFile = new FileCreationInformation();
                            string fileLoc = Constants.UploadDocumentsDetails.DispTempPathInSolution + xmlAttributeCollection["Name"].Value.ToString();
                            newFile.Content = System.IO.File.ReadAllBytes(fileLoc);
                            newFile.Url = xmlAttributeCollection["Name"].Value.ToString();

                            Microsoft.SharePoint.Client.File uploadFile = rootfolder.Files.Add(newFile);
                            DestContext.Load(uploadFile);
                            DestContext.ExecuteQuery();
                            var currentFileTemp = rootfolder.Files.Where(file => file.Name == uploadFile.Name);
                            var currentFile = currentFileTemp.FirstOrDefault();
                            DestContext.Load(currentFile);
                            DestContext.ExecuteQuery();
                            ListItem currentDoc = currentFile.ListItemAllFields;
                            var parentCT = DestContext.Web.ContentTypes.GetById(xmlAttributeCollection["ParentContentTypeID"].Value.ToString());
                            DestContext.Load(parentCT);
                            DestContext.ExecuteQuery();
                            currentDoc["ContentTypeId"] = parentCT.StringId;
                            currentDoc["Title"] = xmlAttributeCollection["Title"].Value.ToString();
                            currentDoc.Update();
                            DestContext.ExecuteQuery();

                            currentFile.CheckOut();
                            currentFile.CheckIn("Updated with properties", CheckinType.MajorCheckIn);
                            currentFile.Publish("Updated with properties");
                            DestContext.ExecuteQuery();
                        }
                    }
                }
                rootfolder = destMasterPageGallery.RootFolder;
                DestContext.Load(rootfolder);
                DestContext.ExecuteQuery();
                rootfolder = rootfolder.Folders.Add(Constants.UploadDocumentsDetails.UNSW);
                rootfolder = rootfolder.Folders.Add(Constants.UploadDocumentsDetails.DisplayTemplates);
                rootfolder = rootfolder.Folders.Add(Constants.UploadDocumentsDetails.Refiners);
                DestContext.Load(rootfolder);
                DestContext.ExecuteQuery();
                existingFiles = rootfolder.Files;
                DestContext.Load(existingFiles);
                DestContext.ExecuteQuery();
                existingFilesCount = rootfolder.Files.Count;
                XmlNodeList refiners = xmlDoc.GetElementsByTagName("Refiners");
                nodes = xmlDoc.GetElementsByTagName("Refiner");
                if (nodes.Count != 0)
                {
                    for (int i = 0; i < nodes.Count; i++)
                    {
                        var xmlAttributeCollection = nodes[i].Attributes;
                        if (existingFilesCount == 0)
                        {
                            string dispTempId = xmlAttributeCollection["ParentContentTypeID"].Value.ToString();
                            ContentType dispTempCCtyupe = DestContext.Web.AvailableContentTypes.GetById(dispTempId);
                            DestContext.Load(dispTempCCtyupe);
                            DestContext.ExecuteQuery();

                            Web webSite = DestContext.Web;
                            DestContext.Load(webSite);
                            PublishingWeb web = PublishingWeb.GetPublishingWeb(DestContext, webSite);
                            DestContext.Load(web);
                            List displayTempCT = DestContext.Site.RootWeb.Lists.GetByTitle("Master Page Gallery");
                            DestContext.Load(displayTempCT);
                            DestContext.ExecuteQuery();
                            displayTempCT.ContentTypesEnabled = true;
                            displayTempCT.EnableFolderCreation = true;
                            if (!displayTempCT.ContentTypeExistsById(dispTempId))
                            {
                                displayTempCT.ContentTypes.AddExistingContentType(dispTempCCtyupe);
                                displayTempCT.Update();
                            }
                            DestContext.ExecuteQuery();
                            FileCreationInformation newFile = new FileCreationInformation();
                            string fileLoc = Constants.UploadDocumentsDetails.RefinersPathInSolution + Constants.UploadDocumentsDetails.UNSWIntranet_Filter_MultiValue;
                            newFile.Content = System.IO.File.ReadAllBytes(fileLoc);
                            newFile.Url = Constants.UploadDocumentsDetails.UNSWIntranet_Filter_MultiValue;

                            Microsoft.SharePoint.Client.File uploadFile = rootfolder.Files.Add(newFile);
                            DestContext.Load(uploadFile);
                            DestContext.ExecuteQuery();
                            var currentFileTemp = rootfolder.Files.Where(file => file.Name == uploadFile.Name);
                            var currentFile = currentFileTemp.FirstOrDefault();
                            DestContext.Load(currentFile);
                            DestContext.ExecuteQuery();
                            ListItem currentDoc = currentFile.ListItemAllFields;
                            var parentCT = DestContext.Web.ContentTypes.GetById(xmlAttributeCollection["ParentContentTypeID"].Value.ToString());
                            DestContext.Load(parentCT);
                            DestContext.ExecuteQuery();
                            currentDoc["ContentTypeId"] = parentCT.StringId;
                            currentDoc["Title"] = xmlAttributeCollection["Title"].Value.ToString();
                            currentDoc.Update();
                            DestContext.ExecuteQuery();
                            currentFile.CheckOut();
                            currentFile.CheckIn("Updated with properties", CheckinType.MajorCheckIn);
                            currentFile.Publish("Updated with properties");
                            DestContext.ExecuteQuery();
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
            }
        }

        private static void ManageTermStore(Uri configSiteUrl)
        {
            string newSite_realm = TokenHelper.GetRealmFromTargetUrl(configSiteUrl);

            //Get the access token of the required site collection.
            string adminAccessToken = TokenHelper.GetAppOnlyAccessToken(TokenHelper.SharePointPrincipal, configSiteUrl.Authority, newSite_realm).AccessToken;
            try
            {
                using (ClientContext siteContext = TokenHelper.GetClientContextWithAccessToken(configSiteUrl.OriginalString, adminAccessToken))
                {
                    Site site = siteContext.Site;
                    siteContext.Load(site, s => s.RootWeb.Title);
                    siteContext.ExecuteQuery();

                    TaxonomySession taxonomySession = TaxonomySession.GetTaxonomySession(siteContext);
                    siteContext.Load(taxonomySession, ts => ts.TermStores);
                    siteContext.ExecuteQuery();

                    TermStore termStore = taxonomySession.GetDefaultSiteCollectionTermStore();
                    siteContext.Load(termStore, ts => ts.Groups);
                    siteContext.ExecuteQuery();

                    if (termStore != null)
                    {
                        TermGroup termGroup = termStore.GetSiteCollectionGroup(site, true);
                        termGroup.Context.Load(termGroup);
                        termGroup.Context.ExecuteQuery();

                        // Update the default term group name with site title
                        UpdateTermGroup(termGroup, site.RootWeb.Title);

                        //Create a new term set
                        CreateTermSet(termGroup, site.RootWeb.Title);

                    }

                }

            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
                Console.ReadLine();
            }
        }

        private static void CreateUNSWEventCalendar(ClientContext context)
        {
            try
            {
                Web siteWeb = context.Web;
                ListCreationInformation creationInfo = new ListCreationInformation();
                creationInfo.Title = "UNSW Events";
                creationInfo.TemplateType = 106;
                List list = siteWeb.Lists.Add(creationInfo);
                list.Description = "This list is used to create calendar events";
                list.Update();
                context.ExecuteQuery();

                string contentTypeId = "0x010200C9BCB83A9961564E9FD140022919CA36";
                ContentType ctype = context.Web.AvailableContentTypes.GetById(contentTypeId);
                context.Load(ctype);
                context.ExecuteQuery();

                //Add UNSW Events content type
                List eventsList = context.Web.GetListByTitle("UNSW Events");
                context.Load(eventsList);
                context.ExecuteQuery();
                eventsList.ContentTypesEnabled = true;
                if (!eventsList.ContentTypeExistsById(contentTypeId))
                {
                    eventsList.ContentTypes.AddExistingContentType(ctype);
                    eventsList.SetDefaultContentTypeToList(ctype);
                    eventsList.Update();
                }
                context.ExecuteQuery();

                //Create dummy item
                FieldUrlValue url = new FieldUrlValue();
                url.Url = "https://www.unsw.edu.au/";
                url.Description = "";

                ListItemCreationInformation createItem = new ListItemCreationInformation();
                ListItem item = eventsList.AddItem(createItem);
                item["ContentTypeId"] = contentTypeId;
                item["Title"] = "Loreum ipsum";
                item["Location"] = "Loreum ipsum";
                item["UNSWIASSEventDescription"] = "This is a sample event. You may delete it.";
                item["UNSWIASSHyperlink"] = url;
                item["UNSWIASSEventCategory"] = "Public Event";

                item.Update();
                context.ExecuteQuery();
            }
            catch (Exception ex)
            {
                CreateException(ex, exClient);
                Console.WriteLine(ex.Message);
            }
        }

        private static void CreateStaffContactsList(ClientContext context)
        {
            try
            {
                Web siteWeb = context.Web;
                ListCreationInformation creationInfo = new ListCreationInformation();
                creationInfo.Title = "Staff";
                creationInfo.TemplateType = 105;
                List list = siteWeb.Lists.Add(creationInfo);
                list.Description = "This list is used to create staff contacts";
                list.Update();
                context.ExecuteQuery();

                //Enable content types
                context.ExecuteQuery();
                List staffList = context.Web.GetListByTitle("Staff");
                context.Load(staffList);
                context.ExecuteQuery();
                staffList.ContentTypesEnabled = true;
                staffList.Update();
                context.ExecuteQuery();

                //Add existing columns to the list
                Field lastName = context.Site.RootWeb.Fields.GetByInternalNameOrTitle("UNSWIASSLastName");
                context.Load(lastName);
                context.ExecuteQuery();
                staffList.Fields.Add(lastName);
                staffList.Update();
                context.ExecuteQuery();

                Field staffPhoto = context.Site.RootWeb.Fields.GetByInternalNameOrTitle("UNSWIASSStaffPhoto");
                context.Load(staffPhoto);
                context.ExecuteQuery();
                staffList.Fields.Add(staffPhoto);
                staffList.Update();
                context.ExecuteQuery();

                Field phone = context.Site.RootWeb.Fields.GetByInternalNameOrTitle("UNSWIASSPhone");
                context.Load(phone);
                context.ExecuteQuery();
                staffList.Fields.Add(phone);
                staffList.Update();
                context.ExecuteQuery();

                Field UNSWportfolio = context.Site.RootWeb.Fields.GetByInternalNameOrTitle("UNSWIASSPortfolio");
                context.Load(UNSWportfolio);
                context.ExecuteQuery();
                staffList.Fields.Add(UNSWportfolio);
                staffList.Update();
                context.ExecuteQuery();

                //Create new columns for the list
                staffList.Fields.AddFieldAsXml("<Field DisplayName='Role' Type='Note' />", true, AddFieldOptions.AddFieldInternalNameHint);
                string formula = "<Formula>=[First Name]&amp;\" \"&amp;UNSWIASSLastName</Formula>" +
                    "<FieldRefs>" +
                        "<FieldRef Name='FirstName' />" +
                        "<FieldRef Name='UNSWIASSLastName' />" +
                    "</FieldRefs>";
                context.Site.RootWeb.Fields.AddFieldAsXml("<Field DisplayName='Staff Name' Name='UNSWIASSName' StaticName='UNSWIASSName' Type='Calculated' ResultType='Text'>" + formula + "</Field>", true, AddFieldOptions.AddFieldInternalNameHint);
                context.ExecuteQuery();

                Field staffName = context.Site.RootWeb.Fields.GetByInternalNameOrTitle("UNSWIASSName");
                context.Load(staffName);
                context.ExecuteQuery();
                staffList.Fields.Add(staffName);
                staffList.Update();
                context.ExecuteQuery();

                //Create dummy item
                ListItemCreationInformation createItem = new ListItemCreationInformation();
                ListItem item = staffList.AddItem(createItem);

                item["Title"] = "Loreum ipsum";
                item["FirstName"] = "Loreum ipsum";
                item["UNSWIASSLastName"] = "Loreum ipsum";
                item["UNSWIASSPhone"] = "1234567890";
                item["UNSWIASSPortfolio"] = "Administration";
                item["Role"] = "Administration";
                var imageUrl = context.Url + "/Style Library/Branding/Images/HomeNewsFeatures.png";
                item["UNSWIASSStaffPhoto"] = "<img src='" + imageUrl + "'>";
                item.Update();
                context.ExecuteQuery();
            }
            catch (Exception ex)
            {
                CreateException(ex, exClient);
                Console.WriteLine(ex.Message);
            }
        }

        /// <summary>
        /// Method to update the term group name with site name
        /// </summary>
        /// <param name="termGroup"><see cref="TermGroup"/></param>
        /// <param name="termGrpupName">Term group Name</param>
        private static void UpdateTermGroup(TermGroup termGroup, string termGrpupName)
        {

            if (!string.Equals(termGroup.Name, termGrpupName, StringComparison.InvariantCultureIgnoreCase))
            {
                termGroup.Name = termGrpupName;
                termGroup.Context.Load(termGroup);
                termGroup.Context.ExecuteQuery();
            }
            else
            {
                Console.WriteLine($"Term Group: {termGrpupName} is already updated with site title name");
            }
        }

        /// <summary>
        /// Method to create a new term set name
        /// </summary>
        /// <param name="termGroup"><see cref="TermGroup"/></param>
        /// <param name="siteTitle">Site title name. </param>
        private static void CreateTermSet(TermGroup termGroup, string siteTitle)
        {

            // New term set name
            string newTermSetName = siteTitle + GetConfigValues(Constants.CreateTermSetDetails.NewTermSetName).ToString();

            // New Term Set LCID 
            int lcid = Convert.ToInt32(GetConfigValues(Constants.CreateTermSetDetails.NewTermSetLCID));

            // Term set GUID
            Guid guid = Guid.NewGuid();

            // Code to check if term set is already exist or not.
            TermSetCollection termSetCollection = termGroup.TermSets;
            termSetCollection.Context.Load(termSetCollection, tscol => tscol.Include(ts => ts.Name).Where(ts => (ts.Name == newTermSetName)));
            termSetCollection.Context.ExecuteQuery();

            if (termSetCollection.Count == 0)
            {
                TermSet newTermSet = termGroup.CreateTermSet(newTermSetName, guid, lcid);
                newTermSet.Context.Load(newTermSet);
                newTermSet.Context.ExecuteQuery();

            }
            else
            {
                Console.WriteLine($"Termset: {newTermSetName} is already created");
            }
            /// tClient2.Flush();

        }

        private static void CreateImageRenditions(ClientContext clientContext)
        {
            try
            {
                List displayTempCT = clientContext.Site.RootWeb.Lists.GetByTitle(Constants.UploadDocumentsDetails.Master_Page_Gallery);
                clientContext.Load(displayTempCT);
                clientContext.ExecuteQuery();
                var rootfolder = displayTempCT.RootFolder;
                clientContext.Load(rootfolder);
                clientContext.Load(rootfolder.Files);
                clientContext.ExecuteQuery();

                var currentFileTemp = rootfolder.Files.Where(file1 => file1.Name == Constants.ImageRenditionsDetails.ImageRenditionFileName);
                var currentFile = currentFileTemp.FirstOrDefault();
                clientContext.Load(currentFile);
                clientContext.ExecuteQuery();
                currentFile.DeleteObject();
                clientContext.ExecuteQuery();
                FileCreationInformation newFile = new FileCreationInformation();
                string fileLoc = Constants.ImageRenditionsDetails.ImageRenditionFilePath + Constants.ImageRenditionsDetails.ImageRenditionFileName;
                newFile.Content = System.IO.File.ReadAllBytes(fileLoc);
                newFile.Url = Constants.ImageRenditionsDetails.ImageRenditionFileName;

                Microsoft.SharePoint.Client.File uploadFile = rootfolder.Files.Add(newFile);
                uploadFile.Publish("Updated with custom image renditions");
                clientContext.Load(uploadFile);
                clientContext.ExecuteQuery();
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
            }
        }

        private static void CreatePropertyBags(ListItem siteReq, ClientContext clientContext)
        {
            Web web = clientContext.Site.RootWeb;
            var allProperties = web.AllProperties;
            if (siteReq["UNSWDivision"] != null)
            {
                allProperties["UNSW DIVISION/FACULTY"] = siteReq["UNSWDivision"].ToString();
                Console.WriteLine("UNSW DIVISION/FACULTY Property created");
            }
            if (siteReq["UNSWSchool"] != null)
            {
                allProperties["UNSW School"] = siteReq["UNSWSchool"].ToString();
                Console.WriteLine("UNSW School Property created");
            }
            if (siteReq["UNSWOrganization"] != null)
            {
                allProperties["UNSW Organization"] = siteReq["UNSWOrganization"].ToString();
                Console.WriteLine("UNSW Organization Property created");
            }
            web.Update();
            clientContext.ExecuteQuery();
        }

        #region IAAS branding code

        private static void UploadFiles(ClientContext DestContext, XmlDocument xmlDoc)
        {
            try
            {
                List destMasterPageGallery = DestContext.Web.Lists.GetByTitle(Constants.UploadDocumentsDetails.styleLibrary);
                DestContext.Load(destMasterPageGallery);
                DestContext.ExecuteQuery();

                destMasterPageGallery.EnableFolderCreation = true;
                destMasterPageGallery.Update();
                DestContext.ExecuteQuery();

                var rootfolder = destMasterPageGallery.RootFolder;
                DestContext.Load(rootfolder);
                DestContext.ExecuteQuery();
                rootfolder = rootfolder.Folders.Add("Branding");
                DestContext.ExecuteQuery();
                DestContext.Load(rootfolder);
                DestContext.ExecuteQuery();
                Folder cssFolder = rootfolder.Folders.Add("CSS");
                DestContext.Load(cssFolder);
                DestContext.ExecuteQuery();
                XmlNodeList nodeList = xmlDoc.GetElementsByTagName("cssFiles");
                var nodes = xmlDoc.GetElementsByTagName("cssFile");
                Int32 existingFilesCount = 0;
                FileCollection existingFiles;
                existingFiles = cssFolder.Files;
                DestContext.Load(existingFiles);
                DestContext.ExecuteQuery();
                existingFilesCount = cssFolder.Files.Count;
                if (existingFilesCount == 0)
                {
                    if (nodes.Count != 0)
                    {
                        for (int i = 0; i < nodes.Count; i++)
                        {
                            var xmlAttributeCollection = nodes[i].Attributes;
                            FileCreationInformation newFile = new FileCreationInformation();
                            string fileLoc = Constants.UploadDocumentsDetails.CSSPathInSolution + xmlAttributeCollection["Name"].Value.ToString();
                            newFile.Content = System.IO.File.ReadAllBytes(fileLoc);
                            newFile.Url = xmlAttributeCollection["Name"].Value.ToString();

                            Microsoft.SharePoint.Client.File uploadFile = cssFolder.Files.Add(newFile);
                            DestContext.Load(uploadFile);
                            DestContext.ExecuteQuery();
                            var currentFileTemp = cssFolder.Files.Where(file => file.Name == uploadFile.Name);
                            var currentFile = currentFileTemp.FirstOrDefault();
                            DestContext.Load(currentFile);
                            DestContext.ExecuteQuery();

                            currentFile.CheckIn("Updated with properties", CheckinType.MajorCheckIn);
                            currentFile.Publish("Updated with properties");
                            DestContext.ExecuteQuery();

                        }
                    }
                }

                Folder fontFolder = rootfolder.Folders.Add("fonts");
                DestContext.Load(fontFolder);
                DestContext.ExecuteQuery();
                existingFiles = fontFolder.Files;
                DestContext.Load(existingFiles);
                DestContext.ExecuteQuery();
                existingFilesCount = fontFolder.Files.Count;
                if (existingFilesCount == 0)
                {
                    XmlNodeList fontFiles = xmlDoc.GetElementsByTagName("fontFiles");
                    nodes = xmlDoc.GetElementsByTagName("fontFile");
                    if (nodes.Count != 0)
                    {
                        for (int i = 0; i < nodes.Count; i++)
                        {
                            var xmlAttributeCollection = nodes[i].Attributes;
                            FileCreationInformation newFile = new FileCreationInformation();
                            string fileLoc = Constants.UploadDocumentsDetails.fontsPathInSolution + xmlAttributeCollection["Name"].Value.ToString();
                            newFile.Content = System.IO.File.ReadAllBytes(fileLoc);
                            newFile.Url = xmlAttributeCollection["Name"].Value.ToString();

                            Microsoft.SharePoint.Client.File uploadFile = fontFolder.Files.Add(newFile);
                            DestContext.Load(uploadFile);
                            DestContext.ExecuteQuery();
                            var currentFileTemp = fontFolder.Files.Where(file => file.Name == uploadFile.Name);
                            var currentFile = currentFileTemp.FirstOrDefault();
                            DestContext.Load(currentFile);
                            DestContext.ExecuteQuery();

                            currentFile.CheckIn("Updated with properties", CheckinType.MajorCheckIn);
                            currentFile.Publish("Updated with properties");
                            DestContext.ExecuteQuery();
                        }
                    }
                }
                Folder imagesFolder = rootfolder.Folders.Add("Images");
                DestContext.Load(imagesFolder);
                DestContext.ExecuteQuery();
                existingFiles = imagesFolder.Files;
                DestContext.Load(existingFiles);
                DestContext.ExecuteQuery();
                existingFilesCount = imagesFolder.Files.Count;
                if (existingFilesCount == 0)
                {
                    XmlNodeList imageFiles = xmlDoc.GetElementsByTagName("Images");
                    nodes = xmlDoc.GetElementsByTagName("Image");
                    if (nodes.Count != 0)
                    {
                        for (int i = 0; i < nodes.Count; i++)
                        {
                            var xmlAttributeCollection = nodes[i].Attributes;
                            FileCreationInformation newFile = new FileCreationInformation();
                            string fileLoc = Constants.UploadDocumentsDetails.ImagesPathInSolution + xmlAttributeCollection["Name"].Value.ToString();
                            newFile.Content = System.IO.File.ReadAllBytes(fileLoc);
                            newFile.Url = xmlAttributeCollection["Name"].Value.ToString();

                            Microsoft.SharePoint.Client.File uploadFile = imagesFolder.Files.Add(newFile);
                            DestContext.Load(uploadFile);
                            DestContext.ExecuteQuery();
                            var currentFileTemp = imagesFolder.Files.Where(file => file.Name == uploadFile.Name);
                            var currentFile = currentFileTemp.FirstOrDefault();
                            DestContext.Load(currentFile);
                            DestContext.ExecuteQuery();

                            currentFile.CheckIn("Updated with properties", CheckinType.MajorCheckIn);
                            currentFile.Publish("Updated with properties");
                            DestContext.ExecuteQuery();
                        }
                    }
                }
                Folder scriptsFolder = rootfolder.Folders.Add("Scripts");
                DestContext.Load(scriptsFolder);
                DestContext.ExecuteQuery();
                existingFiles = scriptsFolder.Files;
                DestContext.Load(existingFiles);
                DestContext.ExecuteQuery();
                existingFilesCount = scriptsFolder.Files.Count;
                if (existingFilesCount == 0)
                {
                    XmlNodeList scriptFiles = xmlDoc.GetElementsByTagName("Scripts");
                    nodes = xmlDoc.GetElementsByTagName("Script");
                    if (nodes.Count != 0)
                    {
                        for (int i = 0; i < nodes.Count; i++)
                        {
                            var xmlAttributeCollection = nodes[i].Attributes;

                            FileCreationInformation newFile = new FileCreationInformation();
                            string fileLoc = Constants.UploadDocumentsDetails.ScriptsPathInSolution + xmlAttributeCollection["Name"].Value.ToString();
                            newFile.Content = System.IO.File.ReadAllBytes(fileLoc);
                            newFile.Url = xmlAttributeCollection["Name"].Value.ToString();

                            Microsoft.SharePoint.Client.File uploadFile = scriptsFolder.Files.Add(newFile);
                            DestContext.Load(uploadFile);
                            DestContext.ExecuteQuery();
                            var currentFileTemp = scriptsFolder.Files.Where(file => file.Name == uploadFile.Name);
                            var currentFile = currentFileTemp.FirstOrDefault();
                            DestContext.Load(currentFile);
                            DestContext.ExecuteQuery();

                            currentFile.CheckIn("Updated with properties", CheckinType.MajorCheckIn);
                            currentFile.Publish("Updated with properties");
                            DestContext.ExecuteQuery();
                        }
                    }
                }

                //Code to upload Responsive UI files
                rootfolder = destMasterPageGallery.RootFolder;
                Folder responsiveUIFilesFolder = rootfolder.Folders.Add("ResponsiveUI");
                DestContext.Load(responsiveUIFilesFolder);
                DestContext.ExecuteQuery();
                existingFiles = responsiveUIFilesFolder.Files;
                DestContext.Load(existingFiles);
                DestContext.ExecuteQuery();
                existingFilesCount = responsiveUIFilesFolder.Files.Count;
                if (existingFilesCount == 0)
                {
                    XmlNodeList responsiveUIFiles = xmlDoc.GetElementsByTagName("ResponsiveUIFiles");
                    nodes = xmlDoc.GetElementsByTagName("ResponsiveUIFile");
                    if (nodes.Count != 0)
                    {
                        for (int i = 0; i < nodes.Count; i++)
                        {
                            var xmlAttributeCollection = nodes[i].Attributes;

                            FileCreationInformation newFile = new FileCreationInformation();
                            string fileLoc = Constants.UploadDocumentsDetails.ResponsiceUIFilesPathInSolution + xmlAttributeCollection["Name"].Value.ToString();
                            newFile.Content = System.IO.File.ReadAllBytes(fileLoc);
                            newFile.Url = xmlAttributeCollection["Name"].Value.ToString();

                            Microsoft.SharePoint.Client.File uploadFile = responsiveUIFilesFolder.Files.Add(newFile);
                            DestContext.Load(uploadFile);
                            DestContext.ExecuteQuery();
                            var currentFileTemp = responsiveUIFilesFolder.Files.Where(file => file.Name == uploadFile.Name);
                            var currentFile = currentFileTemp.FirstOrDefault();
                            DestContext.Load(currentFile);
                            DestContext.ExecuteQuery();

                            currentFile.CheckIn("Updated with properties", CheckinType.MajorCheckIn);
                            currentFile.Publish("Updated with properties");
                            DestContext.ExecuteQuery();
                        }
                    }
                }
                //Create Staff folder in Images library
                List imagesList = DestContext.Web.Lists.GetByTitle("Images");
                DestContext.Load(imagesList);
                DestContext.ExecuteQuery();

                imagesList.EnableFolderCreation = true;
                imagesList.Update();
                DestContext.ExecuteQuery();

                var folder = imagesList.RootFolder;
                DestContext.Load(folder);
                DestContext.ExecuteQuery();
                folder = folder.Folders.Add("Staff");
                DestContext.ExecuteQuery();

                //Upload placeholder image
                existingFiles = folder.Files;
                DestContext.Load(existingFiles);
                DestContext.ExecuteQuery();
                existingFilesCount = folder.Files.Count;
                if (existingFilesCount == 0)
                {
                    XmlNodeList imageFiles = xmlDoc.GetElementsByTagName("PlaceholderImage");
                    nodes = xmlDoc.GetElementsByTagName("Image");
                    if (nodes.Count != 0)
                    {
                        for (int i = 0; i < nodes.Count; i++)
                        {
                            var xmlAttributeCollection = nodes[i].Attributes;
                            FileCreationInformation newFile = new FileCreationInformation();
                            string fileLoc = Constants.UploadDocumentsDetails.ImagesPathInSolution + xmlAttributeCollection["Name"].Value.ToString();
                            newFile.Content = System.IO.File.ReadAllBytes(fileLoc);
                            newFile.Url = xmlAttributeCollection["Name"].Value.ToString();

                            Microsoft.SharePoint.Client.File uploadFile = folder.Files.Add(newFile);
                            DestContext.Load(uploadFile);
                            DestContext.ExecuteQuery();
                            var currentFileTemp = folder.Files.Where(file => file.Name == uploadFile.Name);
                            var currentFile = currentFileTemp.FirstOrDefault();
                            DestContext.Load(currentFile);
                            DestContext.ExecuteQuery();

                            //currentFile.CheckIn("Updated with properties", CheckinType.MajorCheckIn);
                            //currentFile.Publish("Updated with properties");
                            //DestContext.ExecuteQuery();
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                CreateException(ex, exClient);
                Console.WriteLine(ex.Message);
            }
        }
        public static void CreateHomePageAndFooterList(string newSiteUrl)
        {
            Uri configSiteUrl = new Uri(newSiteUrl);
            string realm = TokenHelper.GetRealmFromTargetUrl(configSiteUrl);

            //Get the access token of the required site collection.
            string siteAdminAccessToken = TokenHelper.GetAppOnlyAccessToken(TokenHelper.SharePointPrincipal, configSiteUrl.Authority, realm).AccessToken;
            try
            {
                using (ClientContext siteContext = TokenHelper.GetClientContextWithAccessToken(configSiteUrl.OriginalString, siteAdminAccessToken))
                {
                    //Get News content type by id
                    string contentTypeId = Constants.ColumnNContentTypesDetails.NewsContentTypeID;
                    ContentType ctype = siteContext.Web.AvailableContentTypes.GetById(contentTypeId);
                    siteContext.Load(ctype);
                    siteContext.ExecuteQuery();

                    Web webSite = siteContext.Web;
                    siteContext.Load(webSite);
                    PublishingWeb web = PublishingWeb.GetPublishingWeb(siteContext, webSite);
                    siteContext.Load(web);
                    List pages = siteContext.Site.RootWeb.Lists.GetByTitle("Pages");
                    siteContext.Load(pages);
                    siteContext.ExecuteQuery();
                    pages.ContentTypesEnabled = true;
                    pages.EnableFolderCreation = true;
                    if (!pages.ContentTypeExistsById(contentTypeId))
                    {
                        pages.ContentTypes.AddExistingContentType(ctype);
                        pages.Update();
                    }
                    siteContext.ExecuteQuery();
                    ListItemCollection existingPages = pages.GetItems(CamlQuery.CreateAllItemsQuery());
                    siteContext.Load(existingPages, items => items.Include(item => item.DisplayName).Where(obj => obj.DisplayName == "Home"));
                    siteContext.ExecuteQuery();
                    if (existingPages != null && existingPages.Count > 0)
                    { }
                    else
                    {
                        List publishingLayouts = siteContext.Site.RootWeb.Lists.GetByTitle("Master Page Gallery");
                        ListItemCollection allItems = publishingLayouts.GetItems(CamlQuery.CreateAllItemsQuery());
                        siteContext.Load(allItems, items => items.Include(item => item.DisplayName).Where(obj => obj.DisplayName == "UNSWIntranetHome"));
                        siteContext.ExecuteQuery();
                        ListItem layout = allItems.Where(x => x.DisplayName == "UNSWIntranetHome").FirstOrDefault();
                        siteContext.Load(layout);
                        PublishingPageInformation publishingPageInfo = new PublishingPageInformation();
                        publishingPageInfo.Name = "Home.aspx";
                        publishingPageInfo.PageLayoutListItem = layout;
                        PublishingPage publishingPage = web.AddPublishingPage(publishingPageInfo);
                        publishingPage.ListItem.File.CheckIn(string.Empty, CheckinType.MajorCheckIn);
                        publishingPage.ListItem.File.Publish(string.Empty);
                        //publishingPage.ListItem.File.Approve(string.Empty);
                        siteContext.Load(publishingPage);
                        siteContext.Load(publishingPage.ListItem.File, obj => obj.ServerRelativeUrl);
                        siteContext.ExecuteQuery();

                        //Set welcome page
                        var rootFolder = siteContext.Web.RootFolder;
                        siteContext.Load(rootFolder);
                        siteContext.ExecuteQuery();
                        rootFolder.WelcomePage = "Pages/Home.aspx";
                        rootFolder.Update();
                        siteContext.ExecuteQuery();
                    }

                    CreateFolder(siteContext, "Search", pages);
                    CreateFolder(siteContext, "News", pages);
                    CreateFolder(siteContext, "Events", pages);
                    //CreateFolder(siteContext, "Faculty-Resources", pages);
                    //CreateFolder(siteContext, "Have-Your-Say", pages);

                    CreateSearchPage(siteContext, pages);
                    CreateStaffSearchPage(siteContext, pages);
                    CreateNewsPage(siteContext, pages);

                    //Create FooterContent List
                    Web siteWeb = siteContext.Web;
                    ListCreationInformation creationInfo = new ListCreationInformation();
                    creationInfo.Title = "FooterContent";
                    creationInfo.TemplateType = 100;
                    List list = siteWeb.Lists.Add(creationInfo);
                    list.Description = "This list is used to create Footer Content";
                    list.Update();
                    siteContext.ExecuteQuery();
                    List newlist = siteWeb.Lists.GetByTitle("FooterContent");
                    siteContext.Load(newlist);
                    siteContext.ExecuteQuery();
                    string schemaMultilineTextField = "<Field ID='{C1DD4D26-3B37-46D2-B07E-A6531A536B4A}' Type='Note' Name='Content' StaticName='Content' DisplayName = 'Content' NumLines = '15' RichText = 'FALSE' Sortable = 'FALSE' />";
                    Field multilineTextField = newlist.Fields.AddFieldAsXml(schemaMultilineTextField, true, AddFieldOptions.AddFieldInternalNameHint);
                    newlist.Update();
                    siteContext.ExecuteQuery();
                }
            }
            catch (Exception ex)
            {
                CreateException(ex, exClient);
                Console.WriteLine(ex.Message);
            }

        }

        private static void CreateFolder(ClientContext context, string folderName, List pagesList)
        {
            try
            {
                ListItemCollection existingItems = pagesList.GetItems(CamlQuery.CreateAllItemsQuery());
                context.Load(existingItems, itms => itms.Include(itm => itm.DisplayName).Where(obj => obj.DisplayName == folderName));
                context.ExecuteQuery();
                if (existingItems.Count == 0)
                {
                    ListItemCreationInformation createItem = new ListItemCreationInformation();
                    createItem.UnderlyingObjectType = FileSystemObjectType.Folder;
                    createItem.LeafName = folderName;
                    ListItem lstItem = pagesList.AddItem(createItem);
                    lstItem["Title"] = folderName;
                    lstItem.Update();
                    context.ExecuteQuery();
                }
            }
            catch (Exception ex)
            {
                CreateException(ex, exClient);
                Console.WriteLine(ex.Message);
            }
        }

        private static void CreateSearchPage(ClientContext context, List pagesList)
        {
            try
            {
                //Check if page exists. If not, then create
                context.Load(context.Web, url => url.ServerRelativeUrl);
                context.ExecuteQuery();
                Microsoft.SharePoint.Client.File file = context.Web.GetFileByServerRelativeUrl(context.Web.ServerRelativeUrl + "/Pages/Search/Default.aspx");
                context.Load(file, fe => fe.Exists);
                context.ExecuteQuery();
                if (!file.Exists)
                {
                    Web webSite = context.Web;
                    context.Load(webSite);
                    PublishingWeb web = PublishingWeb.GetPublishingWeb(context, webSite);
                    context.Load(web);

                    Folder searchFolder = webSite.GetFolderByServerRelativeUrl(pagesList.ParentWebUrl + "/Pages/Search");
                    context.Load(searchFolder);
                    context.ExecuteQuery();

                    List publishingLayouts = context.Site.RootWeb.Lists.GetByTitle("Master Page Gallery");
                    ListItemCollection allItems = publishingLayouts.GetItems(CamlQuery.CreateAllItemsQuery());
                    context.Load(allItems, items => items.Include(item => item.DisplayName).Where(obj => obj.DisplayName == "BlankWebPartPage"));
                    context.ExecuteQuery();
                    ListItem layout = allItems.Where(x => x.DisplayName == "BlankWebPartPage").FirstOrDefault();
                    context.Load(layout);
                    PublishingPageInformation publishingPageInfo = new PublishingPageInformation();
                    publishingPageInfo.Name = "Default.aspx";
                    publishingPageInfo.Folder = searchFolder;
                    publishingPageInfo.PageLayoutListItem = layout;
                    PublishingPage publishingPage = web.AddPublishingPage(publishingPageInfo);
                    publishingPage.ListItem.File.CheckIn(string.Empty, CheckinType.MajorCheckIn);
                    publishingPage.ListItem.File.Publish(string.Empty);
                    //publishingPage.ListItem.File.Approve(string.Empty);
                    context.Load(publishingPage);
                    context.Load(publishingPage.ListItem.File, obj => obj.ServerRelativeUrl);
                    context.ExecuteQuery();
                }
            }
            catch (Exception ex)
            {
                CreateException(ex, exClient);
                Console.WriteLine(ex.Message);
            }
        }
        private static void CreateStaffSearchPage(ClientContext context, List pagesList)
        {
            try
            {
                //Check if page exists. If not, then create
                context.Load(context.Web, url => url.ServerRelativeUrl);
                context.ExecuteQuery();
                Microsoft.SharePoint.Client.File file = context.Web.GetFileByServerRelativeUrl(context.Web.ServerRelativeUrl + "/Pages/Search/Staff.aspx");
                context.Load(file, fe => fe.Exists);
                context.ExecuteQuery();
                if (!file.Exists)
                {
                    Web webSite = context.Web;
                    context.Load(webSite);
                    PublishingWeb web = PublishingWeb.GetPublishingWeb(context, webSite);
                    context.Load(web);

                    Folder searchFolder = webSite.GetFolderByServerRelativeUrl(pagesList.ParentWebUrl + "/Pages/Search");
                    context.Load(searchFolder);
                    context.ExecuteQuery();

                    List publishingLayouts = context.Site.RootWeb.Lists.GetByTitle("Master Page Gallery");
                    ListItemCollection allItems = publishingLayouts.GetItems(CamlQuery.CreateAllItemsQuery());
                    context.Load(allItems, items => items.Include(item => item.DisplayName).Where(obj => obj.DisplayName == "BlankWebPartPage"));
                    context.ExecuteQuery();
                    ListItem layout = allItems.Where(x => x.DisplayName == "BlankWebPartPage").FirstOrDefault();
                    context.Load(layout);
                    PublishingPageInformation publishingPageInfo = new PublishingPageInformation();
                    publishingPageInfo.Name = "Staff.aspx";
                    publishingPageInfo.Folder = searchFolder;
                    publishingPageInfo.PageLayoutListItem = layout;
                    PublishingPage publishingPage = web.AddPublishingPage(publishingPageInfo);
                    publishingPage.ListItem.File.CheckIn(string.Empty, CheckinType.MajorCheckIn);
                    publishingPage.ListItem.File.Publish(string.Empty);
                    //publishingPage.ListItem.File.Approve(string.Empty);
                    context.Load(publishingPage);
                    context.Load(publishingPage.ListItem.File, obj => obj.ServerRelativeUrl);
                    context.ExecuteQuery();
                }
            }
            catch (Exception ex)
            {
                CreateException(ex, exClient);
                Console.WriteLine(ex.Message);
            }
        }
        private static void CreateNewsPage(ClientContext context, List pagesList)
        {
            try
            {
                //Check if page exists. If not, then create
                context.Load(context.Web, sUrl => sUrl.ServerRelativeUrl);
                context.ExecuteQuery();
                Microsoft.SharePoint.Client.File file = context.Web.GetFileByServerRelativeUrl(context.Web.ServerRelativeUrl + "/Pages/News/News.aspx");
                context.Load(file, fe => fe.Exists);
                context.ExecuteQuery();
                if (!file.Exists)
                {
                    Web webSite = context.Web;
                    context.Load(webSite);
                    PublishingWeb web = PublishingWeb.GetPublishingWeb(context, webSite);
                    context.Load(web);

                    Folder newsFolder = webSite.GetFolderByServerRelativeUrl(pagesList.ParentWebUrl + "/Pages/News");
                    context.Load(newsFolder);
                    context.ExecuteQuery();


                    //List publishingLayouts = context.Site.RootWeb.Lists.GetByTitle("Master Page Gallery");
                    //ListItemCollection allItems = publishingLayouts.GetItems(CamlQuery.CreateAllItemsQuery());
                    //context.Load(allItems, items => items.Include(item => item.DisplayName).Where(obj => obj.DisplayName == "BlankWebPartPage"));
                    //context.ExecuteQuery();
                    //ListItem layout = allItems.Where(x => x.DisplayName == "BlankWebPartPage").FirstOrDefault();
                    //context.Load(layout);
                    //PublishingPageInformation publishingPageInfo = new PublishingPageInformation();
                    //publishingPageInfo.Name = "News.aspx";
                    //publishingPageInfo.Folder = newsFolder;
                    //publishingPageInfo.PageLayoutListItem = layout;
                    //PublishingPage publishingPage = web.AddPublishingPage(publishingPageInfo);
                    //context.Load(publishingPage);
                    //context.Load(publishingPage.ListItem.File, obj => obj.ServerRelativeUrl);
                    //context.ExecuteQuery();
                    CreateCustomPages(context, web, newsFolder, "Master Page Gallery", "UNSWIntranetSearchGridPage", "News.aspx", false);
                    CreateCustomPages(context, web, newsFolder, "Master Page Gallery", "UNSWPageFromDocLayout", "PageName.aspx", true);
                }
            }
            catch (Exception ex)
            {
                CreateException(ex, exClient);
                Console.WriteLine(ex.Message);
            }
        }

        private static void CreateCustomPages(ClientContext context, PublishingWeb web, Folder newsFolder, string listName, string pageLayoutName, string pageName, bool isProportiesSet)
        {
            try
            {
                List publishingLayouts = context.Site.RootWeb.Lists.GetByTitle(listName);
                ListItemCollection allItems = publishingLayouts.GetItems(CamlQuery.CreateAllItemsQuery());
                context.Load(allItems, items => items.Include(item => item.DisplayName).Where(obj => obj.DisplayName == pageLayoutName));
                context.ExecuteQuery();
                ListItem layout = allItems.Where(x => x.DisplayName == pageLayoutName).FirstOrDefault();
                context.Load(layout);
                PublishingPageInformation publishingPageInfo = new PublishingPageInformation();
                publishingPageInfo.Name = pageName;
                publishingPageInfo.Folder = newsFolder;
                publishingPageInfo.PageLayoutListItem = layout;
                PublishingPage publishingPage = web.AddPublishingPage(publishingPageInfo);
                if (isProportiesSet)
                {
                    FieldUrlValue url = new FieldUrlValue();
                    url.Url = "https://www.unsw.edu.au/";
                    url.Description = "";

                    //publishingPage.ListItem["title"] = "loreum ipsum";
                    var imageurl = context.Url + "/style library/branding/images/homenewsfeatures.png";
                    publishingPage.ListItem["PublishingPageImage"] = "<img src='" + imageurl + "'>";
                    publishingPage.ListItem["UNSWIASSPublishDate"] = DateTime.Now;
                    publishingPage.ListItem["UNSWIASSSummary"] = "this is a sample page. you may edit it or remove it.";
                    publishingPage.ListItem["UNSWIASSNewsCategory"] = "News";
                    publishingPage.ListItem["UNSWIASSHyperlink"] = url;
                    publishingPage.ListItem["UNSWIASSExpiryDate"] = DateTime.Now.AddDays(1);
                    publishingPage.ListItem["UNSWIASSFeatured"] = true;
                    publishingPage.ListItem["UNSWIASSVideoUrl"] = url;
                    publishingPage.ListItem.Update();
                    publishingPage.ListItem.File.CheckIn(string.Empty, CheckinType.MajorCheckIn);
                    publishingPage.ListItem.File.Publish(string.Empty);
                }
                context.Load(publishingPage);
                context.Load(publishingPage.ListItem.File, obj => obj.ServerRelativeUrl);
                context.ExecuteQuery();
                Console.WriteLine($"{pageName} Page is created successfully");
            }
            catch (Exception ex)
            {
                Console.WriteLine($"{pageName} Page is not created with News pagelayout");
                Console.WriteLine(ex.Message + ex.StackTrace);
            }
        }

        private static void CreateWebParts(ClientContext clientContext, XmlDocument xmlDoc, Uri configSiteUrl)
        {
            try
            {
                XmlNodeList nodeList = xmlDoc.GetElementsByTagName("WebParts");
                var nodes = xmlDoc.GetElementsByTagName("WebPart");
                if (nodes.Count != 0)
                {
                    for (int i = 0; i < nodes.Count; i++)
                    {
                        var xmlAttributeCollection = nodes[i].Attributes;
                        Microsoft.SharePoint.Client.File page = clientContext.Web.GetFileByUrl(configSiteUrl.OriginalString + xmlAttributeCollection["PageURL"].Value.ToString());
                        clientContext.Load(page);
                        clientContext.ExecuteQuery();
                        if (page.CheckOutType != CheckOutType.Online)
                        {
                            // Check out  
                            page.CheckOut();
                        }
                        var wpm = page.GetLimitedWebPartManager(PersonalizationScope.Shared);
                        //siteContext.Load(wpm.WebParts,wps => wps.Include(wp => wp.ZoneId == xmlAttributeCollection["WebpartZoneID"].Value.ToString()));
                        string wpZoneId = xmlAttributeCollection["WebpartZoneID"].Value.ToString();
                        clientContext.Load(wpm.WebParts, wps => wps.Include(wp => wp.ZoneId).Where(wp => (wp.ZoneId == wpZoneId)));
                        clientContext.ExecuteQuery();
                        var wpCount = wpm.WebParts.Count;
                        if (wpCount == 0 || wpCount == 1)
                        {
                            var webPartSchemaXml = @GetConfigValues(xmlAttributeCollection["WebpartSchema"].Value.ToString()).ToString();
                            var zoneid = xmlAttributeCollection["WebpartZoneID"].Value.ToString();
                            var zoneIndex = Convert.ToInt16(xmlAttributeCollection["ZoneIndex"].Value);

                            var importedWebPart = wpm.ImportWebPart(webPartSchemaXml);
                            var webPart = wpm.AddWebPart(importedWebPart.WebPart, zoneid, zoneIndex);
                            page.CheckIn("Added " + xmlAttributeCollection["WebpartType"].Value.ToString() + " in " + xmlAttributeCollection["WebpartZoneID"].Value.ToString() + " Webpart zone", CheckinType.MajorCheckIn);
                            page.Publish("Added " + xmlAttributeCollection["WebpartType"].Value.ToString() + " in " + xmlAttributeCollection["WebpartZoneID"].Value.ToString() + " Webpart zone");
                            clientContext.ExecuteQuery();
                        }

                    }
                }
            }
            catch (Exception ex)
            {
                CreateException(ex, exClient);
                Console.WriteLine(ex.Message);
            }
        }

        public static void IAASBranding(string newSiteUrl)
        {
            Uri configSiteUrl = new Uri(newSiteUrl);
            string realm = TokenHelper.GetRealmFromTargetUrl(configSiteUrl);

            //Get the access token of the required site collection.
            string siteAdminAccessToken = TokenHelper.GetAppOnlyAccessToken(TokenHelper.SharePointPrincipal, configSiteUrl.Authority, realm).AccessToken;
            try
            {
                using (ClientContext siteContext = TokenHelper.GetClientContextWithAccessToken(configSiteUrl.OriginalString, siteAdminAccessToken))
                {

                    Site site = siteContext.Site;
                    siteContext.Load(site, w => w.RootWeb);
                    siteContext.ExecuteQuery();

                    var siteUrl = site.RootWeb.Url;
                    // Apply AlternateCSS Url
                    //site.RootWeb.AlternateCssUrl = site.RootWeb.ServerRelativeUrl + "/Style Library/Branding/CSS/UNSWIntranetCore.css";
                    //site.RootWeb.Update();
                    //siteContext.ExecuteQuery();


                    // Update site logo
                    site.RootWeb.SiteLogoUrl = site.RootWeb.ServerRelativeUrl + "/Style Library/Branding/Images/logo-unsw.png";
                    site.RootWeb.Update();
                    siteContext.ExecuteQuery();


                    // Add css references
                    DeleteCustomActionCSS(siteContext, "UNSWIntranetCore.css");
                    UserCustomAction customActionUNSWcss = site.UserCustomActions.Add();
                    customActionUNSWcss.Location = "ScriptLink";
                    customActionUNSWcss.Sequence = 100;
                    customActionUNSWcss.ScriptBlock = @"document.write('<link rel =""stylesheet"" href =""" + newSiteUrl + @"/Style Library/Branding/CSS/UNSWIntranetCore.css"" />');";
                    customActionUNSWcss.Name = "UNSWCssLink";
                    customActionUNSWcss.Update();
                    siteContext.ExecuteQuery();

                    DeleteCustomActionCSS(siteContext, "faculty.css");
                    UserCustomAction customActionFacultycss = site.UserCustomActions.Add();
                    customActionFacultycss.Location = "ScriptLink";
                    customActionFacultycss.Sequence = 101;
                    customActionFacultycss.ScriptBlock = @"document.write('<link rel =""stylesheet"" href =""" + newSiteUrl + @"/Style Library/Branding/CSS/faculty.css"" />');";
                    customActionFacultycss.Name = "UNSWFacultyCssLink";
                    customActionFacultycss.Update();
                    siteContext.ExecuteQuery();

                    DeleteCustomActionCSS(siteContext, "jquery-ui.css");
                    UserCustomAction customActionJqueryUICss = site.UserCustomActions.Add();
                    customActionJqueryUICss.Location = "ScriptLink";
                    customActionJqueryUICss.Sequence = 102;
                    customActionJqueryUICss.ScriptBlock = @"document.write('<link rel =""stylesheet"" href =""" + newSiteUrl + @"/Style Library/Branding/CSS/jquery-ui.css"" />');";
                    customActionJqueryUICss.Name = "JqueryUICssLink";
                    customActionJqueryUICss.Update();
                    siteContext.ExecuteQuery();

                    DeleteCustomActionCSS(siteContext, "font-awesome.min.css");
                    UserCustomAction customActionFontCSS = site.UserCustomActions.Add();
                    customActionFontCSS.Location = "ScriptLink";
                    customActionFontCSS.Sequence = 103;
                    customActionFontCSS.ScriptBlock = @"document.write('<link rel =""stylesheet"" href =""" + newSiteUrl + @"/Style Library/Branding/CSS/font-awesome.min.css"" />');";
                    customActionFontCSS.Name = "FontAwesomeCSSLink";
                    customActionFontCSS.Update();
                    siteContext.ExecuteQuery();

                    // Responsive UI css file script injection
                    DeleteCustomActionCSS(siteContext, "SP-Responsive-UI.css"); 
                    UserCustomAction customActionRespUICSS = site.UserCustomActions.Add();
                    customActionRespUICSS.Location = "ScriptLink";
                    customActionRespUICSS.Sequence = 110;
                    customActionRespUICSS.ScriptBlock = @"document.write('<link rel =""stylesheet"" href =""" + newSiteUrl + @"/Style Library/ResponsiveUI/SP-Responsive-UI.css"" />');";
                    customActionRespUICSS.Name = "ResponsiveUILink";
                    customActionRespUICSS.Update();
                    siteContext.ExecuteQuery();

                    // Add javascript references

                    // Responsive UI js file script injection
                    DeleteCustomActionJS(siteContext, "SP-Responsive-UI.js");
                    UserCustomAction customActionRespUIJS = site.UserCustomActions.Add();
                    customActionRespUIJS.Location = "ScriptLink";
                    customActionRespUIJS.ScriptSrc = "~SiteCollection/Style Library/ResponsiveUI/SP-Responsive-UI.js";
                    customActionRespUIJS.Sequence = 1010;
                    customActionRespUIJS.Name = "ResponsiveUIJS";
                    customActionRespUIJS.Update();
                    siteContext.ExecuteQuery();

                    DeleteCustomActionJS(siteContext, "jquery-2.2.1.min.js");
                    UserCustomAction customActionJquery = site.UserCustomActions.Add();
                    customActionJquery.Location = "ScriptLink";
                    customActionJquery.ScriptSrc = "~SiteCollection/Style Library/Branding/Scripts/jquery-2.2.1.min.js";
                    customActionJquery.Sequence = 1000;
                    customActionJquery.Update();
                    siteContext.ExecuteQuery();

                    DeleteCustomActionJS(siteContext, "jquery-ui.min.js");
                    UserCustomAction customActionJqueryUI = site.UserCustomActions.Add();
                    customActionJqueryUI.Location = "ScriptLink";
                    customActionJqueryUI.ScriptSrc = "~SiteCollection/Style Library/Branding/Scripts/jquery-ui.min.js";
                    customActionJqueryUI.Sequence = 1001;
                    customActionJqueryUI.Update();
                    siteContext.ExecuteQuery();

                    DeleteCustomActionJS(siteContext, "jquery.ui.touch-punch.min.js");
                    UserCustomAction customActionJqueryUITouch = site.UserCustomActions.Add();
                    customActionJqueryUITouch.Location = "ScriptLink";
                    customActionJqueryUITouch.ScriptSrc = "~SiteCollection/Style Library/Branding/Scripts/jquery.ui.touch-punch.min.js";
                    customActionJqueryUITouch.Sequence = 1002;
                    customActionJqueryUITouch.Update();
                    siteContext.ExecuteQuery();

                    DeleteCustomActionJS(siteContext, "UNSWIntranetCore.js");
                    UserCustomAction customActionJSInjection = site.UserCustomActions.Add();
                    customActionJSInjection.Location = "ScriptLink";
                    customActionJSInjection.ScriptSrc = "~SiteCollection/Style Library/Branding/Scripts/UNSWIntranetCore.js";
                    customActionJSInjection.Sequence = 1200;
                    customActionJSInjection.Update();
                    siteContext.ExecuteQuery();

                    DeleteCustomActionJS(siteContext, "faculty.js");
                    UserCustomAction customActionFacultyJSInjection = site.UserCustomActions.Add();
                    customActionFacultyJSInjection.Location = "ScriptLink";
                    customActionFacultyJSInjection.ScriptSrc = "~SiteCollection/Style Library/Branding/Scripts/faculty.js";
                    customActionFacultyJSInjection.Sequence = 1300;
                    customActionFacultyJSInjection.Update();
                    siteContext.ExecuteQuery();

                }

            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
                Console.ReadLine();
            }
        }
        private static void DeleteCustomActionJS(ClientContext context, string fileName)
        {
            try
            {
                Site site = context.Site;
                context.Load(site, x => x.UserCustomActions);
                context.ExecuteQuery();

                List<UserCustomAction> ualist = new List<UserCustomAction>();
                foreach (var ca in site.UserCustomActions)
                {
                    if (ca.ScriptSrc != null && ca.ScriptSrc.Contains(fileName))
                    {
                        ualist.Add(ca);
                        //ca.DeleteObject();
                    }
                }

                foreach (var aa in ualist)
                {
                    aa.DeleteObject();
                }

                context.ExecuteQuery();
            }
            catch (Exception ex)
            {
                CreateException(ex, exClient);
                Console.WriteLine(ex.Message);
            }
        }

        private static void DeleteCustomActionCSS(ClientContext context, string fileName)
        {
            try
            {
                Site site = context.Site;
                context.Load(site, x => x.UserCustomActions);
                context.ExecuteQuery();

                List<UserCustomAction> ualist = new List<UserCustomAction>();
                foreach (var ca in site.UserCustomActions)
                {
                    if (ca.ScriptBlock != null && ca.ScriptBlock.Contains(fileName))
                    {
                        ualist.Add(ca);
                        //ca.DeleteObject();
                    }
                }

                foreach (var aa in ualist)
                {
                    aa.DeleteObject();
                }

                context.ExecuteQuery();
            }
            catch (Exception ex)
            {
                CreateException(ex, exClient);
                Console.WriteLine(ex.Message);
            }
        }
        #endregion

        /// <summary>
        /// This method creates events in Azure Portal
        /// </summary>
        /// <param name="appEvent"></param>
        /// <param name="tClient"></param>
        public static void CreateEvent(string appEvent, Microsoft.ApplicationInsights.TelemetryClient eventClient)
        {
            eventClient.Context.InstrumentationKey = instrumentationKey;
            eventClient.TrackEvent(appEvent);
        }

        /// <summary>
        /// This method creates exceptions in Azure Portal
        /// </summary>
        /// <param name="appException"></param>
        /// <param name="exceptionClient"></param>
        public static void CreateException(System.Exception appException, Microsoft.ApplicationInsights.TelemetryClient exceptionClient)
        {
            exceptionClient.Context.InstrumentationKey = instrumentationKey;
            ExceptionTelemetry exTelemetry = new ExceptionTelemetry(appException);
            exceptionClient.TrackException(exTelemetry);
        }
    }
}


