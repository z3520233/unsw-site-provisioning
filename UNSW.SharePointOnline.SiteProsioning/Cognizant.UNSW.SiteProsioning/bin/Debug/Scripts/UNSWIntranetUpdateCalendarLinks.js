<script type="text/javascript">
var siteURL = "https://unsw.sharepoint.com/sites/ASSembly/";
var calendarExternalLink;
var clientContext;
var results;
var oList;
var count=0;
var deferred;
//$("WPQ1_nav_prev_a").click();
//$("WPQ1_nav_next_a").click(setTimeout(ExecuteOrDelayUntilScriptLoaded( UpdateCalendarLinksForExternalEvents, "sp.js"), 500));
//$(document).on('click', 'WPQ1_nav_prev_a', function(){setTimeout(ExecuteOrDelayUntilScriptLoaded( UpdateCalendarLinksForExternalEvents, "sp.js"), 500)});



$(document).ready(function () {
 // code here


LoadSodByKey("SP.UI.ApplicationPages.Calendar.js", function () {
        WaitForCalendarToLoad();
    });
	});


function WaitForCalendarToLoad() {
	// running your function for the first time
//	updateMonthLinks(); 
 setTimeout(ExecuteOrDelayUntilScriptLoaded( UpdateCalendarLinksForExternalEvents, "sp.js"), 500);
	var _onItemsSucceed = SP.UI.ApplicationPages.CalendarStateHandler.prototype.onItemsSucceed; 
	SP.UI.ApplicationPages.CalendarStateHandler.prototype.onItemsSucceed = function($p0, $p1) { 
		_onItemsSucceed.call(this, $p0, $p1);

		// now let it call your function each time the calendar is loaded
		// updateMonthLinks();
 
 
 setTimeout(ExecuteOrDelayUntilScriptLoaded( UpdateCalendarLinksForExternalEvents, "sp.js"), 500);
	}
}
function updateMonthLinks() 
{
 var nextHREF = $('#WPQ1_nav_next_a').attr("href");
nextHREF = nextHREF + "setTimeout(window.location.reload(false),500);";
$('#WPQ1_nav_next_a').attr("href",nextHREF);

var prevHREF = $('#WPQ1_nav_prev_a').attr("href");
prevHREF = prevHREF + "setTimeout(window.location.reload(false),500);";
$('#WPQ1_nav_prev_a').attr("href",prevHREF);

}

function UpdateCalendarLinksForExternalEvents()
{
  var clientContext = new SP.ClientContext(siteURL);
	var web = clientContext.get_web();
	loginUser = web.get_currentUser();	
	clientContext.load(loginUser);
	clientContext.executeQueryAsync(Function.createDelegate(this, this.MyCalendarHook), Function.createDelegate(this, this.onQueryFailed)); 
	
}


function retrieveHyperLinks(itemId)
{
//console.log("itemid: " + itemId);
var siteURL = "https://unsw.sharepoint.com/sites/ASSembly/_api/web/lists/getbytitle('UNSW Events')/items";
try{
					var calendarLinks = $.ajax({
							  url: siteURL +"("+itemId+")",
							   method: "GET",
							   async: false, 
							   headers: { "Accept": "application/json; odata=verbose" },
							   success: 
								  function displayResultForLog(result) 
									 {
									   if (result.hasOwnProperty("d")) {
										   result = result.d;
										 //  console.log (result);
									   
									   if (result !== undefined && result != null )
											{
											try{
												calendarExternalLink= "";
												//console.log("here1" + result["UNSWFass_Hyperlink"]);
												if(result["UNSWFass_Hyperlink"]!=null)
													{
														calendarExternalLink = result["UNSWFass_Hyperlink"].Url;
														//console.log(calendarExternalLink);
														var string = "ID="+itemId;
														$(".ms-acal-title a").each(function(){												
																var itemID =  $(this).attr('href').split('?ID=')[1];			 
														if(itemID == itemId)
															{ $(this).attr('href',calendarExternalLink);
															// console.log("After link update");
															// return true;
															 }
															 });
													}
											
											}
											catch(e)
											{
											//console.log("error as value is null");
											calendarExternalLink="";
											//return false;
											}
												
											}
									   }
								   				}
							  ,
							  error: function (error) {
								 // console.log("Error: "+ JSON.stringify(error));
								 // return false;
							 }
					  });
					 
					  }
					  catch(e)
					  {
					 // console.log("error");
					  }
					  //calendarLinks.push(calendarLinks);
					  
}

function MyCalendarHook() {     
 
 var data="";
 var calendarExternalLink;
 var pollCount=0;
 if (pollCount > 20)
      return;
   //if the calendar hasn't loaded yet, then wait .5 seconds
   if ($('.ms-acal-title a').length < 1) {
      pollCount++;
      setTimeout(function(){MyCalendarHook();}, 3000);     
   }
   else {
      //the calendar has loaded, so do work
      $(".ms-acal-title a").each(function(){
			calendarExternalLink="";
             var itemId =  $(this).attr('href').split('?ID=')[1];					
			calendarExternalLink = retrieveHyperLinks(itemId);					
        });
		}
		
  
    }

</script> ​<br/>