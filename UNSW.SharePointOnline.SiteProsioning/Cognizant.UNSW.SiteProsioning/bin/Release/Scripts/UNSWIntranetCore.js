'use strict';
var constants = {
    termSetGuids: {
        resourceCategory: "59e748ed-3424-4b0f-8a51-22a7215e5980",
        teamCategory: "ad1c481f-bd2b-4efc-bbf4-4be4442e130b",
        rolesAudience: "06E15C87-47C3-4189-94D5-A71204E01571",
        BusSchoolUnit: "99342006-19d9-4d76-ae6d-6a49808a1b3d",
        locationTermSet: "b49f64b3-4722-4336-9a5c-56c326b344d4",
        keywordProd: "755cd64a-dd0c-4add-be00-7f4a69e633f1",
        keywordsTest: "6b4b33cc-0d0b-481a-b94d-f99349662751",
        keywordsDev: "00cc67f4-5748-4a75-9be4-1a9b4126f0a4",
        connectNavTermSetId: "2014eac4-9491-4371-b6dc-ffdb89cbcb25",
        connectResourceCatsTermSetId: "59e748ed-3424-4b0f-8a51-22a7215e5980"
    },
    termCustomPropKeys: {
        teamStructureSource: "teamStructureSource",
        icon: "icon",
        hiddenOnCustomNav: "hiddenOnCustomNav",
        resourcePageUrl: "resourcePageUrl",
        userDepartmentBasedCategory: "userDepartmentBasedCategory",
        serachDrivenResources: "serachDrivenResources",
        defaultTabPageUrl: "defaultTabPageUrl"
    },
    renditionsIds: { businessBalanceArticle: "RenditionId=14" },
    termStoreCustomPropertyKeys: { teamOverViewParent: "teamOverViewParent" },
    sessionStorageKeys: { myEventsKey: "myEvents", alertIdsKey: "alertIds", personalistionItemsKey: "personalistionItems" },
    userPropertyFieldNames: {
        UNSWBUSSchoolUnit: "UNSWBUSSchoolUnit",
        WorkEmail: "WorkEmail",
        LastName: "LastName",
        FirstName: "FirstName",
        PictureURL: "PictureURL",
        WorkPhone: "WorkPhone",
        Title: "Title",
        UNSWBUSFireWarden: "UNSWBUSFireWarden",
        UNSWBUSJusticeOfPeace: "UNSWBUSJusticeOfPeace",
        UNSWBUSFirstAidOfficer: "UNSWBUSFirstAidOfficer",
        UNSWBUSEmergencyControlTeam: "UNSWBUSEmergencyControlTeam",
        UNSWBUSWorkWeek: "UNSWBUSWorkWeek",
        BaseOfficeLocation: "BaseOfficeLocation",
        Skills: "Skills",
        PastProjects: "PastProjects",
        AboutMe: "AboutMe",
        UNSWBUSQualifications: "UNSWBUSQualifications"
    },
    userProfilrPropertyNames: {
        UNSWBUSSchoolUnit: "UNSWBUSSchoolUnit",
        WorkEmail: "WorkEmail",
        LastName: "LastName",
        FirstName: "FirstName",
        PictureURL: "PictureURL",
        WorkPhone: "WorkPhone",
        Title: "Title",
        UNSWFireWarden: "UNSWFireWarden",
        UNSWJusticeOfPeace: "UNSWJusticeofPeace",
        UNSWFirstAidOfficer: "UNSWFirstAidOfficer",
        UNSWEmergencyControlTeam: "UNSWEmergencyControlTeam",
        UNSWWorkWeek: "UNSWWorkWeek",
        OfficeLocation: "SPS-Location",
        Skills: "SPS-Skills",
        PastProjects: "SPS-PastProjects",
        AboutMe: "AboutMe",
        UNSWQualifications: "UNSWQualifications",
        UNSWLanguages: "UNSWLanguages"
    },
    fieldNames: { referenceID: "UnswBus_ReferenceID"
    },
    htmlElementIds: {
        teamStructureSection: "teamStructureSection",
        dynamicCategories: "dynamicCategories",
        termBasedModeTitle: "termBasedModeTitle",
        termBasedModeDescription: "termBasedModeDescription",
        listBasedModeTitle: "listBasedModeTitle",
        subCatCardsConatiner: "subCatCardsConatiner",
        featuredResourceCardContainer: "featuredResourceCardConatienr",
        resourceCardsContainer: "resourceCardsConatienr",
        resourceCentreTabs: "resourceCentreTabs",
        searchNavTabsContainer: "searchNavTabsContainer",
        relatedKeyWords: "relatedKeyWords",
        issueUrl: "issueUrl",
        dialogFormReportIssue: "dialogFormReportIssue",
        dialogFormFav: "dialogFormFav",
        favDescrip: "favDescrip",
        favUrl: "favUrl",
        favTitle: "favTitle",
        letterRefiner: "letterRefiner",
        pageContactSection: "pageContactSection",
        myFavsSelected: "myFavsSelected",
        reqFldMessage: "reqFldMessage",
        favItemDescription: "favItemDescription",
        favItemUrl: "favItemUrl",
        favItemTitle: "favItemTitle",
        addFavItem: "addFavItem",
        appAudienceFilter: "appAudienceFilter",
        eventsSection: "eventsSection",
        alertsSection: "alertsSection",
        favsTab: "favsTab",
        favsList: "favsList",
        sitesTab: "sitesTab",
        sitesList: "sitesList",
        toolTab: "toolTab",
        toolsList: "toolsList",
        profilePicFile: "profilePicFile",
        popTopicsContainer: "popTopicsContainer",
        pageTitle: "DeltaPlaceHolderPageTitleInTitleArea",
        myProfileTab: "myProfile",
        myprofileForm: "myprofileForm",
        departmentEdit: "departmentEdit",
        schoolUnit: "schoolUnit",
        baseOfficeLocation: "baseOfficeLocation",
        skills: "skills",
        saveProfile: "saveProfile",
        issueTitle: "issueTitle",
        resourceCentreNotice: "resourceCentreNotice",
        categoryLink: "categoryLink",
        profileEditMode: "profileEditMode",
        globalBusyIndicatorDialog: "globalBusyIndicatorDialog",
        pastProjects: "pastProjects",
        myprofileUploadImage: "myprofileUploadImage",
        profilePicUploader: "profilePicUploader",
        userWidgetsSectionRow: "userWidgetsSectionRow",
        editProfile: "editProfile",
        cancelEdit: "cancelEdit",
        saveProfileEdit: "saveProfileEdit",
        imageUpdateOverlay: "imageUpdateOverlay",
        profilePicCancel: "profilePicCancel",
        profilePicSave: "profilePicSave",
        widgetSerachButton: "widgetSerachButton",
        serachTextWidget: "serachTextWidget",
        featuredPeopleContainer: "featuredPeopleContainer",
        userWidgetsSection: "userWidgetsSection",
        myEventsSelected: "myEventsSelected",
        mySitesSelected: "mySitesSelected",
    },
    restEndPoints: {
        bbDyanmicCategoriesEp: "/_api/search/query?properties='SourceName:Business Balance Dynamic Categories,SourceLevel:SPSite'&selectproperties='Title,Path,UnswBusDescriptionOWSTEXT,OriginalPath,UnswBusFeaturedLink1OWSURLH,UnswBusFeaturedLink2OWSURLH,UnswBusFeaturedLink3OWSURLH,UnswBusFeaturedLink4OWSURLH,owstaxIdUnswBusBusinessBalanceCategory'",
        bbStaticCategoriesEp: "/_api/search/query?properties='SourceName:Business Balance Static Categories,SourceLevel:SPSite'&selectproperties='Title,Path,UnswBusDescriptionOWSTEXT,OriginalPath,UnswBusFeaturedLink1OWSURLH,UnswBusFeaturedLink2OWSURLH,UnswBusFeaturedLink3OWSURLH,UnswBusFeaturedLink4OWSURLH,owstaxIdUnswBusBusinessBalanceCategory'",
        bbArtcilePagesEp: "/_api/search/query?properties='SourceName:Business Balance Articles - All,SourceLevel:SPSite'&selectproperties='Title,Path,UnswBusSummaryOWSMTXT,PublishingPageImageOWSIMGE,owstaxIdUnswBusBusinessBalanceCategory'",
        resourceByCategory: "/_api/search/query?properties='SourceName:Resources by Category,SourceLevel:SPSite'&trimduplicates=false&rowlimit=100&selectproperties='BusTitle,Path,URLOWSURLH,UnswBusResourceCategoryTaxonomyTerm,ListItemId,UnswBusFeaturedOWSBOOL,UnswBusDescriptionOWSTEXT,UnswBusResourcePageTemplateOWSCHCS,UnswBusCategoryHeadingOWSBOOL'&category={0}",
        resourceProperties: "/_api/search/query?properties='SourceName:Resource Category Properties,SourceLevel:SPSite'&trimduplicates=false&rowlimit=100&selectproperties='Title,UnswBusHyperlinkOWSURLH,UnswBusResourceCategoryTaxonomyTerm,ListItemId,UnswBusFeaturedOWSBOOL,UnswBusDescriptionOWSTEXT,UnswBusResourcePageTemplateOWSCHCS,UnswBusCategoryHeadingOWSBOOL'&ResourceCat={0}",
        searchNavigationNodes: "/_api/web/Navigation/GetNodeById(1040)/Children",
        currentPageResourceCategory: "/_api/web/lists(guid'{0}')/items({1})?$select=UnswBus_ResourceCategory",
        add_reportIssue: "/_api/Web/Lists/GetByTitle('Report Issue')/Items",
        get_userProperty: "/_api/SP.UserProfiles.PeopleManager/GetUserProfilePropertyFor(accountName=@v,propertyName='{0}')?@v='{1}'",
        get_pageContact: "/_api/web/lists(guid'{0}')/items({1})?$select=PublishingContact/Name&$expand=PublishingContact",
        get_myEvents: "/Personalisation/_api/web/lists/GetByTitle('My Events')/items?$select=ID,UnswBus_Values&$filter=UnswBus_AssignedUser/ID eq '{0}'",
        get_myFavsItem: "/Personalisation/_api/web/lists/GetByTitle('My Favourite')/items?$select=UnswBus_Values&$filter=UnswBus_AssignedUser/ID eq '{0}'",
        get_alerts: "/_api/web/lists/GetByTitle('Alerts')/items?$select=ID,Title,UnswBus_Description,UnswBus_Priority,UnswBus_Hyperlink&$orderby=BUSPublishDate desc&$filter=BUSPublishDate le '{0}' and BUSExpiryDate ge '{1}'",
        get_appDefinitions: "/Personalisation/_api/web/lists/GetByTitle('Apps')/items?" +
             "$select=UnswBus_ReferenceID,Title,UnswBus_Description,UnswBus_Hyperlink,UnswBus_Icon,UnswBus_Static,UnswBus_RoleAudience&$orderby=UnswBus_SortingNumber desc&$filter=((UnswBus_Static eq '1') {0})",
        get_myApps: "/Personalisation/_api/web/lists/GetByTitle('My Apps')/items?$select=ID,UnswBus_Values&$filter=UnswBus_AssignedUser/ID eq '{0}'",
        get_mySites: "/Personalisation/_api/web/lists/GetByTitle('My Collab Sites')/items?$select=ID,UnswBus_Values&$filter=UnswBus_AssignedUser/ID eq '{0}'",
        get_myFavs: "/Personalisation/_api/web/lists/GetByTitle('My Favourite')/items?$select=ID,UnswBus_Values&$filter=UnswBus_AssignedUser/ID eq '{0}'",
        add_profilePic: "/_api/sp.userprofiles.peoplemanager/setmyprofilepicture",
        get_popTopics: "/_api/search/query?properties='SourceName:Resource Centre Popular Topics,SourceLevel:SPSite'&trimduplicates=false&rowlimit=100&selectproperties='Title,UnswBusHyperlinkOWSURLH'&category={0}",
        get_myWidgets: "/Personalisation/_api/web/lists/GetByTitle('My Widgets')/items?$select=ID,UnswBus_Values&$filter=UnswBus_AssignedUser/ID eq '{0}'",
        get_profilePropRanking: "/_api/web/lists/GetByTitle('User Profile Scoring')/items?$select=Title,UnswBus_Rating",
        get_profileProp: "/_api/SP.UserProfiles.PeopleManager/GetUserProfilePropertyFor(accountName=@v,propertyName='{0}')?@v='{1}'",
        get_featuredPeople:  "/_api/web/lists/GetByTitle('Featured People')/items?" +
                 "$select=UnswBus_Hyperlink,UnswBus_AssignedUser/Name&$expand=UnswBus_AssignedUser&$filter=BUSPublishDate le '{0}' and BUSExpiryDate ge '{0}' or BUSExpiryDate eq null",
        get_featuredPeopleProps: "/_api/search/query?'&properties='SourceName:Business School People,SourceLevel:SPSite'&querytext='{0}'&selectproperties='UNSWBUSJobTitle,UNSWBUSSchoolUnit,UNSWBUSFirstName,UNSWBUSLastName,PictureURL,AccountName,WorkEmail'",
        get_widgetDefs:  "/Personalisation/_api/web/lists/GetByTitle('Widgets')/items?" +
          "$select=UnswBus_ReferenceID,UnswBus_SortingNumber,Title,UnswBus_Description,UnswBus_ResultSourceId,UnswBus_WidgetType,UnswBus_Static,UnswBus_Hyperlink&$filter=UnswBus_Static eq '1'",
        get_widgetDefsFiltered: "/Personalisation/_api/web/lists/GetByTitle('Widgets')/items?" +
              "$select=UnswBus_ReferenceID,UnswBus_SortingNumber,Title,UnswBus_Description,UnswBus_ResultSourceId,UnswBus_WidgetType,UnswBus_Static,UnswBus_Hyperlink&$filter=((UnswBus_Static eq '1') or {0}",
        get_myFavsListItems: "/Personalisation/_api/Web/Lists/GetByTitle('My Favourite')/Items",

},
    pageRenderMode: { termbased: "termbased", listBased: "listbased" },
    queryStringParams: {
        categoryParam: { name: "category", template: "category={0}" },
        modeParam: { name: "mode", template: "mode={0}" },
        propIdParam: { name: "propId", template: "propId={0}" },
        serachKeyword: { name: "k", template: "k={0}" },
        sourceTerm: { name: "sourceTerm", template: "sourceTerm={0}" },
        pathTerms: { name: "pathTerms", template: "pathTerms={0}" },
        sourceUrl: { name: "sourceUrl", template: "sourceUrl={0}" },
        searchDrivenResource: { name: "sdr", template: "sdr={0}" }
    },
    pageUrls: {
        newsGrid: "/news-and-events",
        assetPageUrl: "/ResourceCentre/Pages/Resource-Assets.aspx",
        subcatsPageUrl: "/ResourceCentre/Pages/Resource-Assets-Subcategory.aspx",
        globalSearch: "/pages/search/default.aspx",
        peopleSearch: "/people-directory",
        eventsRollup: "/news-and-events/events",
        myProfile: "/Personalisation/Pages/Personalisation.aspx#myProfile",
        myEvents: "/Personalisation/Pages/Personalisation.aspx#myEvents",
        myTools: "/Personalisation/Pages/Personalisation.aspx#tools",
        mySites: "/Personalisation/Pages/Personalisation.aspx#mySites",
        myFavs: "/Personalisation/Pages/Personalisation.aspx#myFavs",
        myWidgets: "/Personalisation/Pages/Personalisation.aspx#myWidgets",
        userPhotoPage: "/_vti_bin/DelveApi.ashx/people/profileimage?userId={0}&size=L",

    },
    listMetadataTypes: { ReportIssueListItem: "SP.Data.ReportIssueListItem" },
    teamUpApi: {
        allEventsEp: "https://api.teamup.com/ks33bc6e4f8b43c18b/subcalendars/",
        eventsEp: "https://api.teamup.com/ks33bc6e4f8b43c18b/events?startDate={0}&endDate={1}",
        token: "5abc9ca3024f4c75542a21ecd390db54a3b494fcffa03ccb12cac0a32171da5a"
    },
    siteColumns: {
        UnswBus_Values: "UnswBus_Values",
    },
    managedProps: {
        owstaxIdUnswBusRelatedNewsKeywords: "owstaxIdUnswBusRelatedNewsKeywords",
        UnswBusEnterpriseKeywords: "RefinableString15",
        UnswBusNewsCategory: "RefinableString04",
        UnswBusBusinessBalanceCategory: "RefinableString03"
    },
    widgetTypes: {
        departmental: "departmental widget",
        general: "general widget",
        resultSource: "result source driven widget",
    },
    staticWidgetIdentifiers :{
        myprofile: "widget-myprofile",
        featuredPeople: "widget-featuredpeople",
        peopleSearch: "widget-peopleserach",

    },
    dataFields: {
        refId: "data-refId",
        title: "data-title",
        url: "data-url",
        description: "data-description",

    }

};

var globalObjects = {
    navigationTree: null,
    currentPageList: null,
    currentPageItemId: null,
    webAbsoluteUrl: null,
    availableBusSchoolUnits: []
}

var htmlTemplates = {
    auxBarAppItem: "<div class='col col-2 tools-item'>" +
                                     "<a href='{0}' target='_blank'>" +
                                         "<div class='m-b-md'>" +
                                         "<img class='roundImage-app' src='{1}'>" +
                                         "</div> {2}" +
                                     "</a>" +

                                 "</div>",

    auxBarSiteItem: "<li class='col col-4'>" +
              "<a href='{0}' target='_blank'>" +
              "<div>" +
                  "<span><i class='fa fa-desktop'></i>{1}</span>" +
              "</div></a>" +
          "</li>",

    auxBarFavItem: "<li class='col col-4'>" +
               "<a href='{0}' target='_blank'>" +
               "<div>" +
                   "<span><i class='fa fa-star'></i>{1}</span>" +
               "</div></a>" +
           "</li>",
    "widget-peoplesearch": "",
    "widget-myprofile": "",
    "widget-featuredpeople": "",
    bbCatgeorySection: "<div class='container-info m-t-md m-b-md bb-dyanmic-conatiner'>" +
    "<div class='row row-nw row-sb'>" +
         "<div class='col col-remaining m-l-lg '>" +
                "<div ><a href='{0}'><span class='heading-feature'>{1}</span></a></div>" +
                "<div class='m-b-md  m-t-md ie-flex-fix-1 '><p>{2}</p></div>" +
                "<div class='bb-featured-links'>{3}</div>" +
         "</div>" +
          "<div class='col border-l p-l-lg ie-flex-fix-2'>" +
             "<a href='{4}'>{5}" +
             "<div class='heading-section-tile m-t-md'>{6}</div></a>" +
             "<div class='bb-dynamic-article-text'>{7}</div>" +
         "</div>" +
    "</div>" +
"</div>",
    bbCatgeorySectionAlternate: "<div class='container-info m-t-md m-b-md bb-dyanmic-conatiner'>" +
"<div class='row row-nw row-sb'>" +
    "<div class='col m-r-md ie-flex-fix-2'>" +
         "<a href='{4}'>{5}" +
         "<div class='heading-section-tile m-t-md'>{6}</div></a>" +
         "<div class='bb-dynamic-article-text'>{7}</div>" +
     "</div>" +
     "<div class='col col-remaining border-l p-l-lg '>" +
           "<div ><a href='{0}'><span class='heading-feature'>{1}</span></a></div>" +
            "<div class='m-b-md  m-t-md ie-flex-fix-1'><p>{2}</p></div>" +
           "<div class='bb-featured-links'>{3}</div>" +
     "</div>" +
"</div>" +
"</div>",
    bbCategoryStaticConatiner: "<div class='m-t-md m-b-md'>" +
        "<div class='row row-sb'>{0}</div>" +
    "</div>",
    bbCategoryStaticItem: "<div class='col bb-static-conatiner container-info'>" +
              "<div ><a href='{0}'><span class='heading-feature'>{1}</span></a></div>" +
               "<div class='m-b-md  m-t-md'>{2}</div>" +
              "<div class='bb-featured-links'>{3}</div>" +
        "</div>",
    bbFeaturedLinks: "<div><a href='{0}'>{1}</a></div>",
    teamAccordianConatiner: "<h3 class=''>{0}</h3><div class='row'>{1}</div>",
    teamAccordianItem: "<div class='col col-1-3'>" +
            "<div class='row row-vt row-nw'><span class='m-r-sm'> <i class='fa fa-angle-right'></i> </span> <a href='{0}'>{1}</a>" +
             "</div>" +
        "</div>",
    pageContactTemplate: "<div id='pageContactDisplay' class=''>" +
      "<div class='m-t-md m-l-md border-tb p-t-md'> <span class='text-upper text-bold text-11'>{7}</span>" +
      "<div class='p-t-sm'>For more info please contact</div>" +
      " <div class='row row-sb p-t-md p-b-md p-r-md'>" +
      "<div class='col relative-pos'>" +
       "<div  class='push-left roundImage-app' style='background-image: url({2});'>" +
      "<div id='contactImage' style='display: none;' class='roundImage-md centre'>" +
      "</div>" +
      "</div>" +
       "</div>" +
         "   <div  class='col col-13 pagecontact-details'>" +
        " <div class='row row-nw row-vt'>" +
        " <div class='col-nm'>" +
        "<span class='text-bold'>{0} {1}</span> <br/> {3} </div>" +
         "</div>" +
         "<div>{4} <br/> <a href='mailto:{5}'>{5}</a> </br> Ph: {6}" +
         "</div>" +
       "</div>" +
      " </div>" +
      "</div>" +
        "</div>",
    toolItem: "<li id='{0}' class='{5}' data-refId='{0}' data-appIcon='{1}' data-title='{2}' data-description='{3}' data-url='{4}' data-roleAudience='{6}' >" +
			"<div class='row row-nw row-vc'>" +
				"<div class='col m-r-md'>" +
				  "<img class='roundImage-app-sm' src='{1}'>" +
				"</div>" +
				"<div style='list-style: outside none none;' class='col col-remaining'>" +
					"<span class='text-bold'>{2}</span><br/>{3}" +
				"</div>" +
				"<div class='col icon-move'><i class='fa fa-arrows'></i></div>" +
			"</div>" +
			"</li>",
    favsItem: "<li data-url='{0}'  data-title='{1}' data-description='{2}' class='personalise-item'  >" +
  "<div class='row row-nw row-vc edit-row'>" +
  "<div class='col m-r-md '>" +
       "<span class='fa-stack fa-lg widget-list-icon'>" +
                          "<i class='fa fa-circle fa-stack-2x icon-background-2'></i>" +
                          "<i class='fa fa-link fa-stack-1x widget-icon-color'></i>" +
                      "</span>" +
   "</div>" +
       "<div class='col col-remaining'>" +
        "<div class='favItemDispMode'><span  class='text-bold dispTitle edit-item'>{1}</span> <span class='m-l-xsm editFavItem clickable edit-item'><i class='fa fa-pencil-square-o'></i></span> <br/> <span class='p-l-xsm dispDescription'>{2}</span></div>" +
        "<div class='favItemEditMode' style='display: none;'>" +
         "<div class='p-b-sm'>Title <span class='req-fld'>*</span> <div class='row'><input  type='text' value='{1}' class='favItemTitleInput col-remaining inputbox' ></div> </div>" +
         "<div class='p-b-sm'>Url <span class='req-fld'>*</span><div class='row'><input  type='text' value='{0}' class='favItemUrlInput col-remaining inputbox' ></div></div>" +
         "<div class='p-b-sm'>Descripion<div  class='row'><input  type='text' value='{2}' class='favItemDescriptionInput col-remaining inputbox'> </div></div>" +
        "</div>" +
       "</div>" +
       "<div class='col icon-move'><i class='fa fa-arrows'></i> <i class='clickable removeItem m-l-sm fa fa-times'></i></div>" +
  "</div>" +
  "<div class='row row-nw row-vc row-r m-r-xlg p-r-lg'>" +
      "<div id='editProfileSection' class='m-t-md favItemEditMode' style='display: none'>" +
          "<button   class='editFavItemSave btn btn-primary'>Save</button>" +
          "<button class='editFavItemCancel btn btn-primary btn-primary-nbg '>Cancel</button>" +
           "<div class='m-l-sm m-t-md req-fld reqFldMessage' style='display: none;'>Please complete required fields.</div>" +
      "</div>" +

    "</div>" +
  "</li>",
    widgetItem: "<li id='{0}' class='{8}' data-wId='{0}' data-title='{1}' data-description='{2}' data-type='{3}' data-link='{4}' data-linkDescription='{5}' data-order='{6}' data-resultSourceId='{7}'>" +
           "<div class='row row-nw row-vc'>" +
               "<div  class='col col-remaining'>" +
                   "<span class='text-bold'>{1}</span> <br/> {2}" +
               "</div>" +
			   	"<div class='col icon-move'><i class='fa fa-arrows'></i></div>" +
           "</div>" +
           "</li>",
    sitesItem: "<li data-url='{0}' data-title='{1}' data-description='{2}'>" +
			"<div class='row row-nw row-vc'>" +
			"<div class='col m-r-md '>" +
				"<span class='fa-stack fa-lg widget-list-icon'>" +
                                   "<i class='fa fa-circle fa-stack-2x icon-background-2'></i>" +
                                   "<i class='fa fa fa-desktop fa-stack-1x widget-icon-color'></i>" +
                               "</span>" +
			"</div>" +
				"<div data-site='{0}' class='col col-remaining site-item'>" +
					"<a href='{0}'><span class='text-bold personalise-item-header-link'>{1}</span></a><br/>{2}" +
				"</div>" +
					"<div class='col icon-move'><i class='fa fa-arrows'></i></div>" +
			"</div>" +
			"</li>",
    subCalItem: " <li data-refId='{0}' data-title='{1}'>" +
          "<div class='row row-nw row-vc'>" +
"<div class='col m-r-md '>" +
				"<span class='fa-stack fa-lg widget-list-icon'>" +
                                   "<i class='fa fa-circle fa-stack-2x icon-background-2'></i>" +
                                   "<i class='fa fa fa-calendar fa-stack-1x widget-icon-color'></i>" +
                               "</span>" +
			"</div>" +
              "<div  class='col col-remaining site-item'>" +
                  "<span class='text-bold'>{1}</span>" +
              "</div>" +
			  	"<div class='col icon-move'><i class='fa fa-arrows'></i></div>" +
          "</div>" +
          "</li>",
    nonstaticWidget: "<div id='{0}' class='masonry-item widget-container'>" +
                   "<div class='widget-close push-right clickable'><i class='icons-widget-close fa fa-times {5}'></i></div>" +
                   "<div class='p-t-lg p-l-md p-b-md'>" +
                       "<div class='heading-card'>{1}</div>" +
                       "<ul class='widget-items'>" +
                       "{2}" +
                       "</ul>" +
                   "</div>" +
                   "<div>" +
                  "</div>" +
                   "{3}",
    nonstaticWidgetButton: "<div class='p-b-md p-l-md'><a href='{0}' class='btn btn-primary btn-widget btn-widget-darker'>{1}</a></div>",
    nonstaticWidgetItem: "<li>" +
                             "<div class='row row-vt row-nw'>" +
                               "<div class='col-nm m-r-sm'><span class='fa-stack fa-lg widget-list-icon'>" +
                                   "<i class='fa fa-circle fa-stack-2x widget-icon-background'></i>" +
                                   "<i class='fa fa-link fa-stack-1x widget-icon-color'></i>" +
                               "</span></div>" +
                               "<div class='col-nm p-r-md'><a href='{0}'>{1}</a><div>" +
                              "</div>" +
                            "</li>",
    featuredPeopleContainer: "<div id='{3}' class='masonry-item widget-container widget-featured-people'>" +
                    "<div class='widget-close push-right clickable'><i class='icons-widget-close fa fa-times {2}'></i></div>" +
                    "<div id='featuredPeopleContainer'>" +
                    "{0}" +
                    "<div class='push-right p-b-sm'>" +
                    "<i class='next-chevron fa fa-chevron-right m-r-md m-l-md clickable' style='{1}'></i>" +
                    " </div>" +
                    "</div>" +
                    "</div>",
    featuredPerson: "<div class='featured-person centre' style='display: none'>" +
                            "<div class='heading-card p-t-xlg p-b-md'>Featured People</div>" +
                             "<div class='widget-featured-people-image-container'><div  class='roundImage' style='background-image: url({0});'> </div></div>" +
                            "<div class='p-t-lg link-featured'>Meet {1} {2} </div>" +
                            "<div class='text-bold'>{3}</div>" +
                            "<div>{4}</div>" +
                            "</div>",
    featuredPersonLinkable: "<div class='featured-person centre' style='display: none'>" +
                                "<div class='heading-card p-t-xlg p-b-md'>Featured People</div>" +
                                 "<a href='{5}'><div class='widget-featured-people-image-container'><div  class='roundImage' style='background-image: url({0});'> </div></div>" +
                                "<div class='p-t-lg link-featured'>Meet {1} {2} </div></a>" +
                                "<div class='text-bold'>{3}</div>" +
                                "<div>{4}</div>" +
                                "</div>",
    searchWidet: "<div id='{0}' class='masonry-item widget-container widget-bg-1'>" +
                    "<div class='widget-close push-right clickable'><i class='icons-widget-close fa fa-times {3}'></i></div>" +
                    "<div class='p-t-lg p-l-sm p-r-md'>" +
                        "<div class='heading-card p-b-sm'>{1}</div>" +
                       "<div class='row row-nw'><input type='text' id='peopleSearchBoxWidget' class='inputbox col-remaining' placeholder='Search by name, unit or skill' /></div>" +
                       "<div class='push-right p-b-md m-t-md'><a id='peopleSearchButtonWidget' class='btn btn-primary'>Search</a>" +
                    "</div>" +
                    "<div>" +
                   "</div>" +
                "</div>",
    myProfile: "<div id='{6}' class='masonry-item widget-container widget-myprofile'> " +
     "<div class='widget-close push-right clickable'><i class='icons-widget-close fa fa-times {5}'></i></div> " +
     "<div class='widget-close push-right clickable '></div> " +
     "<div class='p-t-lg p-l-lg'>" +
          "<div class='heading-card'>Hello {0}</div> " +
          "<div class='row row-nw row-sb p-t-md p-b-md p-r-md'>  " +
               "<div class='col col-7 relative-pos'>    " +
                    "<div class='push-left roundImage' style='background-image: url({1});'>" +
                         "<div id='imageUpdateOverlay' style='display: none;' class='roundImage centre overlay-profile-Image'>    " +
                              "<div class='overlay-contents'><i class='fa fa-camera icons'></i></div>" +
                              "</div> " +
                          "</div> " +
                    "</div> " +
               "<div class='col-m col-remaining'> " +
                    "<div id='profileDisplayMode' class='dispSection'>" +
                         "<div class='row row-nw row-sb'><div id='JobTitleDisplay' class='col'>{2}</div>  <div id='editProfile' class='col clickable'><i class='fa fa-pencil-square-o'></i></div></div>" +
                         "<div id='departmentDisplay' class=''>{3}</div>" +
                         "<div id='workPhoneDisplay' class=''>{4}</div>" +
                         "</div>" +
                    "<div id='profileEditMode' class='editSection' style='display: none;'>" +
                         "<div class='form-field-group'>" +
                              "Job Title <span class='req-fld'>*</span>" +
                              "<div class='row'>" +
                                   "<input type='text' id='jobTitleEdit' value='{2}' data-profilefield='Title' class='inputbox inputbox-small req-val '  /> " +
                                   "</div>" +
                              "</div>" +
                         "<div class='form-field-group'>" +
                              "Department <span class='req-fld'>*</span>" +
                              "<div class='row'>" +
                                   "<input type='text' id='departmentEdit' value='{3}' data-profilefield='UNSWBUSSchoolUnit' class='inputbox inputbox-small req-val ' />" +
                                   "</div>" +
                              "</div>" +
                         "<div class='form-field-group'>" +
                              "Work Phone <span class='req-fld'>*</span>" +
                              "<div class='row'>" +
                                   "<input type='text' id='workPhoneEdit' value='{4}' data-profilefield='WorkPhone' class='inputbox  inputbox-small req-val'  />" +
                                   "</div>" +
                              "</div>" +
                         "</div>" +
                    "<div id='editProfileSection' class='m-t-md editSection editProfileButtonContainer' style='display: none'>" +
                         "<button id='saveProfileEdit' class='btn btn-primary'>Save</button>" +
                         "<button id='cancelEdit' class='btn btn-primary btn-primary-nbg '>Cancel</button>" +
                          "<div id='requiredFieldMessage' class='m-l-sm m-t-md req-fld reqFldMessage' style='display: none;'>Please complete required fields.</div>" +
                         "</div> " +
                    "<div>" +
                         "<div class='p-t-xlg'>Profile completeness</div> " +
                         "<div id='profileCompletenessBar'></div> " +
                         "</div> " +
                    "<div class='p-t-md'><a class='widget-static-link' href='{7}'>Improve your profile</a></div> " +
                    "</div> " +
               " </div> " +
          "</div>" +
     "</div>",
    alerts: "<div class='ui-widget' style='display: none'>" +
                              "<div class='ui-state-alert-{0}' style='padding: 0 .7em;'>" +
                                  "<div class='row'>" +
                                      "<div class='col-nm col-remaining'>" +
                                         "<p>" +
                                              "<i class='fa fa-bell m-r-md m-l-md'></i>" +
                                              "<span class='alert-header'>{1}</span> {2}" +
                                          "</p>" +
                                      "</div>" +
                                      "<div class='col-nm col-2 '>" +
                                          "<p>" +
                                              "<a href='#' class='alert-next' style='{3}'>" +
                                                  "<i class='fa fa-chevron-right m-r-md m-l-md '></i>" +
                                              "</a>" +
                                              "<a href='#' class='alert-close'>" +
                                                  "<i class='fa fa-times m-r-md m-l-md push right'></i>" +
                                              "</a>" +
                                          "</p>" +
                                      "</div>" +
                                  "</div>" +
                              "</div>" +
                          "</div>",
    alertWithLink: "<div class='ui-widget' style='display: none'>" +
                              "<div class='ui-state-alert-{0}' style='padding: 0 .7em;'>" +
                                  "<div class='row'>" +
                                      "<div class='col-nm col-remaining'><a href='{4}'>" +
                                         "<p>" +
                                              "<i class='fa fa-bell m-r-md m-l-md'></i>" +
                                              "<span class='alert-header'>{1}</span> {2}" +
                                          "</p></a>" +
                                      "</div>" +
                                      "<div class='col-nm col-2 '>" +
                                          "<p>" +
                                              "<a href='#' class='alert-next' style='{3}'>" +
                                                  "<i class='fa fa-chevron-right m-r-md m-l-md '></i>" +
                                              "</a>" +
                                              "<a href='#' class='alert-close'>" +
                                                  "<i class='fa fa-times m-r-md m-l-md push right'></i>" +
                                              "</a>" +
                                          "</p>" +
                                      "</div>" +
                                  "</div>" +
                              "</div>" +
                          "</div>",
    eventsContainer: "<div class='col col-10'><div id='eventsContiner' class='widget-container'><div class='heading-card p-t-md p-l-md'>Upcoming Events</div><div class='p-t-md p-l-md p-b-md'>{0}</div>" +
        "<div class='row row-r p-b-md p-l-md p-r-md'><a href='{1}' class='btn btn-primary btn-widget btn-widget-darker'>View All Events</a></div></div>",
    eventItem:
        "<div class='row row-nw row-vt'>" +
  "<div class='col event-date-box centre '>{0}<br/><span class='event-month'>{1}</span></div>" +
  "<div class='col'>{2}<br/><span class='text-subtitle'>{3}</span></div>" +
     "</div>",
    featuredResourceCard: "<div  class='col col-remaining card-featured '>" +
                   "<div class='p-t-md p-l-lg p-r-lg p-b-lg'>" +
                       "<div class='m-b-md'><span class='text-featured text-upper'>Featured</span></div>" +
                       "<div class='heading-feature'>"
                       + "<span class='m-r-md fa-stack fa-lg icons-large'><i class='fa fa-circle fa-stack-2x  icon-search-background'></i>" +
                          "<i class='fa fa-lightbulb-o fa-stack-1x icon-feature-color'></i></span></i><a href='{4}'><span  class='heading-feature'>{0}</span></a></div>" +
                        "<div class='row-m row-nw '>" +
                            "<div class='col-m col-10'>{1}</div>" +
                             "<div class='col-m col-10'>" +
                                "<div class='resource-card '>" +
                                    "<div class='row row-vt resource-card-items'>{2}</div>" +
                                     "<div class='row m-t-md m-r-md showMoreConatiner'  style='display: none'>" +
                                        "<div  class='p-b-md p-l-md btn btn-primary btn-widget btn-widget-darker showMoreButtonFeatured'>Show more +</div> " +
                                     "</div>" +
                                "</div>" +
                             "</div>" +
                        "</div>" +
                        "<div class='row row-r'>{3} </div>" +
                   "</div>" +
     "</div>",
    resourceCard: "<div  class='col-m resource-container p-t-lg p-l-md p-r-md resource-card'>" +
                  "<div class='heading-section-tile'><a href='{3}'><span class='heading-section-tile'>{0}</span></a></div>" +
                  "<ul class='widget-items'>" +
                  "{1}" +
                  "</ul>" +
              "<div class='row m-r-md m-b-md showMoreConatiner'  style='display: none'><div  class='p-b-md p-l-md btn btn-primary btn-widget showMoreButton btn-widget-darker'>Show more +</div> </div> </div>",
    resourceCardItem: "<li>" +
                                   "<a href='{0}'>{1}</a>" +
                                 "</li>",
    featuredResourceCardItem: "<div class='col col-10'>" +
                                   "<span class='icons-small'> <i class='fa fa-angle-right'></i> </span> <a href='{0}'>{1}</a>" +
                                 "</div>",
    seeAllButton: "<div><a href='{0}' class='btn btn-primary btn-widget'>See all {1} categories</a></div>",
    subCategoryResourceCard: "<div  class='col widget-container p-t-lg p-l-md'>" +
                      "<a href='{2}'><div class='heading-section-tile'><span class='heading-section-tile'>{0}</span></div></a>" +
                      "<ul class='widget-items'>" +
                      "{1}" +
                      "</ul>" +
                  "<div class='row m-r-md m-b-md showMoreConatiner'  style='display: none'><div  class='p-b-md p-l-md btn btn-primary btn-widget showMoreButton btn-widget-darker'>Show more +</div> </div> </div>",
    tabs: "<a id='{0}' class='tab-header col-c col-3 clickable {1}' href='{2}'>{3}</a>",
    serachNavTab: "<a id='{0}' class='serachNavTab tab-header col-c col-3 clickable {1}' href='{2}'>{3}</a>",
    tag: "<span class='tag-item'><a href='{0}'></a></span>",
    alphabetRefiner: "<div class='col-nm p-l-sm p-r-sm p-b-sm tab-active'><a class='alpahbetRefiner' id='filterByAll' href='#k='>All</a></div>",
    letterRefiner: "<div class='letter-refiner-item col-nm p-b-sm'><a class='alpahbetRefiner' id='filterBy{1}' href='#k={0}%3A{1}*'>{1}</a></div>",
    confirmDialogWrapper: "<div style='padding: 10px; max-width: 500px; word-wrap: break-word;'>{0}</div>",
    busyIndicatorDialog: "<div style='padding: 10px; max-width: 500px; word-wrap: break-word;'><div class='centre'><div class='m-b-md'>{0}</div><img class='centre-img' src='{1}/Style%20Library/UNSW/images/loader.gif' /></div></div>",
    options: "<option value='{0}'>{0}</option>",
    noEventsMessage: "<div class='row row-nw row-vt p-t-md'>No upcoming events available to show</div>",
    noEventsSelectedMessage: "<div class='row row-nw row-vt p-t-md p-l-md'><a class='p-r-md' href='{0}'>You have no event calendars selected, please click here to select event calendars to show.</a></div>",
    noAuxBarItemsMessage: "<div class='col'>There are no items available, please visit the <a href='{0}'>'Personalise'</a> section and add some.<div>",
    noDepartmentSetMessage: "<div id='noDepartmentSetMessage' class='centre container-info-padded m-t-lg'>" +
        "<p>Widgets cannot be displayed right now.</p><p>A Business School Unit has not been assigned on your profile.</p><p> <a id='setMyProfileBtn' class='btn btn-primary btn-lg' href='{0}'><i class='fa fa fa-user icons m-r-sm'></i> Set my Profile</a></p>" +
    "</div>",
    noWidgetItemsMessage: "<div class='row row-nw row-vt'>There are currently no links available</div>",
    favItemsListWrapper: "<ul id='favsList'  class='auxGeneralList'>{0}</ul>",
    siteItemsListWrapper: "<ul id='sitesList'  class='auxGeneralList'>{0}</ul>",
    toolItemsListWrapper: "<div id='toolsList' class='row'>{0}</div>",
    popTopicsContainer: "<div class='row row-c row-vc popular-topics-container'> <span class='text-bold article-byline text-intro'>Popular topics: </span>{0}</div>",
    popTopicItem: "<span class='pop-topic-item'><a href='{0}'>{1}</a></span>{2}",
    breadCrumb: "<a href='{0}' class='crumb'>{1}</a>",
    breadCrumbLast: "<span class='crumb currpage-crumb'>{0}</span>",
    selectedDepartmentMessage: "<div class='centre'>You are assigned the {0}. If you would like to change this please go to <a href='{1}'>Personalise settings.</a><div>"


};



var siteCollectionUrl = "";
var webUrl = "";
var pageRelativeUrl = location.pathname;
var pageAbsoluteUrl = document.location.protocol + "//" + document.location.hostname + document.location.pathname
var userLoginName;
var userId;
//personalistion vars
var PersonalisationSite = "/Personalisation";
var auxBarAppValues = [];
var auxBarSiteValues = [];
var auxBarFavValues = [];
var widgets = [];
var myEvents = { events: [] };

var navTree = [];
var membershipProvider = "i:0#.f|membership|";
var userProperties = [];
var friendlyBreadcrumbGenerated = false;
/*Local Store Keys*/
var myWidgetsKey = "myWidgets";
var myAppsKey = "myApps";
var mySitesKey = "mySites";
var myFavsKey = "myFavs";
var auxBarProcessingComplete = false;
var auxBarFinalCall = $.Deferred();
var getUserWidgetsCall = $.Deferred();
var getSubCalsTeamUp = $.Deferred();
var setupNavigationTree = $.Deferred();
var setupTeamStructure = false;

function getLocalWidgets() {
    //localStorage.setObj(myWidgetsKey, remaingWidgets);

    var localAlertIds = localStorage.getObj("alertIds");
    console.log(localAlertIds + " --> " + typeof (localAlertIds));
    var wids = localStorage.getItem(myWidgetsKey);
    console.log(wids + " --> " + typeof (wids));
    if (wids) {
        console.log('true ');
    }



}

$(document).ready(function () {

    //$("a[href='https://unsw-my.sharepoint.com/person.aspx']").attr('href', 'http://maps.google.com/');
	
	//$("#O365_SubLink_ShellAboutMe").attr("href", "http://www.google.com");
    setupGolabVars();
    //setup the left hand navigation
    setupNav();
    //setup the footer content
    setupFooter()

    var inDesignMode = document.forms[MSOWebPartPageFormName].MSOLayout_InDesignMode.value;

    

    //Below code is to dynamically get the height of contentBox div and set that height to left navigation
    $("#s4-workspace").scroll(function () {
        var height = $("#contentBox").height();
        $("#sidebar-wrapper").css("min-height", height);
    });
    
    

    if (inDesignMode == "1") {
        // page is in edit mode
        //hide the aux bar and dont call SP to get items
		
		// To hide the top bar, remove the , this funtionality currently customised for ASB Connect // below
        // $("#AuxRow").hide();
        
		$("#contentBoxWrapper").removeClass("centre-page-content");
        $("#contentBoxWrapper").removeClass("centre-page-content-editMode");
    }
    else {
        // page is in browse mode
        
        // *** The setupAuxBarNew, getAlerts functions is customised to ASB Connect *** 
		
		//getAlerts();
		//setupAuxBarNew();
    }



    $("#saveToFavourites").click(function () {
        getFavouritesForUpdate();
    });

    //------------------ACCORDION SECTION START-------------------------
    $("#auxTabs").tabs({
        collapsible: true,
        show: { effect: "blind", duration: 300 },
        hide: { effect: "blind", duration: 300 },
        active: false
    });
    $('#auxTabHeaders li').on('click', function () {
        $(this).siblings().each(function () {
            $(this).children("a").children("span").children().first("i").next().removeClass('fa-chevron-up');
            $(this).children("a").children("span").children().first("i").next().addClass('fa-chevron-down');
        })
        var activeTab = $(this).children("a").children("span").children().first("i").next();
        if (activeTab.hasClass('fa-chevron-down')) {

            activeTab.removeClass('fa-chevron-down');
            activeTab.addClass('fa-chevron-up');
        }
        else {
            activeTab.removeClass('fa-chevron-up');
            activeTab.addClass('fa-chevron-down');
        }
    });
    //------------------ACCORDION SECTION END-------------------------
    //setupSearchButtonClickEvent();



    var mns = "main-nav-scrolled navmenu-fixed";
    var hdr = $('#s4-titlerow').height();
    var o365suiteBarHeight = $('#suiteBarTop').height();
    var ribbonRowHeight = $('#s4-ribbonrow').height();
    var contentWrapperWidth = $("#page-content-wrapper").width();
    var fixedItemsTopPadding = o365suiteBarHeight + ribbonRowHeight;
    console.log(fixedItemsTopPadding + " + " + hdr);
    $('#s4-workspace').bind('scroll', function () {
        if ($(this).scrollTop() > hdr) {

            $("#AuxRow").addClass("aux-row-scrolled");
            $("#AuxRow").css("margin-top", fixedItemsTopPadding);
            $("#AuxRow").css("max-width", contentWrapperWidth);

            $("#navmenu").addClass(mns);
            $("#navmenu").css("margin-top", fixedItemsTopPadding);

        } else {
            $("#AuxRow").removeClass("aux-row-scrolled");
            $("#AuxRow").css("margin-top", 0);
            $("#navmenu").removeClass(mns);
            $("#navmenu").css("margin-top", 0);
        }
    });

    var inDesignMode = document.forms[MSOWebPartPageFormName].MSOLayout_InDesignMode.value;
    if (inDesignMode == "1")
    {
        if($('#MSOTlPn_MainTD').length > 0){
                $('#MSOTlPn_MainTD').insertBefore($('#userWidgetsSection'))
        }
    }
});

function setupFooter() {
    var url = _spPageContextInfo.siteAbsoluteUrl + "/_api/web/lists/GetByTitle('FooterContent')/items";
    $.ajax({
        url: url,
        type: "GET",
        async: false,

        headers: {
            "accept": "application/json;odata=verbose"
        },
        success: function (data) {
            if (data.d.results.length > 0) {
                var text = data.d.results[0].Content;
                $("#contentBox").append(text);
            }
        },
        error: function (xhr, text_status) {
        }
    });
}

function setupHomePageTabs() {
  //handle home page widget tab clicks
$("#widgetsTabButton").click(function () {
       $("#widgetsTabButton").addClass("tab-active");
       $("#yammerTabButton").removeClass("tab-active");
       $("#widgetsTab").show()
       $("#yammerTab").hide()
       });

    $("#yammerTabButton").click(function () {
       $("#widgetsTabButton").removeClass("tab-active");
    $("#yammerTabButton").addClass("tab-active");
    $("#widgetsTab").hide()
    $("#yammerTab").show()
    });
}
function personaliseTabHandlers() {
    var toolTabHeader = $("#tools-header");
    var myFavsTabHeader = $("#myFavs-header");
    var myWidgetsTabHeader = $("#myWidgets-header");
    var mySitesTabHeader = $("#mySites-header");
    var myEventsTabHeader = $("#myEvents-header");
    var myProfileTabHeader = $("#myProfile-header");


    toolTabHeader.click(function () {

        var tabName = toolTabHeader.attr('id').split('-')[0];
        var dataTab = $("#" + tabName);
        dataTab.show()
        dataTab.siblings().hide();
        toolTabHeader.addClass("tab-active");
        toolTabHeader.siblings().removeClass("tab-active");
    });
    myFavsTabHeader.click(function () {
        var tabName = myFavsTabHeader.attr('id').split('-')[0];
        var dataTab = $("#" + tabName);
        dataTab.show()
        dataTab.siblings().hide();
        myFavsTabHeader.addClass("tab-active");
        myFavsTabHeader.siblings().removeClass("tab-active");
    });
    myWidgetsTabHeader.click(function () {
        var tabName = myWidgetsTabHeader.attr('id').split('-')[0];
        var dataTab = $("#" + tabName);
        dataTab.show()
        dataTab.siblings().hide();
        myWidgetsTabHeader.addClass("tab-active");
        myWidgetsTabHeader.siblings().removeClass("tab-active");
    });
    mySitesTabHeader.click(function () {
        var tabName = mySitesTabHeader.attr('id').split('-')[0];
        var dataTab = $("#" + tabName);
        dataTab.show()
        dataTab.siblings().hide();
        mySitesTabHeader.addClass("tab-active");
        mySitesTabHeader.siblings().removeClass("tab-active");
    });
    myEventsTabHeader.click(function () {
        var tabName = myEventsTabHeader.attr('id').split('-')[0];
        var dataTab = $("#" + tabName);
        dataTab.show()
        dataTab.siblings().hide();
        myEventsTabHeader.addClass("tab-active");
        myEventsTabHeader.siblings().removeClass("tab-active");
    });

    myProfileTabHeader.click(function () {
        var tabName = myProfileTabHeader.attr('id').split('-')[0];
        var dataTab = $("#" + tabName);
        dataTab.show()
        dataTab.siblings().hide();
        myProfileTabHeader.addClass("tab-active");
        myProfileTabHeader.siblings().removeClass("tab-active");
    });

}

function setupGolabVars() {

    siteCollectionUrl = _spPageContextInfo.siteAbsoluteUrl;
    webUrl = _spPageContextInfo.webServerRelativeUrl;
    userLoginName = membershipProvider + _spPageContextInfo.userLoginName;
    userId = _spPageContextInfo.userId;
    globalObjects.webAbsoluteUrl = _spPageContextInfo.webAbsoluteUrl;
    globalObjects.currentPageList = _spPageContextInfo.pageListId,
    globalObjects.currentPageItemId = _spPageContextInfo.pageItemId
}


//--------------TAXONOMY FUNCTIONS START--------------------------------

function setupNav() {

    try {
        getTermSetAsTree(constants.termSetGuids.connectNavTermSetId, true, function (terms) {
            var navHtml = '';
            //set the global nav oject
            globalObjects.navigationTree = terms.children;
            //TODO: wirte to session stroage
            // Kick off the term rendering
            for (var i = 0; i < terms.children.length; i++) {
                //Recursively loop through the termset and create list items
                navHtml += renderTerm(terms.children[i]);
            }

            //Below code is to change UI in search page

    //if($('div#SearchBox').length == 1 && $('div#SearchBox').css('display') == 'none')
    if(($('div#SearchBox').length == 1 && $('div#SearchBox').css('display') == 'none') || ($('div#SearchBox').length > 1))
    {
        var breadcrumbHtml = "<div class='row m-l-lg m-t-lg m-r-lg'><div class='col col-15'><div class='s4-notdlg noindex'>" +
                         "<span id='customBreadcrumb'></span></div></div><div class='col col-5 clickable' id='saveToFavourites'>" +
                         "<div id='connectNotificationArea' class='notification-area' style='display: none;'>" +
                         "</div>"; //<span class='push-right text-upper'><i class='fa fa-star m-r-sm icons-green'></i>Save to my favourites</span></div></div>";
        $(breadcrumbHtml).insertBefore('#DeltaPlaceHolderMain');

        $("#DeltaPlaceHolderMain").prepend($("#pageTitle"));
        $("#pageTitle").removeAttr("style");
        $("#pageTitle").addClass('centre');
        $('.welcome.blank-wp').addClass('content-area-background p-b-lg');
        $($('.ms-table.ms-fullWidth')[0]).addClass('centre-page-content-fass');
    }
    //else{
    //    $('#pageTitle').hide();
    //}
    if($('#customBreadcrumb').length == 0)
    {
            $('#pageTitle').hide();
    }

            var siteUrl = _spPageContextInfo.siteAbsoluteUrl;

			$('#DeltaPlaceHolderMain').css('margin-top','16px')
			
            //Below code is to add div with id "top"
            $("#s4-bodyContainer").prepend($('<div>', { id: 'top' }));

            //Below code is to update favicon
            var url = siteUrl + "/Style Library/Branding/Images/favicon.ico";
            $("#favicon").attr("href", "" + url + "");

            if (window.location.href.indexOf("IsDlg") == -1) {
            //Add grey bar above the middle content
            $("#contentBox").prepend($('<div>', { id: 'AuxRow', 'class': 'noindex aux-row-scrollfix', 'style': 'margin-top: 0px; max-width: 100%;' })
                .append($('<div>', { id: 'auxTabs', 'class': 'ui-tabs ui-widget ui-widget-content ui-corner-all ui-tabs-collapsible' })
                .append($('<ul>', { id: 'auxTabHeaders', 'class': 'ui-tabs-nav ui-helper-reset ui-helper-clearfix ui-widget-header ui-corner-all', 'role': 'tablist' }))));
           

          }
            //Code ends here


            $("#siteIcon").addClass("p-l-lg");

            //Add site title logo
            var titleLogoPath = _spPageContextInfo.siteAbsoluteUrl + "/Style Library/Branding/Images/title-logo.png";

            $(".ms-breadcrumb-top").hide();
            //if(document.location.href.toLowerCase().indexOf('search') == -1){
                //if($("#pageTitle").css('display') == 'none'){
                //$("#pageTitle").hide();
            //}

            $("#titleAreaRow").children('div').eq(1)
            .append($('<div>', { 'class': 'push-left connectLogoDivider' })
            .append($('<div>', { 'class': 'p-l-lg' })
            .append($('<a>', { 'href': '' + siteUrl + '' })
            .append($('<img>', { id: 'titleLogo', 'src': '' + titleLogoPath + '', style: 'height:64px;width:274px' })))));

            //Add search control
            $("#searchInputBox div").hide();

            $("#searchInputBox").append($('<div>', {style: 'width:310px;padding: 16px 24px 0px 0px'})//Changed by Ajo
                .append($('<input>', { type: 'text', id: 'globalSearchBox', 'class': 'inputbox', 'placeholder': 'Search' }))
                .append($('<a>', { id: 'globalSearchButton', 'class': 'btn btn-primary m-l-sm serachButton' }).append('SEARCH')
                .append($('<i>', { 'class': 'fa fa-search icons' }))));

            setupSearchButtonClickEvent();

            $("#DeltaPlaceHolderLeftNavBar").hide();
            $("#sideNavBox").css("float", "left");
            $("#sideNavBox").css("margin-left", "0px");
            $("#sideNavBox").css("margin-right", "0px");
            $("#sideNavBox").css("width", "245px");
            var contentBoxHeight = $("#contentBox").height();// - $('#AuxRow').height();
     		var divStyle="min-height:"+contentBoxHeight + "px";//+260+"px";
            //added by  ajo-->'style':divStyle in the below line
            $("#sideNavBox").append($('<div>', { id: 'sidebar-wrapper', 'class': 'col-nm s4-notdlg open','style':divStyle  })
                .append($('<div>', { id: 'navmenu' })
                .append($('<ul>', { 'class': '' }))));

            // Append the create HTML to the bottom of the page
            $("#navmenu ul").replaceWith("<ul class=''>" + navHtml + "</ul>");

            generateBreadcrumbForNonTermDrivenPage();

            //setup click event for nav item
            $("#navmenu li a span").click(function () {
                window.location = $(this).parent().attr('href');
                return false;
            });

            //setup click for nav item expander
            $('#navmenu li.has-sub>a').on('click', function () {
                //event.preventDefault ? event.preventDefault() : event.returnValue = false;
                var element = $(this).parent('li');
                if (element.hasClass('open')) {
                    element.removeClass('open');
                    element.find('li').removeClass('open');
                    element.find('ul').slideUp();
                    element.children('a').children(".holder").text('+');
                }
                else {
                    element.addClass('open');
                    element.children('ul').slideDown();
                    element.siblings('li').children('ul').slideUp();
                    element.siblings('li').removeClass('open');
                    element.siblings('li').find('li').removeClass('open');
                    element.siblings('li').find('ul').slideUp();
                    element.children('a').children(".holder").text('-');
                    element.siblings('li').children('a').children(".holder").text('+'); //change holder status of other holder items that are open
                }
                return false;
            });
            $('#navmenu>ul>li.has-sub>a').append('<div class="holder">+</div>');

            //if sub navigation elemnt is active open up its parent
            var activeParent = $("#navmenu .activeNav").parents();
            activeParent.addClass('open');
            activeParent.children('ul').slideDown();
            activeParent.children('a').children(".holder").text('-');
            if (setupTeamStructure) {
                setupTeamsStructure();
            }
            
            //Below code is to display title and navigation link in _layouts and _catalogs pages
            if (((window.location.href).indexOf("_layouts") != -1) || ((window.location.href).indexOf("_catalogs") != -1)) {
                $("#DeltaPlaceHolderMain").prepend($("#pageTitle"));
                $("#pageTitle").removeAttr("style");
            }
        });

    } catch (e) {
        console.log("error loading navigation menu, exception: " + e);

    }

};

/**
 * Returns a termset, based on ID
 *
 * @param {string} id - Termset ID
 * @param {object} callback - Callback function to call upon completion and pass termset into
 */
var getTermSet = function (id, callback) {
    SP.SOD.loadMultiple(['sp.js'], function () {
        // Make sure taxonomy library is registered
        SP.SOD.registerSod('sp.taxonomy.js', SP.Utilities.Utility.getLayoutsPageUrl('sp.taxonomy.js'));
        SP.SOD.registerSod('sp.publishing.js', SP.Utilities.Utility.getLayoutsPageUrl('sp.publishing.js'));
        SP.SOD.executeFunc('sp.publishing.js', 'SP.Publishing.Navigation.NavigationTermSet', function () {
            SP.SOD.loadMultiple(['sp.taxonomy.js'], function () {
                var ctx = SP.ClientContext.get_current(),
                    taxonomySession = SP.Taxonomy.TaxonomySession.getTaxonomySession(ctx),
                    termStore = taxonomySession.getDefaultSiteCollectionTermStore(),
                    termSet = termStore.getTermSet(id),
                    terms = termSet.getAllTerms();
                ctx.load(terms);

                ctx.executeQueryAsync(Function.createDelegate(this, function (sender, args) {
                    callback(terms);
                }),

                Function.createDelegate(this, function (sender, args) {
                    console.log(args);
                }));
            });
        });
    });
};

/**
 * Returns a term with children, based on ID
 *
 * @param {string} termSetId - Term Set ID
 * @param {string} termId - Term ID
 * @param {object} callback - Callback function to call upon completion and pass termset into
 */
var getTerm = function (termSetId, termId, callback) {
    SP.SOD.loadMultiple(['sp.js'], function () {
        // Make sure taxonomy library is registered
        SP.SOD.registerSod('sp.taxonomy.js', SP.Utilities.Utility.getLayoutsPageUrl('sp.taxonomy.js'));
        SP.SOD.registerSod('sp.publishing.js', SP.Utilities.Utility.getLayoutsPageUrl('sp.publishing.js'));
        SP.SOD.executeFunc('sp.publishing.js', 'SP.Publishing.Navigation.NavigationTermSet', function () {
            SP.SOD.loadMultiple(['sp.taxonomy.js'], function () {
                var ctx = SP.ClientContext.get_current(),

                taxonomySession = SP.Taxonomy.TaxonomySession.getTaxonomySession(ctx),
                    termStore = taxonomySession.getDefaultSiteCollectionTermStore(),
                    termSet = termStore.getTermSet(termSetId),
                 term = termSet.getTerm(termId),
                 terms = termSet.get_terms(),
                    allTerms = termSet.getAllTerms();
                ctx.load(termSet);
                ctx.load(term);
                ctx.load(terms, 'Include(Id, Name, PathOfTerm, TermsCount, LocalCustomProperties, Terms)');
                ctx.load(allTerms, 'Include(Id, Name, PathOfTerm, TermsCount, LocalCustomProperties, Terms)');
                ctx.executeQueryAsync(Function.createDelegate(this, function (sender, args) {
                    callback(allTerms, term);
                }),

                Function.createDelegate(this, function (sender, args) {
                    console.log(args);
                }));
            });
        });
    });
};

/**
 * Returns a single term, based on ID
 *
 * @param {string} termSetId - Term Set ID
 * @param {string} termId - Term ID
 * @param {object} callback - Callback function to call upon completion and pass termset into
 */
var getSingleTerm = function (termSetId, termId, callback) {
    SP.SOD.loadMultiple(['sp.js'], function () {
        // Make sure taxonomy library is registered
        SP.SOD.registerSod('sp.taxonomy.js', SP.Utilities.Utility.getLayoutsPageUrl('sp.taxonomy.js'));
        SP.SOD.registerSod('sp.publishing.js', SP.Utilities.Utility.getLayoutsPageUrl('sp.publishing.js'));
        SP.SOD.executeFunc('sp.publishing.js', 'SP.Publishing.Navigation.NavigationTermSet', function () {
            SP.SOD.loadMultiple(['sp.taxonomy.js'], function () {
                var ctx = SP.ClientContext.get_current(),
                taxonomySession = SP.Taxonomy.TaxonomySession.getTaxonomySession(ctx),
                    termStore = taxonomySession.getDefaultSiteCollectionTermStore(),
                    termSet = termStore.getTermSet(termSetId),
                 term = termSet.getTerm(termId);

                ctx.load(termSet);
                ctx.load(term);
                ctx.executeQueryAsync(Function.createDelegate(this, function (sender, args) {
                    callback(term);
                }),

                Function.createDelegate(this, function (sender, args) {
                    console.log(args);
                }));
            });
        });
    });
};

var getTermsWithCustomProperties = function (termSetId, customPropertyKey, callback) {
    SP.SOD.loadMultiple(['sp.js'], function () {
        // Make sure taxonomy library is registered
        SP.SOD.registerSod('sp.taxonomy.js', SP.Utilities.Utility.getLayoutsPageUrl('sp.taxonomy.js'));
        SP.SOD.registerSod('sp.publishing.js', SP.Utilities.Utility.getLayoutsPageUrl('sp.publishing.js'));
        SP.SOD.executeFunc('sp.publishing.js', 'SP.Publishing.Navigation.NavigationTermSet', function () {
            SP.SOD.loadMultiple(['sp.taxonomy.js'], function () {
                var ctx = SP.ClientContext.get_current(),
                taxonomySession = SP.Taxonomy.TaxonomySession.getTaxonomySession(ctx),
                    termStore = taxonomySession.getDefaultSiteCollectionTermStore(),
                    termSet = termStore.getTermSet(termSetId, 'Include(Id, Name, PathOfTerm, TermsCount, LocalCustomProperties, Terms)');

                ctx.load(termSet);
                ctx.executeQueryAsync(Function.createDelegate(this, function (sender, args) {
                    var term = termSet.getTermsWithCustomProperty(customPropertyKey);
                    var properties = term.get_objectData().get_properties()[customPropertyKey];
                    console.log(properties);
                    callback(term);
                }),

                Function.createDelegate(this, function (sender, args) {
                    console.log(args);
                }));
            });
        });
    });
};

function getwebRelUrlForNavTerm(navTerm, ctx) {
    var dfd = $.Deferred(function (navTerm, ctx) {

        var userInfoList = navTerm.getWebRelativeFriendlyUrl();

        ctx.load(users);
        ctx.executeQueryAsync(
            function () {
                // Successfull
                dfd.resolve(userInfoList);       // the argument here (users) is returned to the calling method below.
            },
            function (sender, args) {
                // Failure
                dfd.reject(args.get_message());
            });
    });
    return dfd.promise();
};


function loopNavTerms(navTerms, ctx) {
    //var navArray = [];
    //var navTermsEnumerator = navTerms.getEnumerator();
    //while (navTermsEnumerator.moveNext()) {
    //    var currNavTerm = navTermsEnumerator.get_current();
    //    var frienlyUrl = currNavTerm.getWebRelativeFriendlyUrl().get_value();
    //    ctx.load(frienlyUrl);
    //    navArray.push(frienlyUrl);
    //}
    //    ctx.executeQueryAsync(
    //    function (sender, args) {
    //        for (var i = 0; i < navArray.length; i++) {
    //            //do whatever you need to do with userNames here
    //            console.log(navArray[i]);
    //        }
    //    },
    //    function (sender, args) {
    //        console.load(args);
    //    }
    //);

    //$.each(navTerms, function (index, navTerm) {
    //    console.log(navTerm);
    //});
    for (var i = 0; i < navTerms.get_count() ; i++) {


        var navTerm = navTerms.getItemAtIndex(i);
        //console.log(navTerm.get_friendlyUrlSegment().get_value());
        //console.log(navTerm.get_id().toString());
        //console.log(navTerm.get_title().get_value());
        //console.log(navTerm.getWebRelativeFriendlyUrl());
        //console.log(navTerm.get_excludeFromGlobalNavigation());
        //console.log(navTerm.get_friendlyUrlSegment().get_value());

        //var frienlyUrl = navTerm.getWebRelativeFriendlyUrl();
        //ctx.load(navTerm);
        //ctx.load(frienlyUrl);
    }
    //    ctx.executeQueryAsync(
    //    function (sender, args) {
    //        console.log(navTerm.get_friendlyUrlSegment().get_value());
    //        console.log(navTerm.get_id().toString());
    //        console.log(navTerm.get_title().get_value());
    //        console.log(navTerm.getWebRelativeFriendlyUrl());
    //        console.log(navTerm.get_excludeFromGlobalNavigation());
    //        console.log(navTerm.get_friendlyUrlSegment().get_value());


    //        console.log(navTerm.getWebRelativeFriendlyUrl().get_value());
    //    },
    //    function (sender, args) {
    //        console.load(args);
    //    }
    //);
}

/**
 * Returns an array object of terms as a tree
 *
 * @param {string} id - Termset ID
 * @param {string} isTermSet - Is the term id for a term set if so set to true if its for a term then set to false
 * @param {object} callback - Callback function to call upon completion and pass termset into
 */
var getTermSetAsTree = function (id, isTermSet, callback) {
    var tree = null;
    if (isTermSet) {

        getTermSet(id, function (terms) {
            tree = buildTreeFromTerms(terms);
            callback(tree);
        });
    }
    else {
        getTerm(id, function (terms) {
            tree = buildTreeFromTerms(terms);
            callback(tree);
        });
    }



}

var getTermAsTree = function (termSetId, termId, callback) {
    var tree = null;
    getTerm(termSetId, termId, function (terms, term) {
        var taregtTermPath = term.get_pathOfTerm();
        var filteredTerms = filterTermsOnPath(taregtTermPath, terms);
        // tree = buildTreeFromSpecifcTerm(terms, term);
        var tree = buildTreeFromFilteredTerms(filteredTerms, filteredTerms[0].get_terms());
        callback(tree);
    });

}

function filterTermsOnPath(path, terms) {
    var termsEnumerator = terms.getEnumerator();
    var newNodes = new Array();
    var filteredTermsArray = new Array();

    while (termsEnumerator.moveNext()) {
        var currentTerm = termsEnumerator.get_current();
        var currTermPath = currentTerm.get_pathOfTerm();
        if (currTermPath.indexOf(path) > -1) {
            filteredTermsArray.push(currentTerm);
        }
    }
    return filteredTermsArray;
}


function buildTreeFromTerms(terms) {
    var termsEnumerator = terms.getEnumerator(),
        tree = {
            term: terms,
            children: []
        };

    // Loop through each term
    while (termsEnumerator.moveNext()) {
        var currentTerm = termsEnumerator.get_current();
        var currentTermPath = currentTerm.get_pathOfTerm().split(';');

        var children = tree.children;

        // Loop through each part of the path
        for (var i = 0; i < currentTermPath.length; i++) {
            var foundNode = false;

            for (var j = 0; j < children.length; j++) {
                if (children[j].name === currentTermPath[i]) {
                    foundNode = true;
                    break;
                }
            }

            // Select the node, otherwise create a new one
            var term = foundNode ? children[j] : { name: currentTermPath[i], children: [] };

            // If we're a child element, add the term properties
            if (i === currentTermPath.length - 1) {
                term.term = currentTerm;
                term.title = currentTerm.get_name();
                term.guid = currentTerm.get_id().toString();
                term.firendlyUrl = currentTerm.get_localCustomProperties()['_Sys_Nav_FriendlyUrlSegment']; //currentTerm.get_localCustomProperties()['_Sys_Nav_SimpleLinkUrl'];
                term.targetUrl = currentTerm.get_localCustomProperties()['_Sys_Nav_TargetUrl'];
                term.icon = currentTerm.get_localCustomProperties()[constants.termCustomPropKeys.icon];
                term.hidden = currentTerm.get_localCustomProperties()[constants.termCustomPropKeys.hiddenOnCustomNav];
                term.path = currentTerm.get_pathOfTerm();
                term.resourcePageUrl = currentTerm.get_localCustomProperties()[constants.termCustomPropKeys.resourcePageUrl];
                term.isDepartmentBasedResource = currentTerm.get_localCustomProperties()[constants.termCustomPropKeys.userDepartmentBasedCategory];
                term.teamStructureSource = currentTerm.get_localCustomProperties()[constants.termCustomPropKeys.teamStructureSource];
                term.searchDrivenResources = currentTerm.get_localCustomProperties()[constants.termCustomPropKeys.serachDrivenResources];
                term.description = currentTerm.get_description();
                term.defaultTabPageUrl = currentTerm.get_localCustomProperties()[constants.termCustomPropKeys.defaultTabPageUrl];
                term.parentGuid = currentTerm.get_parent();
                //console.log(propes);
            }

            // If the node did exist, let's look there next iteration
            if (foundNode) {
                children = term.children;
            }
                // If the segment of path does not exist, create it
            else {
                children.push(term);

                // Reset the children pointer to add there next iteration
                if (i !== currentTermPath.length - 1) {
                    children = term.children;
                }
            }
        }
    }

    tree = sortTermsFromTree(tree);

    return tree;
}


/**
 * Sort children array of a term tree by a sort order
 *
 * @param {obj} tree The term tree
 * @return {obj} A sorted term tree
 */
var sortTermsFromTree = function (tree) {
    // Check to see if the get_customSortOrder function is defined. If the term is actually a term collection,
    // there is nothing to sort.
    if (tree.children.length && tree.term.get_customSortOrder) {
        var sortOrder = null;

        if (tree.term.get_customSortOrder()) {
            sortOrder = tree.term.get_customSortOrder();
        }

        // If not null, the custom sort order is a string of GUIDs, delimited by a :
        if (sortOrder) {
            sortOrder = sortOrder.split(':');

            tree.children.sort(function (a, b) {
                var indexA = sortOrder.indexOf(a.guid);
                var indexB = sortOrder.indexOf(b.guid);

                if (indexA > indexB) {
                    return 1;
                } else if (indexA < indexB) {
                    return -1;
                }

                return 0;
            });
        }
            // If null, terms are just sorted alphabetically
        else {
            tree.children.sort(function (a, b) {
                if (a.title > b.title) {
                    return 1;
                } else if (a.title < b.title) {
                    return -1;
                }

                return 0;
            });
        }
    }

    for (var i = 0; i < tree.children.length; i++) {
        tree.children[i] = sortTermsFromTree(tree.children[i]);
    }

    return tree;
};

//
/**
 * Recursive function that will create the html for the navigation term
 *
 * @param {object} term - a navigation term with relevant it properties
 */
var renderTerm = function (term, parentFriendUrl) {
    if (term.isDepartmentBasedResource) {

        $.when(getUserProfileDetails(userLoginName)).done(function (userProfileProperties) {
            var userDepObject = findObject(userProfileProperties, "key", constants.userPropertyFieldNames.UNSWBUSSchoolUnit);
            $.when(getTermSetAsTree(constants.termSetGuids.resourceCategory, true, function (termsetTree) {
                //get the current resource term name that is being rendered on the page
                var mySchools = findObject(termsetTree.children, 'title', 'My School');
                var currUserSchool = findObject(mySchools.children, 'title', userDepObject.value);
                if (!currUserSchool) {
                    $('#navmenu a[href="' + siteCollectionUrl + "/" + mySchools.resourcePageUrl + '"]').hide();
                }
            }));

        });
    }
    var hasChildren = term.children && term.children.length;
    if (hasChildren) {
        //this is to ensure that not all the children are hidden on the nav, if all the children should be hidden on the nav then set hasChildren to fasle, this will
        //ensure that the correct css class is applied to the nav item (i.e. it will ensure its not expandale)
        var shouldBeExpandable = false;
        for (var childCount = term.children.length, i = 0; i < childCount; i++) {
            if (!term.children[i].hidden) {
                shouldBeExpandable = true;
                break;
            }
        }
        hasChildren = shouldBeExpandable;
    }
    var activeNavClass = "";

    var friendUrlPrefrix = "";
    if (parentFriendUrl && parentFriendUrl.length) {
        friendUrlPrefrix = parentFriendUrl + '/' + term.firendlyUrl;

    }
    var navItemUrl = friendUrlPrefrix == "" ? term.firendlyUrl : friendUrlPrefrix
    //store this terms path and firendly url path to be used later to build breadcrumb for non firenly url pages
    navTree.push({ title: term.title, path: term.path, firendlyUrlPath: navItemUrl, targetUrl: term.targetUrl });
    if (term.hidden && term.hidden.toLowerCase() == 'true') {
        return "";
    }

    else {
        var fullNavItemUrl = siteCollectionUrl + '/' + navItemUrl;
        // console.log(navItemUrl);
        if (pageAbsoluteUrl == fullNavItemUrl) {
            //if current page location is for current term, then set its nav item as active and create its breadcrumb (only for frednly url pages)
            activeNavClass = "activeNav";
            var currentPagePathItems = term.path.split(';');
            var currentPagePathUrls = navItemUrl.split('/');
            var breadCrumb = buildBreadcrumb(currentPagePathItems, currentPagePathUrls);
            $("#customBreadcrumb").append(breadCrumb);
            friendlyBreadcrumbGenerated = true;
        }
        var icon = "<i class='nav-icon {0}'></i>";
         var iconHtml = "";
        if (term.icon) {
            iconHtml = icon.format(term.icon);
        }
        var navItem = '<li name=\'' + term.title.replace(' ', '') + '\'><a href=\'' + fullNavItemUrl + '\' class=\'navItem ' + activeNavClass + '\'><span>' + iconHtml + term.title + '</span></a>';
        var navItemWithChildren = '<li name=\'' + term.title.replace(' ', '') + '\' class=\'has-sub navItem\' ><a href=\'' + fullNavItemUrl + '\' class=\'' + activeNavClass + '\'><span>' + iconHtml + term.title + '</span></a>';

        var html = hasChildren ? navItemWithChildren : navItem;

        if (hasChildren) {
            html += '<ul>';
            //recursive
            for (var i = 0; i < term.children.length; i++) {
                html += renderTerm(term.children[i], friendUrlPrefrix + term.firendlyUrl);
            }

            html += '</ul>';
        }

        return html + '</li>';
    }

}



function generateBreadcrumbForNonTermDrivenPage() {
    if (!friendlyBreadcrumbGenerated) {
        var parentNavItem = null;
        //grab the root crumb from the global navtree object
        $.each(navTree, function (index, navItem) {
            //if (navItem.targetUrl && navItem.targetUrl.indexOf(webUrl) > -1) {
            //    parentNavItem = navItem;
            //    return false;
            //}
            if (navItem.targetUrl) {
                var navItemPath = navItem.targetUrl.substring(0, navItem.targetUrl.lastIndexOf('/'));
                if (window.location.href.toLowerCase().indexOf(navItemPath.toLowerCase()) > -1) {
                    if (parentNavItem == null) {
                        parentNavItem = navItem;
                    }
                    else {
                        // get a closer parent then just the root or the Pages library 
                        if (navItem.targetUrl.length > parentNavItem.targetUrl.length) {
                            parentNavItem = navItem;
                        }
                    }
                }
            }

            if (parentNavItem == null && navItem.targetUrl && navItem.targetUrl.indexOf(webUrl) > -1) {
                parentNavItem = navItem;
                return false;
            }
        });
        //if a root breadcrumb is found for this non-friendly url page, then build out a breadcrumb for the page, else do not generate breadcrumb
        if (parentNavItem) {
            //build out the root of the breadcrumb based on the parentNavItem object
            var parentNavItemPathArray = parentNavItem.path.split(';');
            var parentNavItemPathUrls = parentNavItem.firendlyUrlPath.split('/');
            var breadCrumb = buildBreadcrumb(parentNavItemPathArray, parentNavItemPathUrls);
            //activate the left nav tab relative to the parentNavItem
            activateLefNavItem(parentNavItem.firendlyUrlPath);
            //if the current page is a resource centre assets page, check its query string params to build out its breadcrumb
            //these params will only be set on resocure centre assess pages
            var resourcePageMode = getParameterByName(constants.queryStringParams.modeParam.name, window.location.href);
            var currPageSourceCat = getParameterByName(constants.queryStringParams.sourceTerm.name, window.location.href);
            var currPagePathTerms = getParameterByName(constants.queryStringParams.pathTerms.name, window.location.href);
            var currPageParentTermUrl = getParameterByName(constants.queryStringParams.sourceUrl.name, window.location.href);
            var sdrTerm = getParameterByName(constants.queryStringParams.searchDrivenResource.name, window.location.href);
            if (currPageSourceCat) {
                $.when(getTermSetAsTree(constants.termSetGuids.resourceCategory, true, function (termsetTree) {
                    //get the current resource term name that is being rendered on the page
                    var currPageResourceCategoryName = "";
                    if (resourcePageMode == constants.pageRenderMode.termbased) {
                        var object = $("#" + constants.htmlElementIds.termBasedModeTitle);
                        currPageResourceCategoryName = $("#" + constants.htmlElementIds.termBasedModeTitle).text();
                    }
                    else {
                        currPageResourceCategoryName = $("#" + constants.htmlElementIds.pageTitle).text();
                    };
                    var currPageSourceCatObject = null;
                    if (sdrTerm) {
                        //get the sdr terms
                        var sdrTerms = [];
                        for (var resourceTermsCount = termsetTree.children.length, i = 0; i < resourceTermsCount; i++) {
                            if (termsetTree.children[i].searchDrivenResources) {
                                var currPageSdrChildTrem = findObject(termsetTree.children[i].children, 'guid', currPageSourceCat);
                                if (currPageSdrChildTrem) {
                                    currPageSdrChildTrem.resourcePageUrl = termsetTree.children[i].resourcePageUrl; //the the school resource page url to match the parent tab url as sdr items are dyanmically generated and dont have a url
                                    currPageSourceCatObject = currPageSdrChildTrem;
                                    break;
                                };
                            };
                        };
                    }
                    else {
                        currPageSourceCatObject = findObject(termsetTree.children, 'guid', currPageSourceCat);
                    };

                    //activate the left nav tab relative to the sourceTermObject
                    activateLefNavItem(currPageSourceCatObject.resourcePageUrl);

                    //now build out the path terms
                    if (currPageParentTermUrl) {

                        breadCrumb += " > " + htmlTemplates.breadCrumb.format(siteCollectionUrl + '/' + currPageSourceCatObject.resourcePageUrl, currPageSourceCatObject.title);
                        var decSourceUrl = decodeURIComponent(currPageParentTermUrl);
                        breadCrumb += " > " + htmlTemplates.breadCrumb.format(decSourceUrl, currPagePathTerms);
                        breadCrumb += " > " + htmlTemplates.breadCrumbLast.format(currPageResourceCategoryName);
                    }
                    else {

                        if (currPageSourceCatObject) {
                            var resourceCentrAssetsPath = " > " + htmlTemplates.breadCrumb.format(siteCollectionUrl + '/' + currPageSourceCatObject.resourcePageUrl, currPageSourceCatObject.title) +
                             " > " +
                             htmlTemplates.breadCrumbLast.format(currPageResourceCategoryName);
                            //apend the resource asset crumb path to the root crumb generated above
                            breadCrumb += resourceCentrAssetsPath;
                        };
                    };
                    $("#customBreadcrumb").append(breadCrumb);
                }));
            }
            else {
                //if this page is a standard page without a friendly url then build out a breadcrumb for it based on its location relative to the global navterm object
                breadCrumb += " > " + htmlTemplates.breadCrumbLast.format(document.title);
                $("#customBreadcrumb").append(breadCrumb);
            };

        }
    }
}

//--------------TAXONOMY FUNCTIONS END--------------------------------

var appDefinitions = [];
var widgetDefinitions = [];
var collabSites = [];
var subCalsDefinition = [];

var MyWidgetsItem;
var MySitesItem;
var MyFavsItem;
var MyToolsItem;
var MyEventsItem;
function getAllPersonalisationItems() {
    var batchExecutor = new RestBatchExecutor(siteCollectionUrl + "/Personalisation", { 'X-RequestDigest': $('#__REQUESTDIGEST').val() });
    //get app defintions
    var getAppsEP = siteCollectionUrl + "/Personalisation/_api/web/lists/GetByTitle('Apps')/items?" +
           "$select=UnswBus_ReferenceID,Title,UnswBus_Description,UnswBus_Hyperlink,UnswBus_Icon,UnswBus_RoleAudience&$orderby=UnswBus_SortingNumber desc,Title asc";
    //get wid defintions
    var getWidgetsEP = siteCollectionUrl + "/Personalisation/_api/web/lists/GetByTitle('Widgets')/items?" +
           "$select=UnswBus_ReferenceID,Title,UnswBus_Description&$orderby=UnswBus_SortingNumber desc,Title asc";
    //get collab sites
    var getSitesEp = siteCollectionUrl + "/Personalisation/_api/search/query?properties='SourceName:My Collab Sites,SourceLevel:SPSite'&selectproperties='Title,Path,SiteDescription'&rowlimit=300&trimduplicates=false";
    var commands = [];

    //add calls to batch request
    var batchRequest = new BatchRequest();
    batchRequest = new BatchRequest();
    batchRequest.endpoint = getAppsEP;
    batchRequest.headers = { 'accept': 'application/json;odata=verbose' }
    commands.push({ id: batchExecutor.loadRequest(batchRequest), title: "Apps", queryType: "list" });

    batchRequest = new BatchRequest();
    batchRequest.endpoint = getWidgetsEP;
    batchRequest.headers = { 'accept': 'application/json;odata=verbose' }
    commands.push({ id: batchExecutor.loadRequest(batchRequest), title: "Widgets", queryType: "list" });

    batchRequest = new BatchRequest();
    batchRequest.endpoint = getSitesEp;
    batchRequest.headers = { 'accept': 'application/json;odata=verbose' }
    commands.push({ id: batchExecutor.loadRequest(batchRequest), title: "Sites", queryType: "search" });


    batchExecutor.executeAsync().done(function (queryResults) {
        var d = queryResults;
        $.each(queryResults, function (index, resultItem) {

            var command = $.grep(commands, function (command) {
                return resultItem.id === command.id;
            });
            if (command.length) {
                var commandTitle = command[0].title;
                var commandQueryType = command[0].queryType;
                if (commandQueryType == "list") {
                    var data = resultItem.result.result.d;
                    if (data.results) {
                        if (commandTitle == "Apps") {
                            var itemsCount = data.results.length;
                            for (var i = 0; i < itemsCount ; i++) {
                                //extract tax term
                                var roleAud = "";
                                if (data.results[i].UnswBus_RoleAudience.results) {
                                    var ItemRoleField = data.results[i].UnswBus_RoleAudience.results;
                                    var rolesCount = ItemRoleField.length;
                                    for (var K = 0; K < rolesCount; K++) {
                                        roleAud += ItemRoleField[K].Label + '#';
                                    }
                                    // roleAud = data.results[i].UnswBus_RoleAudience.results[0].Label;
                                }
                                var obj = {
                                    refId: data.results[i].UnswBus_ReferenceID,
                                    title: data.results[i].Title,
                                    description: data.results[i].UnswBus_Description,
                                    icon: data.results[i].UnswBus_Icon,
                                    roleAudience: roleAud
                                };
                                appDefinitions.push(obj);
                            }
                        }
                        else if (commandTitle == "Widgets") {
                            var itemsCount = data.results.length;
                            for (var i = 0; i < itemsCount ; i++) {
                                var obj = {
                                    wId: data.results[i].UnswBus_ReferenceID,
                                    title: data.results[i].Title,
                                    description: data.results[i].UnswBus_Description
                                };
                                widgetDefinitions.push(obj);
                            }
                        }
                    }
                }
                else if (commandQueryType == "search") {
                    if (resultItem.result.result.d.query.PrimaryQueryResult) {
                        var searchResultItems = resultItem.result.result.d.query.PrimaryQueryResult.RelevantResults.Table.Rows.results;
                        var resultCount = resultItem.result.result.d.query.PrimaryQueryResult.RelevantResults.RowCount;
                        if (commandTitle == "Sites") {
                            for (var i = 0; i < resultCount; i++) {
                                var item = searchResultItems[i];
                                var obj = {
                                    title: item.Cells.results[2].Value,
                                    url: item.Cells.results[3].Value,
                                    description: item.Cells.results[4].Value
                                };
                                collabSites.push(obj);
                            }
                        }
                    }

                }
            }
        });
        renderPersonalisationItems();
    }).fail(function (err) {
        //TODO:SEND TO ERROR PROCESSOR// alert(JSON.stringify(err));
    });
}

function renderPersonalisationItems() {
    //render out tools
    var toolAvailabeHtml = "";
    var toolSelectedHtml = "";
    var myAppsCount = personalistionItems.myAppDefintions ? personalistionItems.myAppDefintions.length : 0;
    var availableApps = trimArray(personalistionItems.myAppDefintions, appDefinitions, 'refId');
    //build out availabe apps
    var availableAppsCount = availableApps.length;
    for (var i = 0; i < availableAppsCount; i++) {
        toolAvailabeHtml += htmlTemplates.toolItem.format(availableApps[i].refId, availableApps[i].icon.Url, availableApps[i].title, availableApps[i].description, availableApps[i].url, '', availableApps[i].roleAudience);
    }

    //build out selected apps
    for (var i = 0; i < myAppsCount ; i++) {
        var myAppObject = personalistionItems.myAppDefintions[i];
        var staticItem = myAppObject.staticItem ? 'static-item' : '';
        toolSelectedHtml += htmlTemplates.toolItem.format(myAppObject.refId, myAppObject.icon.Url, myAppObject.title, myAppObject.description, myAppObject.url, staticItem, myAppObject.roleAudience);
    }
    $("#toolsAvailable").append(toolAvailabeHtml);
    $("#toolsSelected").append(toolSelectedHtml);

    $("#resetApps").click(function () {
        confirmBuilder('Are you sure you want to reset your selections?', function () {
            $("#toolsSelected > li").not(".static-item").remove().appendTo("#toolsAvailable");
            //update user list item
            updateTools();
        }, function () {
        },
          'Confirm reset'
        );

        return false;
    });
    //register event for filter list
    $('#appAudienceFilter').on('change', function () {
        var filterVal = $(this).val();
        if (filterVal) {
            $("#toolsAvailable").children("[data-roleaudience!='" + filterVal + "']").hide();
            $("#toolsAvailable").children("[data-roleaudience*='" + filterVal + "']").show();

        }
        else {
            $("#toolsAvailable").children().show();
        }
    });

    //render out  favs
    var favsSelectedHtml = "";
    var myFavsCount = personalistionItems.favourites.values ? personalistionItems.favourites.values.favourites.length : 0;
    for (var i = 0; i < myFavsCount ; i++) {
        var myFavsObject = personalistionItems.favourites.values.favourites[i];
        favsSelectedHtml += htmlTemplates.favsItem.format(myFavsObject.url, myFavsObject.title, myFavsObject.description);
    }
    $("#myFavsSelected").append(favsSelectedHtml);
    //register event handlers
    editFavItemsEventListener();

    //render out wids
    var widsAvailabeHtml = "";
    var widsSelectedHtml = "";
	
	// Below commented out codes are customise code for ASB Connect
	
    // $.when(getUserWidgets()).done(function () {
        // getUserWidgetDefinitions().done(function () {
            // var myWidsCount = personalistionItems.myWidgetDefintions ? personalistionItems.myWidgetDefintions.length : 0;;
            // var availableWids = trimArray(personalistionItems.myWidgetDefintions, widgetDefinitions, 'wId');

            // //build out availabe wids
            // var availableWidsCount = availableWids.length;
            // for (var i = 0; i < availableWidsCount; i++) {
                // widsAvailabeHtml += htmlTemplates.widgetItem.format(availableWids[i].wId, availableWids[i].title, availableWids[i].description, availableWids[i].type, availableWids[i].link, availableWids[i].linkDescription, availableWids[i].order, availableWids[i].resultSourceId, '');
            // }

            // //build out selected wids
            // for (var i = 0; i < myWidsCount ; i++) {
                // var myWidget = personalistionItems.myWidgetDefintions[i];
                // var staticItem = myWidget.staticItem ? 'static-item' : '';
                // widsSelectedHtml += htmlTemplates.widgetItem.format(myWidget.wId, myWidget.title, myWidget.description, myWidget.type, myWidget.link, myWidget.linkDescription, myWidget.order, myWidget.resultSourceId, staticItem);
            // }

            // $("#resetWidgets").click(function () {
                // confirmBuilder('Are you sure you want to reset your selections?', function () {
                    // $("#myWidgetsSelected > li").not(".static-item").remove().appendTo("#myWidgetsAvailable");
                    // //update user list item
                    // updateWidgets();
                // }, function () {
                // },
                  // 'Confirm reset'
                // );

                // return false;
            // });
            // $("#myWidgetsAvailable").append(widsAvailabeHtml);
            // $("#myWidgetsSelected").append(widsSelectedHtml);
        // })
    // });




    //render out sites
    var sitesAvailabeHtml = "";
    var sitesSelectedHtml = "";
    var mySitesCount = personalistionItems.collabSites.values ? personalistionItems.collabSites.values.collabSites.length : 0;
    var availableSites = mySitesCount > 0 ? trimArray(personalistionItems.collabSites.values.collabSites, collabSites, 'url') : collabSites;

    //build out availabe sites
    var availableSitesCount = availableSites.length;
    for (var i = 0; i < availableSitesCount; i++) {
        var siteDescription = availableSites[i].description ? availableSites[i].description : "";
        console.log("available sites output - index: " + i + ", site: " + availableSites[i].title + ", descrip: " + siteDescription);
        sitesAvailabeHtml += htmlTemplates.sitesItem.format(availableSites[i].url, availableSites[i].title, siteDescription);
    }

    //build out selected sites
    for (var i = 0; i < mySitesCount ; i++) {
        var mySitesObject = personalistionItems.collabSites.values.collabSites[i];
        if (mySitesObject) {
            var siteDescription = mySitesObject.description ? mySitesObject.description : "";
            console.log("my sites output - index: " + i + ", site: " + mySitesObject.title + ", descrip: " + siteDescription);
            sitesSelectedHtml += htmlTemplates.sitesItem.format(mySitesObject.url, mySitesObject.title, siteDescription);
        };

    }

    $("#resetSites").click(function () {
        confirmBuilder('Are you sure you want to reset your selections?', function () {
            $("#mySitesSelected > li").not(".static-item").remove().appendTo("#mySitessAvailable");
            //update user list item
            updateSites();
        }, function () {
        },
          'Confirm reset'
        );

        return false;
    });

    $("#mySitessAvailable").append(sitesAvailabeHtml);
    $("#mySitesSelected").append(sitesSelectedHtml);


	// Below commented out codes are customised for ASB Connect
	
    // $.when(getUserEvents(), getSubCalendars()).done(function () {

        // var myEventsCount = personalistionItems.events.values ? personalistionItems.events.values.events.length : 0;
        // var availableSubCalendarsHtml = "";
        // var selectedSubCalendarsHtml = "";
        // //var availableCals = trimArray(myEvents.events, subCalsDefinition, 'id');
        // var availableCals = myEventsCount > 0 ? trimArray(personalistionItems.events.values.events, subCalsDefinition, 'id') : subCalsDefinition;

        // //build out availabe wids
        // var availableCalsCount = availableCals.length;
        // for (var i = 0; i < availableCalsCount; i++) {
            // availableSubCalendarsHtml += htmlTemplates.subCalItem.format(availableCals[i].id, availableCals[i].name);
        // }

        // //build out selected wids
        // for (var i = 0; i < myEventsCount ; i++) {
            // var myEventObject = personalistionItems.events.values.events[i];
            // selectedSubCalendarsHtml += htmlTemplates.subCalItem.format(myEventObject.id, myEventObject.name);
        // }

        // $("#resetEvents").click(function () {
            // confirmBuilder('Are you sure you want to reset your selections?', function () {
                // $("#myEventsSelected > li").not(".static-item").remove().appendTo("#myEventsAvailable");
                // //update user list item
                // updateEvents();
            // }, function () {
            // },
              // 'Confirm reset'
            // );

            // return false;
        // });
        // $("#myEventsAvailable").append(availableSubCalendarsHtml);
        // $("#myEventsSelected").append(selectedSubCalendarsHtml);
    // });
	
	
    setupSortableLists();
}


/**
 * Makes sure that the sortable lists heights are the same,
 * this is in place due to issues with Chrome not behaving well when the list height was specified at 100%, this made it hard for users to drag and drop items in short lists
 * this method will set the height of two sortable lists to the same height, this makes it easier to drag and drop items from a long list to a short one.
 * @param {string} sourceList - Id of sourceList
 * @param {string} targetList - Id of targetList
 */
function calculateSortableListHeight(sourceList, targetList) {
    var heightDiff = $("#" + sourceList).height() - $("#" + targetList).height();
    $("#" + sourceList).css('padding-bottom', (Math.abs(heightDiff) - heightDiff) / 2 + 'px');
    $("#" + targetList).css('padding-bottom', (Math.abs(heightDiff) + heightDiff) / 2 + 'px');
}

function setupSortableLists() {
    //tools list
    $("#toolsAvailable, #toolsSelected").sortable({
        create: function (event, ui) {
            calculateSortableListHeight("toolsAvailable", "toolsSelected");
        },
        stop: function (event, ui) {
            //if the user does not yet have a personalistion item created, then create it
            if (!personalistionItems.apps.listItemObject) {
                var defaultEventsObject = "";
                var url = "/Personalisation/_api/Web/Lists/GetByTitle('My Apps')/Items";
                var listItemData = {
                    __metadata: { 'type': 'SP.Data.MyAppsListItem' },
                    Title: userLoginName,
                    UnswBus_Values: defaultEventsObject,
                    UnswBus_AssignedUserId: userId
                };
                $.when(addNewItem(url, listItemData)).done(function (data) {
                    personalistionItems.apps.listItemObject = { __metadata: data.d.__metadata, ID: data.d.ID, UnswBus_Values: data.d.UnswBus_Values };
                    updateTools();
                });
            }
            else {
                updateTools();
            }
            calculateSortableListHeight("toolsAvailable", "toolsSelected");
        },
        connectWith: "#toolsAvailable, #toolsSelected",
        cancel: ".static-item"
    });
    //Widgets list
    $("#myWidgetsAvailable, #myWidgetsSelected").sortable({
        create: function (event, ui) {
            calculateSortableListHeight("myWidgetsAvailable", "myWidgetsSelected");
        },
        stop: function (event, ui) {
            //if the user does not yet have a personalistion item created, then create it
            if (!personalistionItems.widgets.listItemObject) {
                var defaultEventsObject = "";
                var url = "/Personalisation/_api/Web/Lists/GetByTitle('My Widgets')/Items";
                var listItemData = {
                    __metadata: { 'type': 'SP.Data.MyWidgetsListItem' },
                    Title: userLoginName,
                    UnswBus_Values: JSON.stringify(defaultEventsObject),
                    UnswBus_AssignedUserId: userId
                };
                $.when(addNewItem(url, listItemData)).done(function (data) {
                    personalistionItems.widgets.listItemObject = { __metadata: data.d.__metadata, ID: data.d.ID, UnswBus_Values: data.d.UnswBus_Values };
                    updateWidgets();
                });
            }
            else {
                updateWidgets();
            }
            calculateSortableListHeight("myWidgetsAvailable", "myWidgetsSelected");
        },
        connectWith: "#myWidgetsAvailable, #myWidgetsSelected",
        cancel: ".static-item"
    });
    //sites list
    $("#mySitessAvailable, #mySitesSelected").sortable({
        create: function (event, ui) {
            calculateSortableListHeight("mySitessAvailable", "mySitesSelected");
        },
        stop: function (event, ui) {
            //if the user does not yet have a personalistion item created, then create it
            if (!personalistionItems.collabSites.listItemObject) {
                var defaultEventsObject = { collabSites: [] }
                var url = "/Personalisation/_api/Web/Lists/GetByTitle('My Collab Sites')/Items";
                var listItemData = {
                    __metadata: { 'type': 'SP.Data.MyCollabSitesListItem' },
                    Title: userLoginName,
                    UnswBus_Values: JSON.stringify(defaultEventsObject),
                    UnswBus_AssignedUserId: userId
                };
                $.when(addNewItem(url, listItemData)).done(function (data) {
                    personalistionItems.collabSites.listItemObject = { __metadata: data.d.__metadata, ID: data.d.ID, UnswBus_Values: data.d.UnswBus_Values };
                    updateSites();
                });
            }
            else {
                updateSites();
            }
            calculateSortableListHeight("mySitessAvailable", "mySitesSelected");
        },
        connectWith: "#mySitessAvailable, #mySitesSelected"
    });

    $("#myFavsSelected").sortable({

        stop: function (event, ui) {

            updateFavs();

        }
    });

    $("#myEventsAvailable, #myEventsSelected").sortable({
        create: function (event, ui) {
            calculateSortableListHeight("myEventsAvailable", "myEventsSelected");
        },
        stop: function (event, ui) {
            //if the user does not yet have a personalistion item created, then create it
            if (!personalistionItems.events.listItemObject) {
                var defaultEventsObject = { events: [] }
                var url = "/Personalisation/_api/Web/Lists/GetByTitle('My Events')/Items";
                var listItemData = {
                    __metadata: { 'type': 'SP.Data.MyEventsListItem' },
                    Title: userLoginName,
                    UnswBus_Values: JSON.stringify(defaultEventsObject),
                    UnswBus_AssignedUserId: userId
                };
                $.when(addNewItem(url, listItemData)).done(function (data) {
                    personalistionItems.events.listItemObject = { __metadata: data.d.__metadata, ID: data.d.ID, UnswBus_Values: data.d.UnswBus_Values };
                    updateEvents();
                });
            }
            else {
                updateEvents();
            }
            calculateSortableListHeight("myEventsAvailable", "myEventsSelected");
        },
        connectWith: "#myEventsAvailable, #myEventsSelected"
    });

}

function updateTools() {
    var updatedMyToolsList = [];
    var elements = $("#toolsSelected").children();
    var updatedValue = "";
    $.each($("#toolsSelected").children(), function () {
        var ids = $(this).attr("data-refId");
        var auxBarAppItem = {
            refId: $(this).attr("data-refId"),
            title: $(this).attr("data-title"),
            description: $(this).attr("data-description"),
            icon: $(this).attr("data-appIcon"),
            url: $(this).attr("data-url")
        };
        updatedValue += $(this).attr("data-refId") + ";";
        updatedMyToolsList.push(auxBarAppItem);
    });
    //set local store value with updates
    personalistionItems.apps.values = updatedValue;
    personalistionItems.myAppDefintions = updatedMyToolsList;
    personalistionItems.apps.listItemObject.UnswBus_Values = updatedValue;
    //MyToolsItem.UnswBus_Values = updatedValue
    updateItemNoMetaTag(personalistionItems.apps.listItemObject.__metadata.uri, personalistionItems.apps.listItemObject, JSON.stringify(personalistionItems.apps.listItemObject), function (data) {
        //display suncess message here
    });

}

function updateWidgets() {
    var updatedList = [];
    var elements = $("#myWidgetsSelected").children();
    var updatedValue = "";
    $.each($("#myWidgetsSelected").children(), function () {
        var wigetItem = {
            wId: $(this).attr("data-wid"),
            title: $(this).attr("data-title"),
            description: $(this).attr("data-description"),
            link: $(this).attr("data-link"),
            linkDescription: $(this).attr("data-linkDescription"),
            order: $(this).attr("data-order"),
            resultSourceId: $(this).attr("data-resultSourceId")
        };
        updatedValue += $(this).attr("data-wid") + ";";
        updatedList.push(wigetItem);
    });
    //set local store value with updates
    personalistionItems.widgets.values = updatedValue;
    personalistionItems.widgetDefinitions = updatedList;
    personalistionItems.widgets.listItemObject.UnswBus_Values = updatedValue
    updateItemNoMetaTag(personalistionItems.widgets.listItemObject.__metadata.uri, personalistionItems.widgets.listItemObject, JSON.stringify(personalistionItems.widgets.listItemObject), function (data) {
    });
}

//TODO: Make Async
function updateFavs() {
    //if the user does not yet have a personalistion item created, then create it
    if (!personalistionItems.favourites.listItemObject) {
        var defaultEventsObject = { favourites: [] };
        var url = constants.restEndPoints.get_myFavsListItems;
        var listItemData = {
            __metadata: { 'type': 'SP.Data.MyFavouriteListItem' },
            Title: userLoginName,
            UnswBus_Values: JSON.stringify(defaultEventsObject),
            UnswBus_AssignedUserId: userId
        };
        $.when(addNewItem(url, listItemData)).done(function (data) {
            personalistionItems.favourites.listItemObject = { __metadata: data.d.__metadata, ID: data.d.ID, UnswBus_Values: data.d.UnswBus_Values };
            updateFavs();
        });
    }
    else {
        var myFavsObject = { favourites: [] }
        $.each($("#" + constants.htmlElementIds.myFavsSelected).children(), function () {
            var item = {
                url: $(this).attr("data-url"),
                title: $(this).attr("data-title"),
                description: $(this).attr("data-description")
            };
            myFavsObject.favourites.push(item);
        });
        localStorage.setObj(myFavsKey, myFavsObject);
        personalistionItems.favourites.values = myFavsObject;
        var newValues = JSON.stringify(myFavsObject);
        personalistionItems.favourites.listItemObject.UnswBus_Values = newValues
        updateItemNoMetaTag(personalistionItems.favourites.listItemObject.__metadata.uri, personalistionItems.favourites.listItemObject, JSON.stringify(personalistionItems.favourites.listItemObject), function (data) {
            //TODO: write to local storage
        });
    }

}



function updateSites() {

    var mySiteObject = { collabSites: [] }

    $.each($("#" + constants.htmlElementIds.mySitesSelected).children(), function () {
        var item = {
            url: $(this).attr(constants.dataFields.url),
            title: $(this).attr(constants.dataFields.title),
            description: $(this).attr(constants.dataFields.description)
        };
        mySiteObject.collabSites.push(item);
    });
    localStorage.setObj(mySitesKey, mySiteObject);
    personalistionItems.collabSites.values = mySiteObject;
    var newValues = JSON.stringify(mySiteObject);
    personalistionItems.collabSites.listItemObject.UnswBus_Values = newValues;
    updateItemNoMetaTag(personalistionItems.collabSites.listItemObject.__metadata.uri, personalistionItems.collabSites.listItemObject, JSON.stringify(personalistionItems.collabSites.listItemObject), function () {
    });
}

function updateEvents() {

    var updateVals = { events: [] };
    $.each($("#" + constants.htmlElementIds.myEventsSelected).children(), function () {
        var item = {
            id: parseInt($(this).attr(constants.dataFields.refId)),
            name: $(this).attr(constants.dataFields.title),
        };
        updateVals.events.push(item);
    });
    localStorage.setObj(constants.sessionStorageKeys.myEventsKey, updateVals);
    personalistionItems.events.values = updateVals;
    personalistionItems.events.listItemObject.UnswBus_Values = JSON.stringify(updateVals)
    updateItemNoMetaTag(personalistionItems.events.listItemObject.__metadata.uri, personalistionItems.events.listItemObject, JSON.stringify(personalistionItems.events.listItemObject), function () {
    });
}

function createPersonalistionItem(listName, defaultValue) {
    addNewItem(url, data)
}

function getUserWidgets() {

    var defTask = $.Deferred();
    var url = constants.restEndPoints.get_myWidgets.format(userId);
    var queryResults = getItems(url, function (data) {
        var widgetListItem = data.d.results[0];
        if (widgetListItem) {
            personalistionItems.widgets.values = widgetListItem.UnswBus_Values
            personalistionItems.widgets.listItemObject = widgetListItem;
        }
        defTask.resolve();
    });
    return defTask;

}

function getUserWidgetDefinitions() {
    var defTask = $.Deferred();
    var widgetDefinitionsEp = constants.restEndPoints.get_widgetDefs;
    var filter = "";
    var widgetIdsString = personalistionItems.widgets.values
    if (widgetIdsString) {
        var widgetIds = widgetIdsString.split(';');
        for (var f = 0; f < widgetIds.length; f++) {
            if (f != widgetIds.length - 1) {
                filter = filter + "( " + constants.fieldNames.referenceID + " eq '" +widgetIds[f]+ "') or";
            }
            else {
                filter = filter + "( " + constants.fieldNames.referenceID + " eq '" + widgetIds[f] + "'))";
            }
        }
        widgetDefinitionsEp = constants.restEndPoints.get_widgetDefsFiltered.format(filter);
    }
    getItems(widgetDefinitionsEp, function (data) {
        $.each(data.d.results, function (index, widgetItem) {
            var widgetType = widgetItem.UnswBus_WidgetType;
            var widgetId = widgetItem.UnswBus_ReferenceID;
            var widgetTitle = widgetItem.Title;
            var widgetLink;
            var widgetLinkDescription;
            var widgetStatic = widgetItem.UnswBus_Static;
            var widgetDescription = widgetItem.UnswBus_Description;
            var sortOrder = widgetItem.UnswBus_SortingNumber;
            var resultSourceId = widgetItem.UnswBus_ResultSourceId;

            if (widgetItem.UnswBus_Hyperlink) {
                widgetLink = widgetItem.UnswBus_Hyperlink.Url;
                widgetLinkDescription = widgetItem.UnswBus_Hyperlink.Description
            }
            var widgetObject = {
                wId: widgetId,
                title: widgetTitle,
                description: widgetDescription,
                type: widgetType,
                link: widgetLink,
                linkDescription: widgetLinkDescription,
                order: sortOrder,
                resultSourceId: resultSourceId,
                staticItem: widgetStatic,
                items: []
            };
            personalistionItems.myWidgetDefintions.push(widgetObject);
        });
        if (personalistionItems.widgets.values) {
            personalistionItems.myWidgetDefintions.sort(sorter('wId', personalistionItems.widgets.values.split(';')));
        }
        defTask.resolve();
    });
    return defTask;
}


function constructWidgets() {
    //becasue some widgets are based on user dep, we need to call and wait for it to return
    $.when(getUserProfileDetails(userLoginName)).done(function (userProfileProperties) {
        var userDepObject = findObject(userProfileProperties, "key", constants.userPropertyFieldNames.UNSWBUSSchoolUnit);
        if (userDepObject) {
            //now construct the calls for widget with dynamic conetnet and execute to get that contnet back
            var batchExecutor = new RestBatchExecutor(siteCollectionUrl, { 'X-RequestDigest': $('#__REQUESTDIGEST').val() });
            var commands = [];
            var batchRequest;
            $.each(personalistionItems.myWidgetDefintions, function (index, widget) {
                if (widget.type.toLowerCase() == "general widget") {
                    //build serach queries to get the contnets for the widgets
                    batchRequest = new BatchRequest();
                    batchRequest.endpoint = siteCollectionUrl + "/_api/search/query?querytext='UnswBusReferenceIDOWSTEXT:" + widget.wId + "'"
                    + "&properties='SourceName:Widget Items - General,SourceLevel:SPSite'&selectproperties='Title,UnswBusHyperlinkOWSURLH'&trimduplicates=false";;

                    batchRequest.headers = {
                        'accept': 'application/json;odata=verbose'
                    }
                    commands.push({ id: batchExecutor.loadRequest(batchRequest), title: widget.wId, widgetType: widget.type });
                }
                else if (widget.type.toLowerCase() == "departmental widget") {

                    //build serach queries to get the contnets for the widgets
                    batchRequest = new BatchRequest();
                    // batchRequest.endpoint = "/_api/search/query?querytext='UNSWBUSSchoolUnit:+"+userSchoolDepartment+"+UnswBusReferenceIDOWSTEXT:+"+widget.wId+"'"
                    // +"&sourceid='32d02238-f0a0-4222-88c3-b96457d9b2de'&selectproperties='Title,UnswBusHyperlinkOWSURLH'";
                    batchRequest.endpoint = siteCollectionUrl + "/_api/search/query?querytext='UNSWBUSSchoolUnit:" + userDepObject.value + "+UnswBusReferenceIDOWSTEXT:" + widget.wId + "'"
                  + "&properties='SourceName:Widget Items - Departmental,SourceLevel:SPSite'&selectproperties='Title,UnswBusHyperlinkOWSURLH'&trimduplicates=false";
                    batchRequest.headers = {
                        'accept': 'application/json;odata=verbose'
                    }
                    commands.push({ id: batchExecutor.loadRequest(batchRequest), title: widget.wId, widgetType: widget.type });



                }
                else if (widget.type.toLowerCase() == "result source driven widget") {
                    //build serach queries to get the contnets for the widgets
                    batchRequest = new BatchRequest();
                    batchRequest.endpoint = siteCollectionUrl + "/_api/search/query?properties='SourceName:" + widget.resultSourceId + ",SourceLevel:SPSite'&rowlimit=5&trimduplicates=false";
                    batchRequest.headers = {
                        'accept': 'application/json;odata=verbose'
                    }
                    commands.push({ id: batchExecutor.loadRequest(batchRequest), title: widget.wId, widgetType: widget.type });
                }
            });
            //now execute the queries for the dynamic conetnt widgets to get the contents for those widgets
            batchExecutor.executeAsync().done(function (result) {
                var d = result;
                var itemIds = [];
                $.each(result, function (k, v) {
                    var command = $.grep(commands, function (command) {
                        return v.id === command.id;
                    });
                    if (command.length) {
                        var widgetId = command[0].title
                        var widgetType = command[0].widgetType
                        var data = v.result;
                        if (data.result.d.query.PrimaryQueryResult) {
                            //build widget items
                            var searchResultItems = data.result.d.query.PrimaryQueryResult.RelevantResults.Table.Rows.results;
                            $.each(searchResultItems, function (index, resultItem) {
                                if (widgetType.toLowerCase() == constants.widgetTypes.departmental || widgetType.toLowerCase() == constants.widgetTypes.general) {

                                    var linkTitle = resultItem.Cells.results[2].Value;
                                    var link;
                                    if (resultItem.Cells.results[3].Value) {
                                        link = resultItem.Cells.results[3].Value.split(',')[0];
                                    }
                                    var widgetChildItem = {
                                        title: linkTitle, link: link
                                    };
                                    var widgetIndex = $.grep(personalistionItems.myWidgetDefintions, function (e) { return e.wId == widgetId; });//widgets.findIndex(x => x.wId== widgetId)
                                    widgetIndex[0].items.push(widgetChildItem);

                                }
                                else if (widgetType.toLowerCase() == constants.widgetTypes.resultSource) {

                                    var linkTitle = resultItem.Cells.results[3].Value;
                                    var link = resultItem.Cells.results[6].Value;
                                    var widgetChildItem = {
                                        title: linkTitle, link: link
                                    };
                                    var widgetIndex = $.grep(personalistionItems.myWidgetDefintions, function (e) { return e.wId == widgetId; });//widgets.findIndex(x => x.wId== widgetId)
                                    widgetIndex[0].items.push(widgetChildItem);
                                }

                            });
                        }
                    }
                });
                renderWidgets();
            }).fail(function (err) {
                //TODO:SEND TO ERROR PROCESSOR// alert(JSON.stringify(err));
            });
        }
        else {
            $("#" + constants.htmlElementIds.userWidgetsSection).append(htmlTemplates.noDepartmentSetMessage.format(siteCollectionUrl + constants.pageUrls.myProfile));
        }

    });



}

function renderWidgets() {
    //Loop through each widget and render it
    $.each(personalistionItems.myWidgetDefintions, function (index, widget) {
        var widgetItemsHtml = "";
        var widgetHtml = "";
        if (widget.type.toLowerCase() == constants.widgetTypes.departmental ||
            widget.type.toLowerCase() == constants.widgetTypes.general ||
            widget.type.toLowerCase() == constants.widgetTypes.resultSource)
        {

            $.each(widget.items, function (index, widgetItem) {
                //build widget items
                widgetItemsHtml += htmlTemplates.nonstaticWidgetItem.format(widgetItem.link, widgetItem.title);
            });
            var widgetItems = widgetItemsHtml ? widgetItemsHtml : htmlTemplates.noWidgetItemsMessage;
            var widgetButton = widget.link ? htmlTemplates.nonstaticWidgetButton.format(widget.link, widget.linkDescription) : "";
            var fixedWidget = widget.staticItem ? "widget-fixed" : "";
            widgetHtml += htmlTemplates.nonstaticWidget.format(widget.wId, widget.title, widgetItems, widgetButton, fixedWidget);
            $("#"+ constants.htmlElementIds.userWidgetsSectionRow).append(widgetHtml);
        }
        else if (widget.wId.toLowerCase() == constants.staticWidgetIdentifiers.myprofile) {
            $.when(getUserProfileDetails(userLoginName)).done(function (userProfileProperties) {
                renderMyProfileWidget(userProfileProperties, widget);
            });
        }
        else if (widget.wId.toLowerCase() == constants.staticWidgetIdentifiers.featuredPeople) {
            var featuredPeopleQueryText = "";
            var featuredPeopleArray = [];
            var url = constants.restEndPoints.get_featuredPeople.format(new Date().toISOString());
            var queryResults = getItems(url, function (data) {
                $.each(data.d.results, function (index, featuredPerson) {
                    var simpleUserName = featuredPerson.UnswBus_AssignedUser.Name.split(membershipProvider)[1].split('@')[0];
                    var featuredPersonHyperlink = null;
                    if (featuredPerson.UnswBus_Hyperlink) {
                        featuredPersonHyperlink = featuredPerson.UnswBus_Hyperlink.Url;
                    }
                    var featuredPersonObject = { accountName: featuredPerson.UnswBus_AssignedUser.Name, hyperlink: featuredPersonHyperlink };
                    featuredPeopleArray.push(featuredPersonObject);

                    if (index != data.d.results.length - 1) {
                        featuredPeopleQueryText += "AccountName:" + encodeURIComponent(simpleUserName) + " OR ";
                    }
                    else {
                        featuredPeopleQueryText += "AccountName:" + encodeURIComponent(simpleUserName)
                    }

                });
                 if (featuredPeopleQueryText) {
                     var featuredPeopleEP = constants.restEndPoints.get_featuredPeopleProps.format(featuredPeopleQueryText);
                    var itesm = getItems(featuredPeopleEP, function (data) {

                        if (data.d.query.PrimaryQueryResult.RelevantResults.Table.Rows.results) {
                            var resultCount = data.d.query.PrimaryQueryResult.RelevantResults.RowCount
                            //build widget items
                            var featuredPeopleHtml = "";
                            var searchResultItems = data.d.query.PrimaryQueryResult.RelevantResults.Table.Rows.results;

                            $.each(searchResultItems, function (index, resultItem) {
                                var jobTitle = resultItem.Cells.results[2].Value;
                                //clean the job title
                                jobTitle = jobTitle ? jobTitle.split(';')[0] : null;
                                var department = resultItem.Cells.results[3].Value;
                                var firstName = resultItem.Cells.results[4].Value;
                                var lastName = resultItem.Cells.results[5].Value;
                                var profilePictureUrl = resultItem.Cells.results[6].Value;
                                var accountName = resultItem.Cells.results[7].Value;
                                var email = resultItem.Cells.results[8].Value;
                                var featuredPersonObject = findObject(featuredPeopleArray, 'accountName', accountName);
                                var hyperlink = featuredPersonObject ? featuredPersonObject.hyperlink : null;
                                //construct userPhoto url
                                var userPhotoUrl = constants.pageUrls.userPhotoPage.format(email);
                                if (hyperlink) {
                                    featuredPeopleHtml += htmlTemplates.featuredPersonLinkable.format(encodeURI(userPhotoUrl), firstName, lastName, jobTitle, department, hyperlink);
                                } else {
                                    featuredPeopleHtml += htmlTemplates.featuredPerson.format(encodeURI(userPhotoUrl), firstName, lastName, jobTitle, department);
                                };

                            });
                            var pagerHtml = resultCount > 1 ? "display: block" : "display: none";
                            var fixedWidget = widget.staticItem ? "widget-fixed" : "";
                            $("#" + constants.htmlElementIds.userWidgetsSectionRow).append(htmlTemplates.featuredPeopleContainer.format(featuredPeopleHtml, pagerHtml, fixedWidget, widget.wId));
                        }
                        $(".widget-close").click(function () {
                            widgetClose($(this).parent().attr('id'))
                        });
                        //click through effect
                        var featuredPeopleIndex = 0;
                        var featuredPeopleCount = searchResultItems.length;
                        //set the first alert to be displayed
                        $("#" + constants.htmlElementIds.featuredPeopleContainer).children().first().show();
                        $(".next-chevron").click(function () {
                            $("#"+ constants.htmlElementIds.featuredPeopleContainer).children().eq(featuredPeopleIndex).hide()
                            featuredPeopleIndex = (featuredPeopleIndex + 1) % featuredPeopleCount;
                            $("#" + constants.htmlElementIds.featuredPeopleContainer).children().eq(featuredPeopleIndex).show()
                        });
                        setupPeopleSearchClickEvent();
                    });
                }

            });

        }
        else if (widget.wId.toLowerCase() == constants.staticWidgetIdentifiers.peopleSearch) {
            var fixedWidget = widget.staticItem ? "widget-fixed" : "";
            var widgetHtml = htmlTemplates.searchWidet.format(widget.wId, widget.title, widget.link, fixedWidget);
            $("#" + constants.htmlElementIds.userWidgetsSectionRow).append(widgetHtml);
            //register the click handler for the search button
            $("#" + constants.htmlElementIds.widgetSerachButton).click(function () {
                event.preventDefault ? event.preventDefault() : event.returnValue = false;
                //redirect to search page
                var queryVal = $("#" + constants.htmlElementIds.serachTextWidget).val();
            });
        }

    });
    //register the widgets close event
    $(".widget-close").unbind().on('click', function () {
        widgetClose($(this).parent().attr('id'))
    });
}

function widgetClose(widgetId) {

    $("#" + widgetId).remove();
    var remaingWidgets = "";
    //REST call to update the my widgets list
    $("#" + constants.htmlElementIds.userWidgetsSectionRow +" > .widget-container").each(function () {
        remaingWidgets += $(this).attr('id') + ";";
    });
    //write to my widgets personalistion list
    updateMyWidgetsList(remaingWidgets)
}

function renderMyProfileWidget(userProfileProperties, widget) {

    var titleIndex = $.grep(userProfileProperties, function (e) { return e.key == constants.userPropertyFieldNames.Title; });
    var unitIndex = $.grep(userProfileProperties, function (e) { return e.key == constants.userProfilrPropertyNames.UNSWBUSSchoolUnit; });
    var phoneIndex = $.grep(userProfileProperties, function (e) { return e.key == constants.userProfilrPropertyNames.WorkPhone; });
    var profilePicIndex = $.grep(userProfileProperties, function (e) { return e.key == constants.userProfilrPropertyNames.PictureURL; });
    var firstNameIndex = $.grep(userProfileProperties, function (e) { return e.key == constants.userProfilrPropertyNames.FirstName; });
    var emailIndex = $.grep(userProfileProperties, function (e) { return e.key == constants.userProfilrPropertyNames.WorkEmail; });

    var fixedWidget = widget.staticItem ? "widget-fixed" : "";
    var firstName = firstNameIndex[0] ? firstNameIndex[0].value : "";
    var email = emailIndex[0] ? emailIndex[0].value : "";
    var profilePicUrl = profilePicIndex[0] ? profilePicIndex[0].value : "";
    var jobTitle = titleIndex[0] ? titleIndex[0].value : "";
    var schoolUnit = unitIndex[0] ? unitIndex[0].value : "";
    var phone = phoneIndex[0] ? phoneIndex[0].value : "";
    //construct userPhoto url
    var userPhotoUrl = constants.pageUrls.userPhotoPage.format(email);
    var myProfileHtml = htmlTemplates.myProfile.format(firstName, userPhotoUrl, jobTitle, schoolUnit, phone, fixedWidget, widget.wId, siteCollectionUrl + constants.pageUrls.myProfile)
    $("#"+constants.htmlElementIds.userWidgetsSectionRow).append(myProfileHtml);
    $(".widget-close").click(function () {
        widgetClose($(this).parent().attr('id'))
    });
    $('#' + constants.htmlElementIds.editProfile).click(function () {
        //$('#jobTitleEdit').val($('#JobTitleDisplay').text());
        $('#editProfileSection').show();
        //$('#jobTitleEdit').show();
        //$('#JobTitleDisplay').hide();
        $('#imageUpdateOverlay').show();

        $('.dispSection').hide("fast");
        $('.editSection').show("fast");
    });
    $('#' + constants.htmlElementIds.cancelEdit).click(function () {
        //event.preventDefault ? event.preventDefault() : event.returnValue = false;
        $('#editProfileSection').hide();
        $('#imageUpdateOverlay').hide();

        $('.dispSection').show("fast");
        $('.editSection').hide("fast");
        return false;

    });
    $('#' + constants.htmlElementIds.saveProfileEdit).click(function () {
        $(this).siblings("#requiredFieldMessage").hide("fast");
        var fromVaild = formValidatorWidget("#" + constants.htmlElementIds.profileEditMode);
        if (fromVaild) {
            saveUserProfileDetails(constants.htmlElementIds.profileEditMode);
            $('.dispSection').show("fast");
            $('.editSection').hide("fast");
            $('#imageUpdateOverlay').hide();
            var updatedJobTitle = $('#jobTitleEdit').val();
            var updatedDepartment = $('#departmentEdit').val();
            var updatedWorkPhone = $('#workPhoneEdit').val();
            $('#JobTitleDisplay').text(updatedJobTitle);
            $('#departmentDisplay').text(updatedDepartment);
            $('#workPhoneDisplay').text(updatedWorkPhone);
            return false;
        } else {
            $(this).siblings("#requiredFieldMessage").show("fast");
            return false;
        }

    });
    $('#' + constants.htmlElementIds.imageUpdateOverlay).click(function () {
        //  $('#profilePicFile').show();
        //$('#profilePicChangeDialog').dialog("open");
        showProfilePicUploadDialog();
        // setProfilePicture();
    });
    $('#' + constants.htmlElementIds.profilePicCancel).click(function () {
        // $('#profilePicFile').show();
        closeProfilePicUploadDialog();
        // setProfilePicture();
    });
    $('#' + constants.htmlElementIds.profilePicSave).click(function () {



    });

    setAutoCompeletField(constants.htmlElementIds.departmentEdit, constants.termSetGuids.BusSchoolUnit);
    //setup the profile completeness bar
    processProfileCompleteness();
}

/**
 * Saves an values to user profile properties
 * @param {object Array} propertiesForUpdate - array of user property objects to update, object should conatin user profile property name and value
 */
function updateUserProfile(propertiesForUpdate) {
    var deferred = $.Deferred();
    SP.SOD.executeFunc("sp.js", "SP.ClientContext", function () {
        SP.SOD.registerSod("sp.userprofiles.js", SP.Utilities.Utility.getLayoutsPageUrl("sp.userprofiles.js"));
        SP.SOD.executeFunc("sp.userprofiles.js", "SP.UserProfiles.PeopleManager", function () {
            var cc = SP.ClientContext.get_current();
            var peopleManager = new SP.UserProfiles.PeopleManager(cc);
            var userProfileProperties = peopleManager.getMyProperties();
            for (var propertiesCount = propertiesForUpdate.length, i = 0; i < propertiesCount ; i++) {
                var propTitle = propertiesForUpdate[i].propFieldName;
                var propValue = propertiesForUpdate[i].value
                var multiValueProp = propertiesForUpdate[i].multiValue;
                if (multiValueProp) {
                    //Set a multivalue property
                    var multipleValues = propValue.split(',');
                    peopleManager.setMultiValuedProfileProperty(userLoginName, propTitle, multipleValues);
                }
                else {
                    peopleManager.setSingleValueProfileProperty(userLoginName, propTitle, propValue);
                }
                console.log(propTitle + " = " + propValue + ", : " + multiValueProp);
            }

            //Execute the Query.
            cc.executeQueryAsync(function () {
                setTimeout(function () { deferred.resolve(); }, 3000);
            },
            function (sender, args) {
                deferred.reject(args.get_message());
                //On Error
            });
        });
    });
    return deferred.promise();
}


function processProfileCompleteness() {
    var rank = 0;
    var maxRank = 0;
    var propertyRankings = [];
    //call the profile scoreing list
    var url = constants.restEndPoints.get_profilePropRanking;
    var queryResults = getItems(url, function (data) {
        //for each item returned calc the score by checking the users profile for values stroed on the properties returned from the scoring list above
        $.each(data.d.results, function (index, item) {
            var property = item.Title;
            var rank = item.UnswBus_Rating;
            if (rank) {
                propertyRankings.push({ key: property, value: rank });
                maxRank = maxRank + rank;
            }

        });
        var commands = [];
        var userNameEncoded = encodeURIComponent(userLoginName);
        var batchExecutor = new RestBatchExecutor(siteCollectionUrl, { 'X-RequestDigest': $('#__REQUESTDIGEST').val() });
        var batchRequest;
        $.each(propertyRankings, function (index, ranking) {
            //construct call to User profiles service to check these values
            batchRequest = new BatchRequest();
            batchRequest.endpoint = siteCollectionUrl + constants.restEndPoints.get_profileProp.format(ranking.key, userNameEncoded);
            batchRequest.headers = {
                'accept': 'application/json;odata=verbose'
            }
            commands.push({ id: batchExecutor.loadRequest(batchRequest), title: ranking.key, rank: ranking.value });
        });
        batchExecutor.executeAsync().done(function (result) {
            var d = result;
            var itemIds = [];
            $.each(result, function (k, v) {
                var command = $.grep(commands, function (command) {
                    return v.id === command.id;
                });
                if (command.length) {
                    var data = v.result;
                    if (data.result.d.GetUserProfilePropertyFor) {
                        rank += command[0].rank;
                    }
                }
            });
            $("#profileCompletenessBar").progressbar({
                value: (rank / maxRank) * 100
            });
        }).fail(function (err) {
            //TODO:SEND TO ERROR PROCESSOR// alert(JSON.stringify(err));
        });
    });
}

function updateMyWidgetsList(remaingWidgets) {
    var url = constants.restEndPoints.get_myWidgets.format(userId);
    var queryResults = getItems(url, function (data) {
        var existingItem = data.d.results[0];
        //add the new item
        existingItem.UnswBus_Values = remaingWidgets
        updateItem(existingItem.__metadata.uri, existingItem, JSON.stringify(existingItem), function () {
            //write to local storage
            localStorage.setObj(myWidgetsKey, remaingWidgets);
        });
    });
}

function setProfilePicture() {

    // First get the file buffer and then send the request.
    var call = getFileBuffer();
    call.done(function (arrayBuffer) {
        var setupProfilePicCall = setMyProfilePicture(arrayBuffer);
        setupProfilePicCall.done(function () {
            closeBusyIndicator();
            confirmBuilderNoCancel("Your changes have been saved, but they may take some time to take effect. Don't worry if you don't see them right away.",
             function () {
                 $('#busyIndicatorDialog').dialog("close");
             },
             null,
        'Profile Data Saved'
      );
        });
    });
    call.fail(function (error) {
        closeBusyIndicator();
        //TODO:SEND TO ERROR PROCESSOR// alert(error);
    });

    // Get the ArrayBuffer for the picture file.
    function getFileBuffer() {

        // Get the file from the file input control on the page.
        // Pictures can be BMP, JPG, or PNG format and up to 5 million bytes.
        var file = $('#' + constants.htmlElementIds.profilePicFile)[0].files[0];

        var deferred = $.Deferred();
        var reader = new FileReader();
        reader.onloadend = function (e) {
            deferred.resolve(e.target.result);
        }
        reader.onerror = function (e) {
            deferred.reject(e.target.error);
        }
        reader.readAsArrayBuffer(file);
        return deferred.promise();
    }

    // Send the POST request.
    function setMyProfilePicture(arrayBuffer) {
        var deferred = $.Deferred();
        // Get the site URL and construct the endpoint URI for the rest post.
        var setPictureEndpoint = globalObjects.webAbsoluteUrl + constants.restEndPoints.add_profilePic;
        postRequest(setPictureEndpoint, arrayBuffer, function () {
            deferred.resolve();
        });
        return deferred.promise();
    }
}

var personalistionItems = {
    apps: { listItemObject: null, values: null },
    collabSites: { listItemObject: null, values: null },
    favourites: { listItemObject: null, values: null },
    widgets: { listItemObject: null, values: null },
    events: { listItemObject: null, values: null },
    myAppDefintions: [],
    myWidgetDefintions: []
};



var setupAuxBarPromise = $.Deferred();
var personalistionPage = null;
function setupAuxBarNew() {

    //check if we alrerady have the personlisation items in storage
    //var storedpersonalistionItems = JSON.parse(sessionStorage.getItem(storageKeys.personalistionItemsKey));
    //if (storedpersonalistionItems) {
    //    personalistionItems = storedpersonalistionItems;
    //    renderUserAuxBarItemsNew();
    //}
    //else {
    setupAuxBarPromise.resolve($.when(getPersonalistionItems()).done(function () {
        //get the app defintions
        $.when(getAppDefintions()).done(function () {
            //now store the personlisation item in session storage
            try {
                sessionStorage.setItem(constants.sessionStorageKeys.personalistionItemsKey, JSON.stringify(personalistionItems));
            }
            catch (e) {
                if (isQuotaExceeded(e)) {
                    console.log("Session storage is full.");
                }
            }

            renderUserAuxBarItemsNew();
            if (personalistionPage) {
                personaliseTabHandlers();
                getAllPersonalisationItems();
                //TODO: UPTO HERE #################################
            }

        });

    }));
    //}

}

function getPersonalistionItems() {
    var defTask = $.Deferred();

    var batchExecutor = new RestBatchExecutor(siteCollectionUrl + "/Personalisation", { 'X-RequestDigest': $('#__REQUESTDIGEST').val() });

    var commands = [];

    var batchRequest = new BatchRequest();
    batchRequest.endpoint = siteCollectionUrl + constants.restEndPoints.get_myApps.format(userId);
    batchRequest.headers = {
        'accept': 'application/json;odata=verbose'
    }
    commands.push({ id: batchExecutor.loadRequest(batchRequest), title: "apps" });

    batchRequest = new BatchRequest();
    batchRequest.endpoint = siteCollectionUrl + constants.restEndPoints.get_mySites.format(userId);;
    batchRequest.headers = {
        'accept': 'application/json;odata=verbose'
    }
    commands.push({ id: batchExecutor.loadRequest(batchRequest), title: "collabSites", jsonValue: true });

    batchRequest = new BatchRequest();
    batchRequest.endpoint = siteCollectionUrl + constants.restEndPoints.get_myFavs.format(userId);
    batchRequest.headers = {
        'accept': 'application/json;odata=verbose'
    }
    commands.push({ id: batchExecutor.loadRequest(batchRequest), title: "favourites", jsonValue: true });

    batchExecutor.executeAsync().done(function (result) {
        var itemIds = [];
        $.each(result, function (index, resultItem) {
            var command = $.grep(commands, function (command) {
                return resultItem.id === command.id;
            });
            if (command.length) {
                var data = resultItem.result;
                if (data.result.d.results[0]) {
                    var itemValue = null;
                    var returnedValue = data.result.d.results[0].UnswBus_Values
                    if (command[0].jsonValue && returnedValue) {
                        itemValue = jQuery.parseJSON(returnedValue);
                    }
                    else if (returnedValue && !command[0].jsonValue) {
                        itemValue = returnedValue;
                    }
                    personalistionItems[command[0].title].values = itemValue;
                    personalistionItems[command[0].title].listItemObject = data.result.d.results[0];
                }
            }
        });
        defTask.resolve();
    }).fail(function (err) {
        //TODO: process in error function
        //TODO:SEND TO ERROR PROCESSOR//alert(JSON.stringify(err));
    });
    return defTask.promise();
}

function getAppDefintions() {
    var defTask = $.Deferred();
    //var appDefinitionsEp = "/Personalisation/_api/web/lists/GetByTitle('Apps')/items?" +
    //         "$select=UnswBus_ReferenceID,Title,UnswBus_Description,UnswBus_Hyperlink,UnswBus_Icon,UnswBus_Static&$filter=((UnswBus_Static eq '1') ";
    var filter = "";
    if (personalistionItems.apps.values) {
        var filterValues = personalistionItems.apps.values.split(';');
        //construct the filter
        for (var f = 0; f < filterValues.length; f++) {
            filter = filter + "or ( UnswBus_ReferenceID eq '" + filterValues[f] + "')";
        }
    }

    getItems(constants.restEndPoints.get_appDefinitions.format(filter), function (data) {
        var existingItem = data.d.results[0];
        for (var i = 0; i < data.d.results.length; i++) {
            var appItemObject = data.d.results[i];
            if (appItemObject) {
                //extract tax term
                var roleAud = "";
                if (appItemObject.UnswBus_RoleAudience.results) {
                    var ItemRoleField = appItemObject.UnswBus_RoleAudience.results;
                    var rolesCount = ItemRoleField.length;
                    for (var K = 0; K < rolesCount; K++) {
                        roleAud += ItemRoleField[K].Label + '#';
                    }
                }
                var appDefinition = {
                    refId: appItemObject.UnswBus_ReferenceID,
                    title: appItemObject.Title,
                    description: appItemObject.UnswBus_Description,
                    icon: appItemObject.UnswBus_Icon,
                    url: appItemObject.UnswBus_Hyperlink,
                    staticItem: appItemObject.UnswBus_Static,
                    roleAudience: roleAud
                };
                personalistionItems.myAppDefintions.push(appDefinition);
            }

        }
        //sort my apss based on list item value order
        if (personalistionItems.apps.values) {
            personalistionItems.myAppDefintions.sort(sorter('refId', personalistionItems.apps.values.split(';')));
        }
        defTask.resolve();
    });
    return defTask.promise();

};

function renderUserAuxBarItemsNew() {
    var appItemHtml = "";
    var siteItemsHtml = "";
    var favItemsHtml = "";
    var appItemsCount = personalistionItems.myAppDefintions ? personalistionItems.myAppDefintions.length : 0;
    var siteItemsCount = personalistionItems.collabSites.values ? personalistionItems.collabSites.values.collabSites.length : 0;
    var favItemsCount = personalistionItems.favourites.values ? personalistionItems.favourites.values.favourites.length : 0;

    //render my apps
    for (var i = 0; i < appItemsCount; i++) {
        var appDefintionObject = personalistionItems.myAppDefintions[i];
        var appItemHtml = appItemHtml + htmlTemplates.auxBarAppItem.format(appDefintionObject.url.Url, appDefintionObject.icon.Url, appDefintionObject.title);
    }
    var toolItemsSectionHtml = appItemHtml ? htmlTemplates.toolItemsListWrapper.format(appItemHtml) : htmlTemplates.noAuxBarItemsMessage.format(siteCollectionUrl + constants.pageUrls.myTools);
    $("#" + constants.htmlElementIds.toolTab + " ul").replaceWith(toolItemsSectionHtml);


    //render my sites
    for (var i = 0; i < siteItemsCount; i++) {
        var mySiteObject = personalistionItems.collabSites.values.collabSites[i];
        var siteItemsHtml = siteItemsHtml + htmlTemplates.auxBarSiteItem.format(mySiteObject.url, mySiteObject.title);
    }
    var siteItemsSectionHtml = siteItemsHtml ? htmlTemplates.siteItemsListWrapper.format(siteItemsHtml) : htmlTemplates.noAuxBarItemsMessage.format(siteCollectionUrl + constants.pageUrls.mySites);
    $("#" + constants.htmlElementIds.sitesTab + " ul").replaceWith(siteItemsSectionHtml);

    //render my favs
    for (var i = 0; i < favItemsCount; i++) {
        var myFavObject = personalistionItems.favourites.values.favourites[i];
        var favItemsHtml = favItemsHtml + htmlTemplates.auxBarFavItem.format(myFavObject.url, myFavObject.title);
    }
    var favItemsSectionHtml = favItemsHtml ? htmlTemplates.favItemsListWrapper.format(favItemsHtml) : htmlTemplates.noAuxBarItemsMessage.format(siteCollectionUrl + constants.pageUrls.myFavs);
    $("#" + constants.htmlElementIds.favsTab + " ul").replaceWith(favItemsSectionHtml);


    var toolIndex = 9;
    if (appItemsCount > (toolIndex + 1)) {//add the one as the count is not a 0 based index
        $('#' + constants.htmlElementIds.toolsList + ' .tools-item:gt(' + toolIndex + ')').hide();
        $('#' + constants.htmlElementIds.toolTab).children(".loadmore").children().show();
        $('#' + constants.htmlElementIds.toolTab).children(".loadmore").children(":button").click(function () {
            var buttonText = $(this).text();
            if (buttonText && buttonText.toLowerCase().indexOf("load more") > -1) {
                $(this).text("less -");
                $(this).addClass("btn-primary-nbg");
                $('#' + constants.htmlElementIds.toolsList + ' .tools-item:gt(' + toolIndex + ')').show('fast');
            }
            else {
                $(this).html("<span>Load more <i class='fa fa-chevron-down icons-small'></i></span></i>");
                $(this).removeClass("btn-primary-nbg");
                $('#' + constants.htmlElementIds.toolsList + ' .tools-item:gt(' + toolIndex + ')').hide('fast');
            }
            return false;
        });
    }
    else {
        $('#' + constants.htmlElementIds.toolTab).children(".loadmore").children().hide();
    }

    var collabIndex = 9;
    if (siteItemsCount > (collabIndex + 1)) {//add the one as the count is not a 0 based index
        $('#' + constants.htmlElementIds.sitesList + '  li:gt(' + collabIndex + ')').hide();
        $('#' + constants.htmlElementIds.sitesTab).children(".loadmore").children().show();
        $('#' + constants.htmlElementIds.sitesTab).children(".loadmore").children(":button").click(function () {
            event.preventDefault ? event.preventDefault() : event.returnValue = false;
            var buttonText = $(this).text();
            if (buttonText && buttonText.toLowerCase().indexOf("load more") > -1) {
                $(this).text("less -");
                $(this).addClass("btn-primary-nbg");
                $('#' + constants.htmlElementIds.sitesList + ' li:gt(' + collabIndex + ')').show('fast');
            }
            else {
                $(this).html("<span>Load more <i class='fa fa-chevron-down icons-small'></i></span></i>");
                $(this).removeClass("btn-primary-nbg");
                $('#' + constants.htmlElementIds.sitesList + ' li:gt(' + collabIndex + ')').hide('fast');
            }
        });
    }
    else {
        $('#' + constants.htmlElementIds.sitesTab).children(".loadmore").children().hide();
    }

    var favIndex = 9;
    if (favItemsCount > (favIndex + 1)) {//add the one as the count is not a 0 based index
        $('#' + constants.htmlElementIds.favsList + ' li:gt(' + favIndex + ')').hide();
        $('#' + constants.htmlElementIds.favsTab).children(".loadmore").children().show();
        $('#' + constants.htmlElementIds.favsTab).children(".loadmore").children(":button").click(function () {
            event.preventDefault ? event.preventDefault() : event.returnValue = false;
            var buttonText = $(this).text();
            if (buttonText && buttonText.toLowerCase().indexOf("load more") > -1) {
                $(this).text("less -");
                $(this).addClass("btn-primary-nbg");
                $('#' + constants.htmlElementIds.favsList + ' li:gt(' + favIndex + ')').show('fast');
            }
            else {
                $(this).html("<span>Load more <i class='fa fa-chevron-down icons-small'></i></span></i>");
                $(this).removeClass("btn-primary-nbg");
                $('#' + constants.htmlElementIds.favsList + ' li:gt(' + favIndex + ')').hide('fast');
            }
        });
    }
    else {
        $('#' + constants.htmlElementIds.favsTab).children(".loadmore").children().hide();
    }
}


function getAlerts() {
    var queryResults = getItems(constants.restEndPoints.get_alerts.format(new Date().toISOString(), new Date().toISOString()), processAlerts);
}

function processAlerts(data) {


    var lowClass = "low";
    var medClass = "med";
    var highClass = "high";
    var alertHtml = "";
    var localAlertIds = localStorage.getObj(constants.sessionStorageKeys.alertIdsKey);//localStorage["alertIds"];
    var alertIds = []; //used to keep track of active alets Ids
    var activeAlerts = data.d.results;
    //trim the active alearts if the user has closed tem previoulsy
    activeAlerts = trimArraySingleProperty(localAlertIds, activeAlerts, "Id");
    var alertsCount = activeAlerts.length
    //show the alerts container if there are alerts to show
    if (alertsCount > 0) {
        $("#" + constants.htmlElementIds.alertsSection).show();
    };
    var showRotatorChevron = activeAlerts.length > 1 ? '' : 'display: none;';

    for (var i = 0; i < alertsCount; i++) {
        var alertItem = data.d.results[i];
        var alertId = alertItem.ID;
        alertIds.push(alertId);

        var title = alertItem.Title;
        var description = alertItem.UnswBus_Description;
        var priority = alertItem.UnswBus_Priority;
        var alertClass = lowClass;

        if (priority.toLowerCase() == "medium") {
            alertClass = medClass;
        }
        else if (priority.toLowerCase() == "high") {
            alertClass = highClass;
        }


        if (alertItem.UnswBus_Hyperlink) {
            alertHtml += htmlTemplates.alertWithLink.format(alertClass, title, description, showRotatorChevron, alertItem.UnswBus_Hyperlink.Url);
        }
        else {
            alertHtml += htmlTemplates.alerts.format(alertClass, title, description, showRotatorChevron);

        }
    }

    $("#" + constants.htmlElementIds.alertsSection).append(alertHtml);

    //click through effect
    var alertIndex = 0;
    var alertCount = activeAlerts.length;
    //set the first alert to be displayed
    $("#" + constants.htmlElementIds.alertsSection).children().first().show();
    $(".alert-next").click(function () {
        $("#" + constants.htmlElementIds.alertsSection).children().eq(alertIndex).hide()
        alertIndex = (alertIndex + 1) % alertCount;
        $("#" + constants.htmlElementIds.alertsSection).children().eq(alertIndex).show()
    })

    $(".alert-close").click(function () {
        $("#" + constants.htmlElementIds.alertsSection).children().remove();
        //add current alerts to one stored in local stroage if any
        var alertsToHide = [];
        if (localAlertIds) {
            alertsToHide = localAlertIds.concat(alertIds);
        }
        else {
            alertsToHide = alertIds;
        }

        //store the removed alerts ids so that they do not get displayed next time the user loads the page
        localStorage.setObj(constants.sessionStorageKeys.alertIdsKey, alertsToHide);
        $("#" + constants.htmlElementIds.alertsSection).hide();
    });

}

function getFavouritesForUpdate(favObject) {
    $("#connectNotificationArea").html("Saving favourite...").show('fast');
    $.when(addToFavourites(favObject)).done(function () {
        $("#connectNotificationArea").html("Favourite has been saved").show('fast').delay(3000).fadeOut();
    });

}

function addToFavourites(favObject) {
    var defTask = $.Deferred();
    //get object for update
    var queryResults = getItems(constants.restEndPoints.get_myFavsItem.format(userId), function (data) {
        var existingItem = data.d.results[0];
        var currentValues = jQuery.parseJSON(existingItem.UnswBus_Values);
        //add the new item
        if (!favObject) {
            currentValues.favourites.unshift({ title: document.title, url: pageRelativeUrl });
        }
        else {
            currentValues.favourites.unshift({ title: favObject.title, url: favObject.url, description: favObject.description });
        }
        existingItem.UnswBus_Values = JSON.stringify(currentValues);
        var itemProperties = {
            'UnswBus_Values': currentValues.favourites
        };
        updateItem(existingItem.__metadata.uri, existingItem, JSON.stringify(existingItem), function () { defTask.resolve(); });
    });
    return defTask;
}

function getItems(url, callback) {
    $.ajax({
        url: siteCollectionUrl + url,
        type: "GET",
        headers: {
            "accept": "application/json;odata=verbose",
            "X-RequestDigest": $("#__REQUESTDIGEST").val(),
        },
        success: function (data) {
            callback(data);
        },
        error: function (error) {
            //TODO:SEND TO ERROR PROCESSOR//alert(JSON.stringify(error));
        }
    });
}

function getItemsWeb(url, callback) {
    $.ajax({
        url: globalObjects.webAbsoluteUrl + url,
        type: "GET",
        headers: {
            "accept": "application/json;odata=verbose",
            "X-RequestDigest": $("#__REQUESTDIGEST").val(),
        },
        success: function (data) {
            callback(data);
        },
        error: function (error) {
            //TODO:SEND TO ERROR PROCESSOR//alert(JSON.stringify(error));
        }
    });
}

function addNewItem(url, data) {
    return $.ajax({
        url: siteCollectionUrl + url,
        type: "POST",
        headers: {
            "accept": "application/json;odata=verbose",
            "X-RequestDigest": $("#__REQUESTDIGEST").val(),
            "content-Type": "application/json;odata=verbose"
        },
        data: JSON.stringify(data),
        success: function (data) {
            // console.log(data);
        },
        error: function (error) {
            //TODO:SEND TO ERROR PROCESSOR// alert(JSON.stringify(error));
        }
    });
}

function updateItem(url, oldItem, newItem, callback) {
    $.ajax({
        url: url,
        type: "MERGE",
        processData: false,
        headers: {
            "accept": "application/json;odata=verbose",
            "X-RequestDigest": $("#__REQUESTDIGEST").val(),
            "content-Type": "application/json;odata=verbose",
            "X-Http-Method": "MERGE",
            "If-Match": oldItem.__metadata.etag
        },
        data: newItem,
        success: function (data) {
            if (callback) {
                callback(data);
            }

        },
        error: function (error) {
            errorHandler(error);
        }
    });
}

function updateItemNoMetaTag(url, oldItem, newItem, callback) {
    $.ajax({
        url: url,
        type: "POST",
        processData: false,
        headers: {
            "accept": "application/json;odata=verbose",
            "X-RequestDigest": $("#__REQUESTDIGEST").val(),
            "content-Type": "application/json;odata=verbose",
            "X-Http-Method": "MERGE",
            "If-Match": "*"
        },
        data: newItem,
        success: function (data) {
            if (callback) {
                callback(data);
            }

        },
        error: function (error) {
            errorHandler(error);
        }
    });
}

function deleteItem(url, oldItem) {
    $.ajax({
        url: _spPageContextInfo.webAbsoluteUrl + url,
        type: "DELETE",
        headers: {
            "accept": "application/json;odata=verbose",
            "X-RequestDigest": $("#__REQUESTDIGEST").val(),
            "If-Match": oldItem.__metadata.etag
        },
        success: function (data) {

        },
        error: function (error) {
            //TODO:SEND TO ERROR PROCESSOR// alert(JSON.stringify(error));
        }
    });
}

function postRequest(requestUrl, payload, callback) {
    $.ajax({
        url: requestUrl,
        type: "POST",
        data: payload,
        processData: false,
        headers: {
            "accept": "application/json;odata=verbose",
            "X-RequestDigest": $("#__REQUESTDIGEST").val(),
            "content-length": payload.byteLength
        },
        success: function (data) {
            callback(data)
        },
        error: function (error) {
            errorHandler(error);
        }
    });
}

function errorHandler(error) {
    //do somting
}



function getUserEvents() {
    var defTask = $.Deferred();
    getItems(constants.restEndPoints.get_myEvents.format(userId), function (data) {
        var item = data.d.results[0];
        if (item) {
            var values = jQuery.parseJSON(item.UnswBus_Values);
            if (!values) {
                //set a default value
                values = { events: [] };
            }
            personalistionItems.events.listItemObject = item;
            personalistionItems.events.values = values;
        }
        defTask.resolve();
    });
    return defTask;

}

function getSubCalendars() {
    var defTask = $.Deferred();
    $.ajax({
        url: constants.teamUpApi.allEventsEp,
        method: "GET",
        headers: {
            "accept": "application/json",
            "Teamup-Token": constants.teamUpApi.token
        },
        success: function (data) {
            $.each(data.subcalendars, function (index, subcal) {
                var obj = { id: subcal.id, name: subcal.name };
                subCalsDefinition.push(obj);

            });
            defTask.resolve();
        },
        error: function (error) {
            //TODO:SEND TO ERROR PROCESSOR//alert(JSON.stringify(error));
        }
    });
    return defTask;
}

// Below commented out codes are customised for ASB Connect

// function getCalendarEvents() {
    // $.when(getUserEvents()).done(function () {
        // var eventsObject = personalistionItems.events.values;
        // var eventsFilterString = "";
        // if (eventsObject) {
            // var eventsCount = eventsObject.events.length;
            // for (var i = 0; i < eventsCount ; i++) {
                // eventsFilterString += "&subcalendarId[]=" + eventsObject.events[i].id;
            // };
        // }

        // var fromDate = new Date();
        // var fromDateString = fromDate.toISOString('en-au').substring(0, 10);
        // var toDate = new Date();
        // toDate.setDate(toDate.getDate() + 90);
        // var toDateString = toDate.toISOString('en-au').substring(0, 10);
        // var teamupApiUrl = constants.teamUpApi.eventsEp.format(fromDateString, toDateString);
        // if (eventsFilterString) {
            // teamupApiUrl += eventsFilterString;


            // $.ajax({
                // url: teamupApiUrl,
                // method: "GET",
                // headers: {
                    // "accept": "application/json",
                    // "Teamup-Token": constants.teamUpApi.token

                // },
                // success: function (data) {
                    // renderEvents(data, false);
                // },
                // error: function (error) {
                    // //TODO:SEND TO ERROR PROCESSOR// alert(JSON.stringify(error));
                // }
            // });
        // }
        // else {
            // renderEvents(null, true);
        // };
    // });
// }

function renderEvents(eventsObject, noEventsSelected) {
    //default value
    var eventItemsHtml = "";
    if (eventsObject) {
        $.each(eventsObject.events, function (index, event) {

            if (index <= 3) {
                var allDayeEvent = event.all_day;
                var startDate = new Date(event.start_dt);
                var endDate = new Date(event.end_dt);

                var startDateMonth = startDate.toLocaleString('en-au', { month: "short" });
                var startDateDay = startDate.getDate();
                var startTime = formatTime(startDate.getHours()) + ":" + formatTime(endDate.getMinutes());

                var endDateMonth = endDate.toLocaleString('en-au', { month: "short" });
                var endDateDay = endDate.getDate();
                var endTime = formatTime(endDate.getHours()) + ":" + formatTime(endDate.getMinutes());

                var timeString = startTime + "-" + endTime;
                var eventTime = allDayeEvent ? "All day event" : timeString
                eventItemsHtml += htmlTemplates.eventItem.format(startDateDay, startDateMonth, event.title, eventTime);
            }
        });
    }
    var noEventsMessage = noEventsSelected ? htmlTemplates.noEventsSelectedMessage.format(siteCollectionUrl + constants.pageUrls.myEvents) : htmlTemplates.noEventsMessage;
    var eventsSectionHtml = htmlTemplates.eventsContainer.format(eventItemsHtml != "" ? eventItemsHtml : noEventsMessage, siteCollectionUrl + constants.pageUrls.eventsRollup);
    $("#" + constants.htmlElementIds.eventsSection).append(eventsSectionHtml);
}

function formatTime(value) {
    if (value < 10) {
        value = '0' + value;

    }
    return value;

}

function convertTimeTo12HourFormat(hour) {
    var suffix = hour >= 12 ? "PM" : "AM";
    hour = ((hour + 11) % 12 + 1) + suffix;

}

//Extensions
Storage.prototype.setObj = function (key, obj) {
    try {
        return this.setItem(key, JSON.stringify(obj))
    }
    catch (e) {
        if (isQuotaExceeded(e)) {
            console.log("Session storage is full.");
        }
    }

}
Storage.prototype.getObj = function (key) {
    var val = this.getItem(key);
    if (val !== null) {
        try {
            return JSON.parse(val)
        }
        catch (ex) {
            return null;
        }
    }
    else {
        return null;
    }

}
String.prototype.format = function () {
    var s = this,
        i = arguments.length;

    while (i--) {
        s = s.replace(new RegExp('\\{' + i + '\\}', 'gm'), arguments[i]);
    }
    return s;
};

function trimArray(a, b, prop) {
    if (!a || !b) {
        return b;
    }
    for (var i = 0, len = a.length; i < len; i++) {
        for (var j = 0, len2 = b.length; j < len2; j++) {
            if (a[i][prop] === b[j][prop]) {
                b.splice(j, 1);
                len2 = b.length;
            }
        }
    }

    return b;

};

function trimArraySingleProperty(a, b, prop) {
    if (!a || !b) {
        return b;
    }
    for (var i = 0, len = a.length; i < len; i++) {
        for (var j = 0, len2 = b.length; j < len2; j++) {
            if (a[i] === b[j][prop]) {
                b.splice(j, 1);
                len2 = b.length;
            }
        }
    }

    return b;

};

function getAppToolsFilterValues() {
    var optionsHtml = "";
    getTermSet(constants.termSetGuids.rolesAudience, function (terms) {

        if (terms) {
            var termsEnumerator = terms.getEnumerator(),
                        tree = {
                            term: terms,
                            children: []
                        };

            // Loop through each term
            while (termsEnumerator.moveNext()) {
                var currentTerm = termsEnumerator.get_current();
                optionsHtml += htmlTemplates.options.format(currentTerm.get_name());
            }
            $("#" + constants.htmlElementIds.appAudienceFilter).append(optionsHtml);
        }
    });
}


function confirmBuilder(dialogText, okFunc, cancelFunc, dialogTitle) {
    $(htmlTemplates.confirmDialogWrapper.format(dialogText)).dialog({
        draggable: false,
        modal: true,
        resizable: false,
        minWidth: 350,
        title: dialogTitle || 'Confirm',
        minHeight: 100,
        dialogClass: 'no-close connect-dialog',
        buttons: {
            OK: function () {
                if (typeof (okFunc) == 'function') {
                    setTimeout(okFunc, 50);
                }
                $(this).dialog('destroy');
            },
            Cancel: function () {
                if (typeof (cancelFunc) == 'function') {
                    setTimeout(cancelFunc, 50);
                }
                $(this).dialog('destroy');
            }
        },
        create: function () {
            $(this).parent().children(".ui-dialog-buttonpane").children(".ui-dialog-buttonset").children(".ui-button:nth-child(2)").addClass("btn btn-primary-nbg");
            $(this).parent().children(".ui-dialog-buttonpane").children(".ui-dialog-buttonset").children(".ui-button:nth-child(1)").addClass("btn btn-primary");
        },
        open: function () {
            $('.ui-widget-overlay').addClass('custom-overlay');
        },
        close: function () {
            $('.ui-widget-overlay').removeClass('custom-overlay');
        }
    });
}

function confirmBuilderNoCancel(dialogText, okFunc, cancelFunc, dialogTitle) {
    $(htmlTemplates.confirmDialogWrapper.format(dialogText)).dialog({
        draggable: false,
        modal: true,
        resizable: false,
        minWidth: 350,
        title: dialogTitle || 'Confirm',
        minHeight: 100,
        dialogClass: 'no-close connect-dialog',
        buttons: {
            OK: function () {
                if (typeof (okFunc) == 'function') {
                    setTimeout(okFunc, 50);
                }
                $(this).dialog('destroy');
            }
        },
        create: function () {
            $(this).parent().children(".ui-dialog-buttonpane").children(".ui-dialog-buttonset").children(".ui-button:nth-child(2)").addClass("btn btn-primary-nbg");
            $(this).parent().children(".ui-dialog-buttonpane").children(".ui-dialog-buttonset").children(".ui-button:nth-child(1)").addClass("btn btn-primary");
        },
        open: function () {
            $('.ui-widget-overlay').addClass('custom-overlay');
        },
        close: function () {
            $('.ui-widget-overlay').removeClass('custom-overlay');
        }
    });
}




function editFavItemsEventListener() {

    $(".edit-item").unbind().on('click', (function () {
        // event.preventDefault ? event.preventDefault() : event.returnValue = false;
        $(this).closest(".personalise-item").find(".favItemEditMode").show();
        $(this).closest(".personalise-item").find(".favItemDispMode").hide();
        return false;
    }));
    $(".editFavItemSave").unbind().on('click', (function () {
        var updatedTitle = $(this).closest(".personalise-item").find(".favItemTitleInput").val();
        var updatedUrl = $(this).closest(".personalise-item").find(".favItemUrlInput").val();
        var updatedDesrip = $(this).closest(".personalise-item").find(".favItemDescriptionInput").val();

        if (!updatedTitle || !updatedUrl) {
            $(this).siblings(".reqFldMessage").show("fast");
        }
        else {
            $(this).siblings(".reqFldMessage").hide("fast");
            //set data attribs
            $(this).closest(".personalise-item").attr('data-title', updatedTitle);
            $(this).closest(".personalise-item").attr('data-description', updatedDesrip);
            $(this).closest(".personalise-item").attr('data-url', updatedUrl);
            //set display items
            $(this).closest(".personalise-item").find(".dispTitle").text(updatedTitle);
            $(this).closest(".personalise-item").find(".dispDescription").text(updatedDesrip);

            //call update
            updateFavs();
            $(this).closest(".personalise-item").find(".favItemEditMode").hide();
            $(this).closest(".personalise-item").find(".favItemDispMode").show();
        }

        return false;
    }));
    $(".editFavItemCancel").unbind().on('click', (function () {
        //event.preventDefault ? event.preventDefault() : event.returnValue = false;
        $(this).closest(".personalise-item").find(".favItemEditMode").hide();
        $(this).closest(".personalise-item").find(".favItemDispMode").show();
        return false;
    }));
    $(".removeItem").unbind().on('click', (function () {
        var currFavItem = $(this).closest(".personalise-item");
        //event.preventDefault ? event.preventDefault() : event.returnValue = false;
        confirmBuilder('Are you sure you want to delete this item?', function () {
            currFavItem.slideUp("fast", function () {
                $(this).remove();
                updateFavs();
            });
        }, function () {
        },
                'Confirm delete'
              );
        return false;
    }));
    $("#" + constants.htmlElementIds.addFavItem).unbind().on('click', (function () {
        //validate
        var title = $("#" + constants.htmlElementIds.favItemTitle).val();
        var url = $("#" + constants.htmlElementIds.favItemUrl).val();
        var description = $("#" + constants.htmlElementIds.favItemDescription).val();

        if (!title || !url) {
            $("#" + constants.htmlElementIds.reqFldMessage).show("fast");
        }
        else {
            $("#" + constants.htmlElementIds.reqFldMessage).hide("fast");
            $(".form input").val('');
            $("#" + constants.htmlElementIds.favItemUrl).val('http://');
            var newFavItem = htmlTemplates.favsItem.format(url, title, description);
            $("#" + constants.htmlElementIds.myFavsSelected).append(newFavItem);
            updateFavs();
        }
        //recall to attach event handler to elemets added dynamically
        editFavItemsEventListener();
        return false;
    }));

};


function getPageContactDetails(headingOverrideText) {
    var sectionHeading = headingOverrideText ? headingOverrideText : "Contact" //if not heading override is supplied just use a default
    //var currentPageList = _spPageContextInfo.pageListId;
    //var currentPageItemId = _spPageContextInfo.pageItemId;

    var url = constants.restEndPoints.get_pageContact.format(globalObjects.currentPageList.replace('{', '').replace('}', ''), globalObjects.currentPageItemId);
    getItemsWeb(url, function (data) {
        var item = data.d;
        if (item) {
            var userName = item.PublishingContact.Name;
            if (userName) {
                $.when(getUserProfileDetails(userName)).done(function (userProfileProperties) {
                    var titleIndex = $.grep(userProfileProperties, function (e) { return e.key == constants.userPropertyFieldNames.Title; });// userProperties.findIndex(x => x.key== "Title");
                    var unitIndex = $.grep(userProfileProperties, function (e) { return e.key == constants.userPropertyFieldNames.UNSWBUSSchoolUnit; });//userProperties.findIndex(x => x.key== "UNSWBUSSchoolUnit");
                    var phoneIndex = $.grep(userProfileProperties, function (e) { return e.key == constants.userPropertyFieldNames.WorkPhone; });//userProperties.findIndex(x => x.key== "WorkPhone");
                    var profilePicIndex = $.grep(userProfileProperties, function (e) { return e.key == constants.userPropertyFieldNames.PictureURL; });//userProperties.findIndex(x => x.key== "PictureURL");
                    var firstNameIndex = $.grep(userProfileProperties, function (e) { return e.key == constants.userPropertyFieldNames.FirstName; });//userProperties.findIndex(x => x.key== "FirstName");
                    var lastNameIndex = $.grep(userProfileProperties, function (e) { return e.key == constants.userPropertyFieldNames.LastName; });//userProperties.findIndex(x => x.key== "FirstName");
                    var workEmailIndex = $.grep(userProfileProperties, function (e) { return e.key == constants.userPropertyFieldNames.WorkEmail; });//userProperties.findIndex(x => x.key== "FirstName");

                    var firstName = firstNameIndex[0] ? firstNameIndex[0].value : "";
                    var lastName = lastNameIndex[0] ? lastNameIndex[0].value : "";
                    var profilePicUrl = profilePicIndex[0] ? profilePicIndex[0].value : "";
                    var jobTitle = titleIndex[0] ? titleIndex[0].value : "";
                    var schoolUnit = unitIndex[0] ? unitIndex[0].value : "";
                    var email = workEmailIndex[0] ? workEmailIndex[0].value : "";
                    var phone = phoneIndex[0] ? phoneIndex[0].value : "";

                    //construct userPhoto url
                    var userPhotoUrl = constants.pageUrls.userPhotoPage.format(email);

                    var pageContactHtml = htmlTemplates.pageContactTemplate.format(firstName, lastName, userPhotoUrl, jobTitle, schoolUnit, email, phone, sectionHeading);
                    $("#" + constants.htmlElementIds.pageContactSection).append(pageContactHtml);
                });
            }

        }
    });


}

function getUserProfileDetails(userName) {
    var userProfileProperties = [];
    var defTask = $.Deferred();
    var commands = [];
    var userNameEncoded = encodeURIComponent(userName);
    var batchExecutor = new RestBatchExecutor(siteCollectionUrl, { 'X-RequestDigest': $('#__REQUESTDIGEST').val() });
    var batchRequest = new BatchRequest();
    batchRequest.endpoint = siteCollectionUrl + constants.restEndPoints.get_userProperty.format(constants.userProfilrPropertyNames.UNSWBUSSchoolUnit, userNameEncoded)
    batchRequest.headers = {
        'accept': 'application/json;odata=verbose'
    }
    commands.push({ id: batchExecutor.loadRequest(batchRequest), title: constants.userProfilrPropertyNames.UNSWBUSSchoolUnit });

    batchRequest = new BatchRequest();
    batchRequest.endpoint = siteCollectionUrl + constants.restEndPoints.get_userProperty.format(constants.userProfilrPropertyNames.Title, userNameEncoded)
    batchRequest.headers = {
        'accept': 'application/json;odata=verbose'
    }
    commands.push({ id: batchExecutor.loadRequest(batchRequest), title: constants.userProfilrPropertyNames.Title });

    batchRequest = new BatchRequest();
    batchRequest.endpoint = siteCollectionUrl + constants.restEndPoints.get_userProperty.format(constants.userProfilrPropertyNames.WorkPhone, userNameEncoded)
    batchRequest.headers = {
        'accept': 'application/json;odata=verbose'
    }
    commands.push({ id: batchExecutor.loadRequest(batchRequest), title: constants.userProfilrPropertyNames.WorkPhone });

    batchRequest = new BatchRequest();
    batchRequest.endpoint = siteCollectionUrl + constants.restEndPoints.get_userProperty.format(constants.userProfilrPropertyNames.PictureURL, userNameEncoded)
    batchRequest.headers = {
        'accept': 'application/json;odata=verbose'
    }
    commands.push({ id: batchExecutor.loadRequest(batchRequest), title: constants.userProfilrPropertyNames.PictureURL });

    batchRequest = new BatchRequest();
    batchRequest.endpoint = siteCollectionUrl + constants.restEndPoints.get_userProperty.format(constants.userProfilrPropertyNames.FirstName, userNameEncoded)
    batchRequest.headers = {
        'accept': 'application/json;odata=verbose'
    }
    commands.push({ id: batchExecutor.loadRequest(batchRequest), title: constants.userProfilrPropertyNames.FirstName });

    batchRequest = new BatchRequest();
    batchRequest.endpoint = siteCollectionUrl + constants.restEndPoints.get_userProperty.format(constants.userProfilrPropertyNames.LastName, userNameEncoded)
    batchRequest.headers = {
        'accept': 'application/json;odata=verbose'
    }
    commands.push({ id: batchExecutor.loadRequest(batchRequest), title: constants.userProfilrPropertyNames.LastName });

    batchRequest = new BatchRequest();
    batchRequest.endpoint = siteCollectionUrl + constants.restEndPoints.get_userProperty.format(constants.userProfilrPropertyNames.WorkEmail, userNameEncoded);
    batchRequest.headers = {
        'accept': 'application/json;odata=verbose'
    }
    commands.push({ id: batchExecutor.loadRequest(batchRequest), title: constants.userProfilrPropertyNames.WorkEmail });

    batchRequest = new BatchRequest();
    batchRequest.endpoint = siteCollectionUrl + constants.restEndPoints.get_userProperty.format(constants.userProfilrPropertyNames.UNSWFireWarden, userNameEncoded);
    batchRequest.headers = {
        'accept': 'application/json;odata=verbose'
    }
    commands.push({ id: batchExecutor.loadRequest(batchRequest), title: constants.userProfilrPropertyNames.UNSWFireWarden });

    batchRequest = new BatchRequest();
    batchRequest.endpoint = siteCollectionUrl + constants.restEndPoints.get_userProperty.format(constants.userProfilrPropertyNames.UNSWJusticeOfPeace, userNameEncoded);
    batchRequest.headers = {
        'accept': 'application/json;odata=verbose'
    }
    commands.push({ id: batchExecutor.loadRequest(batchRequest), title: constants.userProfilrPropertyNames.UNSWJusticeOfPeace });

    batchRequest = new BatchRequest();
    batchRequest.endpoint = siteCollectionUrl + constants.restEndPoints.get_userProperty.format(constants.userProfilrPropertyNames.UNSWFirstAidOfficer, userNameEncoded);
    batchRequest.headers = {
        'accept': 'application/json;odata=verbose'
    }
    commands.push({ id: batchExecutor.loadRequest(batchRequest), title: constants.userProfilrPropertyNames.UNSWFirstAidOfficer });

    batchRequest = new BatchRequest();
    batchRequest.endpoint = siteCollectionUrl + constants.restEndPoints.get_userProperty.format(constants.userProfilrPropertyNames.UNSWWorkWeek, userNameEncoded);
    batchRequest.headers = {
        'accept': 'application/json;odata=verbose'
    }
    commands.push({ id: batchExecutor.loadRequest(batchRequest), title: constants.userProfilrPropertyNames.UNSWWorkWeek });

    batchRequest = new BatchRequest();
    batchRequest.endpoint = siteCollectionUrl + constants.restEndPoints.get_userProperty.format(constants.userProfilrPropertyNames.OfficeLocation, userNameEncoded);
    batchRequest.headers = {
        'accept': 'application/json;odata=verbose'
    }
    commands.push({ id: batchExecutor.loadRequest(batchRequest), title: constants.userProfilrPropertyNames.OfficeLocation });

    batchRequest = new BatchRequest();
    batchRequest.endpoint = siteCollectionUrl + constants.restEndPoints.get_userProperty.format(constants.userProfilrPropertyNames.Skills, userNameEncoded);
    batchRequest.headers = {
        'accept': 'application/json;odata=verbose'
    }
    commands.push({ id: batchExecutor.loadRequest(batchRequest), title: constants.userProfilrPropertyNames.Skills });

    batchRequest = new BatchRequest();
    batchRequest.endpoint = siteCollectionUrl + constants.restEndPoints.get_userProperty.format(constants.userProfilrPropertyNames.PastProjects, userNameEncoded);
    batchRequest.headers = {
        'accept': 'application/json;odata=verbose'
    }
    commands.push({ id: batchExecutor.loadRequest(batchRequest), title: constants.userProfilrPropertyNames.PastProjects });

    batchRequest = new BatchRequest();
    batchRequest.endpoint = siteCollectionUrl + constants.restEndPoints.get_userProperty.format(constants.userProfilrPropertyNames.AboutMe, userNameEncoded);
    batchRequest.headers = {
        'accept': 'application/json;odata=verbose'
    }
    commands.push({ id: batchExecutor.loadRequest(batchRequest), title: constants.userProfilrPropertyNames.AboutMe });

    batchRequest = new BatchRequest();
    batchRequest.endpoint = siteCollectionUrl + constants.restEndPoints.get_userProperty.format(constants.userProfilrPropertyNames.UNSWQualifications, userNameEncoded);
    batchRequest.headers = {
        'accept': 'application/json;odata=verbose'
    }
    commands.push({ id: batchExecutor.loadRequest(batchRequest), title: constants.userProfilrPropertyNames.UNSWQualifications });

    batchRequest = new BatchRequest();
    batchRequest.endpoint = siteCollectionUrl + constants.restEndPoints.get_userProperty.format(constants.userProfilrPropertyNames.UNSWLanguages, userNameEncoded);
    batchRequest.headers = {
        'accept': 'application/json;odata=verbose'
    }
    commands.push({ id: batchExecutor.loadRequest(batchRequest), title: constants.userProfilrPropertyNames.UNSWLanguages });

    batchExecutor.executeAsync().done(function (result) {
        var d = result;
        var itemIds = [];
        $.each(result, function (k, v) {
            var command = $.grep(commands, function (command) {
                return v.id === command.id;
            });
            if (command.length) {
                var data = v.result;
                if (data.result.d.GetUserProfilePropertyFor) {
                    var obj = {
                        key: command[0].title,
                        value: data.result.d.GetUserProfilePropertyFor
                    };
                    userProfileProperties.push(obj);
                }
            }
        });
        defTask.resolve(userProfileProperties);
    }).fail(function (err) {
        //TODO:SEND TO ERROR PROCESSOR//alert(JSON.stringify(err));
    });
    return defTask;
};


function renderAlphabetRefiner(managedProperty) {
    var letters = "ABCDEFGHIJKLMNOPQRSTUVWXYZ";
    for (var i = 0; i < letters.length; i++) {
        var searchTerm = letters[i];
        htmlTemplates.alphabetRefiner += htmlTemplates.letterRefiner.format(managedProperty, searchTerm);
    };
    $("#" + constants.htmlElementIds.letterRefiner).append(htmlTemplates.alphabetRefiner);
    $(".alpahbetRefiner").on("click", function () {
        var pSibs = $(this).parent().siblings();
        $(this).parent().siblings().removeClass("tab-active");
        $(this).parent().addClass("tab-active");
    });
};

function setupDialogAddToFavs(initialVals) {

    $("#" + constants.htmlElementIds.favTitle).val(initialVals.title);
    $("#" + constants.htmlElementIds.favUrl).val(initialVals.url);
    $("#" + constants.htmlElementIds.favDescrip).val(initialVals.description);
    $("#" + constants.htmlElementIds.dialogFormFav).dialog("open");
}


function setupDialogReportIssue(initialVals) {
    $("#" + constants.htmlElementIds.issueUrl).val(initialVals.url);
    $("#" + constants.htmlElementIds.issueTitle).val("");
    $("#" + constants.htmlElementIds.dialogFormReportIssue).dialog("open");
}

function reportIssue(issueObj) {
    var listItemData = {
        __metadata: { 'type': constants.listMetadataTypes.ReportIssueListItem },
        Title: issueObj.title,
        UnswBus_Hyperlink: { Url: issueObj.url }
    };
    addNewItem(constants.restEndPoints.add_reportIssue, listItemData);
};

function copyToClipboard(value) {
    // create hidden text element, if it doesn't already exist
    var targetId = "_hiddenCopyText_";
    var origSelectionStart, origSelectionEnd;

    // must use a temporary form element for the selection and copy
    target = document.getElementById(targetId);
    if (!target) {
        var target = document.createElement("textarea");
        target.style.position = "absolute";
        target.style.left = "-9999px";
        target.style.top = "0";
        target.id = targetId;
        document.body.appendChild(target);
    }
    target.textContent = value;

    // select the content
    var currentFocus = document.activeElement;
    target.focus();
    target.setSelectionRange(0, target.value.length);

    // copy the selection
    document.execCommand("copy");

    // restore original focus
    if (currentFocus && typeof currentFocus.focus === "function") {
        currentFocus.focus();
    }
    // clear temporary content
    target.textContent = "";
};

function renderKeyWords() {
    var keywordsText = $("#" + constants.htmlElementIds.relatedKeyWords).html();
    keywordsText = replaceAll(keywordsText, ';', ' ');
    $("#" + constants.htmlElementIds.relatedKeyWords).html(keywordsText);
    var keywordElements = $("#" + constants.htmlElementIds.relatedKeyWords).children("span");

    $.each(keywordElements, function (index, keywordItem) {
        var label = $(this).text();
        $(this).wrap(htmlTemplates.tag.format(siteCollectionUrl + constants.pageUrls.newsGrid + "#k=" + constants.managedProps.UnswBusEnterpriseKeywords + ":" + label));
    });
    //remove the ';' added by sp for tax terms
    var emlm = $("#" + constants.htmlElementIds.relatedKeyWords);


}

function setupRelatedCategoryLink(pageType) {
    var categoryUrl = null;
    var pageCategory = $("#" + constants.htmlElementIds.categoryLink).text();
    if (pageType === "newsArticle") {
        categoryUrl = siteCollectionUrl + constants.pageUrls.newsGrid + "#k=" + constants.managedProps.UnswBusNewsCategory + ":" + pageCategory;
    }
    else if (pageType === "businessBalanceArticle") {
        categoryUrl = siteCollectionUrl + constants.pageUrls.newsGrid + "#k=" + constants.managedProps.UnswBusBusinessBalanceCategory + ":" + pageCategory;
    }

    if (categoryUrl) {
        $("#" + constants.htmlElementIds.categoryLink).attr('href', categoryUrl);
    }
}


function setUpResourceCards() {
    var url = constants.restEndPoints.currentPageResourceCategory.format(globalObjects.currentPageList.replace('{', '').replace('}', ''), globalObjects.currentPageItemId);
    getItemsWeb(url, function (data) {
        var item = data.d;
renderRCItems(constants.termSetGuids.connectResourceCatsTermSetId, item.UnswBus_ResourceCategory.results[0].TermGuid);
    });
}

function getCurrPageCatTerms(resourceCatstermSetId, currentPageCatTermId) {
    var defTask = $.Deferred();
    var currPageCatergoryCards = [];
    getTermSetAsTree(resourceCatstermSetId, true, function (resourceCatTermSet) {
        var navHtml = '';
        var resourceCatTerms = resourceCatTermSet.children;
        var serachDrivenSection = false;
        var currPageCatTermObject = findObject(resourceCatTerms, "guid", currentPageCatTermId);
        if (currPageCatTermObject.searchDrivenResources) {
            serachDrivenSection = true;
        }
        if (currPageCatTermObject.isDepartmentBasedResource && currPageCatTermObject.isDepartmentBasedResource.toLowerCase() == 'true') {
            //get the current users department
            $.when(getUserProfileDetails(userLoginName)).done(function (userProfileProperties) {
                var userDep = findObject(userProfileProperties, "key", constants.userPropertyFieldNames.UNSWBUSSchoolUnit).value;
                //retrive the term object for the user department as now we need to narrow down the term set to the users school
                var userDepTermObject = findObject(currPageCatTermObject.children, "title", userDep);
                //set the currPageCatTermObject to the users department term object
                currPageCatTermObject = userDepTermObject;
                if (currPageCatTermObject && currPageCatTermObject.children) {
                    var currPageCatChildCount = currPageCatTermObject.children.length;
                    for (var i = 0; i < currPageCatChildCount; i++) {
                        var totalTermChildNodes = 0;
                        if (currPageCatTermObject.children[i].children) {
                            totalTermChildNodes = currPageCatTermObject.children[i].children.length;
                        }
                        var obj = { guid: currPageCatTermObject.children[i].guid, title: currPageCatTermObject.children[i].title, children: [], termChildNodesCount: totalTermChildNodes };
                        currPageCatergoryCards.push(obj);
                    }
                }

                defTask.resolve(currPageCatergoryCards, resourceCatTermSet, serachDrivenSection, userDepTermObject);
            });
        }
        else {
            if (currPageCatTermObject && currPageCatTermObject.children) {
                var currPageCatChildCount = currPageCatTermObject.children.length;
                for (var i = 0; i < currPageCatChildCount; i++) {
                    var totalTermChildNodes = 0;
                    if (currPageCatTermObject.children[i].children) {
                        totalTermChildNodes = currPageCatTermObject.children[i].children.length;
                    }
                    var obj = { guid: currPageCatTermObject.children[i].guid, title: currPageCatTermObject.children[i].title, children: [], termChildNodesCount: totalTermChildNodes };
                    currPageCatergoryCards.push(obj);
                }
            }
            defTask.resolve(currPageCatergoryCards, resourceCatTermSet, serachDrivenSection);
        };
    });
    return defTask;
}

function renderResourceCentreTabs(resourceCatTermSet) {
    //wait till we get users department
    $.when(getUserProfileDetails(userLoginName)).done(function (userProfileProperties) {
        var userDepObject = findObject(userProfileProperties, "key", constants.userPropertyFieldNames.UNSWBUSSchoolUnit);
        var resourceTabHtml = "";
        var resourceTabsLength = resourceCatTermSet.children.length;
        for (var i = 0; i < resourceTabsLength; i++) {
            var renderTab = true;
            var currResourceTabTerm = resourceCatTermSet.children[i];
            var termName = currResourceTabTerm.name;
            if (currResourceTabTerm.isDepartmentBasedResource && currResourceTabTerm.isDepartmentBasedResource.toLowerCase() == 'true') {
                if (userDepObject) {
                    var userDepTermObject = findObject(currResourceTabTerm.children, "title", userDepObject.value);
                    if (!userDepTermObject) {
                        renderTab = false;
                        // $('#navmenu a[href="' + siteCollectionUrl + "/" + mySchools.resourcePageUrl + '"]').hide();
                    }
                }
                else {
                    renderTab = false;
                }
            };
            if (renderTab) {
                var resourceTabUrl = currResourceTabTerm.resourcePageUrl ? currResourceTabTerm.resourcePageUrl : "#";
                var defaultTabUrl = currResourceTabTerm.defaultTabPageUrl ? currResourceTabTerm.defaultTabPageUrl : null;
                //var href = window.location.href;
                var activeTabClass = (pageAbsoluteUrl.toLowerCase().indexOf(resourceTabUrl.toLowerCase()) > -1) || (defaultTabUrl && pageAbsoluteUrl.toLowerCase().endsWith(defaultTabUrl.toLowerCase())) ? "tab-active" : "";
                resourceTabHtml += htmlTemplates.tabs.format(termName.replace(' ', '').trim() + "tab", activeTabClass, siteCollectionUrl + "/" + resourceTabUrl, termName);
                if (defaultTabUrl && activeTabClass) {
                    //if the default tab is active ensure the lef hand nav item for the default tab is also active
                    activateLefNavItem(resourceTabUrl);
                    //add the breadcrumb for the default resource centre tab
                    var eixstingBreadcrumb = $("#customBreadcrumb").html();
                    if (eixstingBreadcrumb.indexOf('currpage-crumb') < 0) { //only add if it does not exist.
                        var breadCrumb = htmlTemplates.breadCrumbLast.format(termName);
                        $("#customBreadcrumb").append(" > " + breadCrumb);
                    };

                }
            };

        }
        $("#" + constants.htmlElementIds.resourceCentreTabs).append(resourceTabHtml);
    });


}

function activateLefNavItem(navItemUrl) {
    $('#navmenu a[href="' + siteCollectionUrl + "/" + navItemUrl + '"]').addClass('activeNav');
}

function renderRCItems(resourceCatstermSetId, currentPageCatTermId) {
    $.when(getCurrPageCatTerms(resourceCatstermSetId, currentPageCatTermId)).done(function (currPageCatergoryCards, resourceCatTermSet, serachDrivenResources, userDepTermObject) {
        renderResourceCentreTabs(resourceCatTermSet);//render the resource center tabs
        if (userDepTermObject) {
            renderSelectedDepartmentMessage(userDepTermObject);
        }
        renderPopTopics(currentPageCatTermId, userDepTermObject);
        if (!serachDrivenResources) {
            var url = constants.restEndPoints.resourceProperties.format(currentPageCatTermId);
            getItems(url, function (resultItem) {
                var searchResultItems = resultItem.d.query.PrimaryQueryResult ? resultItem.d.query.PrimaryQueryResult.RelevantResults.Table.Rows.results : null;
                var resultCount = resultItem.d.query.PrimaryQueryResult ? resultItem.d.query.PrimaryQueryResult.RelevantResults.RowCount : 0;
                for (var i = 0; i < resultCount; i++) {
                    var item = searchResultItems[i];
                    var termTaxId = "";
                    var itemUrl = null;
                    var termTaxonomyValue = item.Cells.results[4].Value;
                    if (termTaxonomyValue) {
                        termTaxId = termTaxonomyValue.split('L0|#')[0];
                        termTaxId = termTaxId.split('#')[1].trim();
                    }
                    if (item.Cells.results[3].Value) {
                        itemUrl = item.Cells.results[3].Value.split(',')[0];
                    }
                    var obj = {
                        title: item.Cells.results[2].Value,
                        url: itemUrl,
                        itemId: item.Cells.results[5].Value,
                        taxonomyValue: termTaxonomyValue,
                        guid: termTaxId,
                        featured: item.Cells.results[6].Value,
                        description: item.Cells.results[7].Value,
                        pageTemplate: item.Cells.results[8].Value,
                        cardHeading: item.Cells.results[9].Value,
                    };
                    if (obj.taxonomyValue) {
                        var cardsCount = currPageCatergoryCards.length;
                        for (var k = 0; k < cardsCount; k++) {
                            var cardGuid = currPageCatergoryCards[k].guid;
                            if (obj.taxonomyValue.indexOf(cardGuid) > -1) {
                                if ((obj.cardHeading && obj.cardHeading == '1') && (obj.taxonomyValue.indexOf("0" + cardGuid) > -1)) {
                                    if (obj.featured && obj.featured.toLowerCase() == 'true') {
                                        currPageCatergoryCards[k].featured = true;
                                    }
                                    currPageCatergoryCards[k].url = obj.url;
                                    currPageCatergoryCards[k].itemId = obj.itemId;
                                    currPageCatergoryCards[k].description = obj.description;
                                    currPageCatergoryCards[k].pageTemplate = obj.pageTemplate;
                                    currPageCatergoryCards[k].categoryHeading = obj.cardHeading;
                                } else {
                                    currPageCatergoryCards[k].children.push(obj);
                                }

                                break;
                            }
                        }
                    };
                }
                console.log(currPageCatergoryCards);
                if (resultCount > 0) {
                    renderResourceCards(currPageCatergoryCards, serachDrivenResources, currentPageCatTermId);
                }
            });
        }
        else {
            if (userDepTermObject) {
                currentPageCatTermId = userDepTermObject.guid;
            }
            var url = constants.restEndPoints.resourceByCategory.format(currentPageCatTermId);
            getItems(url, function (resultItem) {
                var searchResultItems = resultItem.d.query.PrimaryQueryResult ? resultItem.d.query.PrimaryQueryResult.RelevantResults.Table.Rows.results : null;
                var resultCount = resultItem.d.query.PrimaryQueryResult ? resultItem.d.query.PrimaryQueryResult.RelevantResults.RowCount : 0;
                for (var i = 0; i < resultCount; i++) {
                    var item = searchResultItems[i];
                    var termTaxId = "";
                    var itemUrl = null;
                    var termTaxonomyValue = item.Cells.results[5].Value;
                    if (termTaxonomyValue) {
                        termTaxId = termTaxonomyValue.split('L0|#')[0];
                        termTaxId = termTaxId.split('#')[1].trim();
                    }
                    if (item.Cells.results[4].Value) {
                        itemUrl = item.Cells.results[4].Value.split(',')[0];
                    } else {
                        itemUrl = item.Cells.results[3].Value;
                    }
                    var obj = {
                        title: item.Cells.results[2].Value,
                        url: itemUrl,
                        itemId: item.Cells.results[6].Value,
                        taxonomyValue: termTaxonomyValue,
                        guid: termTaxId,
                        featured: item.Cells.results[7].Value,
                        description: item.Cells.results[8].Value,
                        pageTemplate: item.Cells.results[9].Value,
                        cardHeading: item.Cells.results[10].Value,
                    };
                    if (obj.taxonomyValue) {
                        var cardsCount = currPageCatergoryCards.length;
                        for (var k = 0; k < cardsCount; k++) {
                            var cardGuid = currPageCatergoryCards[k].guid;
                            if (obj.taxonomyValue.indexOf(cardGuid) > -1) {
                                currPageCatergoryCards[k].children.push(obj);
                                break;
                            }
                        }
                    };
                }
                console.log(currPageCatergoryCards);
                if (resultCount > 0) {
                    renderResourceCards(currPageCatergoryCards, serachDrivenResources, currentPageCatTermId);
                }
            });
        }
    });


};

function renderSelectedDepartmentMessage(userDepTermObject) {

    $("#" + constants.htmlElementIds.resourceCentreNotice).append(htmlTemplates.selectedDepartmentMessage.format(userDepTermObject.title, siteCollectionUrl + constants.pageUrls.myProfile));
}

function renderResourceCards(currPageCatergoryCards, serachDrivenResources, currentPageCatTermId) {

    var assetPageUrl = constants.pageUrls.assetPageUrl;
    var subcatsPageUrl = constants.pageUrls.subcatsPageUrl;
    var resourceCardHtml = "";
    var featuredCardHtml = "";
    var resourceCardItemsHtml = "";
    var featuredCardItemsHtml = "";

    var urlParamPropId = constants.queryStringParams.propIdParam.template;
    var urlParamCategory = constants.queryStringParams.categoryParam.template;
    var urlParamMode = constants.queryStringParams.modeParam.template;
    var urlParamSrd = "";
    if (serachDrivenResources) {
        urlParamSrd = constants.queryStringParams.searchDrivenResource.template.format("1");
    };

    for (var i = 0, len = currPageCatergoryCards.length; i < len; i++) {
        if (currPageCatergoryCards[i].children.length === 0) {
            // if no child items from the resource property list then continue
            continue;
        }
        var resourceCardItemsHtml = "";
        featuredCardItemsHtml = "";
        var seeAllButtonHtml = "";
        var resourceCardObject = currPageCatergoryCards[i];
        var headingUrl = "";
        if (resourceCardObject.categoryHeading) {

            var assetsUrl = siteCollectionUrl + assetPageUrl + "?" + urlParamPropId.format(resourceCardObject.itemId) + "&" + urlParamCategory.format(resourceCardObject.guid) + "&" + constants.queryStringParams.sourceTerm.template.format(currentPageCatTermId) + "&" + urlParamSrd;
            var assetsWithSubCatsUrl = siteCollectionUrl + subcatsPageUrl + "?" + urlParamPropId.format(resourceCardObject.itemId) + "&" + urlParamCategory.format("0" + resourceCardObject.guid) + "&" + constants.queryStringParams.sourceTerm.template.format(currentPageCatTermId) + "&" + urlParamSrd; //note 0 at the start of the tax ID will ensure no child items are returned
            var pageTemplate = resourceCardObject.pageTemplate.toLowerCase() == "assets" ? assetsUrl : assetsWithSubCatsUrl;
            var headingUrl = resourceCardObject.url ? resourceCardObject.url : pageTemplate;
        }
        else {
            headingUrl = siteCollectionUrl + assetPageUrl + "?" + urlParamMode.format(constants.pageRenderMode.termbased) + "&" + urlParamCategory.format(resourceCardObject.guid) + "&" + constants.queryStringParams.sourceTerm.template.format(currentPageCatTermId) + "&" + urlParamSrd;;
        }


        var topLevelFeaturedCat = currPageCatergoryCards[i].featured;
        //if (currPageCatergoryCards[i].children) {
        var subCatsDisplayedCount = 0;
        var toolIndex = 10;

        var childLen = currPageCatergoryCards[i].children.length
        //if cat has sub cats then loop through and render them
        for (var k = 0; k < childLen; k++) {

            var childObject = currPageCatergoryCards[i].children[k];
            if (childObject.guid == resourceCardObject.guid) {
                subCatsDisplayedCount++;
            }

            var title = childObject.title;

            var assetsUrl = siteCollectionUrl + assetPageUrl + "?" + urlParamPropId.format(childObject.itemId) + "&" + urlParamCategory.format(childObject.guid) + "&" + constants.queryStringParams.sourceTerm.template.format(currentPageCatTermId) + "&" + urlParamSrd;
            var assetsWithSubCatsUrl = siteCollectionUrl + subcatsPageUrl + "?" + urlParamPropId.format(childObject.itemId) + "&" + urlParamCategory.format("0" + childObject.guid) + "&" + constants.queryStringParams.sourceTerm.template.format(currentPageCatTermId) + "&" + urlParamSrd;  //note 0 at the start of the tax ID will ensure no child items are returned
            // refiner = refinerUrlTemplate.format(childObject.resourceCatPath);
            var pageTemplate = "";
            if (childObject.pageTemplate) {
                pageTemplate = childObject.pageTemplate.toLowerCase() == "assets" ? assetsUrl : assetsWithSubCatsUrl;
            }
            var url = childObject.url ? childObject.url : pageTemplate;
            if (topLevelFeaturedCat) {
                //render the items using the featured items template
                featuredCardItemsHtml += htmlTemplates.featuredResourceCardItem.format(url, title);
            }
            else {
                resourceCardItemsHtml += htmlTemplates.resourceCardItem.format(url, title);
            }
        }
        if (topLevelFeaturedCat) {
            featuredCardHtml += htmlTemplates.featuredResourceCard.format(resourceCardObject.title, resourceCardObject.description, featuredCardItemsHtml, seeAllButtonHtml, headingUrl);
        }
        else {
            resourceCardHtml += htmlTemplates.resourceCard.format(resourceCardObject.title, resourceCardItemsHtml, seeAllButtonHtml, headingUrl);
        }
    }
    $("#" + constants.htmlElementIds.featuredResourceCardContainer).append(featuredCardHtml);
    $("#" + constants.htmlElementIds.resourceCardsContainer).append(resourceCardHtml);
    setupShowMoreButtonsFeaturedResourceCards(constants.htmlElementIds.featuredResourceCardContainer, 9);
    setupShowMoreButtonsResourceCards(constants.htmlElementIds.resourceCardsContainer, 5);

}

function setupShowMoreButtonsResourceCards(continerName, threshold) {
    //var threshold = 5;
    $("#" + continerName + " .resource-card").each(function (index) {
        var childrenCount = $(this).children(".widget-items").children().length;
        if (childrenCount - 1 > threshold) {
            $(this).children(".showMoreConatiner").show();//.children(".showMoreButton").show();
            $(this).children(".widget-items").children("li:gt(" + threshold + ")").hide(); //minus 1 for zero baed index
        }
    });
    //register click event handler
    $('.showMoreButton').unbind().on('click', function () {
        var buttonText = $(this).text();
        if (buttonText && buttonText.toLowerCase() == "show more +") {
            $(this).text("Show less -");
            $(this).parent().siblings(".widget-items").children("li:gt(" + threshold + ")").show("fast");
        }
        else {
            $(this).text("Show more +");
            $(this).parent().siblings(".widget-items").children("li:gt(" + threshold + ")").hide("fast");
        }
    });

}

function setupShowMoreButtonsFeaturedResourceCards(continerName, threshold) {
    //var threshold = 5;
    $("#" + continerName + " .resource-card").each(function (index) {
        var childrenCount = $(this).children(".resource-card-items").children().length;
        if (childrenCount - 1 > threshold) {
            $(this).children(".showMoreConatiner").show();//.children(".showMoreButton").show();
            $(this).children(".resource-card-items").children("div:gt(" + threshold + ")").hide(); //minus 1 for zero baed index
        }
    });
    //register click event handler
    $('.showMoreButtonFeatured').unbind().on('click', function () {
        var buttonText = $(this).text();
        if (buttonText && buttonText.toLowerCase() == "show more +") {
            $(this).text("Show less -");
            $(this).parent().siblings(".resource-card-items").children("div:gt(" + threshold + ")").show("fast");
        }
        else {
            $(this).text("Show more +");
            $(this).parent().siblings(".resource-card-items").children("div:gt(" + threshold + ")").hide("fast");
        }
    });

}

function renderPopTopics(currentPageCatTermId, userDepTermObject) {
    var popTopicCategory = currentPageCatTermId;
    if (userDepTermObject) {
        popTopicCategory = userDepTermObject.guid;
    }
    var url = constants.restEndPoints.get_popTopics.format(popTopicCategory);
    var popTopicItemsHtml = "";
    getItems(url, function (resultItem) {
        var searchResultItems = resultItem.d.query.PrimaryQueryResult ? resultItem.d.query.PrimaryQueryResult.RelevantResults.Table.Rows.results : null;
        var resultCount = resultItem.d.query.PrimaryQueryResult ? resultItem.d.query.PrimaryQueryResult.RelevantResults.RowCount : 0;
        for (var i = 0; i < resultCount; i++) {
            var item = searchResultItems[i];
            var itemUrl = "";
            if (item.Cells.results[3].Value) {
                itemUrl = item.Cells.results[3].Value.split(',')[0];
            }
            var obj = {
                title: item.Cells.results[2].Value,
                url: itemUrl,
            }
            if (i == resultCount - 1) {
                popTopicItemsHtml += htmlTemplates.popTopicItem.format(obj.url, obj.title, "");
            }
            else {
                popTopicItemsHtml += htmlTemplates.popTopicItem.format(obj.url, obj.title, "<span class=''>, </span>");
            }

        }
        if (resultCount > 0) {
            $("#" + constants.htmlElementIds.popTopicsContainer).append(htmlTemplates.popTopicsContainer.format(popTopicItemsHtml));
        }

    });
}


function findById(source, id) {
    return source.filter(function (obj) {
        // coerce both obj.id and id to numbers
        // for val & type comparison
        return obj.guid == id;
    })[0];
}

var findObject = function (objects, fieldToMatch, valToMatch) {
    for (var i = 0, len = objects.length; i < len; i++) {
        if (objects[i][fieldToMatch] === valToMatch)
            return objects[i]; // Return as soon as the object is found
    }
    return null; // The object was not found
}
var findObjectIndex = function (objects, fieldToMatch, valToMatch) {
    for (var i = 0, len = objects.length; i < len; i++) {
        if (objects[i][fieldToMatch] === valToMatch)
            return i; // Return as soon as the object is found
    }
    return null; // The object was not found
}

function findTermFromLoadedTermSet(filteredTermsArray, termId) {
    var filterdCount = filteredTermsArray.length;
    for (var i = 0; i < filterdCount; i++) {
        var currentTerm = filteredTermsArray[i];
        if (currentTerm.get_id().toString() == termId) {
            return currentTerm;
        }
    }
    return null;


}

function buildTreeFromFilteredTerms(allTerms, currentNodeTerms) {
    var termsEnumerator = currentNodeTerms.getEnumerator();
    var newNodes = new Array();

    while (termsEnumerator.moveNext()) {
        //for the current term stub, get all the properties from the fully loaded getAllTerms object
        var currentTerm = findTermFromLoadedTermSet(allTerms, termsEnumerator.get_current().get_id().toString());
        var currTermPath = currentTerm.get_pathOfTerm();

        var newTerm = {
            "guid": currentTerm.get_id().toString(),
            "name": currentTerm.get_name(),
            "children": [],
            "termChildNodesCount": currentTerm.get_termsCount(),
        }
        //populate children (these will not have all properties loaded, hence recurse and replace with the getAllTerms result
        var subTerms = currentTerm.get_terms();
        if (subTerms.get_count() > 0) {
            newTerm.children = buildTreeFromFilteredTerms(allTerms, subTerms)
        }
        newNodes.push(newTerm);
    }
    return newNodes;
}


function renderSubCatsCards() {

    var currPageCategoryId = getParameterByName(constants.queryStringParams.categoryParam.name);
    var currPageCategorySource = getParameterByName(constants.queryStringParams.sourceTerm.name);
    var encSourceUrl = encodeURIComponent(window.location.href);
    //var currPathTerms = getParameterByName(constants.queryStringParams.pathTerms.name);
    //if (currPathTerms) {
    //    currPathTerms = currPathTerms + ';';
    //}
    //else {
    //    currPathTerms = "";
    //};
    if (currPageCategoryId) {
        currPageCategoryId = currPageCategoryId.substring(1); //remove the leading 0 as its needed by this pages search query and not the term store function
    };
    var assetPageUrl = constants.pageUrls.assetPageUrl;
    var resourceCardsHtml = "";
    var resourceCardHtml = "";
    getTerm(constants.termSetGuids.connectResourceCatsTermSetId, currPageCategoryId, function (terms, term) {
        var taregtTermPath = term.get_pathOfTerm();

        var filteredTerms = filterTermsOnPath(taregtTermPath, terms);
        var currRootTerm = findTermFromLoadedTermSet(filteredTerms, term.get_id());
        var tree = buildTreeFromFilteredTerms(filteredTerms, currRootTerm.get_terms());//filteredTerms[0].get_terms());
        var resourceCatTerms = tree;
        for (var i = 0, len = resourceCatTerms.length; i < len; i++) {
            var resourceCardItemHtml = "";
            var seeAllButtonHtml = "";
            var resourceCardObject = resourceCatTerms[i];
            var resourceCardTitle = resourceCardObject.name;
            var cardTitleAssetsUrl = siteCollectionUrl + assetPageUrl + "?" + constants.queryStringParams.modeParam.template.format(constants.pageRenderMode.termbased)
              + "&" + constants.queryStringParams.categoryParam.template.format(resourceCardObject.guid)
                        + "&" + constants.queryStringParams.sourceTerm.template.format(currPageCategorySource)
                        + "&" + constants.queryStringParams.pathTerms.template.format(term.get_name())
                        + "&" + constants.queryStringParams.sourceUrl.template.format(encSourceUrl);

            if (resourceCatTerms[i].children) {
                var subCatsDisplayedCount = 0;
                //if cat has sub cats then loop through and render them
                for (var k = 0, childLen = resourceCatTerms[i].children.length; k < childLen; k++) {
                    var childObject = resourceCatTerms[i].children[k];
                    if (childObject.guid == resourceCardObject.guid) {
                        subCatsDisplayedCount++;
                    }
                    var title = childObject.name;

                    var assetsUrl = siteCollectionUrl + assetPageUrl + "?" + constants.queryStringParams.modeParam.template.format(constants.pageRenderMode.termbased)
                        + "&" + constants.queryStringParams.categoryParam.template.format(childObject.guid)
                        + "&" + constants.queryStringParams.sourceTerm.template.format(currPageCategorySource)
                        + "&" + constants.queryStringParams.pathTerms.template.format(term.get_name())
                        + "&" + constants.queryStringParams.sourceUrl.template.format(encSourceUrl);
                    resourceCardItemHtml += htmlTemplates.resourceCardItem.format(assetsUrl, title);
                }
            }
            if (resourceCardObject.termChildNodesCount > 0) {
                var seeAllResourceCount = resourceCardObject.termChildNodesCount - subCatsDisplayedCount; //we do this so that we dont include items already displayed on the card in the count
                if (seeAllResourceCount > 0) {
                    var seeAllButtonUrl = assetPageUrl + "?" + constants.queryStringParams.modeParam.template.format(constants.pageRenderMode.termbased) + "&" + constants.queryStringParams.categoryParam.template.format(childObject.guid);
                    seeAllButtonHtml = htmlTemplates.seeAllButton.format(seeAllButtonUrl, seeAllResourceCount);
                }
            }
            resourceCardHtml += htmlTemplates.subCategoryResourceCard.format(resourceCardTitle, resourceCardItemHtml, cardTitleAssetsUrl);
        }
        $("#" + constants.htmlElementIds.subCatCardsConatiner).append(resourceCardHtml);
        setupShowMoreButtonsResourceCards(constants.htmlElementIds.subCatCardsConatiner, 5);
    });



}

function renderCategoryDetails() {
    var pageRenderMode = getParameterByName(constants.queryStringParams.modeParam.name);
    if (pageRenderMode == constants.pageRenderMode.termbased) {
        //if the page render mode is not equal to list based then we have to render page with term details from the term store (other wise it will be list based mode and it the page will render based on serach results from the properties list)
        var currPageCategoryId = getParameterByName(constants.queryStringParams.categoryParam.name);
        getSingleTerm(constants.termSetGuids.connectResourceCatsTermSetId, currPageCategoryId, function (term) {
            var termName = term.get_name();
            var termDescription = term.get_description();
            //hide the assest mode title and description
            //$("#assetsModeDescription #assetsModeTitle").hide();
            //set the title and description for the sub cat that got passed in
            $("#" + constants.htmlElementIds.termBasedModeTitle).text(termName);
            $("#" + constants.htmlElementIds.termBasedModeDescription).text(termDescription);
        });
    }

}

function getParameterByName(name, url) {
    if (!url) url = window.location.href;
    url = url.toLowerCase(); // This is just to avoid case sensitiveness
    name = name.replace(/[\[\]]/g, "\\$&").toLowerCase();// This is just to avoid case sensitiveness for query parameter name
    var regex = new RegExp("[?&]" + name + "(=([^&#]*)|&|#|$)"),
        results = regex.exec(url);
    if (!results) return null;
    if (!results[2]) return '';
    return decodeURIComponent(results[2].replace(/\+/g, " "));
}

function getBbDyanmicCategories() {

    var businessBalanceDynamicCategories = [];
    var businessBalanceStaticCategories = [];
    var businessBalanceArticles = [];
    var defTask = $.Deferred();

    var batchExecutor = new RestBatchExecutor(siteCollectionUrl, { 'X-RequestDigest': $('#__REQUESTDIGEST').val() });
    var commands = [];

    var batchRequest = new BatchRequest();
    batchRequest.endpoint = siteCollectionUrl + constants.restEndPoints.bbDyanmicCategoriesEp;
    batchRequest.headers = {
        'accept': 'application/json;odata=verbose'
    }
    commands.push({ id: batchExecutor.loadRequest(batchRequest), title: "bbDyanmicCategories" });

    batchRequest = new BatchRequest();
    batchRequest.endpoint = siteCollectionUrl + constants.restEndPoints.bbStaticCategoriesEp;
    batchRequest.headers = {
        'accept': 'application/json;odata=verbose'
    }
    commands.push({ id: batchExecutor.loadRequest(batchRequest), title: "bbStaticCategories" });

    batchRequest = new BatchRequest();
    batchRequest.endpoint = siteCollectionUrl + constants.restEndPoints.bbArtcilePagesEp;
    batchRequest.headers = {
        'accept': 'application/json;odata=verbose'
    }
    commands.push({ id: batchExecutor.loadRequest(batchRequest), title: "bbArtcilePages" });


    batchExecutor.executeAsync().done(function (result) {
        var itemIds = [];
        $.each(result, function (index, resultItem) {
            var command = $.grep(commands, function (command) {
                return resultItem.id === command.id;
            });

            if (command.length) {
                var commandTitle = command[0].title;
                if (resultItem.result.result.d.query.PrimaryQueryResult) {
                    var searchResultItems = resultItem.result.result.d.query.PrimaryQueryResult.RelevantResults.Table.Rows.results;
                    var resultCount = resultItem.result.result.d.query.PrimaryQueryResult.RelevantResults.RowCount;
                    if (commandTitle == "bbDyanmicCategories") {
                        for (var i = 0; i < resultCount; i++) {
                            var item = searchResultItems[i];
                            var featuedUrl1Object = buildUrlFromUrlManagedPropertyValue(item.Cells.results[6].Value);
                            var featuedUrl2Object = buildUrlFromUrlManagedPropertyValue(item.Cells.results[7].Value);
                            var featuedUrl3Object = buildUrlFromUrlManagedPropertyValue(item.Cells.results[8].Value);
                            var featuedUrl4Object = buildUrlFromUrlManagedPropertyValue(item.Cells.results[9].Value);
                            var obj = {
                                title: item.Cells.results[2].Value,
                                path: item.Cells.results[3].Value,
                                byline: item.Cells.results[4].Value,
                                featuedUrl1: { urlTitle: featuedUrl1Object.urlTitle, url: featuedUrl1Object.url },
                                featuedUrl2: { urlTitle: featuedUrl2Object.urlTitle, url: featuedUrl2Object.url },
                                featuedUrl3: { urlTitle: featuedUrl3Object.urlTitle, url: featuedUrl3Object.url },
                                featuedUrl4: { urlTitle: featuedUrl4Object.urlTitle, url: featuedUrl4Object.url },
                                bbCategory: item.Cells.results[10].Value
                            };
                            businessBalanceDynamicCategories.push(obj);
                        }
                    }
                    if (commandTitle == "bbStaticCategories") {
                        for (var i = 0; i < resultCount; i++) {
                            var item = searchResultItems[i];
                            var featuedUrl1Object = buildUrlFromUrlManagedPropertyValue(item.Cells.results[6].Value);
                            var featuedUrl2Object = buildUrlFromUrlManagedPropertyValue(item.Cells.results[7].Value);
                            var featuedUrl3Object = buildUrlFromUrlManagedPropertyValue(item.Cells.results[8].Value);
                            var featuedUrl4Object = buildUrlFromUrlManagedPropertyValue(item.Cells.results[9].Value);
                            var obj = {
                                title: item.Cells.results[2].Value,
                                path: item.Cells.results[3].Value,
                                byline: item.Cells.results[4].Value,
                                featuedUrl1: { urlTitle: featuedUrl1Object.urlTitle, url: featuedUrl1Object.url },
                                featuedUrl2: { urlTitle: featuedUrl2Object.urlTitle, url: featuedUrl2Object.url },
                                featuedUrl3: { urlTitle: featuedUrl3Object.urlTitle, url: featuedUrl3Object.url },
                                featuedUrl4: { urlTitle: featuedUrl4Object.urlTitle, url: featuedUrl4Object.url },
                                bbCategory: item.Cells.results[10].Value
                            };
                            businessBalanceStaticCategories.push(obj);
                        }
                    }
                    else if (commandTitle == "bbArtcilePages") {
                        for (var i = 0; i < resultCount; i++) {
                            var item = searchResultItems[i];
                            var obj = {
                                title: item.Cells.results[2].Value,
                                path: item.Cells.results[3].Value,
                                summary: item.Cells.results[4].Value,
                                imageUrl: item.Cells.results[5].Value,
                                bbCategory: item.Cells.results[7].Value
                            };
                            businessBalanceArticles.push(obj);
                        }
                    }
                }
            }
        });
        var bbCatPagesLength = businessBalanceDynamicCategories.length;
        var bbArtcilePagesLength = businessBalanceArticles.length;
        for (var i = 0; i < bbCatPagesLength; i++) {
            var currBbCatObject = businessBalanceDynamicCategories[i];
            for (var k = 0; k < bbArtcilePagesLength; k++) {
                var currBbArticleObject = businessBalanceArticles[k];
                if (currBbCatObject.bbCategory == currBbArticleObject.bbCategory) {
                    //add as child and break as we only need on chlid
                    currBbCatObject.relatedArticle = currBbArticleObject;
                    continue;
                }

            }
        }
        defTask.resolve(businessBalanceDynamicCategories, businessBalanceStaticCategories);
    }).fail(function (err) {
        //TODO: process in error function
        //TODO:SEND TO ERROR PROCESSOR// alert(JSON.stringify(err));
    });
    return defTask.promise();


}

function renderBbDyanmicCategories() {

    $.when(getBbDyanmicCategories()).done(function (businessBalanceDynamicCategories, businessBalanceStaticCategories) {
        var bbDynamicCatsSectionHtml = "";
        var bbStaticCatsSectionHtml = "";
        //render out bbDyanmic Cats
        var bbCatPagesLength = businessBalanceDynamicCategories.length;
        for (var i = 0; i < bbCatPagesLength ; i++) {
            var bbCatObject = businessBalanceDynamicCategories[i];
            var mod = i % 2;
            var featLink1Html = bbCatObject.featuedUrl1.url ? htmlTemplates.bbFeaturedLinks.format(bbCatObject.featuedUrl1.url, bbCatObject.featuedUrl1.urlTitle) : "";
            var featLink2Html = bbCatObject.featuedUrl2.url ? htmlTemplates.bbFeaturedLinks.format(bbCatObject.featuedUrl2.url, bbCatObject.featuedUrl2.urlTitle) : "";
            var featLink3Html = bbCatObject.featuedUrl3.url ? htmlTemplates.bbFeaturedLinks.format(bbCatObject.featuedUrl3.url, bbCatObject.featuedUrl3.urlTitle) : "";
            var featLink4Html = bbCatObject.featuedUrl4.url ? htmlTemplates.bbFeaturedLinks.format(bbCatObject.featuedUrl4.url, bbCatObject.featuedUrl4.urlTitle) : "";

            var featuredLinksHtml = featLink1Html + featLink2Html + featLink3Html + featLink4Html;
            var relatedArticleimageUrl = "";
            var relatedArticleTitle = "";
            var relatedArticleSummary = "";
            var relatedArticleUrl = "";

            if (bbCatObject.relatedArticle) {
                relatedArticleimageUrl = bbCatObject.relatedArticle.imageUrl ? bbCatObject.relatedArticle.imageUrl.split('?')[0] + '?' + constants.renditionsIds.businessBalanceArticle + "\" />" : "";
                relatedArticleTitle = bbCatObject.relatedArticle.title;
                relatedArticleSummary = bbCatObject.relatedArticle.summary;
                relatedArticleUrl = bbCatObject.relatedArticle.path;
            }
            if (mod === 0) {
                //use normal template
                bbDynamicCatsSectionHtml += htmlTemplates.bbCatgeorySection.format(bbCatObject.path, bbCatObject.title, bbCatObject.byline, featuredLinksHtml, relatedArticleUrl, relatedArticleimageUrl, relatedArticleTitle, relatedArticleSummary);
            }
            else {
                //use alternate template
                bbDynamicCatsSectionHtml += htmlTemplates.bbCatgeorySectionAlternate.format(bbCatObject.path, bbCatObject.title, bbCatObject.byline, featuredLinksHtml, relatedArticleUrl, relatedArticleimageUrl, relatedArticleTitle, relatedArticleSummary);
            }
        }
        $("#" + constants.htmlElementIds.dynamicCategories).append(bbDynamicCatsSectionHtml);

        //render out bbStatic Cats
        var bbCatPagesLength = businessBalanceStaticCategories.length;
        for (var i = 0; i < bbCatPagesLength ; i++) {
            var bbCatObject = businessBalanceStaticCategories[i];
            var featLink1Html = bbCatObject.featuedUrl1.url ? htmlTemplates.bbFeaturedLinks.format(bbCatObject.featuedUrl1.url, bbCatObject.featuedUrl1.urlTitle) : "";
            var featLink2Html = bbCatObject.featuedUrl2.url ? htmlTemplates.bbFeaturedLinks.format(bbCatObject.featuedUrl2.url, bbCatObject.featuedUrl2.urlTitle) : "";
            var featLink3Html = bbCatObject.featuedUrl3.url ? htmlTemplates.bbFeaturedLinks.format(bbCatObject.featuedUrl3.url, bbCatObject.featuedUrl3.urlTitle) : "";
            var featLink4Html = bbCatObject.featuedUrl4.url ? htmlTemplates.bbFeaturedLinks.format(bbCatObject.featuedUrl4.url, bbCatObject.featuedUrl4.urlTitle) : "";
            var featuredLinksHtml = featLink1Html + featLink2Html + featLink3Html + featLink4Html;
            bbStaticCatsSectionHtml += htmlTemplates.bbCategoryStaticItem.format(bbCatObject.path, bbCatObject.title, bbCatObject.byline, featuredLinksHtml);

        }
        $("#" + constants.htmlElementIds.dynamicCategories).append(htmlTemplates.bbCategoryStaticConatiner.format(bbStaticCatsSectionHtml));
    });


}

function setupTeamsStructure() {
    //$.when(setupNavigationTree).done(function () {
    //    var teamStructureObject = findObject(globalObjects.navigationTree, "teamStructureSource", "true");
    //});

    var teamStructureObject = findObject(globalObjects.navigationTree, constants.termCustomPropKeys.teamStructureSource, "true");
    console.log(teamStructureObject);
    var teamStructureHtml = "";
    var rootTermFriendlyUrl = teamStructureObject.firendlyUrl;
    var teamItemsCount = teamStructureObject.children.length;
    for (var i = 0; i < teamItemsCount; i++) {
        var currConatinerObject = teamStructureObject.children[i];
        var parentFriendlyUrl = currConatinerObject.firendlyUrl;
        var teamItemsHtml = "";
        //var disabledAccordian = "";
        if (currConatinerObject.children) {
            var teamItemsLength = currConatinerObject.children.length;
            // disabledAccordian = teamItemsLength === 0 ? "ui-state-disabled" : "";
            for (var k = 0; k < teamItemsLength; k++) {
                var currTeamItemObject = currConatinerObject.children[k];
                var childFriendlyUrl = currTeamItemObject.firendlyUrl;
                var fullFriendlyUrl = siteCollectionUrl + '/' + rootTermFriendlyUrl + '/' + parentFriendlyUrl + '/' + childFriendlyUrl;
                teamItemsHtml += htmlTemplates.teamAccordianItem.format(fullFriendlyUrl, currTeamItemObject.title);
            }
        }
        if (teamItemsHtml) {
            teamStructureHtml += htmlTemplates.teamAccordianConatiner.format(currConatinerObject.title, teamItemsHtml);
        }

    }
    $("#" + constants.htmlElementIds.teamStructureSection).append(teamStructureHtml);
    $("#" + constants.htmlElementIds.teamStructureSection).accordion({
        collapsible: true,
        autoHeight: false,
        heightStyle: "content",
        icons: {
            "header": "accordian-plus",
            "activeHeader": "accordian-minus"
        }
    });
}

function setupTeamsSection() {
    $.when(getTermSetAsTree(constants.termSetGuids.teamCategory, true, function (termset) {
        var teamStructureHtml = "";

        var teamItemsCount = termset.children.length;
        for (var i = 0; i < teamItemsCount; i++) {
            var currConatinerObject = termset.children[i];
            var teamItemsHtml = "";
            //var disabledAccordian = "";
            if (currConatinerObject.children) {
                var teamItemsLength = currConatinerObject.children.length;
                // disabledAccordian = teamItemsLength === 0 ? "ui-state-disabled" : "";
                for (var k = 0; k < teamItemsLength; k++) {
                    var currTeamItemObject = currConatinerObject.children[k];
                    teamItemsHtml += htmlTemplates.teamAccordianItem.format(currTeamItemObject.targetUrl, currTeamItemObject.title);
                }
            }
            if (teamItemsHtml) {
                teamStructureHtml += htmlTemplates.teamAccordianConatiner.format(currConatinerObject.title, teamItemsHtml);
            }

        }
        $("#" + constants.htmlElementIds.teamStructureSection).append(teamStructureHtml);
        $("#" + constants.htmlElementIds.teamStructureSection).accordion({
            collapsible: true,
            icons: {
                "header": "accordian-plus",
                "activeHeader": "accordian-minus"
            }
        });
    }));
}

function constructTeamsStructure(term, parentFriendUrl) {

    var hasChildren = term.children && term.children.length;
    var friendUrlPrefrix = "";
    if (parentFriendUrl && parentFriendUrl.length) {
        friendUrlPrefrix = parentFriendUrl + '/' + term.firendlyUrl;

    }
    var navItemUrl = friendUrlPrefrix == "" ? term.firendlyUrl : friendUrlPrefrix;
    var fullNavItemUrl = siteCollectionUrl + '/' + navItemUrl;


}

function buildUrlFromUrlManagedPropertyValue(value) {
    if (value) {
        var urlValueArray = value.split(',');
        var url = urlValueArray[0];
        var title = urlValueArray[1];
        return { urlTitle: title, url: url };
    }
    else {
        return { urlTitle: null, url: null };
    }
}



function setupShowHideOnCustomRefiner() {
    var threshold = 4;
    //$("div[id$='-MultiRefiner']").each(function (index) {
    $("div[id$='-MultiRefiner']").each(function (index) {
        var childrenCount = $(this).children("div.refiner-checkbox").children().size()
        if (childrenCount >= 2) {
            childrenCount = childrenCount / 2;
        };
        if (childrenCount - 1 > threshold) {
            $(this).children(".showMore").show();//.children(".showMoreButton").show();
            $(this).children(".refiner-checkbox:gt(" + threshold + ")").hide();
        }
    });
    //register click event handler
    $(".showMore").click(function () {
        var buttonText = $(this).text();
        if (buttonText && buttonText.toLowerCase() == "show more +") {
            $(this).text("Show less -");
            //$(this).addClass("btn-primary-nbg")
            $(this).parent().children(".refiner-checkbox:gt(" + threshold + ")").show("fast");
        }
        else {
            $(this).text("Show more +");
            //$(this).removeClass("btn-primary-nbg")
            $(this).parent().children(".refiner-checkbox:gt(" + threshold + ")").hide("fast");
        }
    });
}

function sliderRefinerFix() {
    $("div[id$='SliderLoadContainer']").css("margin-left", "-8px");
}

function getSerachNavigationLinks() {
    var defTask = $.Deferred();
    var queryResults = getItems(constants.restEndPoints.searchNavigationNodes, function (data) {
        var searchTabsHtml = "";
        var results = data.d.results;
        if (results) {
            var count = results.length;
            for (var i = 0; i < count; i++) {
                var searchNavItem = results[i];
                var activeTabClass = pageAbsoluteUrl.toLowerCase().indexOf(searchNavItem.Url.toLowerCase()) > -1 ? "tab-active" : "";
                searchTabsHtml += htmlTemplates.serachNavTab.format(searchNavItem.Title.replace(' ', '').trim() + "tab", activeTabClass, searchNavItem.Url, searchNavItem.Title);
            }
            $("#" + constants.htmlElementIds.searchNavTabsContainer).append(searchTabsHtml);
        }
        //register search nav tab clikc event
        registerSearchNavTabEvents();
        defTask.resolve();
    });
    return defTask;

}

function registerSearchNavTabEvents() {
    $(".serachNavTab").on("click", function () {
        //get keyword
        //if (window.location.hash) {
        //    keyword = window.location.hash.split('=')[1];
        //}
        //else {
        var keyword = encodeURIComponent(getParameterByName('k', window.location.href));
        //};

        // keyword = keyword.split('#')[0]; //this is done to ensure that only the keword is extracted and nothing else
        var navPageUrl = $(this).attr('href');
        var navSerachUrl = window.location.origin + navPageUrl
        //nav to page with keyword
        if (keyword && keyword != 'null') {
            navSerachUrl = window.location.origin + navPageUrl + "?k=" + keyword;
        }
        else {
            keyword = window.location.hash.split('#k=')[1];
            keyword = keyword.split('#')[0];//this is done to ensure that only the keword is extracted and nothing else
            navSerachUrl = window.location.origin + navPageUrl + "?k=" + keyword;
        };
        window.location.href = navSerachUrl;
        return false;
    });
}
function setupSearchButtonClickEvent() {
    $("#globalSearchButton").on('click', function () {
        var serachKeyword = $("#globalSearchBox").val();
        window.location.href = siteCollectionUrl + constants.pageUrls.globalSearch + "?" + constants.queryStringParams.serachKeyword.template.format(encodeURIComponent(serachKeyword));
    });
    $("#globalSearchBox").keyup(function (event) {
        if (event.keyCode == 13) {
            $("#globalSearchButton").click();
        }
    });
}
function setupPeopleSearchClickEvent() {
    $("#peopleSearchButtonWidget").on('click', function () {
        var serachKeyword = $("#peopleSearchBoxWidget").val();
        window.location.href = siteCollectionUrl + constants.pageUrls.peopleSearch + "?" + constants.queryStringParams.serachKeyword.template.format(encodeURIComponent(serachKeyword));
    });
}

function processTabAnchor() {
    var anchor = window.location.hash;
    if (anchor) {
        anchor = anchor.replace('#', '');
        var tabItem = $("#tabs").children("#" + anchor);
        //checks if tab exists
        if (tabItem.length > 0) {
            //deactivate any default active tab
            $("#tabHeaders").children(".tab-active").removeClass("tab-active");
            //now activate tab based on anchor
            $("#tabHeaders").children("#" + anchor + "-header").addClass('tab-active');
            //hide tabs that are shown
            $("#tabs").children().hide();
            //now show tab based on anchor
            $("#tabs").children("#" + anchor).show();
        };

    };

};

function formValidator(element) {
    var valPassed = true;
    $(element).closest(".ui-dialog").find("input.req-val").each(function () {
        var fieldValue = $(this).val();
        if (!fieldValue) {
            valPassed = false;
        }
    });
    return valPassed;
}

function formValidatorWidget(element) {
    var valPassed = true;
    $(element).find("input.req-val").each(function () {
        var fieldValue = $(this).val();
        if (!fieldValue) {
            valPassed = false;
        }
    });
    return valPassed;
}

function sorter(orderingKey, orderingArray) {
    return function (a, b) {
        var indexA = orderingArray.indexOf(a[orderingKey]);
        var indexB = orderingArray.indexOf(b[orderingKey]);
        if (indexA < indexB) {
            return -1;
        }
        else if (indexA > indexB) {
            return 1;
        }
        else {
            return 0;
        }
    }
};

function buildBreadcrumb(pathTitleArray, pathUrlArray) {
    var breadCrumb = "";
    for (var pagePathTitleCount = pathTitleArray.length, i = 0; i < pagePathTitleCount; i++) {
        if (i === 0 && pagePathTitleCount == 1) {
            breadCrumb += htmlTemplates.breadCrumb.format(siteCollectionUrl + "/" + pathUrlArray[i], pathTitleArray[i]);
            break;
        }
        else if (i === 0) {
            breadCrumb += htmlTemplates.breadCrumb.format(siteCollectionUrl + "/" + pathUrlArray[i], pathTitleArray[i]);
        }

        else if (i === pagePathTitleCount - 1) {
            //last item in the breadcrumb gets a specail css class and no hyperlink
            breadCrumb += ' > ' + htmlTemplates.breadCrumbLast.format(pathTitleArray[i]);
        }
        else {
            breadCrumb += ' > ' + htmlTemplates.breadCrumb.format(siteCollectionUrl + "/" + pathUrlArray[i - 1] + '/' + pathUrlArray[i], pathTitleArray[i]);
        }
    };
    return breadCrumb;
};

function setupProfileEditing() {
    $.when(getUserProfileDetails(userLoginName)).done(function (userProfileProperties) {

        for (var userProfilePropertiesCount = userProfileProperties.length, i = 0; i < userProfilePropertiesCount; i++) {
            var currUserProperty = userProfileProperties[i];
            var inputElement = $("#" + constants.htmlElementIds.myProfileTab).find("[data-profilefield='" + currUserProperty.key + "']");
            var elementType = $(inputElement[0]).attr('tagName');
            if (inputElement[0]) { //if element found set its data
                var cleanUserPropValue = stripHtml(currUserProperty.value);
                var mutilValueField = $(inputElement[0]).attr("data-multiValueField");
                if (inputElement[0].type == "checkbox") {
                    inputElement.prop('checked', parseBool(cleanUserPropValue));
                }
                else if (mutilValueField) {
                    if (cleanUserPropValue) {
                        var formattedValue = cleanUserPropValue.split('|').join(", ");
                        inputElement.val(formattedValue);
                    }

                }
                else if (inputElement[0].tagName == "IMG") {
                    var emailObject = $.grep(userProfileProperties, function (e) { return e.key == "WorkEmail"; });
                    inputElement[0].src = constants.pageUrls.userPhotoPage.format(emailObject[0].value);
                }
                else {
                    inputElement.val(cleanUserPropValue);
                };
            }
        };
    });
    //setup the auto complete fields
    setAutoCompeletField(constants.htmlElementIds.schoolUnit, constants.termSetGuids.BusSchoolUnit);
    setAutoCompeletField(constants.htmlElementIds.baseOfficeLocation, constants.termSetGuids.locationTermSet);
    //we need to set the auto complete field for the OOTB skills term set in this way as the guid for each environment are different
    //get the guid for the keywords term set
    var kewwordsTermSetGuid = null;
    if (siteCollectionUrl.indexOf("unswbusinessdev") > -1) {
        kewwordsTermSetGuid = constants.termSetGuids.keywordsDev;

    } else if (siteCollectionUrl.indexOf("unswtest") > -1) {
        kewwordsTermSetGuid = constants.termSetGuids.keywordsTest;
    }
    else if (siteCollectionUrl.indexOf("unsw") > -1) {
        kewwordsTermSetGuid = constants.termSetGuids.keywordProd;
    }
    if (kewwordsTermSetGuid) {
        setAutoCompeletField(constants.htmlElementIds.skills, kewwordsTermSetGuid, true);
        setAutoCompeletField(constants.htmlElementIds.pastProjects, kewwordsTermSetGuid, true);

    };

    $("#" + constants.htmlElementIds.myprofileUploadImage).on("click", function () {
        showProfilePicUploadDialog();
    });
    //save profile click handler
    $("#" + constants.htmlElementIds.saveProfile).on("click", function () {
        var fromVaild = formValidatorWidget("#" + constants.htmlElementIds.myprofileForm);
        if (fromVaild) {
            saveUserProfileDetails(constants.htmlElementIds.myprofileForm);
            $(this).siblings("#requiredFieldMessage").hide("fast");
            $(this).parent().find(".req-val").each(function () {
                if ($(this).val()) {
                    $(this).removeClass("req-val-highlight");
                }
            });
        }
        else {
            $(this).siblings("#requiredFieldMessage").show("fast");
            $(this).parent().find(".req-val").each(function () {
                if (!$(this).val()) {
                    $(this).addClass("req-val-highlight");
                }
            });
        }
        return false;
    });
}


function parseBool(b) {
    return !(/^(false|0)$/i).test(b) && !!b;
}


function split(val) {
    return val.split(/,\s*/);
}
function extractLast(term) {
    return split(term).pop();
}

function setAutoCompeletField(elementId, termSetId, multiSelect) {
    //setup the autocomplete on for the BusSchoolUnit edit field based on the Business School Unit Term Set
    getTermSet(termSetId, function (BusSchoolUnitTerms) {
        var termsEnum = BusSchoolUnitTerms.getEnumerator();
        // Loop through results
        var termObjects = [];
        while (termsEnum.moveNext()) {
            var currenTerm = termsEnum.get_current();
            //globalObjects.availableBusSchoolUnits.push(currenTerm.get_name());
            termObjects.push(currenTerm.get_name());
        }
        if (multiSelect) {
            $("#" + elementId).autocomplete({
                source: function (request, response) {
                    // delegate back to autocomplete, but extract the last term
                    response($.ui.autocomplete.filter(
                      termObjects, extractLast(request.term)));
                },
                focus: function () {
                    // prevent value inserted on focus
                    return false;
                },
                select: function (event, ui) {
                    var terms = split(this.value);
                    // remove the current input
                    terms.pop();
                    // add the selected item
                    terms.push(ui.item.value);
                    // add placeholder to get the comma-and-space at the end
                    terms.push("");
                    this.value = terms.join(", ");
                    return false;
                }
            });
        }
        else {
            $("#" + elementId).autocomplete({
                source: termObjects
            });
        }

    });

};

function saveUserProfileDetails(elementId) {
    var propertiesForUpdate = [];
    //loop through all the property fields and get the values for update
    $("#" + elementId).find('[data-profileField]').each(function () {
        var fieldType = $(this)[0].type;
        var fieldName = $(this).attr("data-profileField");
        var multiValueField = $(this).attr("data-multiValueField");
        var fieldVal = $(this).val();
        if (fieldType == "checkbox") {
            var checkVal = $(this).prop('checked').toString();
            fieldVal = checkVal;
        };
        if (multiValueField) {
            propertiesForUpdate.push({ propFieldName: fieldName, value: fieldVal, multiValue: true });
        }
        else {
            propertiesForUpdate.push({ propFieldName: fieldName, value: fieldVal, multiValue: false });
        }

    });
    //show busy dialog
    showBusyIndicator("Saving Profile...");
    $.when(updateUserProfile(propertiesForUpdate))
         .done(function () {
             $('#globalBusyIndicatorDialog').dialog("close");
             confirmBuilderNoCancel("Your changes have been saved, but they may take some time to take effect. Don't worry if you don't see them right away.",
             function () { $('#busyIndicatorDialog').dialog("close"); },
             null,
        'Profile Data Saved'
      );
         })
         .fail(function (error) {
             $('#globalBusyIndicatorDialog').dialog("close");
             errorHandler(error);  /*return false;*/
         })
}


function legStartHandler(leg, bus) {
    var elementName = leg.el.id;
    var targetId = leg.$target[0].id;
    var legDisplayOption = leg.options.orientation;
    var taregtHeightOffset = leg.targetHeight;
    var currScrollPos = $("#s4-workspace").scrollTop();
    var scrollPos = 0;
    var targetTop = 0;
    var targetElementTopOffset = leg.$target[0].offsetTop;
    if (targetId) {
        var topPositionAdjustment = leg.options.top;
        var taregtTopOffSetOfDocument = document.getElementById(targetId).documentOffsetTop();
        targetTop = taregtTopOffSetOfDocument;
        if (legDisplayOption == "bottom") {
            targetTop += taregtHeightOffset;
            if (topPositionAdjustment) {
                targetTop += topPositionAdjustment;
            };
        }
        else if (legDisplayOption == "top") {
            targetTop -= taregtHeightOffset;
            if (topPositionAdjustment) {
                targetTop -= topPositionAdjustment;
            };
        }
        else if (legDisplayOption == "right" || legDisplayOption == "left") {
            if (topPositionAdjustment) {
                targetTop -= topPositionAdjustment;
            };
        }
        scrollPos = targetTop;
        if (targetTop >= 181) {
            scrollPos -= 96;
        }
        if (currScrollPos > 181) {
            targetTop -= currScrollPos;
        }
    } else {
        if (legDisplayOption == "centered") {
            targetTop = window.innerHeight / 2;
        }
    }
    $("#" + elementName).offset({ top: targetTop })
    if (targetTop != scrollPos) {
        $('#s4-workspace').animate({
            scrollTop: Math.abs(scrollPos)
        }, 500);
    }
    if (leg.rawData.highlight) {
        leg.$target.addClass('tour-highlight');
        $('.tour-overlay').show();
    }
    return false;
}

Element.prototype.documentOffsetTop = function () {
    return this.offsetTop + (this.offsetParent ? this.offsetParent.documentOffsetTop() : 0);
};

function legEndHandler(leg, bus) {
    if (leg.rawData.highlight) {
        leg.$target.removeClass('tour-highlight');
        $('.tour-overlay').hide();
    }
}

function deactiveSPTrimDuplicates() {
    // Show duplicated results
    if (typeof Srch.U.fillKeywordQuery !== 'undefined') {
        var originalFillKeywordQuery = Srch.U.fillKeywordQuery;
        Srch.U.fillKeywordQuery = function (query, dp) {
            dp.set_trimDuplicates(false);
            originalFillKeywordQuery(query, dp);
        };
    };
};

function hideRelatedArtilceControl() {
    console.log("no items to show");
    $("#relatedNewsContainer").hide();
}

function replaceAll(str, find, replace) {
    return str.replace(new RegExp(escapeRegExp(find), 'g'), replace);
}

function escapeRegExp(str) {
    return str.replace(/([.*+?^=!:${}()|\[\]\/\\])/g, "\\$1");
}


function showBusyIndicator(busyMessage) {
    //show busy dialog
    $("#" + constants.htmlElementIds.globalBusyIndicatorDialog).html(htmlTemplates.busyIndicatorDialog.format(busyMessage, siteCollectionUrl)).dialog({
        autoOpen: true,
        height: 'auto',
        width: 450,
        dialogClass: 'no-close connect-dialog',
        modal: true,
        open: function () {
            $('.ui-widget-overlay').addClass('custom-overlay');
        },
        close: function () {
            $('.ui-widget-overlay').removeClass('custom-overlay');
        }
    });
}

function showProfilePicUploadDialog() {
    //show busy dialog
    $("#" + constants.htmlElementIds.profilePicUploader).dialog({
        autoOpen: true,
        height: 'auto',
        width: 450,
        dialogClass: 'no-close connect-dialog',
        modal: true,
        buttons: {
            "Save": function () {
                var valPassed = formValidator($(this));
                if (valPassed) {
                    $(this).find(".reqFldMessage").hide("fast");
                    $(this).dialog("close");
                    showBusyIndicator("Uploading profile image...");
                    setProfilePicture();
                }
                else {
                    $(this).find(".reqFldMessage").show("fast");
                }
            },
            Cancel: function () {
                $(this).dialog("close");
                $(this).find(".reqFldMessage").hide("fast");
            }
        },
        open: function () {
            $('.ui-widget-overlay').addClass('custom-overlay');
            $(this).parent().children(".ui-dialog-buttonpane").children(".ui-dialog-buttonset").children(".ui-button:nth-child(2)").addClass("btn btn-primary-nbg");
            $(this).parent().children(".ui-dialog-buttonpane").children(".ui-dialog-buttonset").children(".ui-button:nth-child(1)").addClass("btn btn-primary");
        },
        close: function () {
            $('.ui-widget-overlay').removeClass('custom-overlay');
        }
    });
}

function closeProfilePicUploadDialog() {
    $("#" + constants.htmlElementIds.profilePicUploader).dialog("close");
}

function closeBusyIndicator() {
    $("#" + constants.htmlElementIds.globalBusyIndicatorDialog).dialog("close");
}


function stripHtml(value) {
    var rex = /(<([^>]+)>)/ig;
    return value.replace(rex, "");

}

function hideAuxRowTabs() {
    $("#auxTabHeaders").children("li").hide();
}


function isQuotaExceeded(e) {
    var quotaExceeded = false;
    if (e) {
        if (e.code) {
            switch (e.code) {
                case 22:
                    quotaExceeded = true;
                    break;
                case 1014:
                    // Firefox
                    if (e.name === 'NS_ERROR_DOM_QUOTA_REACHED') {
                        quotaExceeded = true;
                    }
                    break;
            }
        }
    }
    return quotaExceeded;
}


