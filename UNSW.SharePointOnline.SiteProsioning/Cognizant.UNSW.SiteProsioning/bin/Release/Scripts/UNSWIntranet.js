'use strict';

$(document).ready(function () {
    var breadcrumbHtml = "<div class='row m-l-lg m-t-lg m-r-lg'><div class='col col-15'><div class='s4-notdlg noindex'>" +
                         "<span id='customBreadcrumb'></span></div></div><div class='col col-5 clickable' id='saveToFavourites'>" +
                         "<div id='connectNotificationArea' class='notification-area' style='display: none;'>" +
                         "</div>"; //<span class='push-right text-upper'><i class='fa fa-star m-r-sm icons-green'></i>Save to my favourites</span></div></div>";
    $(breadcrumbHtml).insertBefore('#DeltaPlaceHolderMain');
    
    $("#DeltaPlaceHolderMain").prepend($("#pageTitle"));
    $("#pageTitle").removeAttr("style");
    $("#pageTitle").addClass('centre');
    $('.welcome.blank-wp').addClass('content-area-background p-b-lg');
    $($('.ms-table.ms-fullWidth')[0]).addClass('centre-page-content-fass');

});


