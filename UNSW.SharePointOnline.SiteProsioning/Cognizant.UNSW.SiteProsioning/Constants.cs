﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace UNSW.SharePointOnline.SiteProsioning
{
    public static class Constants
    {
        public static class AppConfigConstants
        {
            public const string InstrumentationKey = "InstrumentationKey";
            public const string TenantURL = "TenantURL";
            public const string MainSiteURL = "MainSiteURL";
            public const string ConfigListName = "ConfigStore";
        }

        public static class ListDetails
        {
            public const string RequestForNewSiteCollectionListName = "RequestForNewSiteCollectionListName";
            public const string QueryToGetSiteProvisionReqsPart1 = "QueryToGetSiteProvisionReqsPart1";
            public const string QueryToGetSiteProvisionReqsPart2 = "QueryToGetSiteProvisionReqsPart2";
            public const string NewSiteURL = "NewSiteURL";
            public const string IsSiteProvisioned = "IsSiteProvisioned";
            public const string MsgAfterSiteProvisioned = "New Site has been provisioned";
            public const string Yes = "Yes";
        }

        public static class ConfigListDetails
        {
            public const string GetConfigValQueryPart1 = "<View><Query><Where><Eq><FieldRef Name='Title'/><Value Type='Text'>";
            public const string GetConfigValQueryPart2 = "</Value></Eq></Where></Query></View>";
            public const string ConfigValue = "ConfigValue";
        }
        public static class FeatureActivationDetails
        { 
            public const string SitePublishingFeatureID = "f6924d36-2fa8-4f0b-b16d-06b7250180fa";
            public const Int32 SitePublishingTimeOut = 300000;
            public const string MsgAfterSiteFeatureActivated = "Publishing feature activated on site collection level";
            public const string WebPublishingFeatureID = "94c94ca6-b32f-4da9-a9e3-1f3d343d7ecb";
            public const Int32 WebPublishingTimeOut = 150000;
            public const string MsgAfterWebFeatureActivated = "Publishing feature activated on web level";
        }

        public static class ColumnNContentTypesDetails
        {
            public const string UNSWIASSEventDescription = "UNSWIASSEventDescription";
            public const string UNSWIASSEventDescriptionDetails = "<Field DisplayName='UNSWIASSEventDescription' Name='UNSWIASSEventDescription' ID='{0FAE0AE1-FECC-4EBA-B17C-337E32B97626}' Group='UNSW' Type='Note' />";
            public const string UNSWIASSSummary = "UNSWIASSSummary";
            public const string UNSWIASSSummaryDetails = "<Field DisplayName='UNSWIASSSummary' Name='UNSWIASSSummary' ID='{0249F685-A03B-4DAB-870A-8ACF33C0F499}' Group='UNSW' Type='Note' />";
            public const string UNSWIASSEventCategory = "UNSWIASSEventCategory";
            public const string UNSWIASSEventCategoryDetails = "<Field DisplayName='UNSWIASSEventCategory' Name='UNSWIASSEventCategory' ID='{6201DEE1-FB6E-44E5-9824-11BF212012B4}' Group='UNSW' Type='Choice' />";
            public const string UNSWIASSNewsCategory = "UNSWIASSNewsCategory";
            public const string UNSWIASSNewsCategoryDetails = "<Field DisplayName='UNSWIASSNewsCategory' Name='UNSWIASSNewsCategory' ID='{036418FC-7F72-4A88-A6D3-289EB97DC780}' Group='UNSW' Type='Choice' />";
            public const string UNSWIASSEventPublishDate = "UNSWIASSEventPublishDate";
            public const string UNSWIASSEventPublishDateDetails = "<Field DisplayName='UNSWIASSEventPublishDate' Name='UNSWIASSEventPublishDate' ID='{3450BA44-16EB-45F9-A8D7-49E5FF114E0D}' Group='UNSW' Type='DateTime' Format='DateOnly' />";
            public const string UNSWIASSExpiryDate = "UNSWIASSExpiryDate";
            public const string UNSWIASSExpiryDateDetails = "<Field DisplayName='UNSWIASSExpiryDate' Name='UNSWIASSExpiryDate' ID='{4BFA16F9-55BA-49FC-90D2-5A940966084A}' Group='UNSW' Type='DateTime' Format='DateOnly' />";
            public const string UNSWIASSPublishDate = "UNSWIASSPublishDate";
            public const string UNSWIASSPublishDateDetails = "<Field DisplayName='UNSWIASSPublishDate' Name='UNSWIASSPublishDate' ID='{440D755B-EB31-41FF-BB10-2893A9BEF730}' Group='UNSW' Type='DateTime' Format='DateOnly' />";
            public const string UNSWIASSHyperlink = "UNSWIASSHyperlink";
            public const string UNSWIASSHyperlinkDetails = "<Field DisplayName='UNSWIASSHyperlink' Name='UNSWIASSHyperlink' ID='{70083CC4-1479-481A-AE72-B4F2541FCCC4}' Group='UNSW' Type='URL'/>";
            public const string UNSWIASSVideoUrl = "UNSWIASSVideoUrl";
            public const string UNSWIASSVideoUrlDetails = "<Field DisplayName='UNSWIASSVideoUrl' Name='UNSWIASSVideoUrl' ID='{0E1E082C-A025-4DBD-8EAB-FB0BBC179F95}' Group='UNSW' Type='URL'/>";
            public const string UNSWIASSFeatured = "UNSWIASSFeatured";
            public const string UNSWIASSFeaturedDetails = "<Field DisplayName='UNSWIASSFeatured' Name='UNSWIASSFeatured' ID='{F3FC0315-8537-4045-8880-9EE4B006E28F}' Group='UNSW' Type='Boolean'/>";
            public const string UNSWIASSFeaturedEvent = "UNSWIASSFeaturedEvent";
            public const string UNSWIASSFeaturedEventDetails = "<Field DisplayName='UNSWIASSFeaturedEvent' Name='UNSWIASSFeaturedEvent' ID='{13C5D61C-9EB0-46D7-9C7D-3590B2DF566F}' Group='UNSW' Type='Boolean'/>";
            public const string MsgAfterColumnsCreated = "Site Columns created";
            public const string PublishingRollupImage = "PublishingRollupImage";
            public const string ArticlePageID = "0x010100C568DB52D9D0A14D9B2FDCC96666E9F2007948130EC3DB064584E219954237AF3900242457EFB8B24247815D688C526CD44D";
            public const string LinkID = "0x0105";
            public const string EventID = "0x0102";
            public const string AnnouncementID = "0x0104";
            public const string UNSWIntranetContentTypes = "UNSWIntranetContentTypes";
            public const string ContentPage = "ContentPage";
            public const string News = "News";
            public const string PublishingPageImage = "PublishingPageImage";
            public const string Events = "Events";
            public const string Expires = "Expires";
            public const string Link_to_External_Events = "Link to External Events";
            public const string UNSW_Events = "UNSW Events";
            public const string Location = "Location";
            public const string Comments = "Comments";
            public const string MsgAfterContentTypesCreated = "Site content types created";
            public const string NewsContentTypeID = "0x010100C568DB52D9D0A14D9B2FDCC96666E9F2007948130EC3DB064584E219954237AF3900242457EFB8B24247815D688C526CD44D0081CF0F64F75E4C429979249B3C26B2FD";
        }

        public static class UploadDocumentsDetails
        {
            public const string Master_Page_Gallery = "Master Page Gallery";
            public const string UNSW = "UNSW";
            public const string PageLayouts = "PageLayouts";
            public const string DisplayTemplates = "DisplayTemplates";
            public const string Refiners = "Refiners";
            public const string UNSWIntranetEventsPage = "UNSWIntranetEventsPage.aspx";
            public const string UNSWIntranetHome = "UNSWIntranetHome.aspx";
            public const string UNSWIntranetSearchGridPage = "UNSWIntranetSearchGridPage.aspx";
            public const string UNSWPageFromDocLayout = "UNSWPageFromDocLayout.aspx";
            public const string PageLayoutsPathInSolution = ".\\PageLayouts\\";
            public const string Control_SlideImage = "Control_SlideImage.html";
            public const string Item_SlideImage = "Item_SlideImage.html";
            public const string UNSWControl_ListWithPaging = "UNSWControl_ListWithPaging.html";
            public const string UNSWIntranet_2ColTile_Control = "UNSWIntranet_2ColTile_Control.html";
            public const string UNSWIntranet_2ColTile_Item = "UNSWIntranet_2ColTile_Item.html";
            public const string UNSWIntranet_Grid_Control = "UNSWIntranet_Grid_Control.html";
            public const string UNSWIntranet_Grid_Item = "UNSWIntranet_Grid_Item.html";
            public const string UNSWIntranetItem_Picture3Lines = "UNSWIntranetItem_Picture3Lines.html";
            public const string DispTempPathInSolution = ".\\DisplayTemplates\\";
            public const string UNSWIntranet_Filter_MultiValue = "UNSWIntranet_Filter_MultiValue.html";
            public const string RefinersPathInSolution = ".\\Refiners\\";
            public const string MsgAfterDocsUploaded = "Folder structure created and documents uploaded";
            public const string styleLibrary = "Style Library";
            public const string CSSPathInSolution = ".\\css\\";
            public const string fontsPathInSolution = ".\\fonts\\";
            public const string ImagesPathInSolution = ".\\Images\\";
            public const string ScriptsPathInSolution = ".\\Scripts\\";
            public const string ResponsiceUIFilesPathInSolution = ".\\ResponsiveUIFiles\\";
        }

        public static class ImageRenditionsDetails
        {
            public const string ImageRenditionFileName = "PublishingImageRenditions.xml";
            public const string ImageRenditionFilePath = ".\\XML\\";
        }
        public static class CreateTermSetDetails
        {
            public const string NewTermSetName = "NewTermSetName";
            public const string NewTermSetLCID = "NewTermSetLCID";
        }
    }
}
