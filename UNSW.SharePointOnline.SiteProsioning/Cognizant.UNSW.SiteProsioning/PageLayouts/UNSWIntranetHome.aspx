﻿<%@ Page language="C#"   Inherits="Microsoft.SharePoint.Publishing.PublishingLayoutPage,Microsoft.SharePoint.Publishing,Version=16.0.0.0,Culture=neutral,PublicKeyToken=71e9bce111e9429c" meta:progid="SharePoint.WebPartPage.Document" %>
<%@ Register Tagprefix="SharePointWebControls" Namespace="Microsoft.SharePoint.WebControls" Assembly="Microsoft.SharePoint, Version=16.0.0.0, Culture=neutral, PublicKeyToken=71e9bce111e9429c" %> <%@ Register Tagprefix="WebPartPages" Namespace="Microsoft.SharePoint.WebPartPages" Assembly="Microsoft.SharePoint, Version=16.0.0.0, Culture=neutral, PublicKeyToken=71e9bce111e9429c" %> <%@ Register Tagprefix="PublishingWebControls" Namespace="Microsoft.SharePoint.Publishing.WebControls" Assembly="Microsoft.SharePoint.Publishing, Version=16.0.0.0, Culture=neutral, PublicKeyToken=71e9bce111e9429c" %> <%@ Register Tagprefix="PublishingNavigation" Namespace="Microsoft.SharePoint.Publishing.Navigation" Assembly="Microsoft.SharePoint.Publishing, Version=16.0.0.0, Culture=neutral, PublicKeyToken=71e9bce111e9429c" %>
<asp:Content ContentPlaceholderID="PlaceHolderAdditionalPageHead" runat="server">
	<style type="text/css">
		.v4master #s4-leftpanel { display: none; }
		.v4master .s4-ca { margin-left: 0px; }
	</style>
	<SharePointWebControls:CssRegistration name="<% $SPUrl:~sitecollection/Style Library/~language/Themable/Core Styles/pagelayouts15.css %>" runat="server"/>
	<SharePoint:ScriptLink id="ScriptExternalJs" runat="server"  Name="~SiteCollection/Style Library/Branding/Scripts/UNSWIntranetHome.js" OnDemand="false" LoadAfterUI="true" Localizable="false" ></SharePoint:ScriptLink>
	<PublishingWebControls:EditModePanel runat="server" id="editmodestyles">
		<!-- Styles for edit mode only-->
		<SharePointWebControls:CssRegistration name="<% $SPUrl:~sitecollection/Style Library/~language/Themable/Core Styles/editmode15.css %>"
			After="<% $SPUrl:~sitecollection/Style Library/~language/Themable/Core Styles/pagelayouts15.css %>" runat="server"/>
	</PublishingWebControls:EditModePanel>
  </asp:Content>
<asp:Content ContentPlaceholderID="PlaceHolderPageTitle" runat="server">
	<SharePointWebControls:FieldValue id="PageTitle" FieldName="Title" runat="server"/>
</asp:Content>
<asp:Content ContentPlaceholderID="PlaceHolderPageTitleInTitleArea" runat="server">
	<SharePointWebControls:FieldValue FieldName="Title" runat="server"/>
</asp:Content>
	
<asp:Content ContentPlaceholderID="PlaceHolderMain" runat="server">
	<div class="row row-nw row-sb m-t-md centre-page-content">
      <div class="col m-r-md">
            <WebPartPages:WebPartZone runat="server" AllowPersonalization="true" ID="HeaderLeftWebPartZone" FrameType="TitleBarOnly"
					    Title="Header Left Web Part Zone" Orientation="Vertical"><ZoneTemplate></ZoneTemplate></WebPartPages:WebPartZone>
      </div>
       <div id="HeaderRightCol" class="col col-remaining">
         <WebPartPages:WebPartZone runat="server"  AllowPersonalization="true" ID="HeaderRightWebPartZone" FrameType="TitleBarOnly"
					    Title="Header Right Web Part Zone" Orientation="Vertical" ContainerWidth="52.5%"><ZoneTemplate></ZoneTemplate></WebPartPages:WebPartZone>
       </div>
  </div>
    <div id="widgetsTab" style="display: block;" class="content-area-background p-b-lg">
        <div class="row row-vt row-sb centre-page-content" style="padding-top:16px">
         <div class="col">
              <WebPartPages:WebPartZone runat="server" AllowPersonalization="true" ID="WidgetSectionLeftWebPartZone" FrameType="TitleBarOnly" Title="Widget Section Left Web Part Zone" Orientation="Vertical"><ZoneTemplate></ZoneTemplate></WebPartPages:WebPartZone>
    	</div>


        <div  id="userWidgetsSection" class="col">

            <div id="userWidgetsSectionRow" class="masonry-stack">
            </div>
	
            <div id="addWidgetsContainer" class="container-info m-t-5">
              <WebPartPages:WebPartZone runat="server" AllowPersonalization="true" ID="WidgetSectionRightWebPartZone" FrameType="TitleBarOnly" Title="Widget Section Right Web Part Zone" Orientation="Vertical"><ZoneTemplate></ZoneTemplate></WebPartPages:WebPartZone>
            </div>

			
         </div>
		 
	</div>
    </div>
	<div id="yammerTab" style="display: none;">
        <WebPartPages:WebPartZone runat="server" AllowPersonalization="true" ID="YammerTabWebPartZone" FrameType="TitleBarOnly"
					    Title="Yammer Tab Web Part Zone" Orientation="Vertical"><ZoneTemplate></ZoneTemplate></WebPartPages:WebPartZone>
	</div>
    <script>
        $(document).ready(function () {
            setupHomePageTabs();
			// Commented out code below as it is customised code use by the ASB Connect. The actual getCalendarEvents function has been commented out in the UNSWIntranetCore.js
            //getCalendarEvents();
            //$.when(getUserWidgets()).done(function () {
            //    $.when(getUserWidgetDefinitions()).done(function () {
            //        constructWidgets();
            //    });
            //});
        });
        $(window).load(function () {

            if (window.location.hash == '#tour') {
                var tour = $('#connectHomePage').tourbus({
                    onLegStart: function (leg, bus) {
                        legStartHandler(leg, bus);
                    },
                    onLegEnd: function (leg, bus) {
                        legEndHandler(leg, bus);
                    },
                    // auto start when initialized
                    autoDepart: true,
                    // specify a container for the leg markup
                    container: '#s4-bodyContainer',
                    // the depart-point of the tour
                    startAt: 0,
                    leg: {
                    }
                });
                tour.trigger('depart.tourbus');
            }
        });
    </script>
     <ol class='tourbus-legs' id='connectHomePage' style="display:none">

  <li data-orientation='centered' data-highlight="true" data-width='600'>
      <div class="push-right"><i class="fa fa-times icons-large tourbus-stop clickable icon-background-2" aria-hidden="true"></i></div>
      <div class="centre">
            <div class="heading-feature m-b-lg">Welcome to the new Connect</div>
    <h2>Take a look around</h2>
    <p>This quick tour will show you around some of the key features of the 
	intranet. The tour can be restarted at any time from the help section of 
	Connect.</p>
    <div><a href="javascript:void(0);" class="m-t-lg btn btn-primary btn-lg tourbus-next">
		Start Tour</a></div>
      <div class="p-t-sm"><a href="javascript:void(0);" class=" tourbus-stop">
		Skip this</a></div>
      </div>
  
  </li>

  <li data-el='#navmenu' data-highlight="true" data-orientation='right' data-width='400' data-margin="16">
        <div class="push-right"><i class="fa fa-times icons-large tourbus-stop clickable icon-background-2" aria-hidden="true"></i></div>
    <div class="heading-feature m-b-md">Main Navigation</div>
    <p>Provides you access to all main areas of the site. You can expand the 
	menus by clicking on the + sign. If you’re looking for templates, forms, 
	policies and procedures head to the Resource Centre.</p>
    <div class="push-right m-t-lg">
      <a href='javascript:void(0);' class='tourbus-prev btn btn-primary-nbg'>
		Prev</a>  <a href='javascript:void(0);' class='tourbus-next m-l-sm btn btn-primary'>
		Next</a> 
    </div> 
  </li>
<li data-el='#AuxRow' data-highlight="true" data-orientation='bottom' data-top="16" data-width='300' data-margin="16">
      <div class="push-right"><i class="fa fa-times icons-large tourbus-stop clickable icon-background-2" aria-hidden="true"></i></div>
      <div class="heading-feature m-b-md">Personalised menu</div>
    <p>Get quick access to the systems and tools, collaboration sites and 
	favourites you use the most. You can order each of these menus in via the 
	personalised section.</p>
      <div class="push-right m-t-lg">
      <a href='javascript:void(0);' class='tourbus-prev btn btn-primary-nbg'>
		Prev</a>  <a href='javascript:void(0);' class='tourbus-next m-l-sm btn btn-primary'>
		Next</a> 
    </div> 
  </li>
<li data-el='#auxPersonalisationSection' data-highlight="true" data-orientation='left' data-width='300' data-arrow="20" data-margin="16">
      <div class="push-right"><i class="fa fa-times icons-large tourbus-stop clickable icon-background-2" aria-hidden="true"></i></div>
      <div class="heading-feature m-b-md">Personalised button</div>
    <p>Personalise your settings in via this button to control your systems and 
	tools, homepage widgets, collab sites, events calendars and update your full 
	profile.</p>
      <div class="push-right m-t-lg">
      <a href='javascript:void(0);' class='tourbus-prev btn btn-primary-nbg'>
		Prev</a>  <a href='javascript:void(0);' class='tourbus-next m-l-sm btn btn-primary'>
		Next</a> 
    </div>
  </li>
 <li data-el='#widget-myprofile' data-highlight="true" data-orientation='left' data-top="80" data-width='300' data-margin="16">
      <div class="push-right"><i class="fa fa-times icons-large tourbus-stop clickable icon-background-2" aria-hidden="true"></i></div>
      <div class="heading-feature m-b-md">Profile Widget</div>
    <p>Keep your contact details up-to-date in the Connect People Directory. The 
	people widget can be quickly edited allowing you to change your Job title, 
	department or work phone and photo. To update your full profile click 
	improve your profile</p>
      <div class="push-right m-t-lg">
      <a href='javascript:void(0);' class='tourbus-prev btn btn-primary-nbg'>
		Prev</a>  <a href='javascript:void(0);' class='tourbus-next m-l-sm btn btn-primary'>
		Next</a> 
    </div>
             
  <li data-el='#userWidgetsSection' data-highlight="true" data-orientation='left' data-width='300' data-margin="16">
        <div class="push-right"><i class="fa fa-times icons-large tourbus-stop clickable icon-background-2" aria-hidden="true"></i></div>
      <div class="heading-feature m-b-md">Your Widgets</div>
    <p>In the personalise section you can select widgets to display on the 
	homepage. Widgets with an “x” can be closed. If you’re the member of a 
	school, go to the personalise section to add the My School widget</p>
    <div class="push-right m-t-lg">
      <a href='javascript:void(0);' class='tourbus-prev btn btn-primary-nbg'>
		Prev</a>  <a href='javascript:void(0);' class='tourbus-next m-l-sm btn btn-primary'>
		Next</a> 
    </div>
  </li>

    <li data-el='#connectFooter' data-highlight="true" data-orientation='top' data-top="105" data-width='300' data-margin="16">
    <div class="push-right"><i class="fa fa-times icons-large tourbus-stop clickable icon-background-2" aria-hidden="true"></i></div>
    <div class="heading-feature m-b-md">Get in touch</div>
    <p>If you would like any assistance with the site please contact the Connect 
	team. Connect offers workspaces which provide staff with the ability to 
	collaborate on documents and tasks and share information with geographically 
	dispersed teams.</p>
       <div class="push-right m-t-lg">
      <a href='javascript:void(0);' class='tourbus-stop btn btn-primary-nbg'>
		Finish</a>
    </div> 
  </li>

             
</ol>
  
</asp:Content>
