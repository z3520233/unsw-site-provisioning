Import-Module Microsoft.Online.SharePoint.PowerShell -DisableNameChecking


#Add references to SharePoint client assemblies and authenticate to Office 365 site
Add-Type -Path ".\\Dlls\\Microsoft.SharePoint.Client.dll"
Add-Type -Path ".\\Dlls\\Microsoft.SharePoint.Client.Runtime.dll"

#Add-Type -Path "C:\Program Files\Common Files\Microsoft Shared\Web Server Extensions\16\ISAPI\Microsoft.SharePoint.Client.dll"
#Add-Type -Path "C:\Program Files\Common Files\Microsoft Shared\Web Server Extensions\16\ISAPI\Microsoft.SharePoint.Client.Runtime.dll"

function CreateCustomAction([Microsoft.SharePoint.Client.ClientContext]$Context,[string]$fileRef,[int]$seq,[string]$name,[string]$script)
{
    $customAction = $Context.Site.UserCustomActions.Add()
    $customAction.Location = 'ScriptLink'
    if($script -eq 'src'){
        $customAction.ScriptSrc = $fileRef
    }
    if($script -eq 'block') {
        $customAction.ScriptBlock = $fileRef
    }
    $customAction.Sequence = $seq
    $customAction.Name = $name
    $customAction.Update()
    $Context.ExecuteQuery()
    write-host 'Custom Action created ' -ForegroundColor Yellow
}

#Take inputs from user
$Username = Read-Host -Prompt "Please enter your username "
$Password = Read-Host -Prompt "Please enter your password " -AsSecureString

$ScriptPath = Split-Path -Parent $PSCommandPath
write-host 'The Script Path is ' $ScriptPath -ForegroundColor Cyan

$csvFileLocation = -join($ScriptPath,'\EnablingResponsiveUI.csv')
write-host 'The csv file path ' $csvFileLocation -ForegroundColor Cyan

$filesToUploadFolder = -join($ScriptPath,'\FilesToUpload')
$DocLibName = "Style Library"

#Import CSV file placed at the location same as Powershell script file
$SiteCollInfo = import-csv $csvFileLocation | where-object {$_.SiteCollectionURL -ne ""}

write-host 'Imported CSV file ' -ForegroundColor Cyan

ForEach ($item in $SiteCollInfo)
{    
    $SiteUrl = $item.("SiteCollectionURL")
    write-host 'Current Site Collection URL :'$SiteUrl -ForegroundColor Magenta
    If($SiteUrl -ne "")
    {
    	#Bind to site collection
	$Context = New-Object Microsoft.SharePoint.Client.ClientContext($SiteUrl)
	$Creds = New-Object Microsoft.SharePoint.Client.SharePointOnlineCredentials($Username,$Password)
	$Context.Credentials = $Creds
	$rootWeb = $Context.Web
	$Context.Load($rootWeb)
	$Context.ExecuteQuery()

	#Retrieve list
	$List = $Context.Web.Lists.GetByTitle($DocLibName)
	$Context.Load($List)
	$Context.ExecuteQuery()

	#Upload file
	Foreach ($File in (dir $filesToUploadFolder -File))
	{
		$FileStream = New-Object IO.FileStream($File.FullName,[System.IO.FileMode]::Open)
		$FileCreationInfo = New-Object Microsoft.SharePoint.Client.FileCreationInformation
		$FileCreationInfo.Overwrite = $true
		$FileCreationInfo.ContentStream = $FileStream
		$FileCreationInfo.URL = $File
		$newFolder = $List.RootFolder.Folders.Add('ResponsiveUI')
		$Upload = $newFolder.Files.Add($FileCreationInfo)
		$Upload.CheckIn('Auto check-in by PowerShell script', [Microsoft.SharePoint.Client.CheckinType]::MajorCheckIn)
		Write-Host '- File Checked in' -ForegroundColor Green
		$Context.Load($Upload)
		$Context.ExecuteQuery()
	}

write-host 'Adding references to css files ' -ForegroundColor Cyan

#Add UNSWIntranetCore.css reference
$cssUrl = "document.write('<link rel =""stylesheet"" href =""" + $rootWeb.Url + "/Style Library/ResponsiveUI/SP-Responsive-UI.css"" />');"
$AddCA1 = CreateCustomAction -Context $Context -fileRef $cssUrl -seq 110 -name 'ResponsiveUILink' -script 'block'

write-host 'Adding references to js files ' -ForegroundColor Cyan

#Add jquery-2.1.0.min.js reference
$jqueryUrl = '~SiteCollection/Style Library/ResponsiveUI/SP-Responsive-UI.js'
$AddCA4 = CreateCustomAction -Context $Context -fileRef $jqueryUrl -seq 1010 -name 'ResponsiveUIJS' -script 'src'

write-host 'Enabled Responsive UI on site collection ' $SiteUrl  -ForegroundColor Magenta
    }
}