Import-Module Microsoft.Online.SharePoint.PowerShell -DisableNameChecking


#Add references to SharePoint client assemblies and authenticate to Office 365 site
Add-Type -Path ".\\Dlls\\Microsoft.SharePoint.Client.dll"
Add-Type -Path ".\\Dlls\\Microsoft.SharePoint.Client.Runtime.dll"

#Add-Type -Path "C:\Program Files\Common Files\Microsoft Shared\Web Server Extensions\16\ISAPI\Microsoft.SharePoint.Client.dll"
#Add-Type -Path "C:\Program Files\Common Files\Microsoft Shared\Web Server Extensions\16\ISAPI\Microsoft.SharePoint.Client.Runtime.dll"

function RemoveCustomActions([Microsoft.SharePoint.Client.ClientContext]$Context)
{
           $site = $Context.Site
           $customActions = $site.UserCustomActions
           $Context.Load($customActions)
           $Context.ExecuteQuery()
           For($i=$customActions.count-1; $i �gt 0; $i--)
	   {
		$Context.Load($customActions[$i])
		$Context.ExecuteQuery()
		If(($customActions[$i].Name -eq 'ResponsiveUILink') -or ($customActions[$i].Name -eq 'ResponsiveUIJS'))
		{
					$customActions[$i].DeleteObject()
					$Context.ExecuteQuery()
					$customActions.Update()
					$Context.Load($customActions)
           				$Context.ExecuteQuery()
                               	        write-host 'The custom action is deleted ' -ForegroundColor Cyan
		}
	   }
}
		

#Take inputs from user
$Username = Read-Host -Prompt "Please enter your username "
$Password = Read-Host -Prompt "Please enter your password " -AsSecureString
$SiteUrl  = Read-Host -Prompt "Please enter site collection url "

If($SiteUrl -ne "")
{
#Get the context
$Context = New-Object Microsoft.SharePoint.Client.ClientContext($SiteUrl)

#Pass the credentials 
$Creds = New-Object Microsoft.SharePoint.Client.SharePointOnlineCredentials($Username,$Password)
$Context.Credentials = $Creds
$RemoveCA1 = RemoveCustomActions -Context $Context
write-host 'Disabled the responsive UI on ' $SiteUrl -ForegroundColor Yellow
}